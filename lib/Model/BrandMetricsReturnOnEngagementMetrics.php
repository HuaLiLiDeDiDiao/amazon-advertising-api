<?php
/**
 * BrandMetricsReturnOnEngagementMetrics
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Brand Metrics
 *
 * Brand Metrics provides a new measurement solution that quantifies opportunities for your brand at each stage of the customer journey on Amazon, and helps brands understand the value of different shopping engagements that impact stages of that journey. You can now access Awareness and Consideration indices that compare your performance to peers using models predictive of consideration and sales. Brand Metrics quantifies the number of customers in the awareness and consideration marketing funnel stages and is built at scale to measure all shopping engagements with your brand on Amazon, not just ad-attributed engagements. Additionally, BM breaks out key shopping engagements at each stage of the shopping journey, along with the Return on Engagement, so you can measure the historical sales following a consideration event or purchase.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * BrandMetricsReturnOnEngagementMetrics Class Doc Comment
 *
 * @category Class
 * @description Response object containing Historical Sales Value Metrics for the brands
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class BrandMetricsReturnOnEngagementMetrics implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'brandMetricsReturnOnEngagementMetrics';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'viewed_detail_page_only' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount',
        'high_value_customers' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount',
        'brand_customers' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount',
        'branded_searches_and_detail_page_views' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount',
        'total_brand_purchasers' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount',
        'add_to_carts' => '\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'viewed_detail_page_only' => null,
        'high_value_customers' => null,
        'brand_customers' => null,
        'branded_searches_and_detail_page_views' => null,
        'total_brand_purchasers' => null,
        'add_to_carts' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'viewed_detail_page_only' => 'viewedDetailPageOnly',
        'high_value_customers' => 'highValueCustomers',
        'brand_customers' => 'brandCustomers',
        'branded_searches_and_detail_page_views' => 'brandedSearchesAndDetailPageViews',
        'total_brand_purchasers' => 'totalBrandPurchasers',
        'add_to_carts' => 'addToCarts'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'viewed_detail_page_only' => 'setViewedDetailPageOnly',
        'high_value_customers' => 'setHighValueCustomers',
        'brand_customers' => 'setBrandCustomers',
        'branded_searches_and_detail_page_views' => 'setBrandedSearchesAndDetailPageViews',
        'total_brand_purchasers' => 'setTotalBrandPurchasers',
        'add_to_carts' => 'setAddToCarts'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'viewed_detail_page_only' => 'getViewedDetailPageOnly',
        'high_value_customers' => 'getHighValueCustomers',
        'brand_customers' => 'getBrandCustomers',
        'branded_searches_and_detail_page_views' => 'getBrandedSearchesAndDetailPageViews',
        'total_brand_purchasers' => 'getTotalBrandPurchasers',
        'add_to_carts' => 'getAddToCarts'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['viewed_detail_page_only'] = isset($data['viewed_detail_page_only']) ? $data['viewed_detail_page_only'] : null;
        $this->container['high_value_customers'] = isset($data['high_value_customers']) ? $data['high_value_customers'] : null;
        $this->container['brand_customers'] = isset($data['brand_customers']) ? $data['brand_customers'] : null;
        $this->container['branded_searches_and_detail_page_views'] = isset($data['branded_searches_and_detail_page_views']) ? $data['branded_searches_and_detail_page_views'] : null;
        $this->container['total_brand_purchasers'] = isset($data['total_brand_purchasers']) ? $data['total_brand_purchasers'] : null;
        $this->container['add_to_carts'] = isset($data['add_to_carts']) ? $data['add_to_carts'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets viewed_detail_page_only
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getViewedDetailPageOnly()
    {
        return $this->container['viewed_detail_page_only'];
    }

    /**
     * Sets viewed_detail_page_only
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $viewed_detail_page_only viewed_detail_page_only
     *
     * @return $this
     */
    public function setViewedDetailPageOnly($viewed_detail_page_only)
    {
        $this->container['viewed_detail_page_only'] = $viewed_detail_page_only;

        return $this;
    }

    /**
     * Gets high_value_customers
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getHighValueCustomers()
    {
        return $this->container['high_value_customers'];
    }

    /**
     * Sets high_value_customers
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $high_value_customers high_value_customers
     *
     * @return $this
     */
    public function setHighValueCustomers($high_value_customers)
    {
        $this->container['high_value_customers'] = $high_value_customers;

        return $this;
    }

    /**
     * Gets brand_customers
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getBrandCustomers()
    {
        return $this->container['brand_customers'];
    }

    /**
     * Sets brand_customers
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $brand_customers brand_customers
     *
     * @return $this
     */
    public function setBrandCustomers($brand_customers)
    {
        $this->container['brand_customers'] = $brand_customers;

        return $this;
    }

    /**
     * Gets branded_searches_and_detail_page_views
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getBrandedSearchesAndDetailPageViews()
    {
        return $this->container['branded_searches_and_detail_page_views'];
    }

    /**
     * Sets branded_searches_and_detail_page_views
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $branded_searches_and_detail_page_views branded_searches_and_detail_page_views
     *
     * @return $this
     */
    public function setBrandedSearchesAndDetailPageViews($branded_searches_and_detail_page_views)
    {
        $this->container['branded_searches_and_detail_page_views'] = $branded_searches_and_detail_page_views;

        return $this;
    }

    /**
     * Gets total_brand_purchasers
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getTotalBrandPurchasers()
    {
        return $this->container['total_brand_purchasers'];
    }

    /**
     * Sets total_brand_purchasers
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $total_brand_purchasers total_brand_purchasers
     *
     * @return $this
     */
    public function setTotalBrandPurchasers($total_brand_purchasers)
    {
        $this->container['total_brand_purchasers'] = $total_brand_purchasers;

        return $this;
    }

    /**
     * Gets add_to_carts
     *
     * @return \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount
     */
    public function getAddToCarts()
    {
        return $this->container['add_to_carts'];
    }

    /**
     * Sets add_to_carts
     *
     * @param \AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount $add_to_carts add_to_carts
     *
     * @return $this
     */
    public function setAddToCarts($add_to_carts)
    {
        $this->container['add_to_carts'] = $add_to_carts;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
