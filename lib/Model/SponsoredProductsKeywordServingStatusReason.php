<?php
/**
 * SponsoredProductsKeywordServingStatusReason
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SponsoredProductsKeywordServingStatusReason Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SponsoredProductsKeywordServingStatusReason
{
    /**
     * Possible values of this enum
     */
    const TARGETING_CLAUSE_STATUS_LIVE_DETAIL = 'TARGETING_CLAUSE_STATUS_LIVE_DETAIL';
    const TARGETING_CLAUSE_POLICING_SUSPENDED_DETAIL = 'TARGETING_CLAUSE_POLICING_SUSPENDED_DETAIL';
    const TARGETING_CLAUSE_PAUSED_DETAIL = 'TARGETING_CLAUSE_PAUSED_DETAIL';
    const TARGETING_CLAUSE_ARCHIVED_DETAIL = 'TARGETING_CLAUSE_ARCHIVED_DETAIL';
    const TARGETING_CLAUSE_BLOCKED_DETAIL = 'TARGETING_CLAUSE_BLOCKED_DETAIL';
    const AD_GROUP_STATUS_ENABLED_DETAIL = 'AD_GROUP_STATUS_ENABLED_DETAIL';
    const AD_GROUP_PAUSED_DETAIL = 'AD_GROUP_PAUSED_DETAIL';
    const AD_GROUP_ARCHIVED_DETAIL = 'AD_GROUP_ARCHIVED_DETAIL';
    const AD_GROUP_INCOMPLETE_DETAIL = 'AD_GROUP_INCOMPLETE_DETAIL';
    const AD_GROUP_POLICING_PENDING_REVIEW_DETAIL = 'AD_GROUP_POLICING_PENDING_REVIEW_DETAIL';
    const AD_GROUP_POLICING_CREATIVE_REJECTED_DETAIL = 'AD_GROUP_POLICING_CREATIVE_REJECTED_DETAIL';
    const AD_GROUP_LOW_BID_DETAIL = 'AD_GROUP_LOW_BID_DETAIL';
    const CAMPAIGN_STATUS_ENABLED_DETAIL = 'CAMPAIGN_STATUS_ENABLED_DETAIL';
    const CAMPAIGN_PAUSED_DETAIL = 'CAMPAIGN_PAUSED_DETAIL';
    const CAMPAIGN_ARCHIVED_DETAIL = 'CAMPAIGN_ARCHIVED_DETAIL';
    const PENDING_REVIEW_DETAIL = 'PENDING_REVIEW_DETAIL';
    const REJECTED_DETAIL = 'REJECTED_DETAIL';
    const PENDING_START_DATE_DETAIL = 'PENDING_START_DATE_DETAIL';
    const ENDED_DETAIL = 'ENDED_DETAIL';
    const CAMPAIGN_OUT_OF_BUDGET_DETAIL = 'CAMPAIGN_OUT_OF_BUDGET_DETAIL';
    const CAMPAIGN_INCOMPLETE_DETAIL = 'CAMPAIGN_INCOMPLETE_DETAIL';
    const PORTFOLIO_STATUS_ENABLED_DETAIL = 'PORTFOLIO_STATUS_ENABLED_DETAIL';
    const PORTFOLIO_PAUSED_DETAIL = 'PORTFOLIO_PAUSED_DETAIL';
    const PORTFOLIO_ARCHIVED_DETAIL = 'PORTFOLIO_ARCHIVED_DETAIL';
    const PORTFOLIO_OUT_OF_BUDGET_DETAIL = 'PORTFOLIO_OUT_OF_BUDGET_DETAIL';
    const PORTFOLIO_PENDING_START_DATE_DETAIL = 'PORTFOLIO_PENDING_START_DATE_DETAIL';
    const PORTFOLIO_ENDED_DETAIL = 'PORTFOLIO_ENDED_DETAIL';
    const ADVERTISER_POLICING_SUSPENDED_DETAIL = 'ADVERTISER_POLICING_SUSPENDED_DETAIL';
    const ADVERTISER_POLICING_PENDING_REVIEW_DETAIL = 'ADVERTISER_POLICING_PENDING_REVIEW_DETAIL';
    const ADVERTISER_ARCHIVED_DETAIL = 'ADVERTISER_ARCHIVED_DETAIL';
    const ADVERTISER_PAUSED_DETAIL = 'ADVERTISER_PAUSED_DETAIL';
    const ADVERTISER_OUT_OF_BUDGET_DETAIL = 'ADVERTISER_OUT_OF_BUDGET_DETAIL';
    const ADVERTISER_PAYMENT_FAILURE_DETAIL = 'ADVERTISER_PAYMENT_FAILURE_DETAIL';
    const ACCOUNT_OUT_OF_BUDGET_DETAIL = 'ACCOUNT_OUT_OF_BUDGET_DETAIL';
    const OTHER = 'OTHER';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::TARGETING_CLAUSE_STATUS_LIVE_DETAIL,
            self::TARGETING_CLAUSE_POLICING_SUSPENDED_DETAIL,
            self::TARGETING_CLAUSE_PAUSED_DETAIL,
            self::TARGETING_CLAUSE_ARCHIVED_DETAIL,
            self::TARGETING_CLAUSE_BLOCKED_DETAIL,
            self::AD_GROUP_STATUS_ENABLED_DETAIL,
            self::AD_GROUP_PAUSED_DETAIL,
            self::AD_GROUP_ARCHIVED_DETAIL,
            self::AD_GROUP_INCOMPLETE_DETAIL,
            self::AD_GROUP_POLICING_PENDING_REVIEW_DETAIL,
            self::AD_GROUP_POLICING_CREATIVE_REJECTED_DETAIL,
            self::AD_GROUP_LOW_BID_DETAIL,
            self::CAMPAIGN_STATUS_ENABLED_DETAIL,
            self::CAMPAIGN_PAUSED_DETAIL,
            self::CAMPAIGN_ARCHIVED_DETAIL,
            self::PENDING_REVIEW_DETAIL,
            self::REJECTED_DETAIL,
            self::PENDING_START_DATE_DETAIL,
            self::ENDED_DETAIL,
            self::CAMPAIGN_OUT_OF_BUDGET_DETAIL,
            self::CAMPAIGN_INCOMPLETE_DETAIL,
            self::PORTFOLIO_STATUS_ENABLED_DETAIL,
            self::PORTFOLIO_PAUSED_DETAIL,
            self::PORTFOLIO_ARCHIVED_DETAIL,
            self::PORTFOLIO_OUT_OF_BUDGET_DETAIL,
            self::PORTFOLIO_PENDING_START_DATE_DETAIL,
            self::PORTFOLIO_ENDED_DETAIL,
            self::ADVERTISER_POLICING_SUSPENDED_DETAIL,
            self::ADVERTISER_POLICING_PENDING_REVIEW_DETAIL,
            self::ADVERTISER_ARCHIVED_DETAIL,
            self::ADVERTISER_PAUSED_DETAIL,
            self::ADVERTISER_OUT_OF_BUDGET_DETAIL,
            self::ADVERTISER_PAYMENT_FAILURE_DETAIL,
            self::ACCOUNT_OUT_OF_BUDGET_DETAIL,
            self::OTHER,
        ];
    }
}
