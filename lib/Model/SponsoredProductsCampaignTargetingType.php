<?php
/**
 * SponsoredProductsCampaignTargetingType
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SponsoredProductsCampaignTargetingType Class Doc Comment
 *
 * @category Class
 * @description Targeting type of a campaign.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SponsoredProductsCampaignTargetingType
{
    /**
     * Possible values of this enum
     */
    const MANUAL = 'MANUAL';
    const AUTO = 'AUTO';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::MANUAL,
            self::AUTO,
        ];
    }
}
