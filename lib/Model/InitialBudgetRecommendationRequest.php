<?php
/**
 * InitialBudgetRecommendationRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * InitialBudgetRecommendationRequest Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class InitialBudgetRecommendationRequest implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'InitialBudgetRecommendationRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'bidding' => '\AmazonAdvertisingApi\Model\Bidding',
        'ad_groups' => '\AmazonAdvertisingApi\Model\AdGroup[]',
        'end_date' => 'string',
        'targeting_type' => 'string',
        'start_date' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'bidding' => null,
        'ad_groups' => null,
        'end_date' => null,
        'targeting_type' => null,
        'start_date' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'bidding' => 'bidding',
        'ad_groups' => 'adGroups',
        'end_date' => 'endDate',
        'targeting_type' => 'targetingType',
        'start_date' => 'startDate'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'bidding' => 'setBidding',
        'ad_groups' => 'setAdGroups',
        'end_date' => 'setEndDate',
        'targeting_type' => 'setTargetingType',
        'start_date' => 'setStartDate'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'bidding' => 'getBidding',
        'ad_groups' => 'getAdGroups',
        'end_date' => 'getEndDate',
        'targeting_type' => 'getTargetingType',
        'start_date' => 'getStartDate'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const TARGETING_TYPE_MANUAL = 'manual';
    const TARGETING_TYPE_AUTO = 'auto';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTargetingTypeAllowableValues()
    {
        return [
            self::TARGETING_TYPE_MANUAL,
            self::TARGETING_TYPE_AUTO,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['bidding'] = isset($data['bidding']) ? $data['bidding'] : null;
        $this->container['ad_groups'] = isset($data['ad_groups']) ? $data['ad_groups'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['targeting_type'] = isset($data['targeting_type']) ? $data['targeting_type'] : null;
        $this->container['start_date'] = isset($data['start_date']) ? $data['start_date'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['bidding'] === null) {
            $invalidProperties[] = "'bidding' can't be null";
        }
        if ($this->container['ad_groups'] === null) {
            $invalidProperties[] = "'ad_groups' can't be null";
        }
        if ($this->container['targeting_type'] === null) {
            $invalidProperties[] = "'targeting_type' can't be null";
        }
        $allowedValues = $this->getTargetingTypeAllowableValues();
        if (!is_null($this->container['targeting_type']) && !in_array($this->container['targeting_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'targeting_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets bidding
     *
     * @return \AmazonAdvertisingApi\Model\Bidding
     */
    public function getBidding()
    {
        return $this->container['bidding'];
    }

    /**
     * Sets bidding
     *
     * @param \AmazonAdvertisingApi\Model\Bidding $bidding bidding
     *
     * @return $this
     */
    public function setBidding($bidding)
    {
        $this->container['bidding'] = $bidding;

        return $this;
    }

    /**
     * Gets ad_groups
     *
     * @return \AmazonAdvertisingApi\Model\AdGroup[]
     */
    public function getAdGroups()
    {
        return $this->container['ad_groups'];
    }

    /**
     * Sets ad_groups
     *
     * @param \AmazonAdvertisingApi\Model\AdGroup[] $ad_groups The ad group information for this new campaign.
     *
     * @return $this
     */
    public function setAdGroups($ad_groups)
    {
        $this->container['ad_groups'] = $ad_groups;

        return $this;
    }

    /**
     * Gets end_date
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     *
     * @param string $end_date The end date of the campaign in YYYYMMDD format.
     *
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets targeting_type
     *
     * @return string
     */
    public function getTargetingType()
    {
        return $this->container['targeting_type'];
    }

    /**
     * Sets targeting_type
     *
     * @param string $targeting_type Specifies the targeting type.
     *
     * @return $this
     */
    public function setTargetingType($targeting_type)
    {
        $allowedValues = $this->getTargetingTypeAllowableValues();
        if (!in_array($targeting_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'targeting_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['targeting_type'] = $targeting_type;

        return $this;
    }

    /**
     * Gets start_date
     *
     * @return string
     */
    public function getStartDate()
    {
        return $this->container['start_date'];
    }

    /**
     * Sets start_date
     *
     * @param string $start_date The start date of the campaign in YYYYMMDD format.
     *
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->container['start_date'] = $start_date;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
