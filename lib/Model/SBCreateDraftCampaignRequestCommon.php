<?php
/**
 * SBCreateDraftCampaignRequestCommon
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SBCreateDraftCampaignRequestCommon Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SBCreateDraftCampaignRequestCommon implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SBCreateDraftCampaignRequestCommon';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'name' => 'string',
        'budget' => 'float',
        'budget_type' => '\AmazonAdvertisingApi\Model\BudgetType',
        'start_date' => 'string',
        'end_date' => 'string',
        'brand_entity_id' => 'string',
        'bid_optimization' => 'bool',
        'bid_multiplier' => 'float',
        'bid_adjustments' => '\AmazonAdvertisingApi\Model\BidAdjustment[]',
        'portfolio_id' => 'int',
        'creative' => '\AmazonAdvertisingApi\Model\SBCreative',
        'landing_page' => '\AmazonAdvertisingApi\Model\SBLandingPage'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'name' => null,
        'budget' => null,
        'budget_type' => null,
        'start_date' => null,
        'end_date' => null,
        'brand_entity_id' => null,
        'bid_optimization' => null,
        'bid_multiplier' => null,
        'bid_adjustments' => null,
        'portfolio_id' => 'int64',
        'creative' => null,
        'landing_page' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'name' => 'name',
        'budget' => 'budget',
        'budget_type' => 'budgetType',
        'start_date' => 'startDate',
        'end_date' => 'endDate',
        'brand_entity_id' => 'brandEntityId',
        'bid_optimization' => 'bidOptimization',
        'bid_multiplier' => 'bidMultiplier',
        'bid_adjustments' => 'bidAdjustments',
        'portfolio_id' => 'portfolioId',
        'creative' => 'creative',
        'landing_page' => 'landingPage'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'name' => 'setName',
        'budget' => 'setBudget',
        'budget_type' => 'setBudgetType',
        'start_date' => 'setStartDate',
        'end_date' => 'setEndDate',
        'brand_entity_id' => 'setBrandEntityId',
        'bid_optimization' => 'setBidOptimization',
        'bid_multiplier' => 'setBidMultiplier',
        'bid_adjustments' => 'setBidAdjustments',
        'portfolio_id' => 'setPortfolioId',
        'creative' => 'setCreative',
        'landing_page' => 'setLandingPage'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'name' => 'getName',
        'budget' => 'getBudget',
        'budget_type' => 'getBudgetType',
        'start_date' => 'getStartDate',
        'end_date' => 'getEndDate',
        'brand_entity_id' => 'getBrandEntityId',
        'bid_optimization' => 'getBidOptimization',
        'bid_multiplier' => 'getBidMultiplier',
        'bid_adjustments' => 'getBidAdjustments',
        'portfolio_id' => 'getPortfolioId',
        'creative' => 'getCreative',
        'landing_page' => 'getLandingPage'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['budget'] = isset($data['budget']) ? $data['budget'] : null;
        $this->container['budget_type'] = isset($data['budget_type']) ? $data['budget_type'] : null;
        $this->container['start_date'] = isset($data['start_date']) ? $data['start_date'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['brand_entity_id'] = isset($data['brand_entity_id']) ? $data['brand_entity_id'] : null;
        $this->container['bid_optimization'] = isset($data['bid_optimization']) ? $data['bid_optimization'] : true;
        $this->container['bid_multiplier'] = isset($data['bid_multiplier']) ? $data['bid_multiplier'] : null;
        $this->container['bid_adjustments'] = isset($data['bid_adjustments']) ? $data['bid_adjustments'] : null;
        $this->container['portfolio_id'] = isset($data['portfolio_id']) ? $data['portfolio_id'] : null;
        $this->container['creative'] = isset($data['creative']) ? $data['creative'] : null;
        $this->container['landing_page'] = isset($data['landing_page']) ? $data['landing_page'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ($this->container['budget'] === null) {
            $invalidProperties[] = "'budget' can't be null";
        }
        if ($this->container['budget_type'] === null) {
            $invalidProperties[] = "'budget_type' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name The name of the draft campaign. Maximum 128 characters. Names must be unique to the Amazon Ads account to which they are associated.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets budget
     *
     * @return float
     */
    public function getBudget()
    {
        return $this->container['budget'];
    }

    /**
     * Sets budget
     *
     * @param float $budget The budget associated with the draft campaign.
     *
     * @return $this
     */
    public function setBudget($budget)
    {
        $this->container['budget'] = $budget;

        return $this;
    }

    /**
     * Gets budget_type
     *
     * @return \AmazonAdvertisingApi\Model\BudgetType
     */
    public function getBudgetType()
    {
        return $this->container['budget_type'];
    }

    /**
     * Sets budget_type
     *
     * @param \AmazonAdvertisingApi\Model\BudgetType $budget_type budget_type
     *
     * @return $this
     */
    public function setBudgetType($budget_type)
    {
        $this->container['budget_type'] = $budget_type;

        return $this;
    }

    /**
     * Gets start_date
     *
     * @return string
     */
    public function getStartDate()
    {
        return $this->container['start_date'];
    }

    /**
     * Sets start_date
     *
     * @param string $start_date The YYYYMMDD start date of the campaign. Must be equal to or greater than the current date. If not specified, is set to current date by default.
     *
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->container['start_date'] = $start_date;

        return $this;
    }

    /**
     * Gets end_date
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     *
     * @param string $end_date The YYYYMMDD end date of the campaign. Must be greater than the value specified in the `startDate` field. If not specified, the campaign has no end date and runs continuously.
     *
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets brand_entity_id
     *
     * @return string
     */
    public function getBrandEntityId()
    {
        return $this->container['brand_entity_id'];
    }

    /**
     * Sets brand_entity_id
     *
     * @param string $brand_entity_id The brand entity identifier to which the draft campaign is associated. Note that this field is required for sellers. Retrieve using the getBrands or getStores operations in the /v2/stores resource.
     *
     * @return $this
     */
    public function setBrandEntityId($brand_entity_id)
    {
        $this->container['brand_entity_id'] = $brand_entity_id;

        return $this;
    }

    /**
     * Gets bid_optimization
     *
     * @return bool
     */
    public function getBidOptimization()
    {
        return $this->container['bid_optimization'];
    }

    /**
     * Sets bid_optimization
     *
     * @param bool $bid_optimization Set to `true` to have Amazon automatically optimize bids for placements below top of search.
     *
     * @return $this
     */
    public function setBidOptimization($bid_optimization)
    {
        $this->container['bid_optimization'] = $bid_optimization;

        return $this;
    }

    /**
     * Gets bid_multiplier
     *
     * @return float
     */
    public function getBidMultiplier()
    {
        return $this->container['bid_multiplier'];
    }

    /**
     * Sets bid_multiplier
     *
     * @param float $bid_multiplier A bid multiplier. Note that this field can only be set when 'bidOptimization' is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00.
     *
     * @return $this
     */
    public function setBidMultiplier($bid_multiplier)
    {
        $this->container['bid_multiplier'] = $bid_multiplier;

        return $this;
    }

    /**
     * Gets bid_adjustments
     *
     * @return \AmazonAdvertisingApi\Model\BidAdjustment[]
     */
    public function getBidAdjustments()
    {
        return $this->container['bid_adjustments'];
    }

    /**
     * Sets bid_adjustments
     *
     * @param \AmazonAdvertisingApi\Model\BidAdjustment[] $bid_adjustments List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. `Not supported for video campaigns`
     *
     * @return $this
     */
    public function setBidAdjustments($bid_adjustments)
    {
        $this->container['bid_adjustments'] = $bid_adjustments;

        return $this;
    }

    /**
     * Gets portfolio_id
     *
     * @return int
     */
    public function getPortfolioId()
    {
        return $this->container['portfolio_id'];
    }

    /**
     * Sets portfolio_id
     *
     * @param int $portfolio_id The identifier of the Portfolio to which the draft campaign is associated.
     *
     * @return $this
     */
    public function setPortfolioId($portfolio_id)
    {
        $this->container['portfolio_id'] = $portfolio_id;

        return $this;
    }

    /**
     * Gets creative
     *
     * @return \AmazonAdvertisingApi\Model\SBCreative
     */
    public function getCreative()
    {
        return $this->container['creative'];
    }

    /**
     * Sets creative
     *
     * @param \AmazonAdvertisingApi\Model\SBCreative $creative creative
     *
     * @return $this
     */
    public function setCreative($creative)
    {
        $this->container['creative'] = $creative;

        return $this;
    }

    /**
     * Gets landing_page
     *
     * @return \AmazonAdvertisingApi\Model\SBLandingPage
     */
    public function getLandingPage()
    {
        return $this->container['landing_page'];
    }

    /**
     * Sets landing_page
     *
     * @param \AmazonAdvertisingApi\Model\SBLandingPage $landing_page landing_page
     *
     * @return $this
     */
    public function setLandingPage($landing_page)
    {
        $this->container['landing_page'] = $landing_page;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
