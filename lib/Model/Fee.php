<?php
/**
 * Fee
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Billing
 *
 * Get invoice data by invoice ID
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * Fee Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class Fee implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'fee';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'cost' => '\AmazonAdvertisingApi\Model\CurrencyAmount',
        'fee_identifiers' => '\AmazonAdvertisingApi\Model\FeeIdentifiers',
        'fee_type' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'cost' => null,
        'fee_identifiers' => null,
        'fee_type' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cost' => 'cost',
        'fee_identifiers' => 'feeIdentifiers',
        'fee_type' => 'feeType'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cost' => 'setCost',
        'fee_identifiers' => 'setFeeIdentifiers',
        'fee_type' => 'setFeeType'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cost' => 'getCost',
        'fee_identifiers' => 'getFeeIdentifiers',
        'fee_type' => 'getFeeType'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const FEE_TYPE_AUDIENCE_FEE = 'AUDIENCE_FEE';
    const FEE_TYPE__3_P_AUTO_NON_ABSORBED_FEE = '3P_AUTO_NON_ABSORBED_FEE';
    const FEE_TYPE__3_P_NON_ABSORBED_FEE = '3P_NON_ABSORBED_FEE';
    const FEE_TYPE_PLATFORM_FEE = 'PLATFORM_FEE';
    const FEE_TYPE_OMNICHANNEL_METRICS_FEE = 'OMNICHANNEL_METRICS_FEE';
    const FEE_TYPE_REGULATORY_ADVERTISING_FEE = 'REGULATORY_ADVERTISING_FEE';
    const FEE_TYPE__3_P_PREBID_FEE = '3P_PREBID_FEE';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getFeeTypeAllowableValues()
    {
        return [
            self::FEE_TYPE_AUDIENCE_FEE,
            self::FEE_TYPE__3_P_AUTO_NON_ABSORBED_FEE,
            self::FEE_TYPE__3_P_NON_ABSORBED_FEE,
            self::FEE_TYPE_PLATFORM_FEE,
            self::FEE_TYPE_OMNICHANNEL_METRICS_FEE,
            self::FEE_TYPE_REGULATORY_ADVERTISING_FEE,
            self::FEE_TYPE__3_P_PREBID_FEE,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cost'] = isset($data['cost']) ? $data['cost'] : null;
        $this->container['fee_identifiers'] = isset($data['fee_identifiers']) ? $data['fee_identifiers'] : null;
        $this->container['fee_type'] = isset($data['fee_type']) ? $data['fee_type'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['cost'] === null) {
            $invalidProperties[] = "'cost' can't be null";
        }
        if ($this->container['fee_type'] === null) {
            $invalidProperties[] = "'fee_type' can't be null";
        }
        $allowedValues = $this->getFeeTypeAllowableValues();
        if (!is_null($this->container['fee_type']) && !in_array($this->container['fee_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'fee_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cost
     *
     * @return \AmazonAdvertisingApi\Model\CurrencyAmount
     */
    public function getCost()
    {
        return $this->container['cost'];
    }

    /**
     * Sets cost
     *
     * @param \AmazonAdvertisingApi\Model\CurrencyAmount $cost cost
     *
     * @return $this
     */
    public function setCost($cost)
    {
        $this->container['cost'] = $cost;

        return $this;
    }

    /**
     * Gets fee_identifiers
     *
     * @return \AmazonAdvertisingApi\Model\FeeIdentifiers
     */
    public function getFeeIdentifiers()
    {
        return $this->container['fee_identifiers'];
    }

    /**
     * Sets fee_identifiers
     *
     * @param \AmazonAdvertisingApi\Model\FeeIdentifiers $fee_identifiers fee_identifiers
     *
     * @return $this
     */
    public function setFeeIdentifiers($fee_identifiers)
    {
        $this->container['fee_identifiers'] = $fee_identifiers;

        return $this;
    }

    /**
     * Gets fee_type
     *
     * @return string
     */
    public function getFeeType()
    {
        return $this->container['fee_type'];
    }

    /**
     * Sets fee_type
     *
     * @param string $fee_type * `PLATFORM_FEE`: Billable fee set at the Rodeo Entity level by internal users which reflects the cost of using the Amazon DSP   * Supply Cost * Platform Fee % * `AGENCY_FEE`: Non-billable fee set at the Rodeo Order level by external users which reflects the fee that the agency is charging the end customer   * Total Cost * Agency Fee % * `AUDIENCE_FEE`: Billable fee automatically calculated at the Rodeo Line Item level when external users choose Amazon 1P data segments for campaign targeting   * Impressions with Audience Fees * Audience Fee (CPM)/1000 * `3P_[AUTO_]NON_ABSORBED_FEE`: Billable fee automatically calculated at the Rodeo Line Item level when external users choose Automotive data segments and/or DMP data segments for campaign targeting   * Impressions * Billable 3p Fee / 1000 * `REGULATORY_ADVERTISING_FEE`: Fees derive from ads serving in specific countries and/or for ads purchased from advertisers in specific countries during the period in which you are billed. * `OMNICHANNEL_METRICS_FEE`: Billable fee set at DSP order level by internal users, which reflects the cost of using Omnichannel metrics measurement   * Supply Cost * Omnichannel Metrics Fee % * `3P_PREBID_FEE`: Billable fee automatically calculated when external users choose third party prebid targeting products for supply quality filtering.   * Impressions with 3P Prebid Fees * 3P Prebid Fee (CPM)/1000 %
     *
     * @return $this
     */
    public function setFeeType($fee_type)
    {
        $allowedValues = $this->getFeeTypeAllowableValues();
        if (!in_array($fee_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'fee_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['fee_type'] = $fee_type;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
