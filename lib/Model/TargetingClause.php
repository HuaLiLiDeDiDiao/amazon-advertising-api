<?php
/**
 * TargetingClause
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API for Sponsored Display
 *
 * This API enables programmatic access for campaign creation, management, and reporting for Sponsored Display campaigns. For more information on the functionality, see the [Sponsored Display Support Center](https://advertising.amazon.com/help#GTPPHE6RAWC2C4LZ). For API onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/en-us/guides/onboarding/overview) topic.<br/><br/> > This specification is available for download from the **[Advertising API developer portal](https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-display/3-0/openapi.yaml).**
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * TargetingClause Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class TargetingClause extends BaseTargetingClause 
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'TargetingClause';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'target_id' => '\AmazonAdvertisingApi\Model\TargetId',
        'ad_group_id' => '\AmazonAdvertisingApi\Model\AdGroupId',
        'campaign_id' => '\AmazonAdvertisingApi\Model\CampaignId',
        'expression_type' => 'string',
        'expression' => '\AmazonAdvertisingApi\Model\TargetingExpression',
        'resolved_expression' => '\AmazonAdvertisingApi\Model\TargetingExpression'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'target_id' => null,
        'ad_group_id' => null,
        'campaign_id' => null,
        'expression_type' => null,
        'expression' => null,
        'resolved_expression' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes + parent::AmazonAdvertisingApiTypes();
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats + parent::AmazonAdvertisingApiFormats();
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'target_id' => 'targetId',
        'ad_group_id' => 'adGroupId',
        'campaign_id' => 'campaignId',
        'expression_type' => 'expressionType',
        'expression' => 'expression',
        'resolved_expression' => 'resolvedExpression'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'target_id' => 'setTargetId',
        'ad_group_id' => 'setAdGroupId',
        'campaign_id' => 'setCampaignId',
        'expression_type' => 'setExpressionType',
        'expression' => 'setExpression',
        'resolved_expression' => 'setResolvedExpression'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'target_id' => 'getTargetId',
        'ad_group_id' => 'getAdGroupId',
        'campaign_id' => 'getCampaignId',
        'expression_type' => 'getExpressionType',
        'expression' => 'getExpression',
        'resolved_expression' => 'getResolvedExpression'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return parent::attributeMap() + self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return parent::setters() + self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return parent::getters() + self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const EXPRESSION_TYPE_MANUAL = 'manual';
    const EXPRESSION_TYPE_AUTO = 'auto';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getExpressionTypeAllowableValues()
    {
        return [
            self::EXPRESSION_TYPE_MANUAL,
            self::EXPRESSION_TYPE_AUTO,
        ];
    }


    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);

        $this->container['target_id'] = isset($data['target_id']) ? $data['target_id'] : null;
        $this->container['ad_group_id'] = isset($data['ad_group_id']) ? $data['ad_group_id'] : null;
        $this->container['campaign_id'] = isset($data['campaign_id']) ? $data['campaign_id'] : null;
        $this->container['expression_type'] = isset($data['expression_type']) ? $data['expression_type'] : null;
        $this->container['expression'] = isset($data['expression']) ? $data['expression'] : null;
        $this->container['resolved_expression'] = isset($data['resolved_expression']) ? $data['resolved_expression'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = parent::listInvalidProperties();

        $allowedValues = $this->getExpressionTypeAllowableValues();
        if (!is_null($this->container['expression_type']) && !in_array($this->container['expression_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'expression_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets target_id
     *
     * @return \AmazonAdvertisingApi\Model\TargetId
     */
    public function getTargetId()
    {
        return $this->container['target_id'];
    }

    /**
     * Sets target_id
     *
     * @param \AmazonAdvertisingApi\Model\TargetId $target_id target_id
     *
     * @return $this
     */
    public function setTargetId($target_id)
    {
        $this->container['target_id'] = $target_id;

        return $this;
    }

    /**
     * Gets ad_group_id
     *
     * @return \AmazonAdvertisingApi\Model\AdGroupId
     */
    public function getAdGroupId()
    {
        return $this->container['ad_group_id'];
    }

    /**
     * Sets ad_group_id
     *
     * @param \AmazonAdvertisingApi\Model\AdGroupId $ad_group_id ad_group_id
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->container['ad_group_id'] = $ad_group_id;

        return $this;
    }

    /**
     * Gets campaign_id
     *
     * @return \AmazonAdvertisingApi\Model\CampaignId
     */
    public function getCampaignId()
    {
        return $this->container['campaign_id'];
    }

    /**
     * Sets campaign_id
     *
     * @param \AmazonAdvertisingApi\Model\CampaignId $campaign_id campaign_id
     *
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->container['campaign_id'] = $campaign_id;

        return $this;
    }

    /**
     * Gets expression_type
     *
     * @return string
     */
    public function getExpressionType()
    {
        return $this->container['expression_type'];
    }

    /**
     * Sets expression_type
     *
     * @param string $expression_type Tactic T00020 & T00030 ad groups should use 'manual' targeting.
     *
     * @return $this
     */
    public function setExpressionType($expression_type)
    {
        $allowedValues = $this->getExpressionTypeAllowableValues();
        if (!is_null($expression_type) && !in_array($expression_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'expression_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['expression_type'] = $expression_type;

        return $this;
    }

    /**
     * Gets expression
     *
     * @return \AmazonAdvertisingApi\Model\TargetingExpression
     */
    public function getExpression()
    {
        return $this->container['expression'];
    }

    /**
     * Sets expression
     *
     * @param \AmazonAdvertisingApi\Model\TargetingExpression $expression expression
     *
     * @return $this
     */
    public function setExpression($expression)
    {
        $this->container['expression'] = $expression;

        return $this;
    }

    /**
     * Gets resolved_expression
     *
     * @return \AmazonAdvertisingApi\Model\TargetingExpression
     */
    public function getResolvedExpression()
    {
        return $this->container['resolved_expression'];
    }

    /**
     * Sets resolved_expression
     *
     * @param \AmazonAdvertisingApi\Model\TargetingExpression $resolved_expression resolved_expression
     *
     * @return $this
     */
    public function setResolvedExpression($resolved_expression)
    {
        $this->container['resolved_expression'] = $resolved_expression;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
