<?php
/**
 * SBTargetingGetRefinementsForCategoryResponseContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SBTargetingGetRefinementsForCategoryResponseContent Class Doc Comment
 *
 * @category Class
 * @description Response object for /sb/targets/categories/{categoryRefinementId}/refinements containing information on Brand Nodes, Age Range Nodes, and Genre Nodes.     Response is paginated with pagination occurring for all three arrays at once.     Example: If there are 800 brands, 5 age ranges, and 600 genres, the first response will return 500 brands, 5 age ranges, and 500 genres. The next paginated response will return 300 brands, 0 age ranges, and 100 genres.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SBTargetingGetRefinementsForCategoryResponseContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SBTargetingGetRefinementsForCategoryResponseContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'age_ranges' => '\AmazonAdvertisingApi\Model\SBTargetingAgeRange[]',
        'brands' => '\AmazonAdvertisingApi\Model\SBTargetingBrand[]',
        'genres' => '\AmazonAdvertisingApi\Model\SBTargetingGenre[]',
        'next_token' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'age_ranges' => null,
        'brands' => null,
        'genres' => null,
        'next_token' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'age_ranges' => 'ageRanges',
        'brands' => 'brands',
        'genres' => 'genres',
        'next_token' => 'nextToken'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'age_ranges' => 'setAgeRanges',
        'brands' => 'setBrands',
        'genres' => 'setGenres',
        'next_token' => 'setNextToken'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'age_ranges' => 'getAgeRanges',
        'brands' => 'getBrands',
        'genres' => 'getGenres',
        'next_token' => 'getNextToken'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['age_ranges'] = isset($data['age_ranges']) ? $data['age_ranges'] : null;
        $this->container['brands'] = isset($data['brands']) ? $data['brands'] : null;
        $this->container['genres'] = isset($data['genres']) ? $data['genres'] : null;
        $this->container['next_token'] = isset($data['next_token']) ? $data['next_token'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets age_ranges
     *
     * @return \AmazonAdvertisingApi\Model\SBTargetingAgeRange[]
     */
    public function getAgeRanges()
    {
        return $this->container['age_ranges'];
    }

    /**
     * Sets age_ranges
     *
     * @param \AmazonAdvertisingApi\Model\SBTargetingAgeRange[] $age_ranges List of Age Ranges. Use /sb/targets/categories/{categoryRefinementId}/refinements to retrieve Age Ranges. Age Ranges are only available for categories related to children's toys and games.
     *
     * @return $this
     */
    public function setAgeRanges($age_ranges)
    {
        $this->container['age_ranges'] = $age_ranges;

        return $this;
    }

    /**
     * Gets brands
     *
     * @return \AmazonAdvertisingApi\Model\SBTargetingBrand[]
     */
    public function getBrands()
    {
        return $this->container['brands'];
    }

    /**
     * Sets brands
     *
     * @param \AmazonAdvertisingApi\Model\SBTargetingBrand[] $brands List of Brands.
     *
     * @return $this
     */
    public function setBrands($brands)
    {
        $this->container['brands'] = $brands;

        return $this;
    }

    /**
     * Gets genres
     *
     * @return \AmazonAdvertisingApi\Model\SBTargetingGenre[]
     */
    public function getGenres()
    {
        return $this->container['genres'];
    }

    /**
     * Sets genres
     *
     * @param \AmazonAdvertisingApi\Model\SBTargetingGenre[] $genres List of Genres. Use /sb/targets/categories/{categoryRefinementId}/refinements to retrieve Genre Node IDs. Genres are only available for categories related to books.
     *
     * @return $this
     */
    public function setGenres($genres)
    {
        $this->container['genres'] = $genres;

        return $this;
    }

    /**
     * Gets next_token
     *
     * @return string
     */
    public function getNextToken()
    {
        return $this->container['next_token'];
    }

    /**
     * Sets next_token
     *
     * @param string $next_token Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.
     *
     * @return $this
     */
    public function setNextToken($next_token)
    {
        $this->container['next_token'] = $next_token;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
