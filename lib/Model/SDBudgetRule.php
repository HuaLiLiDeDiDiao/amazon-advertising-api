<?php
/**
 * SDBudgetRule
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API for Sponsored Display
 *
 * This API enables programmatic access for campaign creation, management, and reporting for Sponsored Display campaigns. For more information on the functionality, see the [Sponsored Display Support Center](https://advertising.amazon.com/help#GTPPHE6RAWC2C4LZ). For API onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/en-us/guides/onboarding/overview) topic.<br/><br/> > This specification is available for download from the **[Advertising API developer portal](https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-display/3-0/openapi.yaml).**
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SDBudgetRule Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SDBudgetRule implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SDBudgetRule';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'rule_state' => '\AmazonAdvertisingApi\Model\State',
        'last_updated_date' => 'float',
        'created_date' => 'float',
        'rule_details' => '\AmazonAdvertisingApi\Model\SDBudgetRuleDetails',
        'rule_id' => 'string',
        'rule_status' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'rule_state' => null,
        'last_updated_date' => 'int64',
        'created_date' => 'int64',
        'rule_details' => null,
        'rule_id' => null,
        'rule_status' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'rule_state' => 'ruleState',
        'last_updated_date' => 'lastUpdatedDate',
        'created_date' => 'createdDate',
        'rule_details' => 'ruleDetails',
        'rule_id' => 'ruleId',
        'rule_status' => 'ruleStatus'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'rule_state' => 'setRuleState',
        'last_updated_date' => 'setLastUpdatedDate',
        'created_date' => 'setCreatedDate',
        'rule_details' => 'setRuleDetails',
        'rule_id' => 'setRuleId',
        'rule_status' => 'setRuleStatus'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'rule_state' => 'getRuleState',
        'last_updated_date' => 'getLastUpdatedDate',
        'created_date' => 'getCreatedDate',
        'rule_details' => 'getRuleDetails',
        'rule_id' => 'getRuleId',
        'rule_status' => 'getRuleStatus'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['rule_state'] = isset($data['rule_state']) ? $data['rule_state'] : null;
        $this->container['last_updated_date'] = isset($data['last_updated_date']) ? $data['last_updated_date'] : null;
        $this->container['created_date'] = isset($data['created_date']) ? $data['created_date'] : null;
        $this->container['rule_details'] = isset($data['rule_details']) ? $data['rule_details'] : null;
        $this->container['rule_id'] = isset($data['rule_id']) ? $data['rule_id'] : null;
        $this->container['rule_status'] = isset($data['rule_status']) ? $data['rule_status'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['rule_id'] === null) {
            $invalidProperties[] = "'rule_id' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets rule_state
     *
     * @return \AmazonAdvertisingApi\Model\State
     */
    public function getRuleState()
    {
        return $this->container['rule_state'];
    }

    /**
     * Sets rule_state
     *
     * @param \AmazonAdvertisingApi\Model\State $rule_state rule_state
     *
     * @return $this
     */
    public function setRuleState($rule_state)
    {
        $this->container['rule_state'] = $rule_state;

        return $this;
    }

    /**
     * Gets last_updated_date
     *
     * @return float
     */
    public function getLastUpdatedDate()
    {
        return $this->container['last_updated_date'];
    }

    /**
     * Sets last_updated_date
     *
     * @param float $last_updated_date Epoch time of budget rule update. Read-only.
     *
     * @return $this
     */
    public function setLastUpdatedDate($last_updated_date)
    {
        $this->container['last_updated_date'] = $last_updated_date;

        return $this;
    }

    /**
     * Gets created_date
     *
     * @return float
     */
    public function getCreatedDate()
    {
        return $this->container['created_date'];
    }

    /**
     * Sets created_date
     *
     * @param float $created_date Epoch time of budget rule creation. Read-only.
     *
     * @return $this
     */
    public function setCreatedDate($created_date)
    {
        $this->container['created_date'] = $created_date;

        return $this;
    }

    /**
     * Gets rule_details
     *
     * @return \AmazonAdvertisingApi\Model\SDBudgetRuleDetails
     */
    public function getRuleDetails()
    {
        return $this->container['rule_details'];
    }

    /**
     * Sets rule_details
     *
     * @param \AmazonAdvertisingApi\Model\SDBudgetRuleDetails $rule_details rule_details
     *
     * @return $this
     */
    public function setRuleDetails($rule_details)
    {
        $this->container['rule_details'] = $rule_details;

        return $this;
    }

    /**
     * Gets rule_id
     *
     * @return string
     */
    public function getRuleId()
    {
        return $this->container['rule_id'];
    }

    /**
     * Sets rule_id
     *
     * @param string $rule_id The budget rule identifier.
     *
     * @return $this
     */
    public function setRuleId($rule_id)
    {
        $this->container['rule_id'] = $rule_id;

        return $this;
    }

    /**
     * Gets rule_status
     *
     * @return string
     */
    public function getRuleStatus()
    {
        return $this->container['rule_status'];
    }

    /**
     * Sets rule_status
     *
     * @param string $rule_status The budget rule status. Read-only.
     *
     * @return $this
     */
    public function setRuleStatus($rule_status)
    {
        $this->container['rule_status'] = $rule_status;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
