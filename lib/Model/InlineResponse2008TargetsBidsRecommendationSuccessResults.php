<?php
/**
 * InlineResponse2008TargetsBidsRecommendationSuccessResults
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * InlineResponse2008TargetsBidsRecommendationSuccessResults Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class InlineResponse2008TargetsBidsRecommendationSuccessResults implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'inline_response_200_8_targetsBidsRecommendationSuccessResults';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'recommendation_id' => '',
        'recommended_bid' => '\AmazonAdvertisingApi\Model\RecommendedBid',
        'targets' => '\AmazonAdvertisingApi\Model\SBTargetingExpressions',
        'targets_index' => '\AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'recommendation_id' => null,
        'recommended_bid' => null,
        'targets' => null,
        'targets_index' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'recommendation_id' => 'recommendationId',
        'recommended_bid' => 'recommendedBid',
        'targets' => 'targets',
        'targets_index' => 'targetsIndex'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'recommendation_id' => 'setRecommendationId',
        'recommended_bid' => 'setRecommendedBid',
        'targets' => 'setTargets',
        'targets_index' => 'setTargetsIndex'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'recommendation_id' => 'getRecommendationId',
        'recommended_bid' => 'getRecommendedBid',
        'targets' => 'getTargets',
        'targets_index' => 'getTargetsIndex'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['recommendation_id'] = isset($data['recommendation_id']) ? $data['recommendation_id'] : null;
        $this->container['recommended_bid'] = isset($data['recommended_bid']) ? $data['recommended_bid'] : null;
        $this->container['targets'] = isset($data['targets']) ? $data['targets'] : null;
        $this->container['targets_index'] = isset($data['targets_index']) ? $data['targets_index'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets recommendation_id
     *
     * @return 
     */
    public function getRecommendationId()
    {
        return $this->container['recommendation_id'];
    }

    /**
     * Sets recommendation_id
     *
     * @param  $recommendation_id The identifier of the target bid recommendation.
     *
     * @return $this
     */
    public function setRecommendationId($recommendation_id)
    {
        $this->container['recommendation_id'] = $recommendation_id;

        return $this;
    }

    /**
     * Gets recommended_bid
     *
     * @return \AmazonAdvertisingApi\Model\RecommendedBid
     */
    public function getRecommendedBid()
    {
        return $this->container['recommended_bid'];
    }

    /**
     * Sets recommended_bid
     *
     * @param \AmazonAdvertisingApi\Model\RecommendedBid $recommended_bid recommended_bid
     *
     * @return $this
     */
    public function setRecommendedBid($recommended_bid)
    {
        $this->container['recommended_bid'] = $recommended_bid;

        return $this;
    }

    /**
     * Gets targets
     *
     * @return \AmazonAdvertisingApi\Model\SBTargetingExpressions
     */
    public function getTargets()
    {
        return $this->container['targets'];
    }

    /**
     * Sets targets
     *
     * @param \AmazonAdvertisingApi\Model\SBTargetingExpressions $targets targets
     *
     * @return $this
     */
    public function setTargets($targets)
    {
        $this->container['targets'] = $targets;

        return $this;
    }

    /**
     * Gets targets_index
     *
     * @return \AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex
     */
    public function getTargetsIndex()
    {
        return $this->container['targets_index'];
    }

    /**
     * Sets targets_index
     *
     * @param \AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex $targets_index targets_index
     *
     * @return $this
     */
    public function setTargetsIndex($targets_index)
    {
        $this->container['targets_index'] = $targets_index;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
