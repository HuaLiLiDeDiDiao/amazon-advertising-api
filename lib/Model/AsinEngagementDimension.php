<?php
/**
 * AsinEngagementDimension
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Stores
 *
 * No description provided (generated by AmazonAdvertisingApi Codegenhttps://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api)
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * AsinEngagementDimension Class Doc Comment
 *
 * @category Class
 * @description User can use dimensions to aggregate the engagement metrics. Supported dimension types:   * &#x60;ASIN&#x60; - Amazon Standard Identification Number.    When *dimension* is omitted, user can retrieve select metrics aggregated at the store level. See *metrics* for details.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class AsinEngagementDimension
{
    /**
     * Possible values of this enum
     */
    const ASIN = 'ASIN';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::ASIN,
        ];
    }
}
