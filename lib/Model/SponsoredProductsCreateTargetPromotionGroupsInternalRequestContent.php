<?php
/**
 * SponsoredProductsCreateTargetPromotionGroupsInternalRequestContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SponsoredProductsCreateTargetPromotionGroupsInternalRequestContent Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SponsoredProductsCreateTargetPromotionGroupsInternalRequestContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SponsoredProductsCreateTargetPromotionGroupsInternalRequestContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'ad_ids' => 'string[]',
        'existing_campaign_details' => '\AmazonAdvertisingApi\Model\SponsoredProductsExistingCampaignDetails',
        'api_gateway_context' => '\AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext',
        'ad_group_id' => 'string',
        'new_campaign_details' => '\AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignDetails'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'ad_ids' => null,
        'existing_campaign_details' => null,
        'api_gateway_context' => null,
        'ad_group_id' => null,
        'new_campaign_details' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ad_ids' => 'adIds',
        'existing_campaign_details' => 'existingCampaignDetails',
        'api_gateway_context' => 'apiGatewayContext',
        'ad_group_id' => 'adGroupId',
        'new_campaign_details' => 'newCampaignDetails'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ad_ids' => 'setAdIds',
        'existing_campaign_details' => 'setExistingCampaignDetails',
        'api_gateway_context' => 'setApiGatewayContext',
        'ad_group_id' => 'setAdGroupId',
        'new_campaign_details' => 'setNewCampaignDetails'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ad_ids' => 'getAdIds',
        'existing_campaign_details' => 'getExistingCampaignDetails',
        'api_gateway_context' => 'getApiGatewayContext',
        'ad_group_id' => 'getAdGroupId',
        'new_campaign_details' => 'getNewCampaignDetails'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ad_ids'] = isset($data['ad_ids']) ? $data['ad_ids'] : null;
        $this->container['existing_campaign_details'] = isset($data['existing_campaign_details']) ? $data['existing_campaign_details'] : null;
        $this->container['api_gateway_context'] = isset($data['api_gateway_context']) ? $data['api_gateway_context'] : null;
        $this->container['ad_group_id'] = isset($data['ad_group_id']) ? $data['ad_group_id'] : null;
        $this->container['new_campaign_details'] = isset($data['new_campaign_details']) ? $data['new_campaign_details'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['api_gateway_context'] === null) {
            $invalidProperties[] = "'api_gateway_context' can't be null";
        }
        if ($this->container['ad_group_id'] === null) {
            $invalidProperties[] = "'ad_group_id' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets ad_ids
     *
     * @return string[]
     */
    public function getAdIds()
    {
        return $this->container['ad_ids'];
    }

    /**
     * Sets ad_ids
     *
     * @param string[] $ad_ids ad_ids
     *
     * @return $this
     */
    public function setAdIds($ad_ids)
    {
        $this->container['ad_ids'] = $ad_ids;

        return $this;
    }

    /**
     * Gets existing_campaign_details
     *
     * @return \AmazonAdvertisingApi\Model\SponsoredProductsExistingCampaignDetails
     */
    public function getExistingCampaignDetails()
    {
        return $this->container['existing_campaign_details'];
    }

    /**
     * Sets existing_campaign_details
     *
     * @param \AmazonAdvertisingApi\Model\SponsoredProductsExistingCampaignDetails $existing_campaign_details existing_campaign_details
     *
     * @return $this
     */
    public function setExistingCampaignDetails($existing_campaign_details)
    {
        $this->container['existing_campaign_details'] = $existing_campaign_details;

        return $this;
    }

    /**
     * Gets api_gateway_context
     *
     * @return \AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext
     */
    public function getApiGatewayContext()
    {
        return $this->container['api_gateway_context'];
    }

    /**
     * Sets api_gateway_context
     *
     * @param \AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext $api_gateway_context api_gateway_context
     *
     * @return $this
     */
    public function setApiGatewayContext($api_gateway_context)
    {
        $this->container['api_gateway_context'] = $api_gateway_context;

        return $this;
    }

    /**
     * Gets ad_group_id
     *
     * @return string
     */
    public function getAdGroupId()
    {
        return $this->container['ad_group_id'];
    }

    /**
     * Sets ad_group_id
     *
     * @param string $ad_group_id Entity object identifier
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->container['ad_group_id'] = $ad_group_id;

        return $this;
    }

    /**
     * Gets new_campaign_details
     *
     * @return \AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignDetails
     */
    public function getNewCampaignDetails()
    {
        return $this->container['new_campaign_details'];
    }

    /**
     * Sets new_campaign_details
     *
     * @param \AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignDetails $new_campaign_details new_campaign_details
     *
     * @return $this
     */
    public function setNewCampaignDetails($new_campaign_details)
    {
        $this->container['new_campaign_details'] = $new_campaign_details;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
