<?php
/**
 * ModerationResultsAdProgramType
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * ModerationResultsAdProgramType Class Doc Comment
 *
 * @category Class
 * @description The program type of the ad.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class ModerationResultsAdProgramType
{
    /**
     * Possible values of this enum
     */
    const SB_PRODUCT_COLLECTION = 'SB_PRODUCT_COLLECTION';
    const SB_STORE_SPOTLIGHT = 'SB_STORE_SPOTLIGHT';
    const SB_VIDEO = 'SB_VIDEO';
    const SPONSORED_PRODUCTS = 'SPONSORED_PRODUCTS';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::SB_PRODUCT_COLLECTION,
            self::SB_STORE_SPOTLIGHT,
            self::SB_VIDEO,
            self::SPONSORED_PRODUCTS,
        ];
    }
}
