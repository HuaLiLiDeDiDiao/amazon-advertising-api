<?php
/**
 * SBCampaignResponseCommon
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SBCampaignResponseCommon Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SBCampaignResponseCommon extends SBCampaignBase 
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SBCampaignResponseCommon';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'bid_optimization' => 'bool',
        'bid_multiplier' => 'float',
        'bid_adjustments' => '\AmazonAdvertisingApi\Model\BidAdjustment[]',
        'creative' => '\AmazonAdvertisingApi\Model\SBCollectionCreative',
        'landing_page' => ''
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'bid_optimization' => null,
        'bid_multiplier' => null,
        'bid_adjustments' => null,
        'creative' => null,
        'landing_page' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes + parent::AmazonAdvertisingApiTypes();
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats + parent::AmazonAdvertisingApiFormats();
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'bid_optimization' => 'bidOptimization',
        'bid_multiplier' => 'bidMultiplier',
        'bid_adjustments' => 'bidAdjustments',
        'creative' => 'creative',
        'landing_page' => 'landingPage'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'bid_optimization' => 'setBidOptimization',
        'bid_multiplier' => 'setBidMultiplier',
        'bid_adjustments' => 'setBidAdjustments',
        'creative' => 'setCreative',
        'landing_page' => 'setLandingPage'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'bid_optimization' => 'getBidOptimization',
        'bid_multiplier' => 'getBidMultiplier',
        'bid_adjustments' => 'getBidAdjustments',
        'creative' => 'getCreative',
        'landing_page' => 'getLandingPage'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return parent::attributeMap() + self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return parent::setters() + self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return parent::getters() + self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }




    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);

        $this->container['bid_optimization'] = isset($data['bid_optimization']) ? $data['bid_optimization'] : null;
        $this->container['bid_multiplier'] = isset($data['bid_multiplier']) ? $data['bid_multiplier'] : null;
        $this->container['bid_adjustments'] = isset($data['bid_adjustments']) ? $data['bid_adjustments'] : null;
        $this->container['creative'] = isset($data['creative']) ? $data['creative'] : null;
        $this->container['landing_page'] = isset($data['landing_page']) ? $data['landing_page'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = parent::listInvalidProperties();

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets bid_optimization
     *
     * @return bool
     */
    public function getBidOptimization()
    {
        return $this->container['bid_optimization'];
    }

    /**
     * Sets bid_optimization
     *
     * @param bool $bid_optimization Set to `true` to allow Amazon to automatically optimize bids for placements below top of search.
     *
     * @return $this
     */
    public function setBidOptimization($bid_optimization)
    {
        $this->container['bid_optimization'] = $bid_optimization;

        return $this;
    }

    /**
     * Gets bid_multiplier
     *
     * @return float
     */
    public function getBidMultiplier()
    {
        return $this->container['bid_multiplier'];
    }

    /**
     * Sets bid_multiplier
     *
     * @param float $bid_multiplier A bid multiplier. Note that this field can only be set when 'bidOptimization' is set to false. Value is a percentage to two decimal places. For example, if set to -40.00 for a $5.00 bid, the resulting bid is $3.00.
     *
     * @return $this
     */
    public function setBidMultiplier($bid_multiplier)
    {
        $this->container['bid_multiplier'] = $bid_multiplier;

        return $this;
    }

    /**
     * Gets bid_adjustments
     *
     * @return \AmazonAdvertisingApi\Model\BidAdjustment[]
     */
    public function getBidAdjustments()
    {
        return $this->container['bid_adjustments'];
    }

    /**
     * Sets bid_adjustments
     *
     * @param \AmazonAdvertisingApi\Model\BidAdjustment[] $bid_adjustments List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. `Not supported for video campaigns`
     *
     * @return $this
     */
    public function setBidAdjustments($bid_adjustments)
    {
        $this->container['bid_adjustments'] = $bid_adjustments;

        return $this;
    }

    /**
     * Gets creative
     *
     * @return \AmazonAdvertisingApi\Model\SBCollectionCreative
     */
    public function getCreative()
    {
        return $this->container['creative'];
    }

    /**
     * Sets creative
     *
     * @param \AmazonAdvertisingApi\Model\SBCollectionCreative $creative creative
     *
     * @return $this
     */
    public function setCreative($creative)
    {
        $this->container['creative'] = $creative;

        return $this;
    }

    /**
     * Gets landing_page
     *
     * @return 
     */
    public function getLandingPage()
    {
        return $this->container['landing_page'];
    }

    /**
     * Sets landing_page
     *
     * @param  $landing_page landing_page
     *
     * @return $this
     */
    public function setLandingPage($landing_page)
    {
        $this->container['landing_page'] = $landing_page;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
