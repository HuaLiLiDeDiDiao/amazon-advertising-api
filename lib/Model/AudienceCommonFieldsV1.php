<?php
/**
 * AudienceCommonFieldsV1
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Audiences
 *
 * Audience Discovery API
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * AudienceCommonFieldsV1 Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class AudienceCommonFieldsV1 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'AudienceCommonFieldsV1';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'audience_name' => 'string',
        'sub_category' => 'string',
        'update_date' => '\DateTime',
        'description' => 'string',
        'audience_id' => 'string',
        'category' => 'string',
        'create_date' => '\DateTime',
        'status' => 'string',
        'forecasts' => '\AmazonAdvertisingApi\Model\AudienceCommonFieldsV1Forecasts'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'audience_name' => null,
        'sub_category' => null,
        'update_date' => 'date-time',
        'description' => null,
        'audience_id' => null,
        'category' => null,
        'create_date' => 'date-time',
        'status' => null,
        'forecasts' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'audience_name' => 'audienceName',
        'sub_category' => 'subCategory',
        'update_date' => 'updateDate',
        'description' => 'description',
        'audience_id' => 'audienceId',
        'category' => 'category',
        'create_date' => 'createDate',
        'status' => 'status',
        'forecasts' => 'forecasts'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'audience_name' => 'setAudienceName',
        'sub_category' => 'setSubCategory',
        'update_date' => 'setUpdateDate',
        'description' => 'setDescription',
        'audience_id' => 'setAudienceId',
        'category' => 'setCategory',
        'create_date' => 'setCreateDate',
        'status' => 'setStatus',
        'forecasts' => 'setForecasts'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'audience_name' => 'getAudienceName',
        'sub_category' => 'getSubCategory',
        'update_date' => 'getUpdateDate',
        'description' => 'getDescription',
        'audience_id' => 'getAudienceId',
        'category' => 'getCategory',
        'create_date' => 'getCreateDate',
        'status' => 'getStatus',
        'forecasts' => 'getForecasts'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const STATUS_PROCESSING = 'Processing';
    const STATUS_ACTIVE = 'Active';
    const STATUS_FAILED = 'Failed';
    const STATUS_DEPRECATED = 'Deprecated';
    const STATUS_DEACTIVATED = 'Deactivated';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_PROCESSING,
            self::STATUS_ACTIVE,
            self::STATUS_FAILED,
            self::STATUS_DEPRECATED,
            self::STATUS_DEACTIVATED,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['audience_name'] = isset($data['audience_name']) ? $data['audience_name'] : null;
        $this->container['sub_category'] = isset($data['sub_category']) ? $data['sub_category'] : null;
        $this->container['update_date'] = isset($data['update_date']) ? $data['update_date'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['audience_id'] = isset($data['audience_id']) ? $data['audience_id'] : null;
        $this->container['category'] = isset($data['category']) ? $data['category'] : null;
        $this->container['create_date'] = isset($data['create_date']) ? $data['create_date'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['forecasts'] = isset($data['forecasts']) ? $data['forecasts'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['audience_name'] === null) {
            $invalidProperties[] = "'audience_name' can't be null";
        }
        if ($this->container['description'] === null) {
            $invalidProperties[] = "'description' can't be null";
        }
        if ($this->container['audience_id'] === null) {
            $invalidProperties[] = "'audience_id' can't be null";
        }
        if ($this->container['category'] === null) {
            $invalidProperties[] = "'category' can't be null";
        }
        if ($this->container['status'] === null) {
            $invalidProperties[] = "'status' can't be null";
        }
        $allowedValues = $this->getStatusAllowableValues();
        if (!is_null($this->container['status']) && !in_array($this->container['status'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'status', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['forecasts'] === null) {
            $invalidProperties[] = "'forecasts' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets audience_name
     *
     * @return string
     */
    public function getAudienceName()
    {
        return $this->container['audience_name'];
    }

    /**
     * Sets audience_name
     *
     * @param string $audience_name Audience name
     *
     * @return $this
     */
    public function setAudienceName($audience_name)
    {
        $this->container['audience_name'] = $audience_name;

        return $this;
    }

    /**
     * Gets sub_category
     *
     * @return string
     */
    public function getSubCategory()
    {
        return $this->container['sub_category'];
    }

    /**
     * Sets sub_category
     *
     * @param string $sub_category Audience segment sub-category
     *
     * @return $this
     */
    public function setSubCategory($sub_category)
    {
        $this->container['sub_category'] = $sub_category;

        return $this;
    }

    /**
     * Gets update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->container['update_date'];
    }

    /**
     * Sets update_date
     *
     * @param \DateTime $update_date update_date
     *
     * @return $this
     */
    public function setUpdateDate($update_date)
    {
        $this->container['update_date'] = $update_date;

        return $this;
    }

    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     *
     * @param string $description Audience description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets audience_id
     *
     * @return string
     */
    public function getAudienceId()
    {
        return $this->container['audience_id'];
    }

    /**
     * Sets audience_id
     *
     * @param string $audience_id Audience segment identifier
     *
     * @return $this
     */
    public function setAudienceId($audience_id)
    {
        $this->container['audience_id'] = $audience_id;

        return $this;
    }

    /**
     * Gets category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param string $category Audience segment category
     *
     * @return $this
     */
    public function setCategory($category)
    {
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->container['create_date'];
    }

    /**
     * Sets create_date
     *
     * @param \DateTime $create_date create_date
     *
     * @return $this
     */
    public function setCreateDate($create_date)
    {
        $this->container['create_date'] = $create_date;

        return $this;
    }

    /**
     * Gets status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string $status status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $allowedValues = $this->getStatusAllowableValues();
        if (!in_array($status, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'status', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets forecasts
     *
     * @return \AmazonAdvertisingApi\Model\AudienceCommonFieldsV1Forecasts
     */
    public function getForecasts()
    {
        return $this->container['forecasts'];
    }

    /**
     * Sets forecasts
     *
     * @param \AmazonAdvertisingApi\Model\AudienceCommonFieldsV1Forecasts $forecasts forecasts
     *
     * @return $this
     */
    public function setForecasts($forecasts)
    {
        $this->container['forecasts'] = $forecasts;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
