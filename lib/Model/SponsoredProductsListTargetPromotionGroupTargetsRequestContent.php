<?php
/**
 * SponsoredProductsListTargetPromotionGroupTargetsRequestContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SponsoredProductsListTargetPromotionGroupTargetsRequestContent Class Doc Comment
 *
 * @category Class
 * @description Request object for querying target promotion group targets.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SponsoredProductsListTargetPromotionGroupTargetsRequestContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SponsoredProductsListTargetPromotionGroupTargetsRequestContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'max_results' => 'int',
        'next_token' => 'string',
        'target_promotion_group_id_filter' => '\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter',
        'ad_group_id_filter' => '\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'max_results' => 'int32',
        'next_token' => null,
        'target_promotion_group_id_filter' => null,
        'ad_group_id_filter' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'max_results' => 'maxResults',
        'next_token' => 'nextToken',
        'target_promotion_group_id_filter' => 'targetPromotionGroupIdFilter',
        'ad_group_id_filter' => 'adGroupIdFilter'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'max_results' => 'setMaxResults',
        'next_token' => 'setNextToken',
        'target_promotion_group_id_filter' => 'setTargetPromotionGroupIdFilter',
        'ad_group_id_filter' => 'setAdGroupIdFilter'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'max_results' => 'getMaxResults',
        'next_token' => 'getNextToken',
        'target_promotion_group_id_filter' => 'getTargetPromotionGroupIdFilter',
        'ad_group_id_filter' => 'getAdGroupIdFilter'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['max_results'] = isset($data['max_results']) ? $data['max_results'] : null;
        $this->container['next_token'] = isset($data['next_token']) ? $data['next_token'] : null;
        $this->container['target_promotion_group_id_filter'] = isset($data['target_promotion_group_id_filter']) ? $data['target_promotion_group_id_filter'] : null;
        $this->container['ad_group_id_filter'] = isset($data['ad_group_id_filter']) ? $data['ad_group_id_filter'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets max_results
     *
     * @return int
     */
    public function getMaxResults()
    {
        return $this->container['max_results'];
    }

    /**
     * Sets max_results
     *
     * @param int $max_results The maximum number of results requested.
     *
     * @return $this
     */
    public function setMaxResults($max_results)
    {
        $this->container['max_results'] = $max_results;

        return $this;
    }

    /**
     * Gets next_token
     *
     * @return string
     */
    public function getNextToken()
    {
        return $this->container['next_token'];
    }

    /**
     * Sets next_token
     *
     * @param string $next_token Token value allowing to navigate to the next or previous response page
     *
     * @return $this
     */
    public function setNextToken($next_token)
    {
        $this->container['next_token'] = $next_token;

        return $this;
    }

    /**
     * Gets target_promotion_group_id_filter
     *
     * @return \AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter
     */
    public function getTargetPromotionGroupIdFilter()
    {
        return $this->container['target_promotion_group_id_filter'];
    }

    /**
     * Sets target_promotion_group_id_filter
     *
     * @param \AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter $target_promotion_group_id_filter target_promotion_group_id_filter
     *
     * @return $this
     */
    public function setTargetPromotionGroupIdFilter($target_promotion_group_id_filter)
    {
        $this->container['target_promotion_group_id_filter'] = $target_promotion_group_id_filter;

        return $this;
    }

    /**
     * Gets ad_group_id_filter
     *
     * @return \AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter
     */
    public function getAdGroupIdFilter()
    {
        return $this->container['ad_group_id_filter'];
    }

    /**
     * Sets ad_group_id_filter
     *
     * @param \AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter $ad_group_id_filter ad_group_id_filter
     *
     * @return $this
     */
    public function setAdGroupIdFilter($ad_group_id_filter)
    {
        $this->container['ad_group_id_filter'] = $ad_group_id_filter;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
