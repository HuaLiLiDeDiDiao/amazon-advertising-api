<?php
/**
 * StoreSpotlightCreative
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * StoreSpotlightCreative Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class StoreSpotlightCreative implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'StoreSpotlightCreative';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'brand_logo_crop' => '\AmazonAdvertisingApi\Model\AssetCrop',
        'brand_name' => 'string',
        'subpages' => '\AmazonAdvertisingApi\Model\Subpage[]',
        'landing_page' => '\AmazonAdvertisingApi\Model\CreativeLandingPageV2',
        'consent_to_translate' => 'bool',
        'brand_logo_asset_id' => 'string',
        'headline' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'brand_logo_crop' => null,
        'brand_name' => null,
        'subpages' => null,
        'landing_page' => null,
        'consent_to_translate' => null,
        'brand_logo_asset_id' => null,
        'headline' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'brand_logo_crop' => 'brandLogoCrop',
        'brand_name' => 'brandName',
        'subpages' => 'subpages',
        'landing_page' => 'landingPage',
        'consent_to_translate' => 'consentToTranslate',
        'brand_logo_asset_id' => 'brandLogoAssetId',
        'headline' => 'headline'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'brand_logo_crop' => 'setBrandLogoCrop',
        'brand_name' => 'setBrandName',
        'subpages' => 'setSubpages',
        'landing_page' => 'setLandingPage',
        'consent_to_translate' => 'setConsentToTranslate',
        'brand_logo_asset_id' => 'setBrandLogoAssetId',
        'headline' => 'setHeadline'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'brand_logo_crop' => 'getBrandLogoCrop',
        'brand_name' => 'getBrandName',
        'subpages' => 'getSubpages',
        'landing_page' => 'getLandingPage',
        'consent_to_translate' => 'getConsentToTranslate',
        'brand_logo_asset_id' => 'getBrandLogoAssetId',
        'headline' => 'getHeadline'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['brand_logo_crop'] = isset($data['brand_logo_crop']) ? $data['brand_logo_crop'] : null;
        $this->container['brand_name'] = isset($data['brand_name']) ? $data['brand_name'] : null;
        $this->container['subpages'] = isset($data['subpages']) ? $data['subpages'] : null;
        $this->container['landing_page'] = isset($data['landing_page']) ? $data['landing_page'] : null;
        $this->container['consent_to_translate'] = isset($data['consent_to_translate']) ? $data['consent_to_translate'] : null;
        $this->container['brand_logo_asset_id'] = isset($data['brand_logo_asset_id']) ? $data['brand_logo_asset_id'] : null;
        $this->container['headline'] = isset($data['headline']) ? $data['headline'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['brand_name'] === null) {
            $invalidProperties[] = "'brand_name' can't be null";
        }
        if ($this->container['subpages'] === null) {
            $invalidProperties[] = "'subpages' can't be null";
        }
        if ($this->container['brand_logo_asset_id'] === null) {
            $invalidProperties[] = "'brand_logo_asset_id' can't be null";
        }
        if ($this->container['headline'] === null) {
            $invalidProperties[] = "'headline' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets brand_logo_crop
     *
     * @return \AmazonAdvertisingApi\Model\AssetCrop
     */
    public function getBrandLogoCrop()
    {
        return $this->container['brand_logo_crop'];
    }

    /**
     * Sets brand_logo_crop
     *
     * @param \AmazonAdvertisingApi\Model\AssetCrop $brand_logo_crop brand_logo_crop
     *
     * @return $this
     */
    public function setBrandLogoCrop($brand_logo_crop)
    {
        $this->container['brand_logo_crop'] = $brand_logo_crop;

        return $this;
    }

    /**
     * Gets brand_name
     *
     * @return string
     */
    public function getBrandName()
    {
        return $this->container['brand_name'];
    }

    /**
     * Sets brand_name
     *
     * @param string $brand_name The displayed brand name in the ad headline. Maximum length is 30 characters. See [the policy](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#headlines) for headline requirements.
     *
     * @return $this
     */
    public function setBrandName($brand_name)
    {
        $this->container['brand_name'] = $brand_name;

        return $this;
    }

    /**
     * Gets subpages
     *
     * @return \AmazonAdvertisingApi\Model\Subpage[]
     */
    public function getSubpages()
    {
        return $this->container['subpages'];
    }

    /**
     * Sets subpages
     *
     * @param \AmazonAdvertisingApi\Model\Subpage[] $subpages An array of subpages
     *
     * @return $this
     */
    public function setSubpages($subpages)
    {
        $this->container['subpages'] = $subpages;

        return $this;
    }

    /**
     * Gets landing_page
     *
     * @return \AmazonAdvertisingApi\Model\CreativeLandingPageV2
     */
    public function getLandingPage()
    {
        return $this->container['landing_page'];
    }

    /**
     * Sets landing_page
     *
     * @param \AmazonAdvertisingApi\Model\CreativeLandingPageV2 $landing_page landing_page
     *
     * @return $this
     */
    public function setLandingPage($landing_page)
    {
        $this->container['landing_page'] = $landing_page;

        return $this;
    }

    /**
     * Gets consent_to_translate
     *
     * @return bool
     */
    public function getConsentToTranslate()
    {
        return $this->container['consent_to_translate'];
    }

    /**
     * Sets consent_to_translate
     *
     * @param bool $consent_to_translate If set to true and the headline and/or video are not in the marketplace's default language, Amazon will attempt to translate them to the marketplace's default language. If Amazon is unable to translate them, the ad will be rejected by moderation. We only support translating headlines and videos from English to German, French, Italian, Spanish, Japanese, and Dutch. See developer notes for more information.
     *
     * @return $this
     */
    public function setConsentToTranslate($consent_to_translate)
    {
        $this->container['consent_to_translate'] = $consent_to_translate;

        return $this;
    }

    /**
     * Gets brand_logo_asset_id
     *
     * @return string
     */
    public function getBrandLogoAssetId()
    {
        return $this->container['brand_logo_asset_id'];
    }

    /**
     * Sets brand_logo_asset_id
     *
     * @param string $brand_logo_asset_id The identifier of the [brand logo](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#brandlogo) image from the brand store's asset library. Note that for campaigns created in the Amazon Advertising console prior to release of the brand store's assets library, responses will not include a value for this field.
     *
     * @return $this
     */
    public function setBrandLogoAssetId($brand_logo_asset_id)
    {
        $this->container['brand_logo_asset_id'] = $brand_logo_asset_id;

        return $this;
    }

    /**
     * Gets headline
     *
     * @return string
     */
    public function getHeadline()
    {
        return $this->container['headline'];
    }

    /**
     * Sets headline
     *
     * @param string $headline The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. See [the policy](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#headlines) for headline requirements.
     *
     * @return $this
     */
    public function setHeadline($headline)
    {
        $this->container['headline'] = $headline;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
