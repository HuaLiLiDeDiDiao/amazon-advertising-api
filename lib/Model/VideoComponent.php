<?php
/**
 * VideoComponent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * VideoComponent Class Doc Comment
 *
 * @category Class
 * @description Video component which needs to be pre moderated. A publicly accessible videoUrl must be sent.
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class VideoComponent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'VideoComponent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'component_type' => 'string',
        'landing_page' => '\AmazonAdvertisingApi\Model\LandingPage',
        'id' => 'string',
        'url' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'component_type' => null,
        'landing_page' => null,
        'id' => null,
        'url' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'component_type' => 'componentType',
        'landing_page' => 'landingPage',
        'id' => 'id',
        'url' => 'url'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'component_type' => 'setComponentType',
        'landing_page' => 'setLandingPage',
        'id' => 'setId',
        'url' => 'setUrl'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'component_type' => 'getComponentType',
        'landing_page' => 'getLandingPage',
        'id' => 'getId',
        'url' => 'getUrl'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const COMPONENT_TYPE_SPONSORED_BRANDS_VIDEO = 'SPONSORED_BRANDS_VIDEO';
    const COMPONENT_TYPE_OTHER_VIDEO = 'OTHER_VIDEO';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getComponentTypeAllowableValues()
    {
        return [
            self::COMPONENT_TYPE_SPONSORED_BRANDS_VIDEO,
            self::COMPONENT_TYPE_OTHER_VIDEO,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['component_type'] = isset($data['component_type']) ? $data['component_type'] : null;
        $this->container['landing_page'] = isset($data['landing_page']) ? $data['landing_page'] : null;
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['url'] = isset($data['url']) ? $data['url'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['component_type'] === null) {
            $invalidProperties[] = "'component_type' can't be null";
        }
        $allowedValues = $this->getComponentTypeAllowableValues();
        if (!is_null($this->container['component_type']) && !in_array($this->container['component_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'component_type', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['id'] === null) {
            $invalidProperties[] = "'id' can't be null";
        }
        if ($this->container['url'] === null) {
            $invalidProperties[] = "'url' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets component_type
     *
     * @return string
     */
    public function getComponentType()
    {
        return $this->container['component_type'];
    }

    /**
     * Sets component_type
     *
     * @param string $component_type Type of the video component.
     *
     * @return $this
     */
    public function setComponentType($component_type)
    {
        $allowedValues = $this->getComponentTypeAllowableValues();
        if (!in_array($component_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'component_type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['component_type'] = $component_type;

        return $this;
    }

    /**
     * Gets landing_page
     *
     * @return \AmazonAdvertisingApi\Model\LandingPage
     */
    public function getLandingPage()
    {
        return $this->container['landing_page'];
    }

    /**
     * Sets landing_page
     *
     * @param \AmazonAdvertisingApi\Model\LandingPage $landing_page landing_page
     *
     * @return $this
     */
    public function setLandingPage($landing_page)
    {
        $this->container['landing_page'] = $landing_page;

        return $this;
    }

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id Id of the component. The same will be returned as part of the response as well. This can be used to uniquely identify the component from the pre moderation response.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->container['url'];
    }

    /**
     * Sets url
     *
     * @param string $url Url of the video to be pre moderated. The url must be publicly accessible.
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->container['url'] = $url;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
