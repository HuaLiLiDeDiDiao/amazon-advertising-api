<?php
/**
 * BidOptimizationStrategy
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * BidOptimizationStrategy Class Doc Comment
 *
 * @category Class
 * @description Automated bid optimization strategy. |Bid optimization strategy|Description| |------|-----------| |MAXIMIZE_IMMEDIATE_SALES|Bid optimization strategy for maximizing immediate sales.| |MAXIMIZE_NEW_TO_BRAND_CUSTOMERS|Bid optimization strategy for maximizing new to brand customers.| &#x60;Not supported for video campaigns&#x60;
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class BidOptimizationStrategy
{
    /**
     * Possible values of this enum
     */
    const IMMEDIATE_SALES = 'MAXIMIZE_IMMEDIATE_SALES';
    const NEW_TO_BRAND_CUSTOMERS = 'MAXIMIZE_NEW_TO_BRAND_CUSTOMERS';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::IMMEDIATE_SALES,
            self::NEW_TO_BRAND_CUSTOMERS,
        ];
    }
}
