<?php
/**
 * SponsoredProductsDuplicateValueErrorReason
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SponsoredProductsDuplicateValueErrorReason Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SponsoredProductsDuplicateValueErrorReason
{
    /**
     * Possible values of this enum
     */
    const DUPLICATE_VALUE = 'DUPLICATE_VALUE';
    const NAME_NOT_UNIQUE = 'NAME_NOT_UNIQUE';
    const MARKETPLACE_ATTRIBUTES_REPEATED = 'MARKETPLACE_ATTRIBUTES_REPEATED';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::DUPLICATE_VALUE,
            self::NAME_NOT_UNIQUE,
            self::MARKETPLACE_ATTRIBUTES_REPEATED,
        ];
    }
}
