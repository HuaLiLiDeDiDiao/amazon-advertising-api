<?php
/**
 * TargetingExpressionExpressions
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Products
 *
 * Use the Amazon Ads API for Sponsored Products for campaign, ad group, keyword, negative keyword, and product ad management operations. For more information about Sponsored Products, see the [Sponsored Products Support Center](https://advertising.amazon.com/help?entityId=ENTITY3CWETCZD9HEG2#GWGFKPEWVWG2CLUJ). For onboarding information, see the [account setup](guides/onboarding/overview) topic.<br/> <br/> **Note**: This contract contains endpoints with upcoming planned deprecations. For more information on the latest versions and migration details, see [Deprecations](release-notes/deprecations).<br/> <br/>
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * TargetingExpressionExpressions Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class TargetingExpressionExpressions implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'TargetingExpression_expressions';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'type' => 'string[]',
        'value' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'type' => null,
        'value' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'type' => 'type',
        'value' => 'value'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'type' => 'setType',
        'value' => 'setValue'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'type' => 'getType',
        'value' => 'getValue'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const TYPE_QUERY_BROAD_MATCHES = 'queryBroadMatches';
    const TYPE_QUERY_PHRASE_MATCHES = 'queryPhraseMatches';
    const TYPE_QUERY_EXACT_MATCHES = 'queryExactMatches';
    const TYPE_ASIN_CATEGORY_SAME_AS = 'asinCategorySameAs';
    const TYPE_ASIN_BRAND_SAME_AS = 'asinBrandSameAs';
    const TYPE_ASIN_PRICE_LESS_THAN = 'asinPriceLessThan';
    const TYPE_ASIN_PRICE_BETWEEN = 'asinPriceBetween';
    const TYPE_ASIN_PRICE_GREATER_THAN = 'asinPriceGreaterThan';
    const TYPE_ASIN_REVIEW_RATING_LESS_THAN = 'asinReviewRatingLessThan';
    const TYPE_ASIN_REVIEW_RATING_BETWEEN = 'asinReviewRatingBetween';
    const TYPE_ASIN_REVIEW_RATING_GREATER_THAN = 'asinReviewRatingGreaterThan';
    const TYPE_ASIN_SAME_AS = 'asinSameAs';
    const TYPE_QUERY_BROAD_REL_MATCHES = 'queryBroadRelMatches';
    const TYPE_QUERY_HIGH_REL_MATCHES = 'queryHighRelMatches';
    const TYPE_ASIN_SUBSTITUTE_RELATED = 'asinSubstituteRelated';
    const TYPE_ASIN_ACCESSORY_RELATED = 'asinAccessoryRelated';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_QUERY_BROAD_MATCHES,
            self::TYPE_QUERY_PHRASE_MATCHES,
            self::TYPE_QUERY_EXACT_MATCHES,
            self::TYPE_ASIN_CATEGORY_SAME_AS,
            self::TYPE_ASIN_BRAND_SAME_AS,
            self::TYPE_ASIN_PRICE_LESS_THAN,
            self::TYPE_ASIN_PRICE_BETWEEN,
            self::TYPE_ASIN_PRICE_GREATER_THAN,
            self::TYPE_ASIN_REVIEW_RATING_LESS_THAN,
            self::TYPE_ASIN_REVIEW_RATING_BETWEEN,
            self::TYPE_ASIN_REVIEW_RATING_GREATER_THAN,
            self::TYPE_ASIN_SAME_AS,
            self::TYPE_QUERY_BROAD_REL_MATCHES,
            self::TYPE_QUERY_HIGH_REL_MATCHES,
            self::TYPE_ASIN_SUBSTITUTE_RELATED,
            self::TYPE_ASIN_ACCESSORY_RELATED,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['value'] = isset($data['value']) ? $data['value'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets type
     *
     * @return string[]
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param string[] $type The type.
     *
     * @return $this
     */
    public function setType($type)
    {
        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($type) && array_diff($type, $allowedValues)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'type', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->container['value'];
    }

    /**
     * Sets value
     *
     * @param string $value The expression value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->container['value'] = $value;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
