<?php
/**
 * BrandMetricsOverviewResponseMetrics
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Brand Metrics
 *
 * Brand Metrics provides a new measurement solution that quantifies opportunities for your brand at each stage of the customer journey on Amazon, and helps brands understand the value of different shopping engagements that impact stages of that journey. You can now access Awareness and Consideration indices that compare your performance to peers using models predictive of consideration and sales. Brand Metrics quantifies the number of customers in the awareness and consideration marketing funnel stages and is built at scale to measure all shopping engagements with your brand on Amazon, not just ad-attributed engagements. Additionally, BM breaks out key shopping engagements at each stage of the shopping journey, along with the Return on Engagement, so you can measure the historical sales following a consideration event or purchase.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * BrandMetricsOverviewResponseMetrics Class Doc Comment
 *
 * @category Class
 * @description metrics for the overview page
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class BrandMetricsOverviewResponseMetrics implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'brandMetricsOverviewResponse_metrics';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'engaged_shopper_rate_lower_bound' => 'double',
        'engaged_shopper_rate_upper_bound' => 'double',
        'customer_conversion_rate' => 'double',
        'total_shoppers' => 'int',
        'new_to_brand_customer_rate' => 'double'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'engaged_shopper_rate_lower_bound' => 'double',
        'engaged_shopper_rate_upper_bound' => 'double',
        'customer_conversion_rate' => 'double',
        'total_shoppers' => 'int64',
        'new_to_brand_customer_rate' => 'double'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'engaged_shopper_rate_lower_bound' => 'engagedShopperRateLowerBound',
        'engaged_shopper_rate_upper_bound' => 'engagedShopperRateUpperBound',
        'customer_conversion_rate' => 'customerConversionRate',
        'total_shoppers' => 'totalShoppers',
        'new_to_brand_customer_rate' => 'newToBrandCustomerRate'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'engaged_shopper_rate_lower_bound' => 'setEngagedShopperRateLowerBound',
        'engaged_shopper_rate_upper_bound' => 'setEngagedShopperRateUpperBound',
        'customer_conversion_rate' => 'setCustomerConversionRate',
        'total_shoppers' => 'setTotalShoppers',
        'new_to_brand_customer_rate' => 'setNewToBrandCustomerRate'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'engaged_shopper_rate_lower_bound' => 'getEngagedShopperRateLowerBound',
        'engaged_shopper_rate_upper_bound' => 'getEngagedShopperRateUpperBound',
        'customer_conversion_rate' => 'getCustomerConversionRate',
        'total_shoppers' => 'getTotalShoppers',
        'new_to_brand_customer_rate' => 'getNewToBrandCustomerRate'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['engaged_shopper_rate_lower_bound'] = isset($data['engaged_shopper_rate_lower_bound']) ? $data['engaged_shopper_rate_lower_bound'] : null;
        $this->container['engaged_shopper_rate_upper_bound'] = isset($data['engaged_shopper_rate_upper_bound']) ? $data['engaged_shopper_rate_upper_bound'] : null;
        $this->container['customer_conversion_rate'] = isset($data['customer_conversion_rate']) ? $data['customer_conversion_rate'] : null;
        $this->container['total_shoppers'] = isset($data['total_shoppers']) ? $data['total_shoppers'] : null;
        $this->container['new_to_brand_customer_rate'] = isset($data['new_to_brand_customer_rate']) ? $data['new_to_brand_customer_rate'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets engaged_shopper_rate_lower_bound
     *
     * @return double
     */
    public function getEngagedShopperRateLowerBound()
    {
        return $this->container['engaged_shopper_rate_lower_bound'];
    }

    /**
     * Sets engaged_shopper_rate_lower_bound
     *
     * @param double $engaged_shopper_rate_lower_bound Lower bound on the Percentage of unique shoppers in a category that your brand has driven an engagement with or purchase from int the selected time frame divided by the total unique shoppers with 1+ detail page view in the selected category
     *
     * @return $this
     */
    public function setEngagedShopperRateLowerBound($engaged_shopper_rate_lower_bound)
    {
        $this->container['engaged_shopper_rate_lower_bound'] = $engaged_shopper_rate_lower_bound;

        return $this;
    }

    /**
     * Gets engaged_shopper_rate_upper_bound
     *
     * @return double
     */
    public function getEngagedShopperRateUpperBound()
    {
        return $this->container['engaged_shopper_rate_upper_bound'];
    }

    /**
     * Sets engaged_shopper_rate_upper_bound
     *
     * @param double $engaged_shopper_rate_upper_bound Upper bound on the Percentage of unique shoppers in a category that your brand has driven an engagement with or purchase from int the selected time frame divided by the total unique shoppers with 1+ detail page view in the selected category
     *
     * @return $this
     */
    public function setEngagedShopperRateUpperBound($engaged_shopper_rate_upper_bound)
    {
        $this->container['engaged_shopper_rate_upper_bound'] = $engaged_shopper_rate_upper_bound;

        return $this;
    }

    /**
     * Gets customer_conversion_rate
     *
     * @return double
     */
    public function getCustomerConversionRate()
    {
        return $this->container['customer_conversion_rate'];
    }

    /**
     * Sets customer_conversion_rate
     *
     * @param double $customer_conversion_rate Percentage of shoppers moving from \\\"considering\\\" [Brand Name] in the [categoryPath] to \\\"purchased\\\" in the [lookBackPeriod]
     *
     * @return $this
     */
    public function setCustomerConversionRate($customer_conversion_rate)
    {
        $this->container['customer_conversion_rate'] = $customer_conversion_rate;

        return $this;
    }

    /**
     * Gets total_shoppers
     *
     * @return int
     */
    public function getTotalShoppers()
    {
        return $this->container['total_shoppers'];
    }

    /**
     * Sets total_shoppers
     *
     * @param int $total_shoppers Total number of shoppers that interacted with the brand in the given lookback period.
     *
     * @return $this
     */
    public function setTotalShoppers($total_shoppers)
    {
        $this->container['total_shoppers'] = $total_shoppers;

        return $this;
    }

    /**
     * Gets new_to_brand_customer_rate
     *
     * @return double
     */
    public function getNewToBrandCustomerRate()
    {
        return $this->container['new_to_brand_customer_rate'];
    }

    /**
     * Sets new_to_brand_customer_rate
     *
     * @param double $new_to_brand_customer_rate share of customers that had not purchased [Brand Name] products in the last 12 months, but did so in the [lookBackPeriod]
     *
     * @return $this
     */
    public function setNewToBrandCustomerRate($new_to_brand_customer_rate)
    {
        $this->container['new_to_brand_customer_rate'] = $new_to_brand_customer_rate;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
