<?php
/**
 * SBTargetingClause
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * SBTargetingClause Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class SBTargetingClause implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'SBTargetingClause';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'target_id' => 'int',
        'ad_group_id' => 'int',
        'campaign_id' => 'int',
        'expressions' => '\AmazonAdvertisingApi\Model\SBExpression',
        'resolved_expressions' => '\AmazonAdvertisingApi\Model\SBResolvedExpression',
        'state' => '\AmazonAdvertisingApi\Model\SBProductTargetState',
        'bid' => '\AmazonAdvertisingApi\Model\Bid'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'target_id' => 'int64',
        'ad_group_id' => 'int64',
        'campaign_id' => 'int64',
        'expressions' => null,
        'resolved_expressions' => null,
        'state' => null,
        'bid' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'target_id' => 'targetId',
        'ad_group_id' => 'adGroupId',
        'campaign_id' => 'campaignId',
        'expressions' => 'expressions',
        'resolved_expressions' => 'resolvedExpressions',
        'state' => 'state',
        'bid' => 'bid'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'target_id' => 'setTargetId',
        'ad_group_id' => 'setAdGroupId',
        'campaign_id' => 'setCampaignId',
        'expressions' => 'setExpressions',
        'resolved_expressions' => 'setResolvedExpressions',
        'state' => 'setState',
        'bid' => 'setBid'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'target_id' => 'getTargetId',
        'ad_group_id' => 'getAdGroupId',
        'campaign_id' => 'getCampaignId',
        'expressions' => 'getExpressions',
        'resolved_expressions' => 'getResolvedExpressions',
        'state' => 'getState',
        'bid' => 'getBid'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['target_id'] = isset($data['target_id']) ? $data['target_id'] : null;
        $this->container['ad_group_id'] = isset($data['ad_group_id']) ? $data['ad_group_id'] : null;
        $this->container['campaign_id'] = isset($data['campaign_id']) ? $data['campaign_id'] : null;
        $this->container['expressions'] = isset($data['expressions']) ? $data['expressions'] : null;
        $this->container['resolved_expressions'] = isset($data['resolved_expressions']) ? $data['resolved_expressions'] : null;
        $this->container['state'] = isset($data['state']) ? $data['state'] : null;
        $this->container['bid'] = isset($data['bid']) ? $data['bid'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets target_id
     *
     * @return int
     */
    public function getTargetId()
    {
        return $this->container['target_id'];
    }

    /**
     * Sets target_id
     *
     * @param int $target_id The target identifier.
     *
     * @return $this
     */
    public function setTargetId($target_id)
    {
        $this->container['target_id'] = $target_id;

        return $this;
    }

    /**
     * Gets ad_group_id
     *
     * @return int
     */
    public function getAdGroupId()
    {
        return $this->container['ad_group_id'];
    }

    /**
     * Sets ad_group_id
     *
     * @param int $ad_group_id The identifier of the ad group to which the target is associated.
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->container['ad_group_id'] = $ad_group_id;

        return $this;
    }

    /**
     * Gets campaign_id
     *
     * @return int
     */
    public function getCampaignId()
    {
        return $this->container['campaign_id'];
    }

    /**
     * Sets campaign_id
     *
     * @param int $campaign_id The identifier of the campaign to which the target is associated.
     *
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->container['campaign_id'] = $campaign_id;

        return $this;
    }

    /**
     * Gets expressions
     *
     * @return \AmazonAdvertisingApi\Model\SBExpression
     */
    public function getExpressions()
    {
        return $this->container['expressions'];
    }

    /**
     * Sets expressions
     *
     * @param \AmazonAdvertisingApi\Model\SBExpression $expressions expressions
     *
     * @return $this
     */
    public function setExpressions($expressions)
    {
        $this->container['expressions'] = $expressions;

        return $this;
    }

    /**
     * Gets resolved_expressions
     *
     * @return \AmazonAdvertisingApi\Model\SBResolvedExpression
     */
    public function getResolvedExpressions()
    {
        return $this->container['resolved_expressions'];
    }

    /**
     * Sets resolved_expressions
     *
     * @param \AmazonAdvertisingApi\Model\SBResolvedExpression $resolved_expressions resolved_expressions
     *
     * @return $this
     */
    public function setResolvedExpressions($resolved_expressions)
    {
        $this->container['resolved_expressions'] = $resolved_expressions;

        return $this;
    }

    /**
     * Gets state
     *
     * @return \AmazonAdvertisingApi\Model\SBProductTargetState
     */
    public function getState()
    {
        return $this->container['state'];
    }

    /**
     * Sets state
     *
     * @param \AmazonAdvertisingApi\Model\SBProductTargetState $state state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->container['state'] = $state;

        return $this;
    }

    /**
     * Gets bid
     *
     * @return \AmazonAdvertisingApi\Model\Bid
     */
    public function getBid()
    {
        return $this->container['bid'];
    }

    /**
     * Sets bid
     *
     * @param \AmazonAdvertisingApi\Model\Bid $bid bid
     *
     * @return $this
     */
    public function setBid($bid)
    {
        $this->container['bid'] = $bid;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
