<?php
/**
 * AdGroupEx
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Products
 *
 * Use the Amazon Ads API for Sponsored Products for campaign, ad group, keyword, negative keyword, and product ad management operations. For more information about Sponsored Products, see the [Sponsored Products Support Center](https://advertising.amazon.com/help?entityId=ENTITY3CWETCZD9HEG2#GWGFKPEWVWG2CLUJ). For onboarding information, see the [account setup](guides/onboarding/overview) topic.<br/> <br/> **Note**: This contract contains endpoints with upcoming planned deprecations. For more information on the latest versions and migration details, see [Deprecations](release-notes/deprecations).<br/> <br/>
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * AdGroupEx Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class AdGroupEx implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'AdGroupEx';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'ad_group_id' => 'float',
        'name' => 'string',
        'campaign_id' => 'float',
        'default_bid' => 'float',
        'state' => '\AmazonAdvertisingApi\Model\State',
        'creation_date' => 'float',
        'last_updated_date' => 'float',
        'serving_status' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'ad_group_id' => null,
        'name' => null,
        'campaign_id' => null,
        'default_bid' => null,
        'state' => null,
        'creation_date' => null,
        'last_updated_date' => null,
        'serving_status' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'ad_group_id' => 'adGroupId',
        'name' => 'name',
        'campaign_id' => 'campaignId',
        'default_bid' => 'defaultBid',
        'state' => 'state',
        'creation_date' => 'creationDate',
        'last_updated_date' => 'lastUpdatedDate',
        'serving_status' => 'servingStatus'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'ad_group_id' => 'setAdGroupId',
        'name' => 'setName',
        'campaign_id' => 'setCampaignId',
        'default_bid' => 'setDefaultBid',
        'state' => 'setState',
        'creation_date' => 'setCreationDate',
        'last_updated_date' => 'setLastUpdatedDate',
        'serving_status' => 'setServingStatus'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'ad_group_id' => 'getAdGroupId',
        'name' => 'getName',
        'campaign_id' => 'getCampaignId',
        'default_bid' => 'getDefaultBid',
        'state' => 'getState',
        'creation_date' => 'getCreationDate',
        'last_updated_date' => 'getLastUpdatedDate',
        'serving_status' => 'getServingStatus'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }

    const SERVING_STATUS_AD_GROUP_ARCHIVED = 'AD_GROUP_ARCHIVED';
    const SERVING_STATUS_AD_GROUP_PAUSED = 'AD_GROUP_PAUSED';
    const SERVING_STATUS_AD_GROUP_STATUS_ENABLED = 'AD_GROUP_STATUS_ENABLED';
    const SERVING_STATUS_AD_POLICING_SUSPENDED = 'AD_POLICING_SUSPENDED';
    const SERVING_STATUS_AD_GROUP_INCOMPLETE = 'AD_GROUP_INCOMPLETE';
    const SERVING_STATUS_CAMPAIGN_OUT_OF_BUDGET = 'CAMPAIGN_OUT_OF_BUDGET';
    const SERVING_STATUS_CAMPAIGN_PAUSED = 'CAMPAIGN_PAUSED';
    const SERVING_STATUS_CAMPAIGN_ARCHIVED = 'CAMPAIGN_ARCHIVED';
    const SERVING_STATUS_CAMPAIGN_INCOMPLETE = 'CAMPAIGN_INCOMPLETE';
    const SERVING_STATUS_ACCOUNT_OUT_OF_BUDGET = 'ACCOUNT_OUT_OF_BUDGET';
    const SERVING_STATUS_PENDING_START_DATE = 'PENDING_START_DATE';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getServingStatusAllowableValues()
    {
        return [
            self::SERVING_STATUS_AD_GROUP_ARCHIVED,
            self::SERVING_STATUS_AD_GROUP_PAUSED,
            self::SERVING_STATUS_AD_GROUP_STATUS_ENABLED,
            self::SERVING_STATUS_AD_POLICING_SUSPENDED,
            self::SERVING_STATUS_AD_GROUP_INCOMPLETE,
            self::SERVING_STATUS_CAMPAIGN_OUT_OF_BUDGET,
            self::SERVING_STATUS_CAMPAIGN_PAUSED,
            self::SERVING_STATUS_CAMPAIGN_ARCHIVED,
            self::SERVING_STATUS_CAMPAIGN_INCOMPLETE,
            self::SERVING_STATUS_ACCOUNT_OUT_OF_BUDGET,
            self::SERVING_STATUS_PENDING_START_DATE,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['ad_group_id'] = isset($data['ad_group_id']) ? $data['ad_group_id'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['campaign_id'] = isset($data['campaign_id']) ? $data['campaign_id'] : null;
        $this->container['default_bid'] = isset($data['default_bid']) ? $data['default_bid'] : null;
        $this->container['state'] = isset($data['state']) ? $data['state'] : null;
        $this->container['creation_date'] = isset($data['creation_date']) ? $data['creation_date'] : null;
        $this->container['last_updated_date'] = isset($data['last_updated_date']) ? $data['last_updated_date'] : null;
        $this->container['serving_status'] = isset($data['serving_status']) ? $data['serving_status'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getServingStatusAllowableValues();
        if (!is_null($this->container['serving_status']) && !in_array($this->container['serving_status'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'serving_status', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets ad_group_id
     *
     * @return float
     */
    public function getAdGroupId()
    {
        return $this->container['ad_group_id'];
    }

    /**
     * Sets ad_group_id
     *
     * @param float $ad_group_id The identifier of the ad group.
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->container['ad_group_id'] = $ad_group_id;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name The name of the ad group.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets campaign_id
     *
     * @return float
     */
    public function getCampaignId()
    {
        return $this->container['campaign_id'];
    }

    /**
     * Sets campaign_id
     *
     * @param float $campaign_id The identifier of the campaign that the ad group is associated with.
     *
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->container['campaign_id'] = $campaign_id;

        return $this;
    }

    /**
     * Gets default_bid
     *
     * @return float
     */
    public function getDefaultBid()
    {
        return $this->container['default_bid'];
    }

    /**
     * Sets default_bid
     *
     * @param float $default_bid The bid value used when no bid is specified for keywords in the ad group.
     *
     * @return $this
     */
    public function setDefaultBid($default_bid)
    {
        $this->container['default_bid'] = $default_bid;

        return $this;
    }

    /**
     * Gets state
     *
     * @return \AmazonAdvertisingApi\Model\State
     */
    public function getState()
    {
        return $this->container['state'];
    }

    /**
     * Sets state
     *
     * @param \AmazonAdvertisingApi\Model\State $state state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->container['state'] = $state;

        return $this;
    }

    /**
     * Gets creation_date
     *
     * @return float
     */
    public function getCreationDate()
    {
        return $this->container['creation_date'];
    }

    /**
     * Sets creation_date
     *
     * @param float $creation_date The creation date of the ad group in epoch time.
     *
     * @return $this
     */
    public function setCreationDate($creation_date)
    {
        $this->container['creation_date'] = $creation_date;

        return $this;
    }

    /**
     * Gets last_updated_date
     *
     * @return float
     */
    public function getLastUpdatedDate()
    {
        return $this->container['last_updated_date'];
    }

    /**
     * Sets last_updated_date
     *
     * @param float $last_updated_date The date that any value associated with the ad group was last changed, in epoch time.
     *
     * @return $this
     */
    public function setLastUpdatedDate($last_updated_date)
    {
        $this->container['last_updated_date'] = $last_updated_date;

        return $this;
    }

    /**
     * Gets serving_status
     *
     * @return string
     */
    public function getServingStatus()
    {
        return $this->container['serving_status'];
    }

    /**
     * Sets serving_status
     *
     * @param string $serving_status The computed status. See developer notes for more information.
     *
     * @return $this
     */
    public function setServingStatus($serving_status)
    {
        $allowedValues = $this->getServingStatusAllowableValues();
        if (!is_null($serving_status) && !in_array($serving_status, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'serving_status', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['serving_status'] = $serving_status;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
