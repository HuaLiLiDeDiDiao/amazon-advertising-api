<?php
/**
 * TaxBreakup
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Billing
 *
 * Get invoice data by invoice ID
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * TaxBreakup Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class TaxBreakup implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'taxBreakup';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'payer_jurisdiction' => 'string',
        'tax_rate' => 'float',
        'issuer_tax_information' => '\AmazonAdvertisingApi\Model\IssuerTaxRegistrationInfo',
        'third_party_tax_information' => '\AmazonAdvertisingApi\Model\ThirdPartyTaxRegistrationInfo',
        'issuer_jurisdiction' => 'string',
        'payer_tax_information' => '\AmazonAdvertisingApi\Model\PayerTaxRegistrationInfo',
        'tax_amount' => '\AmazonAdvertisingApi\Model\CurrencyAmount',
        'tax_name' => 'string',
        'taxed_jurisdiction_name' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'payer_jurisdiction' => null,
        'tax_rate' => null,
        'issuer_tax_information' => null,
        'third_party_tax_information' => null,
        'issuer_jurisdiction' => null,
        'payer_tax_information' => null,
        'tax_amount' => null,
        'tax_name' => null,
        'taxed_jurisdiction_name' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'payer_jurisdiction' => 'payerJurisdiction',
        'tax_rate' => 'taxRate',
        'issuer_tax_information' => 'issuerTaxInformation',
        'third_party_tax_information' => 'thirdPartyTaxInformation',
        'issuer_jurisdiction' => 'issuerJurisdiction',
        'payer_tax_information' => 'payerTaxInformation',
        'tax_amount' => 'taxAmount',
        'tax_name' => 'taxName',
        'taxed_jurisdiction_name' => 'taxedJurisdictionName'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'payer_jurisdiction' => 'setPayerJurisdiction',
        'tax_rate' => 'setTaxRate',
        'issuer_tax_information' => 'setIssuerTaxInformation',
        'third_party_tax_information' => 'setThirdPartyTaxInformation',
        'issuer_jurisdiction' => 'setIssuerJurisdiction',
        'payer_tax_information' => 'setPayerTaxInformation',
        'tax_amount' => 'setTaxAmount',
        'tax_name' => 'setTaxName',
        'taxed_jurisdiction_name' => 'setTaxedJurisdictionName'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'payer_jurisdiction' => 'getPayerJurisdiction',
        'tax_rate' => 'getTaxRate',
        'issuer_tax_information' => 'getIssuerTaxInformation',
        'third_party_tax_information' => 'getThirdPartyTaxInformation',
        'issuer_jurisdiction' => 'getIssuerJurisdiction',
        'payer_tax_information' => 'getPayerTaxInformation',
        'tax_amount' => 'getTaxAmount',
        'tax_name' => 'getTaxName',
        'taxed_jurisdiction_name' => 'getTaxedJurisdictionName'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['payer_jurisdiction'] = isset($data['payer_jurisdiction']) ? $data['payer_jurisdiction'] : null;
        $this->container['tax_rate'] = isset($data['tax_rate']) ? $data['tax_rate'] : null;
        $this->container['issuer_tax_information'] = isset($data['issuer_tax_information']) ? $data['issuer_tax_information'] : null;
        $this->container['third_party_tax_information'] = isset($data['third_party_tax_information']) ? $data['third_party_tax_information'] : null;
        $this->container['issuer_jurisdiction'] = isset($data['issuer_jurisdiction']) ? $data['issuer_jurisdiction'] : null;
        $this->container['payer_tax_information'] = isset($data['payer_tax_information']) ? $data['payer_tax_information'] : null;
        $this->container['tax_amount'] = isset($data['tax_amount']) ? $data['tax_amount'] : null;
        $this->container['tax_name'] = isset($data['tax_name']) ? $data['tax_name'] : null;
        $this->container['taxed_jurisdiction_name'] = isset($data['taxed_jurisdiction_name']) ? $data['taxed_jurisdiction_name'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['tax_rate'] === null) {
            $invalidProperties[] = "'tax_rate' can't be null";
        }
        if ($this->container['issuer_tax_information'] === null) {
            $invalidProperties[] = "'issuer_tax_information' can't be null";
        }
        if ($this->container['issuer_jurisdiction'] === null) {
            $invalidProperties[] = "'issuer_jurisdiction' can't be null";
        }
        if ($this->container['payer_tax_information'] === null) {
            $invalidProperties[] = "'payer_tax_information' can't be null";
        }
        if ($this->container['tax_amount'] === null) {
            $invalidProperties[] = "'tax_amount' can't be null";
        }
        if ($this->container['tax_name'] === null) {
            $invalidProperties[] = "'tax_name' can't be null";
        }
        if ($this->container['taxed_jurisdiction_name'] === null) {
            $invalidProperties[] = "'taxed_jurisdiction_name' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets payer_jurisdiction
     *
     * @return string
     */
    public function getPayerJurisdiction()
    {
        return $this->container['payer_jurisdiction'];
    }

    /**
     * Sets payer_jurisdiction
     *
     * @param string $payer_jurisdiction Tax jurisdiction of payer (billed customer)
     *
     * @return $this
     */
    public function setPayerJurisdiction($payer_jurisdiction)
    {
        $this->container['payer_jurisdiction'] = $payer_jurisdiction;

        return $this;
    }

    /**
     * Gets tax_rate
     *
     * @return float
     */
    public function getTaxRate()
    {
        return $this->container['tax_rate'];
    }

    /**
     * Sets tax_rate
     *
     * @param float $tax_rate tax_rate
     *
     * @return $this
     */
    public function setTaxRate($tax_rate)
    {
        $this->container['tax_rate'] = $tax_rate;

        return $this;
    }

    /**
     * Gets issuer_tax_information
     *
     * @return \AmazonAdvertisingApi\Model\IssuerTaxRegistrationInfo
     */
    public function getIssuerTaxInformation()
    {
        return $this->container['issuer_tax_information'];
    }

    /**
     * Sets issuer_tax_information
     *
     * @param \AmazonAdvertisingApi\Model\IssuerTaxRegistrationInfo $issuer_tax_information issuer_tax_information
     *
     * @return $this
     */
    public function setIssuerTaxInformation($issuer_tax_information)
    {
        $this->container['issuer_tax_information'] = $issuer_tax_information;

        return $this;
    }

    /**
     * Gets third_party_tax_information
     *
     * @return \AmazonAdvertisingApi\Model\ThirdPartyTaxRegistrationInfo
     */
    public function getThirdPartyTaxInformation()
    {
        return $this->container['third_party_tax_information'];
    }

    /**
     * Sets third_party_tax_information
     *
     * @param \AmazonAdvertisingApi\Model\ThirdPartyTaxRegistrationInfo $third_party_tax_information third_party_tax_information
     *
     * @return $this
     */
    public function setThirdPartyTaxInformation($third_party_tax_information)
    {
        $this->container['third_party_tax_information'] = $third_party_tax_information;

        return $this;
    }

    /**
     * Gets issuer_jurisdiction
     *
     * @return string
     */
    public function getIssuerJurisdiction()
    {
        return $this->container['issuer_jurisdiction'];
    }

    /**
     * Sets issuer_jurisdiction
     *
     * @param string $issuer_jurisdiction Tax jurisdiction of issuer (Amazon billing entity)
     *
     * @return $this
     */
    public function setIssuerJurisdiction($issuer_jurisdiction)
    {
        $this->container['issuer_jurisdiction'] = $issuer_jurisdiction;

        return $this;
    }

    /**
     * Gets payer_tax_information
     *
     * @return \AmazonAdvertisingApi\Model\PayerTaxRegistrationInfo
     */
    public function getPayerTaxInformation()
    {
        return $this->container['payer_tax_information'];
    }

    /**
     * Sets payer_tax_information
     *
     * @param \AmazonAdvertisingApi\Model\PayerTaxRegistrationInfo $payer_tax_information payer_tax_information
     *
     * @return $this
     */
    public function setPayerTaxInformation($payer_tax_information)
    {
        $this->container['payer_tax_information'] = $payer_tax_information;

        return $this;
    }

    /**
     * Gets tax_amount
     *
     * @return \AmazonAdvertisingApi\Model\CurrencyAmount
     */
    public function getTaxAmount()
    {
        return $this->container['tax_amount'];
    }

    /**
     * Sets tax_amount
     *
     * @param \AmazonAdvertisingApi\Model\CurrencyAmount $tax_amount tax_amount
     *
     * @return $this
     */
    public function setTaxAmount($tax_amount)
    {
        $this->container['tax_amount'] = $tax_amount;

        return $this;
    }

    /**
     * Gets tax_name
     *
     * @return string
     */
    public function getTaxName()
    {
        return $this->container['tax_name'];
    }

    /**
     * Sets tax_name
     *
     * @param string $tax_name tax_name
     *
     * @return $this
     */
    public function setTaxName($tax_name)
    {
        $this->container['tax_name'] = $tax_name;

        return $this;
    }

    /**
     * Gets taxed_jurisdiction_name
     *
     * @return string
     */
    public function getTaxedJurisdictionName()
    {
        return $this->container['taxed_jurisdiction_name'];
    }

    /**
     * Sets taxed_jurisdiction_name
     *
     * @param string $taxed_jurisdiction_name Tax jurisdiction for which tax applies, this can be at the country, state or local level.
     *
     * @return $this
     */
    public function setTaxedJurisdictionName($taxed_jurisdiction_name)
    {
        $this->container['taxed_jurisdiction_name'] = $taxed_jurisdiction_name;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
