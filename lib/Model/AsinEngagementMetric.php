<?php
/**
 * AsinEngagementMetric
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Stores
 *
 * No description provided (generated by AmazonAdvertisingApi Codegenhttps://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api)
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * AsinEngagementMetric Class Doc Comment
 *
 * @category Class
 * @description Store Metric Types: Metrics aggregated at the store level. To be used with *dimension* omitted, otherwise a 422 response is returned.  * &#x60;TOTAL_VIEWS&#x60; - Total number of times customers viewed ASINs on the store’s pages. A view can happen once per store page visit.   * &#x60;TOTAL_CLICKS&#x60; - Total count of times a customer clicked an ASIN related widget on the store’s pages.   Asin Metric Types: Metrics aggregated at the ASIN level. To be used with a supported dimension type (see *dimension*), otherwise a 422 response is returned.   * &#x60;RENDERS&#x60; - Number of times the asin rendered on a store page, this does not guarentee the customer saw the asin.  * &#x60;VIEWS&#x60; - Number of times the a customer viewed an ASIN. Can happen once per page visit.   * &#x60;ORDERS&#x60; - Estimated total orders placed by Store visitors on the day of the ASIN view.   Orders can have one or more total units.  * &#x60;UNITS&#x60; - Estimated units purchased by Store visitors during attributed orders for the ASIN.  * &#x60;ADD_TO_CARTS&#x60; - Total number of times an asin was added to cart by a customer on a store page.    * &#x60;IN_STOCK_VIEWS&#x60; - Total views of an asin on a store page while the asin was in stock. For asins with variations, the customer must have selected a variation which as in stock to be counted.   * &#x60;AVERAGE_IN_STOCK_PRICE&#x60; - Average price in local currency the asin was viewed at by customers while it was in stock.    *  &#x60;IN_STOCK_RATE&#x60; - Rate at which customers viewed an asin while it was in stock.    *  &#x60;AVERAGE_SALE_PRICE&#x60; - Average price in local currency for which the asin sold for during the order.    *  &#x60;CONVERSION_RATE&#x60; - Rate at which customers ordered a unit of the item over how many times customers clicked the item.    *  &#x60;CLICKS&#x60; - Count of how many times a customer clicked an asin related widget on the store page.    *  &#x60;CLICK_RATE&#x60; - Rate at which the asin was clicker per view. This ratio can be above one if the widget is interacted with on a widget with engaging features. (Product Showcase, Variation Selection in Product Grid, or Interactive Image)
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class AsinEngagementMetric
{
    /**
     * Possible values of this enum
     */
    const RENDERS = 'RENDERS';
    const VIEWS = 'VIEWS';
    const IN_STOCK_VIEWS = 'IN_STOCK_VIEWS';
    const AVERAGE_IN_STOCK_PRICE = 'AVERAGE_IN_STOCK_PRICE';
    const IN_STOCK_RATE = 'IN_STOCK_RATE';
    const CLICKS = 'CLICKS';
    const CLICK_RATE = 'CLICK_RATE';
    const ADD_TO_CARTS = 'ADD_TO_CARTS';
    const ORDERS = 'ORDERS';
    const UNITS = 'UNITS';
    const AVERAGE_SALE_PRICE = 'AVERAGE_SALE_PRICE';
    const CONVERSION_RATE = 'CONVERSION_RATE';
    const TOTAL_VIEWS = 'TOTAL_VIEWS';
    const TOTAL_CLICKS = 'TOTAL_CLICKS';
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::RENDERS,
            self::VIEWS,
            self::IN_STOCK_VIEWS,
            self::AVERAGE_IN_STOCK_PRICE,
            self::IN_STOCK_RATE,
            self::CLICKS,
            self::CLICK_RATE,
            self::ADD_TO_CARTS,
            self::ORDERS,
            self::UNITS,
            self::AVERAGE_SALE_PRICE,
            self::CONVERSION_RATE,
            self::TOTAL_VIEWS,
            self::TOTAL_CLICKS,
        ];
    }
}
