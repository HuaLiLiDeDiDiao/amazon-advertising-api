<?php
/**
 * InitialBudgetRecommendationResponse
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Products
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * InitialBudgetRecommendationResponse Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class InitialBudgetRecommendationResponse implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'InitialBudgetRecommendationResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'special_events' => '\AmazonAdvertisingApi\Model\SpecialEvent[]',
        'daily_budget' => 'float',
        'recommendation_id' => 'string',
        'benchmark' => '\AmazonAdvertisingApi\Model\Benchmark'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'special_events' => null,
        'daily_budget' => null,
        'recommendation_id' => null,
        'benchmark' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'special_events' => 'specialEvents',
        'daily_budget' => 'dailyBudget',
        'recommendation_id' => 'recommendationId',
        'benchmark' => 'benchmark'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'special_events' => 'setSpecialEvents',
        'daily_budget' => 'setDailyBudget',
        'recommendation_id' => 'setRecommendationId',
        'benchmark' => 'setBenchmark'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'special_events' => 'getSpecialEvents',
        'daily_budget' => 'getDailyBudget',
        'recommendation_id' => 'getRecommendationId',
        'benchmark' => 'getBenchmark'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['special_events'] = isset($data['special_events']) ? $data['special_events'] : null;
        $this->container['daily_budget'] = isset($data['daily_budget']) ? $data['daily_budget'] : null;
        $this->container['recommendation_id'] = isset($data['recommendation_id']) ? $data['recommendation_id'] : null;
        $this->container['benchmark'] = isset($data['benchmark']) ? $data['benchmark'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['special_events'] === null) {
            $invalidProperties[] = "'special_events' can't be null";
        }
        if ($this->container['daily_budget'] === null) {
            $invalidProperties[] = "'daily_budget' can't be null";
        }
        if ($this->container['benchmark'] === null) {
            $invalidProperties[] = "'benchmark' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets special_events
     *
     * @return \AmazonAdvertisingApi\Model\SpecialEvent[]
     */
    public function getSpecialEvents()
    {
        return $this->container['special_events'];
    }

    /**
     * Sets special_events
     *
     * @param \AmazonAdvertisingApi\Model\SpecialEvent[] $special_events A list of special events around the start and end date of the campaign.
     *
     * @return $this
     */
    public function setSpecialEvents($special_events)
    {
        $this->container['special_events'] = $special_events;

        return $this;
    }

    /**
     * Gets daily_budget
     *
     * @return float
     */
    public function getDailyBudget()
    {
        return $this->container['daily_budget'];
    }

    /**
     * Sets daily_budget
     *
     * @param float $daily_budget Recommended daily budget for the new campaign. Note: value -1 means we don’t have enough information to provide a recommendation.
     *
     * @return $this
     */
    public function setDailyBudget($daily_budget)
    {
        $this->container['daily_budget'] = $daily_budget;

        return $this;
    }

    /**
     * Gets recommendation_id
     *
     * @return string
     */
    public function getRecommendationId()
    {
        return $this->container['recommendation_id'];
    }

    /**
     * Sets recommendation_id
     *
     * @param string $recommendation_id Unique identifier for each recommendation.
     *
     * @return $this
     */
    public function setRecommendationId($recommendation_id)
    {
        $this->container['recommendation_id'] = $recommendation_id;

        return $this;
    }

    /**
     * Gets benchmark
     *
     * @return \AmazonAdvertisingApi\Model\Benchmark
     */
    public function getBenchmark()
    {
        return $this->container['benchmark'];
    }

    /**
     * Sets benchmark
     *
     * @param \AmazonAdvertisingApi\Model\Benchmark $benchmark benchmark
     *
     * @return $this
     */
    public function setBenchmark($benchmark)
    {
        $this->container['benchmark'] = $benchmark;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
