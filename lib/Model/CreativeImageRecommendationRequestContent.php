<?php
/**
 * CreativeImageRecommendationRequestContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * CreativeImageRecommendationRequestContent Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class CreativeImageRecommendationRequestContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'CreativeImageRecommendationRequestContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'asins' => 'string[]',
        'asset_sub_type' => '\AmazonAdvertisingApi\Model\AssetSubType',
        'max_num_recommendations' => 'float',
        'asset_programs' => '\AmazonAdvertisingApi\Model\ProgramType[]',
        'locale' => 'string',
        'headline' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'asins' => null,
        'asset_sub_type' => null,
        'max_num_recommendations' => null,
        'asset_programs' => null,
        'locale' => null,
        'headline' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'asins' => 'asins',
        'asset_sub_type' => 'assetSubType',
        'max_num_recommendations' => 'maxNumRecommendations',
        'asset_programs' => 'assetPrograms',
        'locale' => 'locale',
        'headline' => 'headline'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'asins' => 'setAsins',
        'asset_sub_type' => 'setAssetSubType',
        'max_num_recommendations' => 'setMaxNumRecommendations',
        'asset_programs' => 'setAssetPrograms',
        'locale' => 'setLocale',
        'headline' => 'setHeadline'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'asins' => 'getAsins',
        'asset_sub_type' => 'getAssetSubType',
        'max_num_recommendations' => 'getMaxNumRecommendations',
        'asset_programs' => 'getAssetPrograms',
        'locale' => 'getLocale',
        'headline' => 'getHeadline'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['asins'] = isset($data['asins']) ? $data['asins'] : null;
        $this->container['asset_sub_type'] = isset($data['asset_sub_type']) ? $data['asset_sub_type'] : null;
        $this->container['max_num_recommendations'] = isset($data['max_num_recommendations']) ? $data['max_num_recommendations'] : null;
        $this->container['asset_programs'] = isset($data['asset_programs']) ? $data['asset_programs'] : null;
        $this->container['locale'] = isset($data['locale']) ? $data['locale'] : null;
        $this->container['headline'] = isset($data['headline']) ? $data['headline'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['asins'] === null) {
            $invalidProperties[] = "'asins' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets asins
     *
     * @return string[]
     */
    public function getAsins()
    {
        return $this->container['asins'];
    }

    /**
     * Sets asins
     *
     * @param string[] $asins ----------------------------------------------- List types ----------------------------------------------- A list of ASINs
     *
     * @return $this
     */
    public function setAsins($asins)
    {
        $this->container['asins'] = $asins;

        return $this;
    }

    /**
     * Gets asset_sub_type
     *
     * @return \AmazonAdvertisingApi\Model\AssetSubType
     */
    public function getAssetSubType()
    {
        return $this->container['asset_sub_type'];
    }

    /**
     * Sets asset_sub_type
     *
     * @param \AmazonAdvertisingApi\Model\AssetSubType $asset_sub_type asset_sub_type
     *
     * @return $this
     */
    public function setAssetSubType($asset_sub_type)
    {
        $this->container['asset_sub_type'] = $asset_sub_type;

        return $this;
    }

    /**
     * Gets max_num_recommendations
     *
     * @return float
     */
    public function getMaxNumRecommendations()
    {
        return $this->container['max_num_recommendations'];
    }

    /**
     * Sets max_num_recommendations
     *
     * @param float $max_num_recommendations Maximum number of recommendations that API should return. Response will [0, recommendations] recommendations (recommendations are not guaranteed).
     *
     * @return $this
     */
    public function setMaxNumRecommendations($max_num_recommendations)
    {
        $this->container['max_num_recommendations'] = $max_num_recommendations;

        return $this;
    }

    /**
     * Gets asset_programs
     *
     * @return \AmazonAdvertisingApi\Model\ProgramType[]
     */
    public function getAssetPrograms()
    {
        return $this->container['asset_programs'];
    }

    /**
     * Sets asset_programs
     *
     * @param \AmazonAdvertisingApi\Model\ProgramType[] $asset_programs Filter assets by program types. For example, if only [A_PLUS] assets are requested then only assets that were used as A+ content will be recommended. If no program type is provided, recommend assets from all programs
     *
     * @return $this
     */
    public function setAssetPrograms($asset_programs)
    {
        $this->container['asset_programs'] = $asset_programs;

        return $this;
    }

    /**
     * Gets locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->container['locale'];
    }

    /**
     * Sets locale
     *
     * @param string $locale (Optional) locale of creative headline and ASIN titles. If locale is not provided, default locale of marketplace is used. Currently, only en_US and en_CA are supported.
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->container['locale'] = $locale;

        return $this;
    }

    /**
     * Gets headline
     *
     * @return string
     */
    public function getHeadline()
    {
        return $this->container['headline'];
    }

    /**
     * Sets headline
     *
     * @param string $headline The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. See [the policy](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#headlines) for headline requirements.
     *
     * @return $this
     */
    public function setHeadline($headline)
    {
        $this->container['headline'] = $headline;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
