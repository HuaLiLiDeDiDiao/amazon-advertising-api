<?php
/**
 * ListCreativesRequestContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Sponsored Brands campaign management
 *
 * Create and manage Sponsored Brands campaigns.   To learn more about Sponsored Brands campaigns, see:   - [Sponsored Brands overview](guides/sponsored-brands/overview)  - [Sponsored Brands campaign structure](guides/sponsored-brands/campaigns/structure)  - [Get started with Sponsored Brands campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns)
 *
 * OpenAPI spec version: 4.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Model;

use \ArrayAccess;
use \AmazonAdvertisingApi\ObjectSerializer;

/**
 * ListCreativesRequestContent Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class ListCreativesRequestContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $AmazonAdvertisingApiModelName = 'ListCreativesRequestContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiTypes = [
        'creative_type_filter' => '\AmazonAdvertisingApi\Model\CreativeType[]',
        'ad_id' => 'string',
        'next_token' => 'string',
        'max_results' => 'float',
        'creative_version_filter' => 'string[]',
        'creative_status_filter' => '\AmazonAdvertisingApi\Model\CreativeStatus[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $AmazonAdvertisingApiFormats = [
        'creative_type_filter' => null,
        'ad_id' => null,
        'next_token' => null,
        'max_results' => null,
        'creative_version_filter' => null,
        'creative_status_filter' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiTypes()
    {
        return self::$AmazonAdvertisingApiTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function AmazonAdvertisingApiFormats()
    {
        return self::$AmazonAdvertisingApiFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'creative_type_filter' => 'creativeTypeFilter',
        'ad_id' => 'adId',
        'next_token' => 'nextToken',
        'max_results' => 'maxResults',
        'creative_version_filter' => 'creativeVersionFilter',
        'creative_status_filter' => 'creativeStatusFilter'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'creative_type_filter' => 'setCreativeTypeFilter',
        'ad_id' => 'setAdId',
        'next_token' => 'setNextToken',
        'max_results' => 'setMaxResults',
        'creative_version_filter' => 'setCreativeVersionFilter',
        'creative_status_filter' => 'setCreativeStatusFilter'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'creative_type_filter' => 'getCreativeTypeFilter',
        'ad_id' => 'getAdId',
        'next_token' => 'getNextToken',
        'max_results' => 'getMaxResults',
        'creative_version_filter' => 'getCreativeVersionFilter',
        'creative_status_filter' => 'getCreativeStatusFilter'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$AmazonAdvertisingApiModelName;
    }



    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['creative_type_filter'] = isset($data['creative_type_filter']) ? $data['creative_type_filter'] : null;
        $this->container['ad_id'] = isset($data['ad_id']) ? $data['ad_id'] : null;
        $this->container['next_token'] = isset($data['next_token']) ? $data['next_token'] : null;
        $this->container['max_results'] = isset($data['max_results']) ? $data['max_results'] : null;
        $this->container['creative_version_filter'] = isset($data['creative_version_filter']) ? $data['creative_version_filter'] : null;
        $this->container['creative_status_filter'] = isset($data['creative_status_filter']) ? $data['creative_status_filter'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['ad_id'] === null) {
            $invalidProperties[] = "'ad_id' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets creative_type_filter
     *
     * @return \AmazonAdvertisingApi\Model\CreativeType[]
     */
    public function getCreativeTypeFilter()
    {
        return $this->container['creative_type_filter'];
    }

    /**
     * Sets creative_type_filter
     *
     * @param \AmazonAdvertisingApi\Model\CreativeType[] $creative_type_filter Filters creatives by optional creative type. By default, you can list all creative versions regardless of creative type.
     *
     * @return $this
     */
    public function setCreativeTypeFilter($creative_type_filter)
    {
        $this->container['creative_type_filter'] = $creative_type_filter;

        return $this;
    }

    /**
     * Gets ad_id
     *
     * @return string
     */
    public function getAdId()
    {
        return $this->container['ad_id'];
    }

    /**
     * Sets ad_id
     *
     * @param string $ad_id The unique ID of a Sponsored Brands ad.
     *
     * @return $this
     */
    public function setAdId($ad_id)
    {
        $this->container['ad_id'] = $ad_id;

        return $this;
    }

    /**
     * Gets next_token
     *
     * @return string
     */
    public function getNextToken()
    {
        return $this->container['next_token'];
    }

    /**
     * Sets next_token
     *
     * @param string $next_token Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.
     *
     * @return $this
     */
    public function setNextToken($next_token)
    {
        $this->container['next_token'] = $next_token;

        return $this;
    }

    /**
     * Gets max_results
     *
     * @return float
     */
    public function getMaxResults()
    {
        return $this->container['max_results'];
    }

    /**
     * Sets max_results
     *
     * @param float $max_results Set a limit on the number of results returned by an operation.
     *
     * @return $this
     */
    public function setMaxResults($max_results)
    {
        $this->container['max_results'] = $max_results;

        return $this;
    }

    /**
     * Gets creative_version_filter
     *
     * @return string[]
     */
    public function getCreativeVersionFilter()
    {
        return $this->container['creative_version_filter'];
    }

    /**
     * Sets creative_version_filter
     *
     * @param string[] $creative_version_filter Filters creatives by optional creative version. This means you can either list all creative versions without specific creative version filter, all just retrieve a single creative version by providing a specific version identifier.
     *
     * @return $this
     */
    public function setCreativeVersionFilter($creative_version_filter)
    {
        $this->container['creative_version_filter'] = $creative_version_filter;

        return $this;
    }

    /**
     * Gets creative_status_filter
     *
     * @return \AmazonAdvertisingApi\Model\CreativeStatus[]
     */
    public function getCreativeStatusFilter()
    {
        return $this->container['creative_status_filter'];
    }

    /**
     * Sets creative_status_filter
     *
     * @param \AmazonAdvertisingApi\Model\CreativeStatus[] $creative_status_filter Filters creatives by optional creative status. By default, you can list all creative versions regardless of creative status.
     *
     * @return $this
     */
    public function setCreativeStatusFilter($creative_status_filter)
    {
        $this->container['creative_status_filter'] = $creative_status_filter;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
