<?php

namespace AmazonAdvertisingApi;

use InvalidArgumentException;

/***************************/
/** Region/endpoint pairs **/
/***************************/

class Endpoint
{
    // North America
    public const NA = 'https://advertising-api.amazon.com';

    // Europe
    public const EU = 'https://advertising-api-eu.amazon.com';

    // Far East
    public const FE = 'https://advertising-api-fe.amazon.com';

    /**
     * Returns the endpoint for the marketplace with the given ID.
     *
     * @param string $marketplace_id The identifier for the marketplace. (required)
     *
     * @throws InvalidArgumentException
     * @return array of the endpoint details
     *
     *
     * @link https://docs.developer.amazonservices.com/en_US/dev_guide/DG_Endpoints.html
     */
    public static function getByMarketplaceId(string $marketplace_id)
    {
        $map = [
            // North America.
            // Brazil.
            'A2Q3Y263D00KWC' => 'NA',
            // Canada
            'A2EUQ1WTGCTBG2' => 'NA',
            // Mexico.
            'A1AM78C64UM0Y8' => 'NA',
            // US.
            'ATVPDKIKX0DER' => 'NA',
            // Europe.
            // United Arab Emirates (U.A.E.).
            'A2VIGQ35RCS4UG' => 'EU',
            // Belgium.
            'AMEN7PMS3EDWL' => 'EU',
            // Germany.
            'A1PA6795UKMFR9' => 'EU',
            // Egypt.
            'ARBP9OOSHTCHU' => 'EU',
            // Spain.
            'A1RKKUPIHCS9HS' => 'EU',
            // France.
            'A13V1IB3VIYZZH' => 'EU',
            // UK.
            'A1F83G8C2ARO7P' => 'EU',
            // India.
            'A21TJRUUN4KGV' => 'EU',
            // Italy.
            'APJ6JRA9NG5V4' => 'EU',
            // Netherlands.
            'A1805IZSGTT6HS' => 'EU',
            // Poland.
            'A1C3SOZRARQ6R3' => 'EU',
            // Saudi Arabia.
            'A17E79C6D8DWNP' => 'EU',
            // Sweden.
            'A2NODRKZP88ZB9' => 'EU',
            // Turkey.
            'A33AVAJ2PDY3EV' => 'EU',
            // Far East.
            // Singapore.
            'A19VAU5U5O7RUS' => 'FE',
            // Australia.
            'A39IBJ37TRP1C6' => 'FE',
            // Japan.
            'A1VC38T7YXB528' => 'FE',
        ];
        if (!isset($map[$marketplace_id])) {
            throw new InvalidArgumentException(sprintf(
                'Unknown marketplace ID "%s".',
                $marketplace_id
            ));
        }
        $region = $map[$marketplace_id];
        return constant("\AmazonAdvertisingApi\Endpoint::$region");
    }
}
