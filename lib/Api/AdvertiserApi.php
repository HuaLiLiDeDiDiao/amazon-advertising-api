<?php
/**
 * AdvertiserApi
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Advertisers
 *
 * No description provided (generated by AmazonAdvertisingApi Codegenhttps://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api)
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use AmazonAdvertisingApi\ApiException;
use AmazonAdvertisingApi\Configuration;
use AmazonAdvertisingApi\HeaderSelector;
use AmazonAdvertisingApi\ObjectSerializer;

/**
 * AdvertiserApi Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class AdvertiserApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation getAccountBudgetFeatureFlags
     *
     * Gets account budget feature flags information.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse
     */
    public function getAccountBudgetFeatureFlags($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        list($response) = $this->getAccountBudgetFeatureFlagsWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope);
        return $response;
    }

    /**
     * Operation getAccountBudgetFeatureFlagsWithHttpInfo
     *
     * Gets account budget feature flags information.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function getAccountBudgetFeatureFlagsWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        $returnType = '\AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse';
        $request = $this->getAccountBudgetFeatureFlagsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 429:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getAccountBudgetFeatureFlagsAsync
     *
     * Gets account budget feature flags information.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getAccountBudgetFeatureFlagsAsync($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        return $this->getAccountBudgetFeatureFlagsAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getAccountBudgetFeatureFlagsAsyncWithHttpInfo
     *
     * Gets account budget feature flags information.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getAccountBudgetFeatureFlagsAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        $returnType = '\AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse';
        $request = $this->getAccountBudgetFeatureFlagsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getAccountBudgetFeatureFlags'
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function getAccountBudgetFeatureFlagsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        // verify the required parameter 'amazon_advertising_api_client_id' is set
        if ($amazon_advertising_api_client_id === null || (is_array($amazon_advertising_api_client_id) && count($amazon_advertising_api_client_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_client_id when calling getAccountBudgetFeatureFlags'
            );
        }
        // verify the required parameter 'amazon_advertising_api_scope' is set
        if ($amazon_advertising_api_scope === null || (is_array($amazon_advertising_api_scope) && count($amazon_advertising_api_scope) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_scope when calling getAccountBudgetFeatureFlags'
            );
        }

        $resourcePath = '/accountBudgets/featureFlags';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($amazon_advertising_api_client_id !== null) {
            $headerParams['Amazon-Advertising-API-ClientId'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_client_id);
        }
        // header params
        if ($amazon_advertising_api_scope !== null) {
            $headerParams['Amazon-Advertising-API-Scope'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_scope);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/vnd.accountBudgetFeatureFlags.v1+json', 'application/vnd.accountBudgetFeatureFlagsError.v1+json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/vnd.accountBudgetFeatureFlags.v1+json', 'application/vnd.accountBudgetFeatureFlagsError.v1+json'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation updateAccountBudgetFeatureFlags
     *
     * Creates or Updates account budget feature flags information.
     *
     * @param  \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest $body body (required)
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse
     */
    public function updateAccountBudgetFeatureFlags($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        list($response) = $this->updateAccountBudgetFeatureFlagsWithHttpInfo($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
        return $response;
    }

    /**
     * Operation updateAccountBudgetFeatureFlagsWithHttpInfo
     *
     * Creates or Updates account budget feature flags information.
     *
     * @param  \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest $body (required)
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function updateAccountBudgetFeatureFlagsWithHttpInfo($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        $returnType = '\AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse';
        $request = $this->updateAccountBudgetFeatureFlagsRequest($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 401:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 403:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 429:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\AccountBudgetFeatureFlagsError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation updateAccountBudgetFeatureFlagsAsync
     *
     * Creates or Updates account budget feature flags information.
     *
     * @param  \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest $body (required)
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateAccountBudgetFeatureFlagsAsync($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        return $this->updateAccountBudgetFeatureFlagsAsyncWithHttpInfo($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation updateAccountBudgetFeatureFlagsAsyncWithHttpInfo
     *
     * Creates or Updates account budget feature flags information.
     *
     * @param  \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest $body (required)
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function updateAccountBudgetFeatureFlagsAsyncWithHttpInfo($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        $returnType = '\AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse';
        $request = $this->updateAccountBudgetFeatureFlagsRequest($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'updateAccountBudgetFeatureFlags'
     *
     * @param  \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest $body (required)
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function updateAccountBudgetFeatureFlagsRequest($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)
    {
        // verify the required parameter 'body' is set
        if ($body === null || (is_array($body) && count($body) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $body when calling updateAccountBudgetFeatureFlags'
            );
        }
        // verify the required parameter 'amazon_advertising_api_client_id' is set
        if ($amazon_advertising_api_client_id === null || (is_array($amazon_advertising_api_client_id) && count($amazon_advertising_api_client_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_client_id when calling updateAccountBudgetFeatureFlags'
            );
        }
        // verify the required parameter 'amazon_advertising_api_scope' is set
        if ($amazon_advertising_api_scope === null || (is_array($amazon_advertising_api_scope) && count($amazon_advertising_api_scope) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_scope when calling updateAccountBudgetFeatureFlags'
            );
        }

        $resourcePath = '/accountBudgets/featureFlags';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($amazon_advertising_api_client_id !== null) {
            $headerParams['Amazon-Advertising-API-ClientId'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_client_id);
        }
        // header params
        if ($amazon_advertising_api_scope !== null) {
            $headerParams['Amazon-Advertising-API-Scope'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_scope);
        }


        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/vnd.accountBudgetFeatureFlags.v1+json', 'application/vnd.accountBudgetFeatureFlagsError.v1+json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/vnd.accountBudgetFeatureFlags.v1+json', 'application/vnd.accountBudgetFeatureFlagsError.v1+json'],
                ['application/vnd.accountBudgetFeatureFlags.v1+json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
