<?php
/**
 * StoresApi
 * PHP version 5
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */

/**
 * Amazon Ads API - Sponsored Brands
 *
 * Use the Amazon Ads API for Sponsored Brands for campaign, ad group, keyword, negative keyword, drafts, Stores, landing pages, and Brands management operations. For more information about Sponsored Brands, see the [Sponsored Brands Support Center](https://advertising.amazon.com/help#GQFZA83P55P747BZ). For onboarding information, see the [account setup](https://advertising.amazon.com/API/docs/v3/guides/account_setup) topic.
 *
 * OpenAPI spec version: 3.0
 * 
 * Generated by:https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api.git
 * AmazonAdvertisingApi Codegen version: 3.0.51
 */
/**
 * NOTE: This class is auto generated by the AmazonAdvertisingApi code generator program.
 *https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 * Do not edit the class manually.
 */

namespace AmazonAdvertisingApi\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use AmazonAdvertisingApi\ApiException;
use AmazonAdvertisingApi\Configuration;
use AmazonAdvertisingApi\HeaderSelector;
use AmazonAdvertisingApi\ObjectSerializer;

/**
 * StoresApi Class Doc Comment
 *
 * @category Class
 * @package  AmazonAdvertisingApi
 * @author   vv_guo
 * @link    https://gitee.com/HuaLiLiDeDiDiao/amazon-advertising-api
 */
class StoresApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation createAsset
     *
     * Creates a new image asset.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $content_disposition The name of the image file. (required)
     * @param  string $content_type The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| (required)
     * @param  string $asset_info asset_info (optional)
     * @param  string $asset asset (optional)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \AmazonAdvertisingApi\Model\InlineResponse2007
     */
    public function createAsset($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info = null, $asset = null)
    {
        list($response) = $this->createAssetWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset);
        return $response;
    }

    /**
     * Operation createAssetWithHttpInfo
     *
     * Creates a new image asset.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $content_disposition The name of the image file. (required)
     * @param  string $content_type The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| (required)
     * @param  string $asset_info (optional)
     * @param  string $asset (optional)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \AmazonAdvertisingApi\Model\InlineResponse2007, HTTP status code, HTTP response headers (array of strings)
     */
    public function createAssetWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info = null, $asset = null)
    {
        $returnType = '\AmazonAdvertisingApi\Model\InlineResponse2007';
        $request = $this->createAssetRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\InlineResponse2007',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation createAssetAsync
     *
     * Creates a new image asset.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $content_disposition The name of the image file. (required)
     * @param  string $content_type The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| (required)
     * @param  string $asset_info (optional)
     * @param  string $asset (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function createAssetAsync($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info = null, $asset = null)
    {
        return $this->createAssetAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation createAssetAsyncWithHttpInfo
     *
     * Creates a new image asset.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $content_disposition The name of the image file. (required)
     * @param  string $content_type The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| (required)
     * @param  string $asset_info (optional)
     * @param  string $asset (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function createAssetAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info = null, $asset = null)
    {
        $returnType = '\AmazonAdvertisingApi\Model\InlineResponse2007';
        $request = $this->createAssetRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'createAsset'
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $content_disposition The name of the image file. (required)
     * @param  string $content_type The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| (required)
     * @param  string $asset_info (optional)
     * @param  string $asset (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function createAssetRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info = null, $asset = null)
    {
        // verify the required parameter 'amazon_advertising_api_client_id' is set
        if ($amazon_advertising_api_client_id === null || (is_array($amazon_advertising_api_client_id) && count($amazon_advertising_api_client_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_client_id when calling createAsset'
            );
        }
        // verify the required parameter 'amazon_advertising_api_scope' is set
        if ($amazon_advertising_api_scope === null || (is_array($amazon_advertising_api_scope) && count($amazon_advertising_api_scope) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_scope when calling createAsset'
            );
        }
        // verify the required parameter 'content_disposition' is set
        if ($content_disposition === null || (is_array($content_disposition) && count($content_disposition) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $content_disposition when calling createAsset'
            );
        }
        // verify the required parameter 'content_type' is set
        if ($content_type === null || (is_array($content_type) && count($content_type) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $content_type when calling createAsset'
            );
        }

        $resourcePath = '/stores/assets';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // header params
        if ($amazon_advertising_api_client_id !== null) {
            $headerParams['Amazon-Advertising-API-ClientId'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_client_id);
        }
        // header params
        if ($amazon_advertising_api_scope !== null) {
            $headerParams['Amazon-Advertising-API-Scope'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_scope);
        }
        // header params
        if ($content_disposition !== null) {
            $headerParams['Content-Disposition'] = ObjectSerializer::toHeaderValue($content_disposition);
        }
        // header params
        if ($content_type !== null) {
            $headerParams['Content-Type'] = ObjectSerializer::toHeaderValue($content_type);
        }


        // form params
        if ($asset_info !== null) {
            $formParams['assetInfo'] = ObjectSerializer::toFormValue($asset_info);
        }
        // form params
        if ($asset !== null) {
            $multipart = true;
            $formParams['asset'] = \GuzzleHttp\Psr7\try_fopen(ObjectSerializer::toFormValue($asset), 'rb');
        }
        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['multipart/form-data']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }

            // // this endpoint requires Bearer token
            if ($this->config->getAccessToken() !== null) {
            $headers['Authorization'] = 'Bearer ' . $this->config->getAccessToken();
            }

        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Operation listAssets
     *
     * Gets a list of assets associated with a specified brand entity identifier.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $brand_entity_id For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. (optional)
     * @param  \AmazonAdvertisingApi\Model\MediaType $media_type Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. (optional)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \AmazonAdvertisingApi\Model\InlineResponse2006[]
     */
    public function listAssets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id = null, $media_type = null)
    {
        list($response) = $this->listAssetsWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type);
        return $response;
    }

    /**
     * Operation listAssetsWithHttpInfo
     *
     * Gets a list of assets associated with a specified brand entity identifier.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $brand_entity_id For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. (optional)
     * @param  \AmazonAdvertisingApi\Model\MediaType $media_type Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. (optional)
     *
     * @throws \AmazonAdvertisingApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \AmazonAdvertisingApi\Model\InlineResponse2006[], HTTP status code, HTTP response headers (array of strings)
     */
    public function listAssetsWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id = null, $media_type = null)
    {
        $returnType = '\AmazonAdvertisingApi\Model\InlineResponse2006[]';
        $request = $this->listAssetsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string','integer','bool'])) {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\AmazonAdvertisingApi\Model\InlineResponse2006[]',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation listAssetsAsync
     *
     * Gets a list of assets associated with a specified brand entity identifier.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $brand_entity_id For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. (optional)
     * @param  \AmazonAdvertisingApi\Model\MediaType $media_type Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function listAssetsAsync($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id = null, $media_type = null)
    {
        return $this->listAssetsAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation listAssetsAsyncWithHttpInfo
     *
     * Gets a list of assets associated with a specified brand entity identifier.
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $brand_entity_id For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. (optional)
     * @param  \AmazonAdvertisingApi\Model\MediaType $media_type Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function listAssetsAsyncWithHttpInfo($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id = null, $media_type = null)
    {
        $returnType = '\AmazonAdvertisingApi\Model\InlineResponse2006[]';
        $request = $this->listAssetsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'listAssets'
     *
     * @param  string $amazon_advertising_api_client_id The identifier of a client associated with a **Login with Amazon** account. (required)
     * @param  string $amazon_advertising_api_scope The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. (required)
     * @param  string $brand_entity_id For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. (optional)
     * @param  \AmazonAdvertisingApi\Model\MediaType $media_type Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. (optional)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function listAssetsRequest($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id = null, $media_type = null)
    {
        // verify the required parameter 'amazon_advertising_api_client_id' is set
        if ($amazon_advertising_api_client_id === null || (is_array($amazon_advertising_api_client_id) && count($amazon_advertising_api_client_id) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_client_id when calling listAssets'
            );
        }
        // verify the required parameter 'amazon_advertising_api_scope' is set
        if ($amazon_advertising_api_scope === null || (is_array($amazon_advertising_api_scope) && count($amazon_advertising_api_scope) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $amazon_advertising_api_scope when calling listAssets'
            );
        }

        $resourcePath = '/stores/assets';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($brand_entity_id !== null) {
            $queryParams['brandEntityId'] = ObjectSerializer::toQueryValue($brand_entity_id, null);
        }
        // query params
        if ($media_type !== null) {
            $queryParams['mediaType'] = ObjectSerializer::toQueryValue($media_type, null);
        }
        // header params
        if ($amazon_advertising_api_client_id !== null) {
            $headerParams['Amazon-Advertising-API-ClientId'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_client_id);
        }
        // header params
        if ($amazon_advertising_api_scope !== null) {
            $headerParams['Amazon-Advertising-API-Scope'] = ObjectSerializer::toHeaderValue($amazon_advertising_api_scope);
        }


        // body params
        $_tempBody = null;

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/vnd.mediaasset.v3+json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/vnd.mediaasset.v3+json'],
                []
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }

            // // this endpoint requires Bearer token
            if ($this->config->getAccessToken() !== null) {
            $headers['Authorization'] = 'Bearer ' . $this->config->getAccessToken();
            }

        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
