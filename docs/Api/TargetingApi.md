# AmazonAdvertisingApi\TargetingApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveTargetingClause**](TargetingApi.md#archivetargetingclause) | **DELETE** /sd/targets/{targetId} | Sets the &#x60;state&#x60; of a targeting clause to &#x60;archived&#x60;.
[**createTargetingClauses**](TargetingApi.md#createtargetingclauses) | **POST** /sd/targets | Creates one or more targeting clauses.
[**getTargets**](TargetingApi.md#gettargets) | **GET** /sd/targets/{targetId} | Gets a targeting clause specified by identifier.
[**getTargetsEx**](TargetingApi.md#gettargetsex) | **GET** /sd/targets/extended/{targetId} | Gets extended information for a targeting clause.
[**listTargetingClauses**](TargetingApi.md#listtargetingclauses) | **GET** /sd/targets | Gets a list of targeting clauses.
[**listTargetingClausesEx**](TargetingApi.md#listtargetingclausesex) | **GET** /sd/targets/extended | Gets a list of targeting clause objects with extended fields.
[**updateTargetingClauses**](TargetingApi.md#updatetargetingclauses) | **PUT** /sd/targets | Updates one or more targeting clauses.

# **archiveTargetingClause**
> \AmazonAdvertisingApi\Model\TargetResponse archiveTargetingClause($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id)

Sets the `state` of a targeting clause to `archived`.

Equivalent to using the `updateTargetingClauses` operation to set the `state` property of a targeting clause to `archived`. See [Developer Notes](http://advertising.amazon.com/API/docs/guides/developer_notes#Archiving) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$target_id = 789; // int | The identifer of a targeting clause.

try {
    $result = $apiInstance->archiveTargetingClause($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->archiveTargetingClause: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **target_id** | **int**| The identifer of a targeting clause. |

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTargetingClauses**
> \AmazonAdvertisingApi\Model\TargetResponse[] createTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more targeting clauses.

Successfully created targeting clauses are assigned a unique `targetId` value.  Create new targeting clauses for campaigns with tactic 'T00020' using the following: | Contextual targeting clause | Description | |------------------|-------------| | similarProduct | Dynamic segment to target products that are similar to the advertised asin. We recommend using 'similarProduct' targeting for all adGroups. | | asinSameAs=B0123456789 | Target this product. | | asinCategorySameAs=12345 | Target products in the category. | | asinCategorySameAs=12345 asinBrandSameAs=45678 | Target products in the category and brand. |  **Refinements:** - asinBrandSameAs - asinPriceBetween - asinPriceGreaterThan - asinPriceLessThan - asinReviewRatingLessThan - asinReviewRatingGreaterThan - asinReviewRatingBetween - asinIsPrimeShippingEligible - asinAgeRangeSameAs - asinGenreSameAs  **Refinement Notes:** * Brand, price, and review predicates are optional and may only be specified if category is also specified. * Review predicates accept numbers between 0 and 5 and are inclusive. * When using either of the 'between' strings to construct a targeting expression the format of the string is 'double-double' where the first double must be smaller than the second double. Prices are not inclusive. * 'similarProduct' has no expression value or refinements.  Create new targeting clauses for campaigns with tactic 'T00030' using the following: | Audience targeting clause | Description | |------------------|-------------| | views(exactProduct lookback=30) | Target an audience that has viewed the advertised asins in the past 7,14,30,60, or 90 days. Note: This target should only be used for productAds with SKU or ASIN. | | views(similarProduct lookback=60) | Target an audience that has viewed similar products to the advertised asins in the past 7,14,30,60, or 90 days. Note: This target should only be used for productAds with SKU or ASIN.| | views(asinCategorySameAs=12345 lookback=90) | Target an audience that has viewed products in the given category in the past 7,14,30,60, or 90 days. | | views(asinCategorySameAs=12345 asinBrandSameAs=45678 asinPriceBetween=50-100 lookback=60) | Target an audience that has viewed products in the given category, brand, and price range in the past 7,14,30,60, or 90 days. | | purchases(relatedProduct lookback=180) | Target an audience that has purchased a related product in the past 7,14,30,60,90,180 or 365 days. Note: This target should only be used for productAds with SKU or ASIN.| | purchases(exactProduct lookback=365) | Target an audience that has purchased the advertised asins in the past 7,14,30,60,90,180 or 365 days. Note: This target should only be used for productAds with SKU or ASIN.| | purchases(asinCategorySameAs=12345 asinBrandSameAs=45678 asinPriceBetween=50-100 lookback=90) | Target an audience that has purchased products in the given category, brand, and price range in the past 7,14,30,60,90,180 or 365 days |  Create new content targeting clauses with tactic 'T00020' or 'T00030' using the following, example: | Content targeting clause | Description | |------------------|-------------| | contentCategorySameAs=amzn1.iab-content.325 | Target all Movies and Television Shows in the Action or Adventure genre |  Notes on content targeting: * The `contentCategorySameAs` targeting predicate is required  Note: 1. There is a limit of 20 targeting clauses per request for T00030. 2. There is a limit of 1000 targeting clauses per request for T00020. 3. If you receive the error of \"Cannot create targeting clause: audience size is too small\", please expand or broaden your targeting clause to increase the audience size.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateTargetingClause()); // \AmazonAdvertisingApi\Model\CreateTargetingClause[] | A list of targeting clauses for creation.

try {
    $result = $apiInstance->createTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->createTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateTargetingClause[]**](../Model/CreateTargetingClause.md)| A list of targeting clauses for creation. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse[]**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargets**
> \AmazonAdvertisingApi\Model\TargetingClause getTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id)

Gets a targeting clause specified by identifier.

This call returns the minimal set of targeting clause fields.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$target_id = 789; // int | The identifier of a targeting clause.

try {
    $result = $apiInstance->getTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->getTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **target_id** | **int**| The identifier of a targeting clause. |

### Return type

[**\AmazonAdvertisingApi\Model\TargetingClause**](../Model/TargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetsEx**
> \AmazonAdvertisingApi\Model\TargetingClauseEx getTargetsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id)

Gets extended information for a targeting clause.

Gets a targeting clause object with extended fields. Note that this call returns the full set of targeting clause extended fields, but is less efficient than getTarget.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$target_id = 789; // int | The identifier of a targeting clause.

try {
    $result = $apiInstance->getTargetsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->getTargetsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **target_id** | **int**| The identifier of a targeting clause. |

### Return type

[**\AmazonAdvertisingApi\Model\TargetingClauseEx**](../Model/TargetingClauseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listTargetingClauses**
> \AmazonAdvertisingApi\Model\TargetingClause[] listTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of targeting clauses.

Gets a list of targeting clauses objects for a requested set of Sponsored Display targets. Note that the Targeting Clause object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use the target operations that return the TargetingClauseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. 0-indexed record offset for the result set. Defaults to 0.
$count = 56; // int | Optional. Number of records to include in the paged response. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. Restricts results to those with `state` set to values in the specified comma-separated list.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional list of comma separated adGroupIds. Restricts results to targeting clauses with the specified `adGroupId`.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. Restricts results to targeting clauses within campaigns specified in comma-separated list.

try {
    $result = $apiInstance->listTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->listTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. 0-indexed record offset for the result set. Defaults to 0. | [optional]
 **count** | **int**| Optional. Number of records to include in the paged response. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. Restricts results to those with &#x60;state&#x60; set to values in the specified comma-separated list. | [optional] [default to enabled, paused, archived]
 **ad_group_id_filter** | **string**| Optional list of comma separated adGroupIds. Restricts results to targeting clauses with the specified &#x60;adGroupId&#x60;. | [optional]
 **campaign_id_filter** | **string**| Optional. Restricts results to targeting clauses within campaigns specified in comma-separated list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetingClause[]**](../Model/TargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listTargetingClausesEx**
> \AmazonAdvertisingApi\Model\TargetingClauseEx[] listTargetingClausesEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $target_id_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of targeting clause objects with extended fields.

Gets an array of TargetingClauseEx objects for a set of requested targets. Note that this call returns the full set of targeting clause extended fields, but is less efficient than getTargets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. 0-indexed record offset for the result set. Defaults to 0.
$count = 56; // int | Optional. Number of records to include in the paged response. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. Restricts results to keywords with state within the specified comma-separated list. Must be one of: `enabled`, `paused`, or `archived`. Default behavior is to include enabled, paused, and archived.
$target_id_filter = "target_id_filter_example"; // string | Optional. Restricts results to ads with the specified `tagetId` specified in comma-separated list
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional list of comma separated adGroupIds. Restricts results to targeting clauses with the specified `adGroupId`.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. Restricts results to ads within campaigns specified in comma-separated list.

try {
    $result = $apiInstance->listTargetingClausesEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $target_id_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->listTargetingClausesEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. 0-indexed record offset for the result set. Defaults to 0. | [optional]
 **count** | **int**| Optional. Number of records to include in the paged response. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. Restricts results to keywords with state within the specified comma-separated list. Must be one of: &#x60;enabled&#x60;, &#x60;paused&#x60;, or &#x60;archived&#x60;. Default behavior is to include enabled, paused, and archived. | [optional] [default to enabled, paused, archived]
 **target_id_filter** | **string**| Optional. Restricts results to ads with the specified &#x60;tagetId&#x60; specified in comma-separated list | [optional]
 **ad_group_id_filter** | **string**| Optional list of comma separated adGroupIds. Restricts results to targeting clauses with the specified &#x60;adGroupId&#x60;. | [optional]
 **campaign_id_filter** | **string**| Optional. Restricts results to ads within campaigns specified in comma-separated list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetingClauseEx[]**](../Model/TargetingClauseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTargetingClauses**
> \AmazonAdvertisingApi\Model\TargetResponse[] updateTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more targeting clauses.

Updates one or more targeting clauses. Targeting clauses are identified using their targetId. The mutable fields are `bid` and `state`. Maximum length of the array is 100 objects.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateTargetingClause()); // \AmazonAdvertisingApi\Model\UpdateTargetingClause[] | A list of up to 1000 targeting clauses. Mutable fields:
* `state`
* `bid` (only mutable when the targeting clause's adGroup does not have any enabled optimization rule)

try {
    $result = $apiInstance->updateTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingApi->updateTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateTargetingClause[]**](../Model/UpdateTargetingClause.md)| A list of up to 1000 targeting clauses. Mutable fields:
* &#x60;state&#x60;
* &#x60;bid&#x60; (only mutable when the targeting clause&#x27;s adGroup does not have any enabled optimization rule) | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse[]**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

