# AmazonAdvertisingApi\ProductAdsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveProductAd**](ProductAdsApi.md#archiveproductad) | **DELETE** /sd/productAds/{adId} | Sets the status of a sproduct ad to archived.
[**createProductAds**](ProductAdsApi.md#createproductads) | **POST** /sd/productAds | Creates one or more product ads.
[**getProductAd**](ProductAdsApi.md#getproductad) | **GET** /sd/productAds/{adId} | Gets a requested product ad.
[**getProductAdResponseEx**](ProductAdsApi.md#getproductadresponseex) | **GET** /sd/productAds/extended/{adId} | Gets extended information for a product ad.
[**listProductAds**](ProductAdsApi.md#listproductads) | **GET** /sd/productAds | Gets a list of product ads.
[**listProductAdsEx**](ProductAdsApi.md#listproductadsex) | **GET** /sd/productAds/extended | Gets a list of product ads with extended fields.
[**updateProductAds**](ProductAdsApi.md#updateproductads) | **PUT** /sd/productAds | Updates one or more product ads.

# **archiveProductAd**
> \AmazonAdvertisingApi\Model\ProductAdResponse archiveProductAd($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id)

Sets the status of a sproduct ad to archived.

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/en-us/info/developer-notes#archiving) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_id = 789; // int | The identifier of the produce ad.

try {
    $result = $apiInstance->archiveProductAd($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->archiveProductAd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_id** | **int**| The identifier of the produce ad. |

### Return type

[**\AmazonAdvertisingApi\Model\ProductAdResponse**](../Model/ProductAdResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createProductAds**
> \AmazonAdvertisingApi\Model\ProductAdResponse[] createProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more product ads.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateProductAd()); // \AmazonAdvertisingApi\Model\CreateProductAd[] | An array of ProductAd objects. For each object, specify required fields and their values. Required fields are `adGroupId`, `SKU` (for sellers) or `ASIN` (for vendors), and `state`'. Maximum length of the array is 100 objects.

try {
    $result = $apiInstance->createProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->createProductAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateProductAd[]**](../Model/CreateProductAd.md)| An array of ProductAd objects. For each object, specify required fields and their values. Required fields are &#x60;adGroupId&#x60;, &#x60;SKU&#x60; (for sellers) or &#x60;ASIN&#x60; (for vendors), and &#x60;state&#x60;&#x27;. Maximum length of the array is 100 objects. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProductAdResponse[]**](../Model/ProductAdResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProductAd**
> \AmazonAdvertisingApi\Model\ProductAd getProductAd($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id)

Gets a requested product ad.

Note that the ProductAd object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use a product ad operations that returns the ProductAdResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_id = 789; // int | The identifier of the requested product ad.

try {
    $result = $apiInstance->getProductAd($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->getProductAd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_id** | **int**| The identifier of the requested product ad. |

### Return type

[**\AmazonAdvertisingApi\Model\ProductAd**](../Model/ProductAd.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProductAdResponseEx**
> \AmazonAdvertisingApi\Model\ProductAdResponseEx getProductAdResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id)

Gets extended information for a product ad.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_id = 789; // int | The identifier of the requested product ad.

try {
    $result = $apiInstance->getProductAdResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->getProductAdResponseEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_id** | **int**| The identifier of the requested product ad. |

### Return type

[**\AmazonAdvertisingApi\Model\ProductAdResponseEx**](../Model/ProductAdResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProductAds**
> \AmazonAdvertisingApi\Model\ProductAd[] listProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_id_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of product ads.

Gets an array of ProductAd objects for a requested set of Sponsored Display product ads. Note that the ProductAd object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use a product ad operation that returns the ProductAdResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of product ads. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of ProductAd objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten product ad set `startIndex=0` and `count=10`. To return the next ten product ads, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only products ads associated with campaigns that have state set to one of the values in the comma-delimited list.
$ad_id_filter = "ad_id_filter_example"; // string | Optional. The returned array includes only product ads with identifiers matching those in the comma-delimited string.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional. The returned array is filtered to include only products ads associated with ad groups identifiers in the comma-delimited list.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array is filtered to include only product ads associated with the campaign identifiers in the comma-delimited list.

try {
    $result = $apiInstance->listProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_id_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->listProductAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of product ads. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of ProductAd objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten product ad set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten product ads, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only products ads associated with campaigns that have state set to one of the values in the comma-delimited list. | [optional] [default to enabled, paused, archived]
 **ad_id_filter** | **string**| Optional. The returned array includes only product ads with identifiers matching those in the comma-delimited string. | [optional]
 **ad_group_id_filter** | **string**| Optional. The returned array is filtered to include only products ads associated with ad groups identifiers in the comma-delimited list. | [optional]
 **campaign_id_filter** | **string**| Optional. The returned array is filtered to include only product ads associated with the campaign identifiers in the comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProductAd[]**](../Model/ProductAd.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProductAdsEx**
> \AmazonAdvertisingApi\Model\ProductAdResponseEx[] listProductAdsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_id_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of product ads with extended fields.

Gets an array of ProductAdResponseEx objects for a set of requested ad groups. The ProductAdResponseEx object includes the extended set of available fields.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of product ads. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of ProduceAdEx objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten product ads set `startIndex=0` and `count=10`. To return the next ten campaigns, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list.
$ad_id_filter = "ad_id_filter_example"; // string | Optional. The returned array includes only product ads with identifiers matching those in the comma-delimited string.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional. The returned array is filtered to include only products ads associated with ad groups identifiers in the comma-delimited list.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array is filtered to include only product ads associated with the campaign identifiers in the comma-delimited list.

try {
    $result = $apiInstance->listProductAdsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_id_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->listProductAdsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of product ads. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of ProduceAdEx objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten product ads set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten campaigns, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list. | [optional] [default to enabled, paused, archived]
 **ad_id_filter** | **string**| Optional. The returned array includes only product ads with identifiers matching those in the comma-delimited string. | [optional]
 **ad_group_id_filter** | **string**| Optional. The returned array is filtered to include only products ads associated with ad groups identifiers in the comma-delimited list. | [optional]
 **campaign_id_filter** | **string**| Optional. The returned array is filtered to include only product ads associated with the campaign identifiers in the comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProductAdResponseEx[]**](../Model/ProductAdResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProductAds**
> \AmazonAdvertisingApi\Model\ProductAdResponse[] updateProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more product ads.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProductAdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateProductAd()); // \AmazonAdvertisingApi\Model\UpdateProductAd[] | An array of ProductAd objects. For each object, specify a product ad identifier and the only mutable field, `state`. Maximum length of the array is 100 objects.

try {
    $result = $apiInstance->updateProductAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductAdsApi->updateProductAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateProductAd[]**](../Model/UpdateProductAd.md)| An array of ProductAd objects. For each object, specify a product ad identifier and the only mutable field, &#x60;state&#x60;. Maximum length of the array is 100 objects. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProductAdResponse[]**](../Model/ProductAdResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

