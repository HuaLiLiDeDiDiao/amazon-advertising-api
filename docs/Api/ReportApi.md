# AmazonAdvertisingApi\ReportApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generateBrandMetricsReport**](ReportApi.md#generatebrandmetricsreport) | **POST** /insights/brandMetrics/report | Generate Brand Metrics Report. Each response record will include the following dimensional fields (in addition to the requested metrics) brand Namecategory, TreeNamecategory, HierarchylookbackPeriod, metricsComputationDate
[**getBrandMetricsReport**](ReportApi.md#getbrandmetricsreport) | **GET** /insights/brandMetrics/report/{reportId} | Retrieve the status and the URL of the Brand Metrics Report being generated

# **generateBrandMetricsReport**
> \AmazonAdvertisingApi\Model\BrandMetricsGenerateReportResponse generateBrandMetricsReport($amazon_advertising_api_scope, $amazon_advertising_api_client_id, $body)

Generate Brand Metrics Report. Each response record will include the following dimensional fields (in addition to the requested metrics) brand Namecategory, TreeNamecategory, HierarchylookbackPeriod, metricsComputationDate

Generates the Brand Metrics report in CSV or JSON format. Customize the report by passing a specific categoryTreeName, categoryPath, brandName, reportStartDate, reportEndDate, lookbackPeriod, format or a list of metrics from the available metrics in the metrics field. If an empty request body is passed, report for the latest available report date in JSON format will get generated with all the available brands and metrics for an advertiser. The report may or may not contain the Brand Metrics data for one or more brands depending on data availability.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The profile Id, for example, 195213312458027.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The client Id, for example, amzn1.application-oa2-client.8baa9caa3eac48eab89780e73ce03b19.
$body = new \AmazonAdvertisingApi\Model\BrandMetricsGenerateReportRequest(); // \AmazonAdvertisingApi\Model\BrandMetricsGenerateReportRequest | Create request body to generate the Brand Metrics Report

try {
    $result = $apiInstance->generateBrandMetricsReport($amazon_advertising_api_scope, $amazon_advertising_api_client_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->generateBrandMetricsReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_scope** | **string**| The profile Id, for example, 195213312458027. |
 **amazon_advertising_api_client_id** | **string**| The client Id, for example, amzn1.application-oa2-client.8baa9caa3eac48eab89780e73ce03b19. |
 **body** | [**\AmazonAdvertisingApi\Model\BrandMetricsGenerateReportRequest**](../Model/BrandMetricsGenerateReportRequest.md)| Create request body to generate the Brand Metrics Report | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\BrandMetricsGenerateReportResponse**](../Model/BrandMetricsGenerateReportResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.insightsBrandMetrics.v1+json
 - **Accept**: application/vnd.insightsBrandMetrics.v1+json, application/vnd.insightsBrandMetricsError.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBrandMetricsReport**
> \AmazonAdvertisingApi\Model\BrandMetricsGetReportByIdResponse getBrandMetricsReport($report_id, $amazon_advertising_api_scope, $amazon_advertising_api_client_id)

Retrieve the status and the URL of the Brand Metrics Report being generated

Fetch the location and status of the report for the brands for which the metrics are available. The URL to the report is only available when the status of the report is SUCCESSFUL  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$report_id = "report_id_example"; // string | The report Id to be fetched
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The profile Id, for example, 195213312458027.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The client Id, for example, amzn1.application-oa2-client.8baa9caa3eac48eab89780e73ce03b19.

try {
    $result = $apiInstance->getBrandMetricsReport($report_id, $amazon_advertising_api_scope, $amazon_advertising_api_client_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->getBrandMetricsReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **report_id** | **string**| The report Id to be fetched |
 **amazon_advertising_api_scope** | **string**| The profile Id, for example, 195213312458027. |
 **amazon_advertising_api_client_id** | **string**| The client Id, for example, amzn1.application-oa2-client.8baa9caa3eac48eab89780e73ce03b19. |

### Return type

[**\AmazonAdvertisingApi\Model\BrandMetricsGetReportByIdResponse**](../Model/BrandMetricsGetReportByIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.insightsBrandMetrics.v1+json, application/vnd.insightsBrandMetricsError.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

