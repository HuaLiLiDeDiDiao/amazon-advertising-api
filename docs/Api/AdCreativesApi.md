# AmazonAdvertisingApi\AdCreativesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBrandVideoCreative**](AdCreativesApi.md#createbrandvideocreative) | **POST** /sb/ads/creatives/brandVideo | Create new version of brand video ad creative
[**createProductCollectionCreative**](AdCreativesApi.md#createproductcollectioncreative) | **POST** /sb/ads/creatives/productCollection | Create new version of product collection ad creative
[**createStoreSpotlightCreative**](AdCreativesApi.md#createstorespotlightcreative) | **POST** /sb/ads/creatives/storeSpotlight | Create new version of store spotlight ad creative
[**createVideoCreative**](AdCreativesApi.md#createvideocreative) | **POST** /sb/ads/creatives/video | Create new version of video ad creative
[**listCreatives**](AdCreativesApi.md#listcreatives) | **POST** /sb/ads/creatives/list | List ad creatives

# **createBrandVideoCreative**
> \AmazonAdvertisingApi\Model\CreateBrandVideoCreativeResponseContent createBrandVideoCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept)

Create new version of brand video ad creative

This API creates a new version of an existing creative for given Sponsored Brands Ad by supplying brand video creative content  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdCreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateBrandVideoCreativeRequestContent(); // \AmazonAdvertisingApi\Model\CreateBrandVideoCreativeRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API.
$accept = new \AmazonAdvertisingApi\Model\AcceptHeader(); // \AmazonAdvertisingApi\Model\AcceptHeader | Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type.

try {
    $result = $apiInstance->createBrandVideoCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdCreativesApi->createBrandVideoCreative: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateBrandVideoCreativeRequestContent**](../Model/CreateBrandVideoCreativeRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API. |
 **accept** | [**\AmazonAdvertisingApi\Model\AcceptHeader**](../Model/.md)| Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreateBrandVideoCreativeResponseContent**](../Model/CreateBrandVideoCreativeResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbAdCreativeResource.v4+json
 - **Accept**: application/vnd.sbAdCreativeResource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createProductCollectionCreative**
> \AmazonAdvertisingApi\Model\CreateProductCollectionCreativeResponseContent createProductCollectionCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept)

Create new version of product collection ad creative

This API creates a new version of creative for given Sponsored Brands ad by supplying product collection creative content  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdCreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateProductCollectionCreativeRequestContent(); // \AmazonAdvertisingApi\Model\CreateProductCollectionCreativeRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API.
$accept = new \AmazonAdvertisingApi\Model\AcceptHeader(); // \AmazonAdvertisingApi\Model\AcceptHeader | Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type.

try {
    $result = $apiInstance->createProductCollectionCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdCreativesApi->createProductCollectionCreative: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateProductCollectionCreativeRequestContent**](../Model/CreateProductCollectionCreativeRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API. |
 **accept** | [**\AmazonAdvertisingApi\Model\AcceptHeader**](../Model/.md)| Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreateProductCollectionCreativeResponseContent**](../Model/CreateProductCollectionCreativeResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbAdCreativeResource.v4+json
 - **Accept**: application/vnd.sbAdCreativeResource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createStoreSpotlightCreative**
> \AmazonAdvertisingApi\Model\CreateStoreSpotlightCreativeResponseContent createStoreSpotlightCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept)

Create new version of store spotlight ad creative

This API creates a new version of creative for given Sponsored Brands ad by supplying store spotlight creative content  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdCreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateStoreSpotlightCreativeRequestContent(); // \AmazonAdvertisingApi\Model\CreateStoreSpotlightCreativeRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API.
$accept = new \AmazonAdvertisingApi\Model\AcceptHeader(); // \AmazonAdvertisingApi\Model\AcceptHeader | Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type.

try {
    $result = $apiInstance->createStoreSpotlightCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdCreativesApi->createStoreSpotlightCreative: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateStoreSpotlightCreativeRequestContent**](../Model/CreateStoreSpotlightCreativeRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API. |
 **accept** | [**\AmazonAdvertisingApi\Model\AcceptHeader**](../Model/.md)| Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreateStoreSpotlightCreativeResponseContent**](../Model/CreateStoreSpotlightCreativeResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbAdCreativeResource.v4+json
 - **Accept**: application/vnd.sbAdCreativeResource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVideoCreative**
> \AmazonAdvertisingApi\Model\CreateVideoCreativeResponseContent createVideoCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept)

Create new version of video ad creative

This API creates a new version of an existing creative for given Sponsored Brands ad by supplying video creative content  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdCreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateVideoCreativeRequestContent(); // \AmazonAdvertisingApi\Model\CreateVideoCreativeRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API.
$accept = new \AmazonAdvertisingApi\Model\AcceptHeader(); // \AmazonAdvertisingApi\Model\AcceptHeader | Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type.

try {
    $result = $apiInstance->createVideoCreative($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdCreativesApi->createVideoCreative: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateVideoCreativeRequestContent**](../Model/CreateVideoCreativeRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API. |
 **accept** | [**\AmazonAdvertisingApi\Model\AcceptHeader**](../Model/.md)| Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreateVideoCreativeResponseContent**](../Model/CreateVideoCreativeResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbAdCreativeResource.v4+json
 - **Accept**: application/vnd.sbAdCreativeResource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCreatives**
> \AmazonAdvertisingApi\Model\ListCreativesResponseContent listCreatives($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept)

List ad creatives

This API gets an array of all Sponsored Brands creatives that qualify the given resource identifiers and filters  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdCreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\ListCreativesRequestContent(); // \AmazonAdvertisingApi\Model\ListCreativesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API.
$accept = new \AmazonAdvertisingApi\Model\AcceptHeader(); // \AmazonAdvertisingApi\Model\AcceptHeader | Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type.

try {
    $result = $apiInstance->listCreatives($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $accept);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdCreativesApi->listCreatives: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\ListCreativesRequestContent**](../Model/ListCreativesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and use profileId from the response to pass as input. This is a required header for advertisers and integrators using the Advertising API. |
 **accept** | [**\AmazonAdvertisingApi\Model\AcceptHeader**](../Model/.md)| Clients request a specific version of a resource using the Accept request-header field set to the value field of the desired content-type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ListCreativesResponseContent**](../Model/ListCreativesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbAdCreativeResource.v4+json
 - **Accept**: application/vnd.sbAdCreativeResource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

