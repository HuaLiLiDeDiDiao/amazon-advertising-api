# AmazonAdvertisingApi\AsynchronousReportsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAsyncReport**](AsynchronousReportsApi.md#createasyncreport) | **POST** /reporting/reports | Creates a report request
[**deleteAsyncReport**](AsynchronousReportsApi.md#deleteasyncreport) | **DELETE** /reporting/reports/{reportId} | Deletes a report by id
[**getAsyncReport**](AsynchronousReportsApi.md#getasyncreport) | **GET** /reporting/reports/{reportId} | Gets a generation status of report by id

# **createAsyncReport**
> \AmazonAdvertisingApi\Model\AsyncReport createAsyncReport($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates a report request

Creates a report request. Use this operation to request the creation of a new report for Amazon Advertising Products. Use `adProduct` to specify the Advertising Product of the report.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AsynchronousReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The client identifier of the customer making the request.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\CreateAsyncReportRequest(); // \AmazonAdvertisingApi\Model\CreateAsyncReportRequest | 

try {
    $result = $apiInstance->createAsyncReport($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousReportsApi->createAsyncReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The client identifier of the customer making the request. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateAsyncReportRequest**](../Model/CreateAsyncReportRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AsyncReport**](../Model/AsyncReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.createasyncreportrequest.v3+json
 - **Accept**: application/vnd.createasyncreportresponse.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAsyncReport**
> \AmazonAdvertisingApi\Model\DeleteAsyncReportResponse deleteAsyncReport($report_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Deletes a report by id

Deletes a report by id. Use this operation to cancel a report in a `PENDING` status.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AsynchronousReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$report_id = "report_id_example"; // string | The identifier of the requested report.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->deleteAsyncReport($report_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousReportsApi->deleteAsyncReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **report_id** | **string**| The identifier of the requested report. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\DeleteAsyncReportResponse**](../Model/DeleteAsyncReportResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.deleteasyncreportresponse.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAsyncReport**
> \AmazonAdvertisingApi\Model\AsyncReport getAsyncReport($report_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Gets a generation status of report by id

Gets a generation status of a report by id. Uses the `reportId` value from the response of previously requested report via `POST /reporting/reports` operation. When `status` is set to `COMPLETED`, the report will be available to be downloaded at `url`.  Report generation can take as long as 3 hours. Repeated calls to check report status may generate a 429 response, indicating that your requests have been throttled. To retrieve reports programmatically, your application logic should institute a delay between requests. For more information, see [Retry logic with exponential backoff](concepts/rate-limiting#use-retry-logic-with-exponential-backoff).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AsynchronousReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$report_id = "report_id_example"; // string | The identifier of the requested report.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->getAsyncReport($report_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AsynchronousReportsApi->getAsyncReport: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **report_id** | **string**| The identifier of the requested report. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\AsyncReport**](../Model/AsyncReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.getasyncreportresponse.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

