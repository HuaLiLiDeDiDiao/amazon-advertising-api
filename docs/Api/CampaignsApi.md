# AmazonAdvertisingApi\CampaignsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveCampaign**](CampaignsApi.md#archivecampaign) | **DELETE** /sd/campaigns/{campaignId} | Sets the campaign status to archived.
[**createCampaigns**](CampaignsApi.md#createcampaigns) | **POST** /sd/campaigns | Creates one or more campaigns.
[**getCampaign**](CampaignsApi.md#getcampaign) | **GET** /sd/campaigns/{campaignId} | Gets a requested campaign.
[**getCampaignResponseEx**](CampaignsApi.md#getcampaignresponseex) | **GET** /sd/campaigns/extended/{campaignId} | Gets extended information for a requested campaign.
[**listCampaigns**](CampaignsApi.md#listcampaigns) | **GET** /sd/campaigns | Gets a list of campaigns.
[**listCampaignsEx**](CampaignsApi.md#listcampaignsex) | **GET** /sd/campaigns/extended | Gets a list of campaigns with extended fields.
[**updateCampaigns**](CampaignsApi.md#updatecampaigns) | **PUT** /sd/campaigns | Updates one or more campaigns.

# **archiveCampaign**
> \AmazonAdvertisingApi\Model\CampaignResponse archiveCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id)

Sets the campaign status to archived.

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/en-us/info/developer-notes#archiving) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$campaign_id = 789; // int | The identifier of the campaign.

try {
    $result = $apiInstance->archiveCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->archiveCampaign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **campaign_id** | **int**| The identifier of the campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\CampaignResponse**](../Model/CampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCampaigns**
> \AmazonAdvertisingApi\Model\CampaignResponse[] createCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more campaigns.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateCampaign()); // \AmazonAdvertisingApi\Model\CreateCampaign[] | An array of Campaign objects. For each object, specify required fields and their values. Required fields are `name`, `tactic`, `state`, and `startDate`. Maximum length of the array is 100 objects. If you don't specify a `budget`, it will be set as the [default budget for your region](https://advertising.amazon.com/API/docs/en-us/concepts/limits#default-budgets). Campaign names must be unique across SD, SB, and SP.
  If you are using Optimization rules, the following campaign budget must be at least:
  - 5x the value of any COST_PER_ORDER threshold.
  - 10x the value of any COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS threshold.
  - 20x the value of any COST_PER_CLICK threshold.


try {
    $result = $apiInstance->createCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->createCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateCampaign[]**](../Model/CreateCampaign.md)| An array of Campaign objects. For each object, specify required fields and their values. Required fields are &#x60;name&#x60;, &#x60;tactic&#x60;, &#x60;state&#x60;, and &#x60;startDate&#x60;. Maximum length of the array is 100 objects. If you don&#x27;t specify a &#x60;budget&#x60;, it will be set as the [default budget for your region](https://advertising.amazon.com/API/docs/en-us/concepts/limits#default-budgets). Campaign names must be unique across SD, SB, and SP.
  If you are using Optimization rules, the following campaign budget must be at least:
  - 5x the value of any COST_PER_ORDER threshold.
  - 10x the value of any COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS threshold.
  - 20x the value of any COST_PER_CLICK threshold.
 | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CampaignResponse[]**](../Model/CampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCampaign**
> \AmazonAdvertisingApi\Model\Campaign getCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id)

Gets a requested campaign.

Returns a Campaign object for a requested campaign. Note that the Campaign object is designed for performance, with a small set of commonly used campaign fields to reduce size. If the extended set of fields is required, use the campaign operations that return the CampaignResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$campaign_id = 789; // int | The identifier of the requested campaign.

try {
    $result = $apiInstance->getCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->getCampaign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **campaign_id** | **int**| The identifier of the requested campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\Campaign**](../Model/Campaign.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCampaignResponseEx**
> \AmazonAdvertisingApi\Model\CampaignResponseEx getCampaignResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id)

Gets extended information for a requested campaign.

Returns a CampaignResponseEx object for a requested campaign. The CampaignResponseEx includes the extended set of available fields.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$campaign_id = 789; // int | The identifier of the requested campaign.

try {
    $result = $apiInstance->getCampaignResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->getCampaignResponseEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **campaign_id** | **int**| The identifier of the requested campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\CampaignResponseEx**](../Model/CampaignResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCampaigns**
> \AmazonAdvertisingApi\Model\Campaign[] listCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $campaign_id_filter, $portfolio_id_filter)

Gets a list of campaigns.

Gets an array of Campaign objects for a requested set of Sponsored Display campaigns. Note that the Campaign object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use the campaign operations that return the CampaignResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 0; // int | Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten campaigns set `startIndex=0` and `count=10`. To return the next ten campaigns, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list.
$name = "name_example"; // string | Optional. The returned array includes only campaign with the specified name using an exact string match.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array includes only campaigns with identifiers matching those specified in the comma-delimited string.
$portfolio_id_filter = "portfolio_id_filter_example"; // string | Optional. The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string.

try {
    $result = $apiInstance->listCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $campaign_id_filter, $portfolio_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->listCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional] [default to 0]
 **count** | **int**| Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten campaigns set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten campaigns, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list. | [optional] [default to enabled, paused, archived]
 **name** | **string**| Optional. The returned array includes only campaign with the specified name using an exact string match. | [optional]
 **campaign_id_filter** | **string**| Optional. The returned array includes only campaigns with identifiers matching those specified in the comma-delimited string. | [optional]
 **portfolio_id_filter** | **string**| Optional. The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Campaign[]**](../Model/Campaign.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCampaignsEx**
> \AmazonAdvertisingApi\Model\CampaignResponseEx[] listCampaignsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $campaign_id_filter, $portfolio_id_filter)

Gets a list of campaigns with extended fields.

Gets an array of CampaignResponseEx objects for a set of requested campaigns.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten campaigns set `startIndex=0` and `count=10`. To return the next ten campaigns, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list.
$name = "name_example"; // string | Optional. The returned array includes only campaign with the specified name using an exact string match.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array includes only campaigns with identifiers matching those specified in the comma-delimited string.
$portfolio_id_filter = "portfolio_id_filter_example"; // string | Optional. The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string.

try {
    $result = $apiInstance->listCampaignsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $campaign_id_filter, $portfolio_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->listCampaignsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten campaigns set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten campaigns, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only campaigns with state set to one of the values in the specified comma-delimited list. | [optional] [default to enabled, paused, archived]
 **name** | **string**| Optional. The returned array includes only campaign with the specified name using an exact string match. | [optional]
 **campaign_id_filter** | **string**| Optional. The returned array includes only campaigns with identifiers matching those specified in the comma-delimited string. | [optional]
 **portfolio_id_filter** | **string**| Optional. The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CampaignResponseEx[]**](../Model/CampaignResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCampaigns**
> \AmazonAdvertisingApi\Model\CampaignResponse[] updateCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more campaigns.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateCampaign()); // \AmazonAdvertisingApi\Model\UpdateCampaign[] | An array of Campaign objects. For each object, specify a campaign identifier and mutable fields with their updated values. The mutable fields are `name`, `state`, `budget`, `startDate`, and `endDate`. Maximum length of the array is 100 objects.

try {
    $result = $apiInstance->updateCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignsApi->updateCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateCampaign[]**](../Model/UpdateCampaign.md)| An array of Campaign objects. For each object, specify a campaign identifier and mutable fields with their updated values. The mutable fields are &#x60;name&#x60;, &#x60;state&#x60;, &#x60;budget&#x60;, &#x60;startDate&#x60;, and &#x60;endDate&#x60;. Maximum length of the array is 100 objects. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CampaignResponse[]**](../Model/CampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

