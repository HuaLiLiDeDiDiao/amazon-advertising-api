# AmazonAdvertisingApi\OptimizationRulesBetaApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**associateOptimizationRulesWithAdGroup**](OptimizationRulesBetaApi.md#associateoptimizationruleswithadgroup) | **POST** /sd/adGroups/{adGroupId}/optimizationRules | Associate one or more optimization rules to an ad group specified by identifier.
[**createOptimizationRules**](OptimizationRulesBetaApi.md#createoptimizationrules) | **POST** /sd/optimizationRules | Creates one or more optimization rules, also known as outcome optimizations.
[**listOptimizationRules**](OptimizationRulesBetaApi.md#listoptimizationrules) | **GET** /sd/optimizationRules | Gets a list of optimization rules.
[**sdAdGroupsAdGroupIdOptimizationRulesGet**](OptimizationRulesBetaApi.md#sdadgroupsadgroupidoptimizationrulesget) | **GET** /sd/adGroups/{adGroupId}/optimizationRules | Gets a list of optimization rules associated to an adgroup specified by identifier.
[**sdOptimizationRulesOptimizationRuleIdGet**](OptimizationRulesBetaApi.md#sdoptimizationrulesoptimizationruleidget) | **GET** /sd/optimizationRules/{optimizationRuleId} | Gets a requested optimization rule.
[**updateOptimizationRules**](OptimizationRulesBetaApi.md#updateoptimizationrules) | **PUT** /sd/optimizationRules | Updates one or more optimization rules.

# **associateOptimizationRulesWithAdGroup**
> \AmazonAdvertisingApi\Model\OptimizationRuleResponse[] associateOptimizationRulesWithAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $body)

Associate one or more optimization rules to an ad group specified by identifier.

* When an optimization rule is associated to an ad group, manual bids for individual targets will be overridden. * Only one optimization rule can be associated per adGroup. This note will be removed when multiple rules are supported per adGroup.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 789; // int | The identifier of the ad group.
$body = new \AmazonAdvertisingApi\Model\CreateAssociatedOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\CreateAssociatedOptimizationRulesRequest | A list of optimization rule identifiers. Only one optimization rule identifier is currently supported per request. This note will be removed when multiple rule identifiers are supported.

For each ad group, only one optimization rule metric name is supported, based on the ad group's `bidOptimization` type. Refer to the following table for the metric names supported for each type.
|  AdGroup.bidOptimization |     Supported OptimizationRule.metricName       |
|------------------|--------------------|
|   reach       | COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS  |
|   clicks      | COST_PER_CLICK          |
|  conversions  | COST_PER_ORDER          |

try {
    $result = $apiInstance->associateOptimizationRulesWithAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->associateOptimizationRulesWithAdGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **int**| The identifier of the ad group. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateAssociatedOptimizationRulesRequest**](../Model/CreateAssociatedOptimizationRulesRequest.md)| A list of optimization rule identifiers. Only one optimization rule identifier is currently supported per request. This note will be removed when multiple rule identifiers are supported.

For each ad group, only one optimization rule metric name is supported, based on the ad group&#x27;s &#x60;bidOptimization&#x60; type. Refer to the following table for the metric names supported for each type.
|  AdGroup.bidOptimization |     Supported OptimizationRule.metricName       |
|------------------|--------------------|
|   reach       | COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS  |
|   clicks      | COST_PER_CLICK          |
|  conversions  | COST_PER_ORDER          | | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRuleResponse[]**](../Model/OptimizationRuleResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRuleResponse[] createOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more optimization rules, also known as outcome optimizations.

* When an optimization rule is associated to an ad group, manual bids for individual targets will be overridden. * Optimization rules can only be associated to ad groups that have productAds with ASIN or SKU. * If you are using optimization rules, the following campaign budget must be at least:   - 5x the value of any COST_PER_ORDER threshold.   - 10x the value of any COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS threshold.   - 20x the value of any COST_PER_CLICK threshold.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateOptimizationRule()); // \AmazonAdvertisingApi\Model\CreateOptimizationRule[] | An array of OptimizationRule objects. For each object, specify required fields and their values. Required fields are `state` and `ruleConditions`.

try {
    $result = $apiInstance->createOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->createOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateOptimizationRule[]**](../Model/CreateOptimizationRule.md)| An array of OptimizationRule objects. For each object, specify required fields and their values. Required fields are &#x60;state&#x60; and &#x60;ruleConditions&#x60;. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRuleResponse[]**](../Model/OptimizationRuleResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRule[] listOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $optimization_rule_id_filter, $ad_group_id_filter)

Gets a list of optimization rules.

Gets an array of OptimizationRule objects for a requested set of Sponsored Display optimization rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of optimization rules. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of OptimizationRule objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten optimization rules set `startIndex=0` and `count=10`. To return the next ten optimization rules, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled"; // string | Optional. The returned array is filtered to include only optimization rules with state set to one of the values in the specified comma-delimited list. Available values:   - enabled   - paused [COMING LATER]   - enabled, paused [COMING LATER]
$name = "name_example"; // string | Optional. The returned array includes only optimization rules with the specified name using an exact string match.
$optimization_rule_id_filter = "optimization_rule_id_filter_example"; // string | Optional. The returned array is filtered to include only optimization rules associated with the optimization rule identifiers in the specified comma-delimited list.  Maximum size limit 50.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional. The returned array is filtered to include only optimization rules associated with the ad group identifiers in the comma-delimited list.  Maximum size limit 50.

try {
    $result = $apiInstance->listOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $name, $optimization_rule_id_filter, $ad_group_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->listOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of optimization rules. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of OptimizationRule objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten optimization rules set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten optimization rules, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only optimization rules with state set to one of the values in the specified comma-delimited list. Available values:   - enabled   - paused [COMING LATER]   - enabled, paused [COMING LATER] | [optional] [default to enabled]
 **name** | **string**| Optional. The returned array includes only optimization rules with the specified name using an exact string match. | [optional]
 **optimization_rule_id_filter** | **string**| Optional. The returned array is filtered to include only optimization rules associated with the optimization rule identifiers in the specified comma-delimited list.  Maximum size limit 50. | [optional]
 **ad_group_id_filter** | **string**| Optional. The returned array is filtered to include only optimization rules associated with the ad group identifiers in the comma-delimited list.  Maximum size limit 50. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRule[]**](../Model/OptimizationRule.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sdAdGroupsAdGroupIdOptimizationRulesGet**
> \AmazonAdvertisingApi\Model\OptimizationRule[] sdAdGroupsAdGroupIdOptimizationRulesGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id)

Gets a list of optimization rules associated to an adgroup specified by identifier.

Gets an OptimizationRule object for a requested Sponsored Display optimization rule.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 789; // int | The identifier of the ad group.

try {
    $result = $apiInstance->sdAdGroupsAdGroupIdOptimizationRulesGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->sdAdGroupsAdGroupIdOptimizationRulesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **int**| The identifier of the ad group. |

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRule[]**](../Model/OptimizationRule.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sdOptimizationRulesOptimizationRuleIdGet**
> \AmazonAdvertisingApi\Model\OptimizationRule sdOptimizationRulesOptimizationRuleIdGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $optimization_rule_id)

Gets a requested optimization rule.

Gets an OptimizationRule object for a requested Sponsored Display optimization rule.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$optimization_rule_id = "optimization_rule_id_example"; // string | The identifier of the requested optimization rule.

try {
    $result = $apiInstance->sdOptimizationRulesOptimizationRuleIdGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $optimization_rule_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->sdOptimizationRulesOptimizationRuleIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **optimization_rule_id** | **string**| The identifier of the requested optimization rule. |

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRule**](../Model/OptimizationRule.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRuleResponse[] updateOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more optimization rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateOptimizationRule()); // \AmazonAdvertisingApi\Model\UpdateOptimizationRule[] | An array of OptimizationRule objects. For each object, specify an optimization rule identifier and mutable fields with their updated values. The mutable fields are `ruleName`, `state`, and `ruleConditions`.

try {
    $result = $apiInstance->updateOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesBetaApi->updateOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateOptimizationRule[]**](../Model/UpdateOptimizationRule.md)| An array of OptimizationRule objects. For each object, specify an optimization rule identifier and mutable fields with their updated values. The mutable fields are &#x60;ruleName&#x60;, &#x60;state&#x60;, and &#x60;ruleConditions&#x60;. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRuleResponse[]**](../Model/OptimizationRuleResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

