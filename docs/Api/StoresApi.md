# AmazonAdvertisingApi\StoresApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAsset**](StoresApi.md#createasset) | **POST** /stores/assets | Creates a new image asset.
[**listAssets**](StoresApi.md#listassets) | **GET** /stores/assets | Gets a list of assets associated with a specified brand entity identifier.

# **createAsset**
> \AmazonAdvertisingApi\Model\InlineResponse2007 createAsset($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset)

Creates a new image asset.

Image assets are stored in the Store Assets Library. Note that there may be a delay before the image is displayed in the console.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$content_disposition = "content_disposition_example"; // string | The name of the image file.
$content_type = "content_type_example"; // string | The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)|
$asset_info = "asset_info_example"; // string | 
$asset = "asset_example"; // string | 

try {
    $result = $apiInstance->createAsset($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $content_disposition, $content_type, $asset_info, $asset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->createAsset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **content_disposition** | **string**| The name of the image file. |
 **content_type** | **string**| The image format type. The following table lists the valid image types: |Image Type|Description| |----------|-----------| |PNG|[Portable network graphics](https://en.wikipedia.org/wiki/Portable_Network_Graphics)| |JPEG|[JPEG](https://en.wikipedia.org/wiki/JPEG)| |GIF|[Graphics interchange format](https://en.wikipedia.org/wiki/GIF)| |
 **asset_info** | **string**|  | [optional]
 **asset** | **string****string**|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listAssets**
> \AmazonAdvertisingApi\Model\InlineResponse2006[] listAssets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type)

Gets a list of assets associated with a specified brand entity identifier.

For sellers or vendors, gets an array of assets associated with the specified brand entity identifier. Vendors are not required to specify a brand entity identifier, and in this case all assets associated with the vendor are returned.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$brand_entity_id = "brand_entity_id_example"; // string | For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ.
$media_type = new \AmazonAdvertisingApi\Model\MediaType(); // \AmazonAdvertisingApi\Model\MediaType | Specifies the media types used to filter the returned array. Currently, only the `brandLogo` type is supported. If not specified, all media types are returned.

try {
    $result = $apiInstance->listAssets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_entity_id, $media_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->listAssets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **brand_entity_id** | **string**| For sellers, this field is required. It is the Brand entity identifier of the Brand for which assets are returned. This identifier is retrieved using the [getBrands operation](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/brands). For vendors, this field is optional. If a vendor does not specify this field, all assets associated with the vendor are returned. For more information about the [difference between a seller and a vendor](https://advertising.amazon.com/resources/faq#advertising-basics), see the Amazon Ads FAQ. | [optional]
 **media_type** | [**\AmazonAdvertisingApi\Model\MediaType**](../Model/.md)| Specifies the media types used to filter the returned array. Currently, only the &#x60;brandLogo&#x60; type is supported. If not specified, all media types are returned. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2006[]**](../Model/InlineResponse2006.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.mediaasset.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

