# AmazonAdvertisingApi\NegativeProductTargetingApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveNegativeTarget**](NegativeProductTargetingApi.md#archivenegativetarget) | **DELETE** /sb/negativeTargets/{negativeTargetId} | Archives a negative target specified by identifier. Note that archiving is permanent, and once a negative target has been archived it can&#x27;t be made active again.
[**createNegativeTargets**](NegativeProductTargetingApi.md#createnegativetargets) | **POST** /sb/negativeTargets | Create one or more negative targets.
[**getNegativeTarget**](NegativeProductTargetingApi.md#getnegativetarget) | **GET** /sb/negativeTargets/{negativeTargetId} | Gets a negative target specified by identifier.
[**listNegativeTargets**](NegativeProductTargetingApi.md#listnegativetargets) | **POST** /sb/negativeTargets/list | Gets a list of product negative targets associated with the client identifier passed in the authorization header, filtered by specified criteria.
[**updateNegativeTargets**](NegativeProductTargetingApi.md#updatenegativetargets) | **PUT** /sb/negativeTargets | Updates one or more negative targets.

# **archiveNegativeTarget**
> \AmazonAdvertisingApi\Model\SBTargetingClauseResponse archiveNegativeTarget($negative_target_id)

Archives a negative target specified by identifier. Note that archiving is permanent, and once a negative target has been archived it can't be made active again.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$negative_target_id = new \AmazonAdvertisingApi\Model\SBNegativeTargetId(); // \AmazonAdvertisingApi\Model\SBNegativeTargetId | 

try {
    $result = $apiInstance->archiveNegativeTarget($negative_target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeProductTargetingApi->archiveNegativeTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **negative_target_id** | [**\AmazonAdvertisingApi\Model\SBNegativeTargetId**](../Model/.md)|  |

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingClauseResponse**](../Model/SBTargetingClauseResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbnegativetarget.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createNegativeTargets**
> \AmazonAdvertisingApi\Model\SBCreateTargetsResponse createNegativeTargets($body)

Create one or more negative targets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\SbNegativeTargetsBody1(); // \AmazonAdvertisingApi\Model\SbNegativeTargetsBody1 | A list of negative targeting clauses for creation. <br/>Note that negative targeting clauses can be created on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 negative targets.

try {
    $result = $apiInstance->createNegativeTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeProductTargetingApi->createNegativeTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SbNegativeTargetsBody1**](../Model/SbNegativeTargetsBody1.md)| A list of negative targeting clauses for creation. &lt;br/&gt;Note that negative targeting clauses can be created on campaigns where serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 negative targets. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBCreateTargetsResponse**](../Model/SBCreateTargetsResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbcreatenegativetargetsrequest.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNegativeTarget**
> \AmazonAdvertisingApi\Model\SBNegativeTargetingClause getNegativeTarget($negative_target_id)

Gets a negative target specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$negative_target_id = new \AmazonAdvertisingApi\Model\SBNegativeTargetId(); // \AmazonAdvertisingApi\Model\SBNegativeTargetId | The identifier of an existing negative target.

try {
    $result = $apiInstance->getNegativeTarget($negative_target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeProductTargetingApi->getNegativeTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **negative_target_id** | [**\AmazonAdvertisingApi\Model\SBNegativeTargetId**](../Model/.md)| The identifier of an existing negative target. |

### Return type

[**\AmazonAdvertisingApi\Model\SBNegativeTargetingClause**](../Model/SBNegativeTargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbnegativetarget.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listNegativeTargets**
> \AmazonAdvertisingApi\Model\InlineResponse20011 listNegativeTargets($body)

Gets a list of product negative targets associated with the client identifier passed in the authorization header, filtered by specified criteria.

**Note**: Negative targets associated with BrandVideo ad groups are only available in v3.2 version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\NegativeTargetsListBody(); // \AmazonAdvertisingApi\Model\NegativeTargetsListBody | A set of filters.

try {
    $result = $apiInstance->listNegativeTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeProductTargetingApi->listNegativeTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\NegativeTargetsListBody**](../Model/NegativeTargetsListBody.md)| A set of filters. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20011**](../Model/InlineResponse20011.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sblistnegativetargetsresponse.v3.2+json, application/vnd.sblistnegativetargetsresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateNegativeTargets**
> \AmazonAdvertisingApi\Model\InlineResponse20012 updateNegativeTargets($body)

Updates one or more negative targets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\SbNegativeTargetsBody(); // \AmazonAdvertisingApi\Model\SbNegativeTargetsBody | A list of negative targets with updated values. <br/>Note that negative targeting clauses can be created on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 negative targets.

try {
    $result = $apiInstance->updateNegativeTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeProductTargetingApi->updateNegativeTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SbNegativeTargetsBody**](../Model/SbNegativeTargetsBody.md)| A list of negative targets with updated values. &lt;br/&gt;Note that negative targeting clauses can be created on campaigns where serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 negative targets. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.updatenegativetargetsresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

