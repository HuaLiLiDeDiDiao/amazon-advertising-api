# AmazonAdvertisingApi\KeywordRecommendationsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getKeywordRecommendations**](KeywordRecommendationsApi.md#getkeywordrecommendations) | **POST** /sb/recommendations/keyword | Gets keyword recommendations

# **getKeywordRecommendations**
> \AmazonAdvertisingApi\Model\SBKeywordSuggestion[] getKeywordRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Gets keyword recommendations

Gets an array of keyword recommendation objects for a set of ASINs included either on a landing page or a Stores page. Vendors may also specify a custom landing page.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\RecommendationsKeywordBody(); // \AmazonAdvertisingApi\Model\RecommendationsKeywordBody | **Must contain exactly only one of** 

 1.) An array of ASINs for which keyword recommendations are generated. 

 2.) The URL of a Stores page. Vendors may also specify the URL of a custom landing page. The products on the landing page are used to generate keyword recommendations. 

 Optional parameters include the max number of suggestions and locale for keyword translations. Supported locales include: Simplified Chinese (locale: “zh_CN”) for US, UK and CA. English (locale:  “en_GB”) for DE, FR, IT and ES. If locale is invalid or unsupported, no translations will be returned.

try {
    $result = $apiInstance->getKeywordRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordRecommendationsApi->getKeywordRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\RecommendationsKeywordBody**](../Model/RecommendationsKeywordBody.md)| **Must contain exactly only one of** 

 1.) An array of ASINs for which keyword recommendations are generated. 

 2.) The URL of a Stores page. Vendors may also specify the URL of a custom landing page. The products on the landing page are used to generate keyword recommendations. 

 Optional parameters include the max number of suggestions and locale for keyword translations. Supported locales include: Simplified Chinese (locale: “zh_CN”) for US, UK and CA. English (locale:  “en_GB”) for DE, FR, IT and ES. If locale is invalid or unsupported, no translations will be returned. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordSuggestion[]**](../Model/SBKeywordSuggestion.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbkeywordrecommendation.v3+json
 - **Accept**: application/vnd.sbkeywordrecommendation.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

