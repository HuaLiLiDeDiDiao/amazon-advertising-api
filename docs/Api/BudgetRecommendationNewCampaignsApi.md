# AmazonAdvertisingApi\BudgetRecommendationNewCampaignsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBudgetRecommendation**](BudgetRecommendationNewCampaignsApi.md#getbudgetrecommendation) | **POST** /sp/campaigns/initialBudgetRecommendation | 

# **getBudgetRecommendation**
> \AmazonAdvertisingApi\Model\InitialBudgetRecommendationResponse getBudgetRecommendation($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



Creates daily budget recommendation along with benchmark metrics when creating a new campaign.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRecommendationNewCampaignsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$body = new \AmazonAdvertisingApi\Model\InitialBudgetRecommendationRequest(); // \AmazonAdvertisingApi\Model\InitialBudgetRecommendationRequest | 

try {
    $result = $apiInstance->getBudgetRecommendation($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRecommendationNewCampaignsApi->getBudgetRecommendation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **body** | [**\AmazonAdvertisingApi\Model\InitialBudgetRecommendationRequest**](../Model/InitialBudgetRecommendationRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InitialBudgetRecommendationResponse**](../Model/InitialBudgetRecommendationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spinitialbudgetrecommendation.v3.4+json
 - **Accept**: application/vnd.spinitialbudgetrecommendation.v3.4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

