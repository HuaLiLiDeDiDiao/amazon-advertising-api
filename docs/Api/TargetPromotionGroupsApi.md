# AmazonAdvertisingApi\TargetPromotionGroupsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTargetPromotionGroupTargets**](TargetPromotionGroupsApi.md#createtargetpromotiongrouptargets) | **POST** /sp/targetPromotionGroups/targets | 
[**createTargetPromotionGroups**](TargetPromotionGroupsApi.md#createtargetpromotiongroups) | **POST** /sp/targetPromotionGroups | 
[**getTargetPromotionGroupsRecommendations**](TargetPromotionGroupsApi.md#gettargetpromotiongroupsrecommendations) | **POST** /sp/targetPromotionGroups/recommendations | 
[**listTargetPromotionGroupTargets**](TargetPromotionGroupsApi.md#listtargetpromotiongrouptargets) | **POST** /sp/targetPromotionGroups/targets/list | 
[**listTargetPromotionGroups**](TargetPromotionGroupsApi.md#listtargetpromotiongroups) | **POST** /sp/targetPromotionGroups/list | 

# **createTargetPromotionGroupTargets**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsResponseContent createTargetPromotionGroupTargets($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



Creates keyword and/or product targets in the manual adGroup that are part of the target promotion group  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetPromotionGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a 'Login with Amazon' account. This is a required     header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->createTargetPromotionGroupTargets($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetPromotionGroupsApi->createTargetPromotionGroupTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsRequestContent**](../Model/SponsoredProductsCreateTargetPromotionGroupTargetsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x27;Login with Amazon&#x27; account. This is a required     header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsResponseContent**](../Model/SponsoredProductsCreateTargetPromotionGroupTargetsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetPromotionGroupTarget.v1+json
 - **Accept**: application/vnd.spTargetPromotionGroupTarget.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTargetPromotionGroups**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupsResponseContent createTargetPromotionGroups($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



Creates a target promotion group, by grouping the auto-targeting adGroupId and manual-targeting adGroups, divided by keyword targeting adGroups, and product targeting adGroups.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetPromotionGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a 'Login with Amazon' account. This is a required     header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->createTargetPromotionGroups($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetPromotionGroupsApi->createTargetPromotionGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupsRequestContent**](../Model/SponsoredProductsCreateTargetPromotionGroupsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x27;Login with Amazon&#x27; account. This is a required     header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupsResponseContent**](../Model/SponsoredProductsCreateTargetPromotionGroupsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetPromotionGroup.v1+json
 - **Accept**: application/vnd.spTargetPromotionGroup.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetPromotionGroupsRecommendations**
> \AmazonAdvertisingApi\Model\SponsoredProductsGetTargetPromotionGroupsRecommendationsResponseContent getTargetPromotionGroupsRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



Retrieves keyword and product targets of an auto-targeting campaign as recommendations for promoting to a manual-targeting campaign. The recommendations are based on performance heuristics of the targets.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetPromotionGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a 'Login with Amazon' account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsGetTargetPromotionGroupsRecommendationsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsGetTargetPromotionGroupsRecommendationsRequestContent | 

try {
    $result = $apiInstance->getTargetPromotionGroupsRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetPromotionGroupsApi->getTargetPromotionGroupsRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x27;Login with Amazon&#x27; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGetTargetPromotionGroupsRecommendationsRequestContent**](../Model/SponsoredProductsGetTargetPromotionGroupsRecommendationsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsGetTargetPromotionGroupsRecommendationsResponseContent**](../Model/SponsoredProductsGetTargetPromotionGroupsRecommendationsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetPromotionGroupsRecommendations.v1+json
 - **Accept**: application/vnd.spTargetPromotionGroupsRecommendations.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listTargetPromotionGroupTargets**
> \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupTargetsResponseContent listTargetPromotionGroupTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



Returns the targets created through target promotion groups for an advertiser and / or given target promotion group.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetPromotionGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a 'Login with Amazon' account. This is a required     header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupTargetsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupTargetsRequestContent | 

try {
    $result = $apiInstance->listTargetPromotionGroupTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetPromotionGroupsApi->listTargetPromotionGroupTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x27;Login with Amazon&#x27; account. This is a required     header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupTargetsRequestContent**](../Model/SponsoredProductsListTargetPromotionGroupTargetsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupTargetsResponseContent**](../Model/SponsoredProductsListTargetPromotionGroupTargetsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetPromotionGroupTarget.v1+json
 - **Accept**: application/vnd.spTargetPromotionGroupTarget.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listTargetPromotionGroups**
> \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupsResponseContent listTargetPromotionGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



Returns the target promotion groups for an advertiser and / or adGroupId, and / or target promotion group id.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetPromotionGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a 'Login with Amazon' account. This is a required     header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupsRequestContent | 

try {
    $result = $apiInstance->listTargetPromotionGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetPromotionGroupsApi->listTargetPromotionGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x27;Login with Amazon&#x27; account. This is a required     header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles     resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a     required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupsRequestContent**](../Model/SponsoredProductsListTargetPromotionGroupsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListTargetPromotionGroupsResponseContent**](../Model/SponsoredProductsListTargetPromotionGroupsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetPromotionGroup.v1+json
 - **Accept**: application/vnd.spTargetPromotionGroup.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

