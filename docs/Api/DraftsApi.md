# AmazonAdvertisingApi\DraftsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDraftCampaigns**](DraftsApi.md#createdraftcampaigns) | **POST** /sb/drafts/campaigns | Creates one or more new draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]
[**deleteDraftCampaign**](DraftsApi.md#deletedraftcampaign) | **DELETE** /sb/drafts/campaigns/{draftCampaignId} | Archives a draft campaign specified by identifier. [PLANNED SHUTOFF DATE 1/31/2024]
[**getDraftCampaign_**](DraftsApi.md#getdraftcampaign_) | **GET** /sb/drafts/campaigns/{draftCampaignId} | Gets a draft campaign specified by identifier. [PLANNED SHUTOFF DATE 1/31/2024]
[**listDraftCampaigns**](DraftsApi.md#listdraftcampaigns) | **GET** /sb/drafts/campaigns | Gets an array of draft campaign objects. [PLANNED SHUTOFF DATE 1/31/2024]
[**submitDraftCampaign**](DraftsApi.md#submitdraftcampaign) | **POST** /sb/drafts/campaigns/submit | Submits one or more existing draft campaigns to the moderation approval queue. [PLANNED SHUTOFF DATE 1/31/2024]
[**updateDraftCampaigns**](DraftsApi.md#updatedraftcampaigns) | **PUT** /sb/drafts/campaigns | Updates one or more draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

# **createDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more new draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

Creates sponsored brands draft campaigns. <br>**To create a video campaign specify adFormat as 'video'. If adFormat is not specified then a product collection draft is created.** <br>Note each draft campaign can have keywords, negative keywords, targets and negative targets with batch size of upto 100. <br>**Note** each draft campaign in this operation supports adding keywords or negative keywords with maximum list size of 100. Additional keywords or negative keywords can be added using [createKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Keywords) or [createNegativeKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative_Keywords). <br>**Note** each draft campaign in this operation supports adding targets or negative targets with maximum list size of 100. Additional targets or negative targets can be added using [createTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Product%20targeting) or [createNegativeTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative%20product%20targeting).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest()); // \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[] | An array of draft campaigns.

try {
    $result = $apiInstance->createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->createDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[]**](../Model/SBCreateDraftCampaignRequest.md)| An array of draft campaigns. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more new draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

Creates sponsored brands draft campaigns. <br>**To create a video campaign specify adFormat as 'video'. If adFormat is not specified then a product collection draft is created.** <br>Note each draft campaign can have keywords, negative keywords, targets and negative targets with batch size of upto 100. <br>**Note** each draft campaign in this operation supports adding keywords or negative keywords with maximum list size of 100. Additional keywords or negative keywords can be added using [createKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Keywords) or [createNegativeKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative_Keywords). <br>**Note** each draft campaign in this operation supports adding targets or negative targets with maximum list size of 100. Additional targets or negative targets can be added using [createTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Product%20targeting) or [createNegativeTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative%20product%20targeting).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest()); // \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[] | An array of draft campaigns.

try {
    $result = $apiInstance->createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->createDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[]**](../Model/SBCreateDraftCampaignRequest.md)| An array of draft campaigns. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more new draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

Creates sponsored brands draft campaigns. <br>**To create a video campaign specify adFormat as 'video'. If adFormat is not specified then a product collection draft is created.** <br>Note each draft campaign can have keywords, negative keywords, targets and negative targets with batch size of upto 100. <br>**Note** each draft campaign in this operation supports adding keywords or negative keywords with maximum list size of 100. Additional keywords or negative keywords can be added using [createKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Keywords) or [createNegativeKeywords](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative_Keywords). <br>**Note** each draft campaign in this operation supports adding targets or negative targets with maximum list size of 100. Additional targets or negative targets can be added using [createTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Product%20targeting) or [createNegativeTargets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Negative%20product%20targeting).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest()); // \AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[] | An array of draft campaigns.

try {
    $result = $apiInstance->createDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->createDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SBCreateDraftCampaignRequest[]**](../Model/SBCreateDraftCampaignRequest.md)| An array of draft campaigns. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.createdraftcampaignsresponse.v3.2+json, application/vnd.createdraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDraftCampaign**
> \AmazonAdvertisingApi\Model\SBDraftCampaignDeleteResponse deleteDraftCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $draft_campaign_id)

Archives a draft campaign specified by identifier. [PLANNED SHUTOFF DATE 1/31/2024]

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/v2/guides/developer_notes) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$draft_campaign_id = 789; // int | The identifier of an existing draft campaign.

try {
    $result = $apiInstance->deleteDraftCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $draft_campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->deleteDraftCampaign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **draft_campaign_id** | **int**| The identifier of an existing draft campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignDeleteResponse**](../Model/SBDraftCampaignDeleteResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.deletedraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDraftCampaign_**
> \AmazonAdvertisingApi\Model\SBDraftCampaign getDraftCampaign_($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $draft_campaign_id)

Gets a draft campaign specified by identifier. [PLANNED SHUTOFF DATE 1/31/2024]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$draft_campaign_id = 789; // int | The identifier of an existing draft campaign.

try {
    $result = $apiInstance->getDraftCampaign_($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $draft_campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->getDraftCampaign_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **draft_campaign_id** | **int**| The identifier of an existing draft campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaign**](../Model/SBDraftCampaign.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbdraftcampaign.v3+json, application/vnd.sbdraftcampaign.v3.2+json, application/vnd.sbdraftcampaign.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBGetDraftCampaignResponse[] listDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $name, $draft_campaign_id_filter, $portfolio_id_filter, $ad_format_filter)

Gets an array of draft campaign objects. [PLANNED SHUTOFF DATE 1/31/2024]

Gets an array of all draft campaigns associated with the client identifier passed in the authorization header, filtered by specified criteria. <br>**Returns both productCollection and video draft campaigns by default. Use adFormatFilter to filter drafts by ad formats.**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 0; // int | Sets a zero-based offset into the requested set of draft campaigns. Use in conjunction with the `count` parameter to control pagination of the returned array.
$count = 56; // int | Sets the number of draft campaigns in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten ad groups set `startIndex=0` and `count=10`. To return the next ten ad groups, set `startIndex=10` and `count=10`, and so on.
$name = "name_example"; // string | The returned array only includes draft campaigns with the specified name.
$draft_campaign_id_filter = "draft_campaign_id_filter_example"; // string | The returned array includes only draft campaigns with identifiers matching those specified in the comma-delimited string.
$portfolio_id_filter = "portfolio_id_filter_example"; // string | The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string.
$ad_format_filter = new \AmazonAdvertisingApi\Model\AdFormat(); // \AmazonAdvertisingApi\Model\AdFormat | The returned array includes only draft campaigns with ad format matching those specified in the comma-delimited adFormats. Returns all drafts if not specified.

try {
    $result = $apiInstance->listDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $name, $draft_campaign_id_filter, $portfolio_id_filter, $ad_format_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->listDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Sets a zero-based offset into the requested set of draft campaigns. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. | [optional] [default to 0]
 **count** | **int**| Sets the number of draft campaigns in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten ad groups set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten ad groups, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. | [optional]
 **name** | **string**| The returned array only includes draft campaigns with the specified name. | [optional]
 **draft_campaign_id_filter** | **string**| The returned array includes only draft campaigns with identifiers matching those specified in the comma-delimited string. | [optional]
 **portfolio_id_filter** | **string**| The returned array includes only campaigns associated with Portfolio identifiers matching those specified in the comma-delimited string. | [optional]
 **ad_format_filter** | [**\AmazonAdvertisingApi\Model\AdFormat**](../Model/.md)| The returned array includes only draft campaigns with ad format matching those specified in the comma-delimited adFormats. Returns all drafts if not specified. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBGetDraftCampaignResponse[]**](../Model/SBGetDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbdraftcampaign.v3+json, application/vnd.batchgetdraftcampaignsresponse.v3.2+json, application/vnd.batchgetdraftcampaignsresponse.v3.3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **submitDraftCampaign**
> \AmazonAdvertisingApi\Model\SBSubmitDraftCampaignResponse submitDraftCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Submits one or more existing draft campaigns to the moderation approval queue. [PLANNED SHUTOFF DATE 1/31/2024]

On successful submission, a campaign is created with an identifier that could be different from the original draft campaign identifier. The new identifier is returned in the response. Note that when a draft campaign is approved, the 'status' and 'servingStatus' fields are changed to values associated with an active campaign.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array({
  "value" : [ 123456789, 987654321 ]
}); // int[] | A comma-delimited list of draft campaign identifiers. Maximum length is 10 draft campaign identifiers.

try {
    $result = $apiInstance->submitDraftCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->submitDraftCampaign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**int[]**](../Model/int.md)| A comma-delimited list of draft campaign identifiers. Maximum length is 10 draft campaign identifiers. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBSubmitDraftCampaignResponse**](../Model/SBSubmitDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbcampaign.v3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.submitdraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\null[]()); // null[] | An array of draft campaign objects with updated values.

try {
    $result = $apiInstance->updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->updateDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**null[]**](../Model/.md)| An array of draft campaign objects with updated values. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\null[]()); // null[] | An array of draft campaign objects with updated values.

try {
    $result = $apiInstance->updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->updateDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**null[]**](../Model/.md)| An array of draft campaign objects with updated values. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDraftCampaigns**
> \AmazonAdvertisingApi\Model\SBDraftCampaignResponse[] updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more draft campaigns. [PLANNED SHUTOFF DATE 1/31/2024]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\DraftsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\null[]()); // null[] | An array of draft campaign objects with updated values.

try {
    $result = $apiInstance->updateDraftCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DraftsApi->updateDraftCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**null[]**](../Model/.md)| An array of draft campaign objects with updated values. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBDraftCampaignResponse[]**](../Model/SBDraftCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json
 - **Accept**: application/vnd.sbdraftcampaignresponse.v3+json, application/vnd.updatedraftcampaignsresponse.v3.2+json, application/vnd.updatedraftcampaignsresponse.v3.3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

