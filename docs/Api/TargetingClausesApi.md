# AmazonAdvertisingApi\TargetingClausesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSponsoredProductsTargetingClauses**](TargetingClausesApi.md#createsponsoredproductstargetingclauses) | **POST** /sp/targets | 
[**deleteSponsoredProductsTargetingClauses**](TargetingClausesApi.md#deletesponsoredproductstargetingclauses) | **POST** /sp/targets/delete | 
[**listSponsoredProductsTargetingClauses**](TargetingClausesApi.md#listsponsoredproductstargetingclauses) | **POST** /sp/targets/list | 
[**updateSponsoredProductsTargetingClauses**](TargetingClausesApi.md#updatesponsoredproductstargetingclauses) | **PUT** /sp/targets | 

# **createSponsoredProductsTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsTargetingClausesResponseContent createSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->createSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingClausesApi->createSponsoredProductsTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsTargetingClausesRequestContent**](../Model/SponsoredProductsCreateSponsoredProductsTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsTargetingClausesResponseContent**](../Model/SponsoredProductsCreateSponsoredProductsTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetingClause.v3+json
 - **Accept**: application/vnd.spTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSponsoredProductsTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsTargetingClausesResponseContent deleteSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->deleteSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingClausesApi->deleteSponsoredProductsTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsTargetingClausesRequestContent**](../Model/SponsoredProductsDeleteSponsoredProductsTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsTargetingClausesResponseContent**](../Model/SponsoredProductsDeleteSponsoredProductsTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetingClause.v3+json
 - **Accept**: application/vnd.spTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSponsoredProductsTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsTargetingClausesResponseContent listSponsoredProductsTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsTargetingClausesRequestContent | 

try {
    $result = $apiInstance->listSponsoredProductsTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingClausesApi->listSponsoredProductsTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsTargetingClausesRequestContent**](../Model/SponsoredProductsListSponsoredProductsTargetingClausesRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsTargetingClausesResponseContent**](../Model/SponsoredProductsListSponsoredProductsTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetingClause.v3+json
 - **Accept**: application/vnd.spTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSponsoredProductsTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsTargetingClausesResponseContent updateSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->updateSponsoredProductsTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingClausesApi->updateSponsoredProductsTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsTargetingClausesRequestContent**](../Model/SponsoredProductsUpdateSponsoredProductsTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsTargetingClausesResponseContent**](../Model/SponsoredProductsUpdateSponsoredProductsTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spTargetingClause.v3+json
 - **Accept**: application/vnd.spTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

