# AmazonAdvertisingApi\DiscoveryApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetchTaxonomy**](DiscoveryApi.md#fetchtaxonomy) | **POST** /audiences/taxonomy/list | Browse the taxonomy of audience categories
[**listAudiences**](DiscoveryApi.md#listaudiences) | **POST** /audiences/list | Gets audience segments based on filters

# **fetchTaxonomy**
> \AmazonAdvertisingApi\Model\FetchTaxonomyResponseV1 fetchTaxonomy($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $advertiser_id, $next_token, $max_results)

Browse the taxonomy of audience categories

Returns a list of audience categories for a given category path  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\DiscoveryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\FetchTaxonomyRequestBodyV1(); // \AmazonAdvertisingApi\Model\FetchTaxonomyRequestBodyV1 | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$advertiser_id = "advertiser_id_example"; // string | The advertiser associated with the advertising account. This parameter is required for the DSP adType, but optional for the SD adType.
$next_token = "next_token_example"; // string | Token from a previous request. Use in conjunction with the `maxResults` parameter to control pagination of the returned array.
$max_results = 56; // int | Sets the maximum number of categories in the returned array. Use in conjunction with the `nextToken` parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned.

try {
    $result = $apiInstance->fetchTaxonomy($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $advertiser_id, $next_token, $max_results);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscoveryApi->fetchTaxonomy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\FetchTaxonomyRequestBodyV1**](../Model/FetchTaxonomyRequestBodyV1.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **advertiser_id** | **string**| The advertiser associated with the advertising account. This parameter is required for the DSP adType, but optional for the SD adType. | [optional]
 **next_token** | **string**| Token from a previous request. Use in conjunction with the &#x60;maxResults&#x60; parameter to control pagination of the returned array. | [optional]
 **max_results** | **int**| Sets the maximum number of categories in the returned array. Use in conjunction with the &#x60;nextToken&#x60; parameter to control pagination. For example, supplying maxResults&#x3D;20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\FetchTaxonomyResponseV1**](../Model/FetchTaxonomyResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listAudiences**
> \AmazonAdvertisingApi\Model\ListAudiencesResponseV1 listAudiences($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $advertiser_id, $can_target, $next_token, $max_results)

Gets audience segments based on filters

Returns a list of audience segments for an advertiser. The result set can be filtered by providing an array of Filter objects. Each item in the resulting set will match all specified filters.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\DiscoveryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\ListAudiencesRequestBodyV1(); // \AmazonAdvertisingApi\Model\ListAudiencesRequestBodyV1 | 
$advertiser_id = "advertiser_id_example"; // string | The advertiser to retrieve segments for. This parameter is required for the DSP adType, but optional for the SD adType.
$can_target = false; // bool | When set to true, only targetable audience segments will be returned.
$next_token = "next_token_example"; // string | Token from a previous request. Use in conjunction with the `maxResults` parameter to control pagination of the returned array.
$max_results = 56; // int | Sets the maximum number of audiences in the returned array. Use in conjunction with the `nextToken` parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned.

try {
    $result = $apiInstance->listAudiences($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $advertiser_id, $can_target, $next_token, $max_results);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscoveryApi->listAudiences: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\ListAudiencesRequestBodyV1**](../Model/ListAudiencesRequestBodyV1.md)|  | [optional]
 **advertiser_id** | **string**| The advertiser to retrieve segments for. This parameter is required for the DSP adType, but optional for the SD adType. | [optional]
 **can_target** | **bool**| When set to true, only targetable audience segments will be returned. | [optional] [default to false]
 **next_token** | **string**| Token from a previous request. Use in conjunction with the &#x60;maxResults&#x60; parameter to control pagination of the returned array. | [optional]
 **max_results** | **int**| Sets the maximum number of audiences in the returned array. Use in conjunction with the &#x60;nextToken&#x60; parameter to control pagination. For example, supplying maxResults&#x3D;20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ListAudiencesResponseV1**](../Model/ListAudiencesResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

