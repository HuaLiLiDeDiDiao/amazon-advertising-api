# AmazonAdvertisingApi\CreativesApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCreatives**](CreativesApi.md#createcreatives) | **POST** /sd/creatives | A POST request of one or more creatives.
[**listCreativeModerations**](CreativesApi.md#listcreativemoderations) | **GET** /sd/moderation/creatives | Gets a list of creative moderations
[**listCreatives**](CreativesApi.md#listcreatives) | **GET** /sd/creatives | Gets a list of creatives
[**postCreativePreview**](CreativesApi.md#postcreativepreview) | **POST** /sd/creatives/preview | Gets creative preview HTML.
[**updateCreatives**](CreativesApi.md#updatecreatives) | **PUT** /sd/creatives | Updates one or more creatives.

# **createCreatives**
> \AmazonAdvertisingApi\Model\CreativeResponse[] createCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

A POST request of one or more creatives.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateCreative()); // \AmazonAdvertisingApi\Model\CreateCreative[] | An array of Creative objects to create. Maximum length of the array is 100 objects. Note - when using productAds with landingPageURL of OFF_AMAZON_LINK, STORE, or MOMENT, the following properties are required all together;
1) headline, 2) brandLogo, and 3) rectCustomImage, squareCustomImage.

try {
    $result = $apiInstance->createCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreativesApi->createCreatives: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateCreative[]**](../Model/CreateCreative.md)| An array of Creative objects to create. Maximum length of the array is 100 objects. Note - when using productAds with landingPageURL of OFF_AMAZON_LINK, STORE, or MOMENT, the following properties are required all together;
1) headline, 2) brandLogo, and 3) rectCustomImage, squareCustomImage. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreativeResponse[]**](../Model/CreativeResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCreativeModerations**
> \AmazonAdvertisingApi\Model\CreativeModeration[] listCreativeModerations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $language, $start_index, $count, $ad_group_id_filter, $creative_id_filter)

Gets a list of creative moderations

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$language = new \AmazonAdvertisingApi\Model\Locale(); // \AmazonAdvertisingApi\Model\Locale | The language of the returned creative moderation metadata.
$start_index = 0; // int | Sets a cursor into the requested set of creative moderations. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 100; // int | Sets the number of creative objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten creative moderations set `startIndex=0` and `count=10`. To return the next ten creative moderations, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | The returned array includes only creative moderations associated with ad group identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the `creativeIdFilter` parameter.
$creative_id_filter = "creative_id_filter_example"; // string | The returned array includes only creative moderations with creative identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the `adGroupIdFilter` parameter.

try {
    $result = $apiInstance->listCreativeModerations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $language, $start_index, $count, $ad_group_id_filter, $creative_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreativesApi->listCreativeModerations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **language** | [**\AmazonAdvertisingApi\Model\Locale**](../Model/.md)| The language of the returned creative moderation metadata. |
 **start_index** | **int**| Sets a cursor into the requested set of creative moderations. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional] [default to 0]
 **count** | **int**| Sets the number of creative objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten creative moderations set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten creative moderations, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional] [default to 100]
 **ad_group_id_filter** | **string**| The returned array includes only creative moderations associated with ad group identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the &#x60;creativeIdFilter&#x60; parameter. | [optional]
 **creative_id_filter** | **string**| The returned array includes only creative moderations with creative identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the &#x60;adGroupIdFilter&#x60; parameter. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreativeModeration[]**](../Model/CreativeModeration.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCreatives**
> \AmazonAdvertisingApi\Model\Creative[] listCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $ad_group_id_filter, $creative_id_filter)

Gets a list of creatives

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 0; // int | Sets a cursor into the requested set of creatives. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 100; // int | Sets the number of creative objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten creatives set `startIndex=0` and `count=10`. To return the next ten creatives, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | The returned array includes only creatives associated with ad group identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the `creativeIdFilter` parameter.
$creative_id_filter = "creative_id_filter_example"; // string | The returned array includes only creatives with identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the `adGroupIdFilter` parameter.

try {
    $result = $apiInstance->listCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $ad_group_id_filter, $creative_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreativesApi->listCreatives: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Sets a cursor into the requested set of creatives. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional] [default to 0]
 **count** | **int**| Sets the number of creative objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten creatives set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten creatives, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional] [default to 100]
 **ad_group_id_filter** | **string**| The returned array includes only creatives associated with ad group identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the &#x60;creativeIdFilter&#x60; parameter. | [optional]
 **creative_id_filter** | **string**| The returned array includes only creatives with identifiers matching those specified in the comma-delimited string. Cannot be used in conjunction with the &#x60;adGroupIdFilter&#x60; parameter. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Creative[]**](../Model/Creative.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postCreativePreview**
> \AmazonAdvertisingApi\Model\CreativePreviewResponse postCreativePreview($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Gets creative preview HTML.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\CreativePreviewRequest(); // \AmazonAdvertisingApi\Model\CreativePreviewRequest | 

try {
    $result = $apiInstance->postCreativePreview($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreativesApi->postCreativePreview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreativePreviewRequest**](../Model/CreativePreviewRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreativePreviewResponse**](../Model/CreativePreviewResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCreatives**
> \AmazonAdvertisingApi\Model\CreativeResponse[] updateCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more creatives.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\CreativesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreativeUpdate()); // \AmazonAdvertisingApi\Model\CreativeUpdate[] | An array of creative objects to update. Maximum length of the array is 100 objects.

try {
    $result = $apiInstance->updateCreatives($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreativesApi->updateCreatives: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreativeUpdate[]**](../Model/CreativeUpdate.md)| An array of creative objects to update. Maximum length of the array is 100 objects. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\CreativeResponse[]**](../Model/CreativeResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

