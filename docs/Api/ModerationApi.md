# AmazonAdvertisingApi\ModerationApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sbModerationCampaignsCampaignIdGet**](ModerationApi.md#sbmoderationcampaignscampaignidget) | **GET** /sb/moderation/campaigns/{campaignId} | Gets the moderation result for a campaign specified by identifier.

# **sbModerationCampaignsCampaignIdGet**
> \AmazonAdvertisingApi\Model\InlineResponse20018 sbModerationCampaignsCampaignIdGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id)

Gets the moderation result for a campaign specified by identifier.

Note that this resource is only available for campaigns in the US marketplace.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ModerationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$campaign_id = 789; // int | The campaign identifier.

try {
    $result = $apiInstance->sbModerationCampaignsCampaignIdGet($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ModerationApi->sbModerationCampaignsCampaignIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **campaign_id** | **int**| The campaign identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20018**](../Model/InlineResponse20018.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbmoderation.v3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

