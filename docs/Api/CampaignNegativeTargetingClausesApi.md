# AmazonAdvertisingApi\CampaignNegativeTargetingClausesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSponsoredProductsCampaignNegativeTargetingClauses**](CampaignNegativeTargetingClausesApi.md#createsponsoredproductscampaignnegativetargetingclauses) | **POST** /sp/campaignNegativeTargets | 
[**deleteSponsoredProductsCampaignNegativeTargetingClauses**](CampaignNegativeTargetingClausesApi.md#deletesponsoredproductscampaignnegativetargetingclauses) | **POST** /sp/campaignNegativeTargets/delete | 
[**listSponsoredProductsCampaignNegativeTargetingClauses**](CampaignNegativeTargetingClausesApi.md#listsponsoredproductscampaignnegativetargetingclauses) | **POST** /sp/campaignNegativeTargets/list | 
[**updateSponsoredProductsCampaignNegativeTargetingClauses**](CampaignNegativeTargetingClausesApi.md#updatesponsoredproductscampaignnegativetargetingclauses) | **PUT** /sp/campaignNegativeTargets | 

# **createSponsoredProductsCampaignNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesResponseContent createSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->createSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeTargetingClausesApi->createSponsoredProductsCampaignNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spCampaignNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSponsoredProductsCampaignNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesResponseContent deleteSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->deleteSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeTargetingClausesApi->deleteSponsoredProductsCampaignNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsDeleteSponsoredProductsCampaignNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spCampaignNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSponsoredProductsCampaignNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesResponseContent listSponsoredProductsCampaignNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesRequestContent | 

try {
    $result = $apiInstance->listSponsoredProductsCampaignNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeTargetingClausesApi->listSponsoredProductsCampaignNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsListSponsoredProductsCampaignNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spCampaignNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSponsoredProductsCampaignNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesResponseContent updateSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->updateSponsoredProductsCampaignNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeTargetingClausesApi->updateSponsoredProductsCampaignNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spCampaignNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

