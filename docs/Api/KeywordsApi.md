# AmazonAdvertisingApi\KeywordsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveKeyword**](KeywordsApi.md#archivekeyword) | **DELETE** /sb/keywords/{keywordId} | Archives a keyword specified by identifier.
[**createKeywords**](KeywordsApi.md#createkeywords) | **POST** /sb/keywords | Creates one or more keywords.
[**getKeyword**](KeywordsApi.md#getkeyword) | **GET** /sb/keywords/{keywordId} | Gets a keyword specified by identifier.
[**listKeywords**](KeywordsApi.md#listkeywords) | **GET** /sb/keywords | Gets an array of keywords, filtered by optional criteria.
[**updateKeywords**](KeywordsApi.md#updatekeywords) | **PUT** /sb/keywords | Updates one or more keywords.

# **archiveKeyword**
> \AmazonAdvertisingApi\Model\SBKeywordResponse archiveKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id)

Archives a keyword specified by identifier.

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/v2/guides/developer_notes) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$keyword_id = 789; // int | 

try {
    $result = $apiInstance->archiveKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordsApi->archiveKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **keyword_id** | **int**|  |

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbkeywordresponse.v3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createKeywords**
> \AmazonAdvertisingApi\Model\SBKeywordResponse createKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more keywords.

Note that `state` can't be set at keyword creation. Keywords submitted for creation have state set to `pending` while under moderation review. Moderation review may take up to 72 hours. <br/>Note that keywords can be created on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 keywords.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SbKeywordsBody1()); // \AmazonAdvertisingApi\Model\SbKeywordsBody1[] | An array of keywords.

try {
    $result = $apiInstance->createKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordsApi->createKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbKeywordsBody1[]**](../Model/SbKeywordsBody1.md)| An array of keywords. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbkeywordresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKeyword**
> \AmazonAdvertisingApi\Model\SBKeyword getKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id, $locale)

Gets a keyword specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$keyword_id = 789; // int | The identifier of an existing keyword.
$locale = "locale_example"; // string | The returned array includes only keywords associated with locale matching those specified by identifier.

try {
    $result = $apiInstance->getKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordsApi->getKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **keyword_id** | **int**| The identifier of an existing keyword. |
 **locale** | **string**| The returned array includes only keywords associated with locale matching those specified by identifier. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeyword**](../Model/SBKeyword.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbkeyword.v3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listKeywords**
> \AmazonAdvertisingApi\Model\SBKeyword[] listKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $match_type_filter, $keyword_text, $state_filter, $campaign_id_filter, $ad_group_id_filter, $keyword_id_filter, $creative_type, $locale)

Gets an array of keywords, filtered by optional criteria.

**Note**: Keywords associated with BrandVideo ad groups are only available in v3.2 version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 0; // int | Sets a zero-based offset into the requested set of keywords. Use in conjunction with the `count` parameter to control pagination of the returned array.
$count = 56; // int | Sets the number of keywords in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten keywords set `startIndex=0` and `count=10`. To return the next ten keywords, set `startIndex=10` and `count=10`, and so on.
$match_type_filter = new \AmazonAdvertisingApi\Model\MatchType(); // \AmazonAdvertisingApi\Model\MatchType | The returned array is filtered to include only keywords with `matchType` set to one of the values in the specified comma-delimited list.
$keyword_text = "keyword_text_example"; // string | The returned array includes only keywords with the specified text.
$state_filter = "enabled,paused"; // string | The returned array is filtered to include only keywords with 'state' set to one of the values in the specified comma-delimited list.
$campaign_id_filter = "campaign_id_filter_example"; // string | The returned array includes only keywords associated with campaigns matching those specified by identifier in the comma-delimited string.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | The returned array includes only keywords associated with ad groups matching those specified by identifier in the comma-delimited string.
$keyword_id_filter = "keyword_id_filter_example"; // string | The returned array includes only keywords with identifiers matching those specified in the comma-delimited string.
$creative_type = new \AmazonAdvertisingApi\Model\CreativeType(); // \AmazonAdvertisingApi\Model\CreativeType | Filter by the type of creative the campaign is associated with. To get keywords associated with non-video campaigns specify 'productCollection'. To get keywords associated with video campaigns, this must be set to 'video'. Returns all keywords if not specified.
$locale = "locale_example"; // string | The returned array includes only keywords with locale matching those specified string.

try {
    $result = $apiInstance->listKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $match_type_filter, $keyword_text, $state_filter, $campaign_id_filter, $ad_group_id_filter, $keyword_id_filter, $creative_type, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordsApi->listKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Sets a zero-based offset into the requested set of keywords. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. | [optional] [default to 0]
 **count** | **int**| Sets the number of keywords in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten keywords set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten keywords, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. | [optional]
 **match_type_filter** | [**\AmazonAdvertisingApi\Model\MatchType**](../Model/.md)| The returned array is filtered to include only keywords with &#x60;matchType&#x60; set to one of the values in the specified comma-delimited list. | [optional]
 **keyword_text** | **string**| The returned array includes only keywords with the specified text. | [optional]
 **state_filter** | **string**| The returned array is filtered to include only keywords with &#x27;state&#x27; set to one of the values in the specified comma-delimited list. | [optional] [default to enabled,paused]
 **campaign_id_filter** | **string**| The returned array includes only keywords associated with campaigns matching those specified by identifier in the comma-delimited string. | [optional]
 **ad_group_id_filter** | **string**| The returned array includes only keywords associated with ad groups matching those specified by identifier in the comma-delimited string. | [optional]
 **keyword_id_filter** | **string**| The returned array includes only keywords with identifiers matching those specified in the comma-delimited string. | [optional]
 **creative_type** | [**\AmazonAdvertisingApi\Model\CreativeType**](../Model/.md)| Filter by the type of creative the campaign is associated with. To get keywords associated with non-video campaigns specify &#x27;productCollection&#x27;. To get keywords associated with video campaigns, this must be set to &#x27;video&#x27;. Returns all keywords if not specified. | [optional]
 **locale** | **string**| The returned array includes only keywords with locale matching those specified string. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeyword[]**](../Model/SBKeyword.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbkeyword.v3.2+json, application/vnd.sbkeyword.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKeywords**
> \AmazonAdvertisingApi\Model\SBKeywordResponse updateKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more keywords.

Keywords submitted for update may have state set to `pending` for moderation review. Moderation may take up to 72 hours. <br/>Note that keywords can be updated on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 keywords.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\KeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SbKeywordsBody()); // \AmazonAdvertisingApi\Model\SbKeywordsBody[] | An array of keywords.

try {
    $result = $apiInstance->updateKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KeywordsApi->updateKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbKeywordsBody[]**](../Model/SbKeywordsBody.md)| An array of keywords. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbkeywordresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

