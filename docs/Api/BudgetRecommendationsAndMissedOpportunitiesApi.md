# AmazonAdvertisingApi\BudgetRecommendationsAndMissedOpportunitiesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBudgetRecommendations**](BudgetRecommendationsAndMissedOpportunitiesApi.md#getbudgetrecommendations) | **POST** /sp/campaigns/budgetRecommendations | Get recommended daily budget and estimated missed opportunities for campaigns.

# **getBudgetRecommendations**
> \AmazonAdvertisingApi\Model\BudgetRecommendationResponse getBudgetRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Get recommended daily budget and estimated missed opportunities for campaigns.

Given a list of campaigns as input, this API provides the following metrics -  <br> <b>1. Recommended daily budget - </b> Estimated daily budget needed to keep the campaign in budget for the full 24-hour period in a day. Consider this daily budget to minimize your campaign's chances of running out of budget. <br> <b>2. Percent time in budget </b> - Actual average percentage of time the campaign was in budget between the start and end date specified in the response. Note: value -1 means we don’t have enough information to compute the campaign’s percent time in budget. <br> <b>3. Estimated missed impressions, clicks and sales </b> - These are the estimated range of additional impressions, clicks and sales the campaign might have generated between the start and end date specified in the response had it been in budget 100% of the time. These are estimates based on historical traffic and the campaign's past performance (e.g. impressions & clicks generated) but not guaranteed.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRecommendationsAndMissedOpportunitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\BudgetRecommendationRequest(); // \AmazonAdvertisingApi\Model\BudgetRecommendationRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->getBudgetRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRecommendationsAndMissedOpportunitiesApi->getBudgetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\BudgetRecommendationRequest**](../Model/BudgetRecommendationRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\BudgetRecommendationResponse**](../Model/BudgetRecommendationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.budgetrecommendation.v3+json
 - **Accept**: application/vnd.budgetrecommendation.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

