# AmazonAdvertisingApi\ProductTargetingApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveTarget**](ProductTargetingApi.md#archivetarget) | **DELETE** /sb/targets/{targetId} | Archives a target specified by identifier. Note that archiving is permanent, and once a target has been archived it can&#x27;t be made active again.
[**createTargets**](ProductTargetingApi.md#createtargets) | **POST** /sb/targets | Create one or more targets.
[**getTarget**](ProductTargetingApi.md#gettarget) | **GET** /sb/targets/{targetId} | Gets a target specified by identifier.
[**listTargets**](ProductTargetingApi.md#listtargets) | **POST** /sb/targets/list | 
[**updateTargets**](ProductTargetingApi.md#updatetargets) | **PUT** /sb/targets | Updates one or more targets.

# **archiveTarget**
> \AmazonAdvertisingApi\Model\SBTargetingClauseResponse archiveTarget($target_id)

Archives a target specified by identifier. Note that archiving is permanent, and once a target has been archived it can't be made active again.

The identifier of an existing target.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$target_id = new \AmazonAdvertisingApi\Model\SBTargetId(); // \AmazonAdvertisingApi\Model\SBTargetId | 

try {
    $result = $apiInstance->archiveTarget($target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingApi->archiveTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target_id** | [**\AmazonAdvertisingApi\Model\SBTargetId**](../Model/.md)|  |

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingClauseResponse**](../Model/SBTargetingClauseResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbtargetresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createTargets**
> \AmazonAdvertisingApi\Model\SBCreateTargetsResponse createTargets($body)

Create one or more targets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\SbTargetsBody1(); // \AmazonAdvertisingApi\Model\SbTargetsBody1 | A list of targeting clauses for creation. <br/>Note that targets can be created on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 targets.

try {
    $result = $apiInstance->createTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingApi->createTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SbTargetsBody1**](../Model/SbTargetsBody1.md)| A list of targeting clauses for creation. &lt;br/&gt;Note that targets can be created on campaigns where serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 targets. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBCreateTargetsResponse**](../Model/SBCreateTargetsResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbcreatetargetsresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTarget**
> \AmazonAdvertisingApi\Model\SBTargetingClause getTarget($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id)

Gets a target specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$target_id = new \AmazonAdvertisingApi\Model\SBTargetId(); // \AmazonAdvertisingApi\Model\SBTargetId | The identifier of an existing target.

try {
    $result = $apiInstance->getTarget($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingApi->getTarget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **target_id** | [**\AmazonAdvertisingApi\Model\SBTargetId**](../Model/.md)| The identifier of an existing target. |

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingClause**](../Model/SBTargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbtarget.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listTargets**
> \AmazonAdvertisingApi\Model\InlineResponse2009 listTargets($body)



Gets a list of product targets associated with the client identifier passed in the authorization header, filtered by specified criteria.  **Note**: Product targets associated with BrandVideo ad groups are only available in v3.2 version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\TargetsListBody(); // \AmazonAdvertisingApi\Model\TargetsListBody | A set of filters.

try {
    $result = $apiInstance->listTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingApi->listTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\TargetsListBody**](../Model/TargetsListBody.md)| A set of filters. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sblisttargetsresponse.v3.2+json, application/vnd.sblisttargetsresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTargets**
> \AmazonAdvertisingApi\Model\InlineResponse20010 updateTargets($body)

Updates one or more targets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\SbTargetsBody(); // \AmazonAdvertisingApi\Model\SbTargetsBody | A list of targets with updated values. <br/>Note that targets can be updated on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 targets.

try {
    $result = $apiInstance->updateTargets($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingApi->updateTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SbTargetsBody**](../Model/SbTargetsBody.md)| A list of targets with updated values. &lt;br/&gt;Note that targets can be updated on campaigns where serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 targets. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20010**](../Model/InlineResponse20010.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.updatetargetsresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

