# AmazonAdvertisingApi\AdsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSponsoredBrandStoreSpotlightAds**](AdsApi.md#createsponsoredbrandstorespotlightads) | **POST** /sb/v4/ads/storeSpotlight | Create store spotlight ads
[**createSponsoredBrandsBrandVideoAds**](AdsApi.md#createsponsoredbrandsbrandvideoads) | **POST** /sb/v4/ads/brandVideo | Create brand video ads
[**createSponsoredBrandsProductCollectionAds**](AdsApi.md#createsponsoredbrandsproductcollectionads) | **POST** /sb/v4/ads/productCollection | Create product collection ads
[**createSponsoredBrandsVideoAds**](AdsApi.md#createsponsoredbrandsvideoads) | **POST** /sb/v4/ads/video | Create video ads
[**deleteSponsoredBrandsAds**](AdsApi.md#deletesponsoredbrandsads) | **POST** /sb/v4/ads/delete | Delete ads
[**listSponsoredBrandsAds**](AdsApi.md#listsponsoredbrandsads) | **POST** /sb/v4/ads/list | List ads
[**updateSponsoredBrandsAds**](AdsApi.md#updatesponsoredbrandsads) | **PUT** /sb/v4/ads | Update ads

# **createSponsoredBrandStoreSpotlightAds**
> \AmazonAdvertisingApi\Model\CreateSponsoredBrandStoreSpotlightAdsResponseContent createSponsoredBrandStoreSpotlightAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Create store spotlight ads

Creates Sponsored Brands store spotlight ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateSponsoredBrandStoreSpotlightAdsRequestContent(); // \AmazonAdvertisingApi\Model\CreateSponsoredBrandStoreSpotlightAdsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->createSponsoredBrandStoreSpotlightAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->createSponsoredBrandStoreSpotlightAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSponsoredBrandStoreSpotlightAdsRequestContent**](../Model/CreateSponsoredBrandStoreSpotlightAdsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateSponsoredBrandStoreSpotlightAdsResponseContent**](../Model/CreateSponsoredBrandStoreSpotlightAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSponsoredBrandsBrandVideoAds**
> \AmazonAdvertisingApi\Model\CreateSponsoredBrandsBrandVideoAdsResponseContent createSponsoredBrandsBrandVideoAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Create brand video ads

Creates Sponsored Brands brand video ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateSponsoredBrandsBrandVideoAdsRequestContent(); // \AmazonAdvertisingApi\Model\CreateSponsoredBrandsBrandVideoAdsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->createSponsoredBrandsBrandVideoAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->createSponsoredBrandsBrandVideoAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsBrandVideoAdsRequestContent**](../Model/CreateSponsoredBrandsBrandVideoAdsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsBrandVideoAdsResponseContent**](../Model/CreateSponsoredBrandsBrandVideoAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSponsoredBrandsProductCollectionAds**
> \AmazonAdvertisingApi\Model\CreateSponsoredBrandsProductCollectionAdsResponseContent createSponsoredBrandsProductCollectionAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Create product collection ads

Creates Sponsored Brands product collection ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateSponsoredBrandsProductCollectionAdsRequestContent(); // \AmazonAdvertisingApi\Model\CreateSponsoredBrandsProductCollectionAdsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->createSponsoredBrandsProductCollectionAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->createSponsoredBrandsProductCollectionAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsProductCollectionAdsRequestContent**](../Model/CreateSponsoredBrandsProductCollectionAdsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsProductCollectionAdsResponseContent**](../Model/CreateSponsoredBrandsProductCollectionAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSponsoredBrandsVideoAds**
> \AmazonAdvertisingApi\Model\CreateSponsoredBrandsVideoAdsResponseContent createSponsoredBrandsVideoAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Create video ads

Creates Sponsored Brands video ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateSponsoredBrandsVideoAdsRequestContent(); // \AmazonAdvertisingApi\Model\CreateSponsoredBrandsVideoAdsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->createSponsoredBrandsVideoAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->createSponsoredBrandsVideoAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsVideoAdsRequestContent**](../Model/CreateSponsoredBrandsVideoAdsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateSponsoredBrandsVideoAdsResponseContent**](../Model/CreateSponsoredBrandsVideoAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSponsoredBrandsAds**
> \AmazonAdvertisingApi\Model\DeleteSponsoredBrandsAdsResponseContent deleteSponsoredBrandsAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Delete ads

Deletes Sponsored Brands ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$body = new \AmazonAdvertisingApi\Model\DeleteSponsoredBrandsAdsRequestContent(); // \AmazonAdvertisingApi\Model\DeleteSponsoredBrandsAdsRequestContent | 

try {
    $result = $apiInstance->deleteSponsoredBrandsAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->deleteSponsoredBrandsAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **body** | [**\AmazonAdvertisingApi\Model\DeleteSponsoredBrandsAdsRequestContent**](../Model/DeleteSponsoredBrandsAdsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\DeleteSponsoredBrandsAdsResponseContent**](../Model/DeleteSponsoredBrandsAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSponsoredBrandsAds**
> \AmazonAdvertisingApi\Model\ListSponsoredBrandsAdsResponseContent listSponsoredBrandsAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

List ads

Lists Sponsored Brands ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$body = new \AmazonAdvertisingApi\Model\ListSponsoredBrandsAdsRequestContent(); // \AmazonAdvertisingApi\Model\ListSponsoredBrandsAdsRequestContent | 

try {
    $result = $apiInstance->listSponsoredBrandsAds($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->listSponsoredBrandsAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **body** | [**\AmazonAdvertisingApi\Model\ListSponsoredBrandsAdsRequestContent**](../Model/ListSponsoredBrandsAdsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ListSponsoredBrandsAdsResponseContent**](../Model/ListSponsoredBrandsAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSponsoredBrandsAds**
> \AmazonAdvertisingApi\Model\UpdateSponsoredBrandsAdsResponseContent updateSponsoredBrandsAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Update ads

Updates Sponsored Brands ads.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\UpdateSponsoredBrandsAdsRequestContent(); // \AmazonAdvertisingApi\Model\UpdateSponsoredBrandsAdsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->updateSponsoredBrandsAds($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdsApi->updateSponsoredBrandsAds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateSponsoredBrandsAdsRequestContent**](../Model/UpdateSponsoredBrandsAdsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateSponsoredBrandsAdsResponseContent**](../Model/UpdateSponsoredBrandsAdsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbadresource.v4+json
 - **Accept**: application/vnd.sbadresource.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

