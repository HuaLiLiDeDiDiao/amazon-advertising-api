# AmazonAdvertisingApi\HeadlineRecommendationsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHeadlineRecommendationsForSD**](HeadlineRecommendationsApi.md#getheadlinerecommendationsforsd) | **POST** /sd/recommendations/creative/headline | 

# **getHeadlineRecommendationsForSD**
> \AmazonAdvertisingApi\Model\SDHeadlineRecommendationResponse getHeadlineRecommendationsForSD($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



You can use this Sponsored Display API to retrieve creative headline recommendations from an array of ASINs.  **Requires one of these permissions**: [\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\HeadlineRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\SDHeadlineRecommendationRequest(); // \AmazonAdvertisingApi\Model\SDHeadlineRecommendationRequest | Request body for SD headline recommendations API.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->getHeadlineRecommendationsForSD($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HeadlineRecommendationsApi->getHeadlineRecommendationsForSD: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SDHeadlineRecommendationRequest**](../Model/SDHeadlineRecommendationRequest.md)| Request body for SD headline recommendations API. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\SDHeadlineRecommendationResponse**](../Model/SDHeadlineRecommendationResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdheadlinerecommendationrequest.v4.0+json
 - **Accept**: application/vnd.sdheadlinerecommendationresponse.v4.0+json, application/vnd.sdheadlinerecommendationschemavalidationexception.v4.0+json, application/vnd.sdheadlinerecommendationaccessdeniedexception.v4.0+json, application/vnd.sdheadlinerecommendationidentifiernotfoundexception.v4.0+json, application/vnd.sdheadlinerecommendationthrottlingexception.v4.0+json, application/vnd.sdheadlinerecommendationinternalserverexception.v4.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

