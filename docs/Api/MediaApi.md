# AmazonAdvertisingApi\MediaApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**completeUpload**](MediaApi.md#completeupload) | **PUT** /media/complete | The API is used to notify that the upload is completed.
[**createUploadResource**](MediaApi.md#createuploadresource) | **POST** /media/upload | Creates an ephemeral resource (upload location). [PLANNED SHUTOFF DATE 1/31/2024]
[**describeMedia**](MediaApi.md#describemedia) | **GET** /media/describe | API to poll for media status

# **completeUpload**
> \AmazonAdvertisingApi\Model\InlineResponse2004 completeUpload($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

The API is used to notify that the upload is completed.

The API should be called once the media is uploaded to the location provided by the /media/upload API endpoint. The API creates a Media resource for the uploaded media. Media resource is comprised of Media Identifier. The Media Identifier can be used to attach media to Ad Program (Sponsored Brands).  The API internally kicks off the asynchronous validation and processing workflow of the uploaded media. As a result, Media may not be immediately available for usage (to create Sponsored Brands Video Campaign) as soon as the response is received. See /media/describe API doc for instructions on when media is ready for campaign creation.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\MediaCompleteBody(); // \AmazonAdvertisingApi\Model\MediaCompleteBody | The  upload location
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->completeUpload($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->completeUpload: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\MediaCompleteBody**](../Model/MediaCompleteBody.md)| The  upload location |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createUploadResource**
> \AmazonAdvertisingApi\Model\UploadLocation createUploadResource($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Creates an ephemeral resource (upload location). [PLANNED SHUTOFF DATE 1/31/2024]

**Going forward, use [Asset library](creative-asset-library) to manage media.** Creates an ephemeral resource (upload location) to upload Media for an Ad Program. The upload location is short lived and expires in 15 minutes. Once the upload is complete, /media/complete API should be used to notify that the upload is complete. <p> The upload location only supports `PUT` HTTP Method to upload the media content. If the upload location expires, API user will get `403 Forbidden` response. </p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\Program(); // \AmazonAdvertisingApi\Model\Program | The Ad Program that the media will be attached to. Specify `SponsoredBrands` with creative type `Video` for Sponsored brands video campaigns.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->createUploadResource($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->createUploadResource: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\Program**](../Model/Program.md)| The Ad Program that the media will be attached to. Specify &#x60;SponsoredBrands&#x60; with creative type &#x60;Video&#x60; for Sponsored brands video campaigns. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\UploadLocation**](../Model/UploadLocation.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **describeMedia**
> \AmazonAdvertisingApi\Model\InlineResponse2005 describeMedia($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $media_id)

API to poll for media status

API to poll for media status. In order to attach media to campaign, media should be in either `PendingDeepValidation` or `Available` status.  `Available` status guarantees that media has completed processing and published for usage.  Though media can be attached to campaign once the status of the media transitions to `PendingDeepValidation`, media could still fail additional validation and transition to `Failed` status. For example in the context of SBV, SBV campaign can be created when status transitions to `PendingDeepValidation`, it could result in SBV campaign to be rejected later if media transitions to `Failed` status.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$media_id = "media_id_example"; // string | Media Identifier

try {
    $result = $apiInstance->describeMedia($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $media_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->describeMedia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **media_id** | **string**| Media Identifier |

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

