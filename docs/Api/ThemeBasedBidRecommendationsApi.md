# AmazonAdvertisingApi\ThemeBasedBidRecommendationsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getThemeBasedBidRecommendationForAdGroupV1**](ThemeBasedBidRecommendationsApi.md#getthemebasedbidrecommendationforadgroupv1) | **POST** /sp/targets/bid/recommendations | Get bid recommendations for ad groups

# **getThemeBasedBidRecommendationForAdGroupV1**
> \AmazonAdvertisingApi\Model\ThemeBasedBidRecommendationResponse getThemeBasedBidRecommendationForAdGroupV1($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Get bid recommendations for ad groups

This API is currently available in US, UK, DE, CA, JP, IN, ES, and FR. The API supports keyword and auto targets only. The API will return a 422 response when an unsupported marketplace or target is provided. For product targets in all marketplaces, and keyword or auto targets in other marketplaces, call /v2/sp/targets/bidRecommendations. For more information, see the bid recommendations API [developer guide](https://advertising.amazon.com/API/docs/en-us/guides/sponsored-products/bid-suggestions/theme-based-bid-suggestions-quickstart-guide)  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ThemeBasedBidRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\BidRecommendationsBody(); // \AmazonAdvertisingApi\Model\BidRecommendationsBody | 

try {
    $result = $apiInstance->getThemeBasedBidRecommendationForAdGroupV1($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemeBasedBidRecommendationsApi->getThemeBasedBidRecommendationForAdGroupV1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\BidRecommendationsBody**](../Model/BidRecommendationsBody.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ThemeBasedBidRecommendationResponse**](../Model/ThemeBasedBidRecommendationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spthemebasedbidrecommendation.v3+json
 - **Accept**: application/vnd.spthemebasedbidrecommendation.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

