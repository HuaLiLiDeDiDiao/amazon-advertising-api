# AmazonAdvertisingApi\NegativeTargetingClausesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSponsoredProductsNegativeTargetingClauses**](NegativeTargetingClausesApi.md#createsponsoredproductsnegativetargetingclauses) | **POST** /sp/negativeTargets | 
[**deleteSponsoredProductsNegativeTargetingClauses**](NegativeTargetingClausesApi.md#deletesponsoredproductsnegativetargetingclauses) | **POST** /sp/negativeTargets/delete | 
[**listSponsoredProductsNegativeTargetingClauses**](NegativeTargetingClausesApi.md#listsponsoredproductsnegativetargetingclauses) | **POST** /sp/negativeTargets/list | 
[**updateSponsoredProductsNegativeTargetingClauses**](NegativeTargetingClausesApi.md#updatesponsoredproductsnegativetargetingclauses) | **PUT** /sp/negativeTargets | 

# **createSponsoredProductsNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesResponseContent createSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->createSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingClausesApi->createSponsoredProductsNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSponsoredProductsNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesResponseContent deleteSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->deleteSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingClausesApi->deleteSponsoredProductsNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsDeleteSponsoredProductsNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSponsoredProductsNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsNegativeTargetingClausesResponseContent listSponsoredProductsNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsNegativeTargetingClausesRequestContent | 

try {
    $result = $apiInstance->listSponsoredProductsNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingClausesApi->listSponsoredProductsNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsListSponsoredProductsNegativeTargetingClausesRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsListSponsoredProductsNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSponsoredProductsNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesResponseContent updateSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingClausesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->updateSponsoredProductsNegativeTargetingClauses($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingClausesApi->updateSponsoredProductsNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesRequestContent**](../Model/SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesResponseContent**](../Model/SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spNegativeTargetingClause.v3+json
 - **Accept**: application/vnd.spNegativeTargetingClause.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

