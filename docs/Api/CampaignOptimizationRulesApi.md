# AmazonAdvertisingApi\CampaignOptimizationRulesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOptimizationRule**](CampaignOptimizationRulesApi.md#createoptimizationrule) | **POST** /sp/rules/campaignOptimization | Creates a campaign optimization rule.
[**deleteCampaignOptimizationRule**](CampaignOptimizationRulesApi.md#deletecampaignoptimizationrule) | **DELETE** /sp/rules/campaignOptimization/{campaignOptimizationId} | Deletes a campaign optimization rule specified by identifier.
[**getCampaignOptimizationRule**](CampaignOptimizationRulesApi.md#getcampaignoptimizationrule) | **GET** /sp/rules/campaignOptimization/{campaignOptimizationId} | Gets a campaign optimization rule specified by identifier.
[**getOptimizationRuleEligibility**](CampaignOptimizationRulesApi.md#getoptimizationruleeligibility) | **POST** /sp/rules/campaignOptimization/eligibility | Gets a campaign optimization rule recommendation for SP campaigns.
[**getRuleNotification**](CampaignOptimizationRulesApi.md#getrulenotification) | **POST** /sp/rules/campaignOptimization/state | Gets campaign optimization rule state. Recommended refresh frequency is once a day.
[**updateOptimizationRule**](CampaignOptimizationRulesApi.md#updateoptimizationrule) | **PUT** /sp/rules/campaignOptimization | Updates a campaign optimization rule.

# **createOptimizationRule**
> \AmazonAdvertisingApi\Model\CreateSPCampaignOptimizationRulesResponse createOptimizationRule($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Creates a campaign optimization rule.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateSPCampaignOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\CreateSPCampaignOptimizationRulesRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->createOptimizationRule($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->createOptimizationRule: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSPCampaignOptimizationRulesRequest**](../Model/CreateSPCampaignOptimizationRulesRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateSPCampaignOptimizationRulesResponse**](../Model/CreateSPCampaignOptimizationRulesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.optimizationrules.v1+json
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCampaignOptimizationRule**
> \AmazonAdvertisingApi\Model\DeleteSPCampaignOptimizationRuleResponse deleteCampaignOptimizationRule($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_optimization_id)

Deletes a campaign optimization rule specified by identifier.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$campaign_optimization_id = "campaign_optimization_id_example"; // string | The sp campaign optimization rule identifier.

try {
    $result = $apiInstance->deleteCampaignOptimizationRule($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_optimization_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->deleteCampaignOptimizationRule: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **campaign_optimization_id** | **string**| The sp campaign optimization rule identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\DeleteSPCampaignOptimizationRuleResponse**](../Model/DeleteSPCampaignOptimizationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCampaignOptimizationRule**
> \AmazonAdvertisingApi\Model\GetSPCampaignOptimizationRuleResponse getCampaignOptimizationRule($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_optimization_id)

Gets a campaign optimization rule specified by identifier.

**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$campaign_optimization_id = "campaign_optimization_id_example"; // string | The sp campaign optimization rule identifier.

try {
    $result = $apiInstance->getCampaignOptimizationRule($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_optimization_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->getCampaignOptimizationRule: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **campaign_optimization_id** | **string**| The sp campaign optimization rule identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\GetSPCampaignOptimizationRuleResponse**](../Model/GetSPCampaignOptimizationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOptimizationRuleEligibility**
> \AmazonAdvertisingApi\Model\SPCampaignOptimizationRecommendationAPIResponse getOptimizationRuleEligibility($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Gets a campaign optimization rule recommendation for SP campaigns.

**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SPCampaignOptimizationRecommendationsAPIRequest(); // \AmazonAdvertisingApi\Model\SPCampaignOptimizationRecommendationsAPIRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->getOptimizationRuleEligibility($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->getOptimizationRuleEligibility: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SPCampaignOptimizationRecommendationsAPIRequest**](../Model/SPCampaignOptimizationRecommendationsAPIRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\SPCampaignOptimizationRecommendationAPIResponse**](../Model/SPCampaignOptimizationRecommendationAPIResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.optimizationrules.v1+json
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRuleNotification**
> \AmazonAdvertisingApi\Model\SPCampaignOptimizationNotificationAPIResponse getRuleNotification($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Gets campaign optimization rule state. Recommended refresh frequency is once a day.

**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SPCampaignOptimizationNotificationAPIRequest(); // \AmazonAdvertisingApi\Model\SPCampaignOptimizationNotificationAPIRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->getRuleNotification($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->getRuleNotification: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SPCampaignOptimizationNotificationAPIRequest**](../Model/SPCampaignOptimizationNotificationAPIRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\SPCampaignOptimizationNotificationAPIResponse**](../Model/SPCampaignOptimizationNotificationAPIResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.optimizationrules.v1+json
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOptimizationRule**
> \AmazonAdvertisingApi\Model\UpdateSPCampaignOptimizationRuleResponse updateOptimizationRule($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Updates a campaign optimization rule.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignOptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\UpdateSPCampaignOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\UpdateSPCampaignOptimizationRulesRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->updateOptimizationRule($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignOptimizationRulesApi->updateOptimizationRule: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateSPCampaignOptimizationRulesRequest**](../Model/UpdateSPCampaignOptimizationRulesRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateSPCampaignOptimizationRuleResponse**](../Model/UpdateSPCampaignOptimizationRuleResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.optimizationrules.v1+json
 - **Accept**: application/vnd.optimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

