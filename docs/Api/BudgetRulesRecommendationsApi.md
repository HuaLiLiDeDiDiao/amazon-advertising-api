# AmazonAdvertisingApi\BudgetRulesRecommendationsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sBGetBudgetRulesRecommendation**](BudgetRulesRecommendationsApi.md#sbgetbudgetrulesrecommendation) | **POST** /sb/campaigns/budgetRules/recommendations | Gets a list of special events with suggested date range and suggested budget increase for a campaign specified by identifier.

# **sBGetBudgetRulesRecommendation**
> \AmazonAdvertisingApi\Model\SBBudgetRulesRecommendationEventResponse sBGetBudgetRulesRecommendation($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Gets a list of special events with suggested date range and suggested budget increase for a campaign specified by identifier.

A rule enables an automatic budget increase for a specified date range or for a special event. The response also includes a suggested budget increase for each special event.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\BudgetRulesRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\BudgetRulesRecommendationsBody(); // \AmazonAdvertisingApi\Model\BudgetRulesRecommendationsBody | 

try {
    $result = $apiInstance->sBGetBudgetRulesRecommendation($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRulesRecommendationsApi->sBGetBudgetRulesRecommendation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\BudgetRulesRecommendationsBody**](../Model/BudgetRulesRecommendationsBody.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBBudgetRulesRecommendationEventResponse**](../Model/SBBudgetRulesRecommendationEventResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/vnd.sbbudgetrulesrecommendation.v3+json
 - **Accept**: application/vnd.sbbudgetrulesrecommendation.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

