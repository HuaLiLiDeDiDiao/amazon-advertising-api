# AmazonAdvertisingApi\BudgetUsageApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sdCampaignsBudgetUsage**](BudgetUsageApi.md#sdcampaignsbudgetusage) | **POST** /sd/campaigns/budget/usage | Budget usage API for SD campaigns

# **sdCampaignsBudgetUsage**
> \AmazonAdvertisingApi\Model\BudgetUsageCampaignResponse sdCampaignsBudgetUsage($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Budget usage API for SD campaigns

**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetUsageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = new \AmazonAdvertisingApi\Model\null(); //  | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = new \AmazonAdvertisingApi\Model\null(); //  | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\BudgetUsageCampaignRequest(); // \AmazonAdvertisingApi\Model\BudgetUsageCampaignRequest | 

try {
    $result = $apiInstance->sdCampaignsBudgetUsage($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetUsageApi->sdCampaignsBudgetUsage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | [****](../Model/.md)| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | [****](../Model/.md)| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\BudgetUsageCampaignRequest**](../Model/BudgetUsageCampaignRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\BudgetUsageCampaignResponse**](../Model/BudgetUsageCampaignResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdcampaignbudgetusage.v1+json
 - **Accept**: application/vnd.sdcampaignbudgetusage.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

