# AmazonAdvertisingApi\OptimizationRulesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**associateOptimizationRulesToCampaign**](OptimizationRulesApi.md#associateoptimizationrulestocampaign) | **POST** /sp/campaigns/{campaignId}/optimizationRules | Associates one or multiple optimization rules with a campaign.
[**createOptimizationRules**](OptimizationRulesApi.md#createoptimizationrules) | **POST** /sp/rules/optimization | Creates one or more optimization rules.
[**searchOptimizationRules**](OptimizationRulesApi.md#searchoptimizationrules) | **POST** /sp/rules/optimization/search | Searches optimization rules based on optional filters.
[**updateOptimizationRules**](OptimizationRulesApi.md#updateoptimizationrules) | **PUT** /sp/rules/optimization | Updates one or more optimization rules.

# **associateOptimizationRulesToCampaign**
> \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignResponse associateOptimizationRulesToCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id, $body)

Associates one or multiple optimization rules with a campaign.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$campaign_id = "campaign_id_example"; // string | The sp campaign identifier.
$body = new \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignRequest(); // \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignRequest | 

try {
    $result = $apiInstance->associateOptimizationRulesToCampaign($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $campaign_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesApi->associateOptimizationRulesToCampaign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **campaign_id** | **string**| The sp campaign identifier. |
 **body** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignRequest**](../Model/OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignResponse**](../Model/OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spoptimizationrules.v1+json
 - **Accept**: application/vnd.spoptimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse createOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more optimization rules.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiCreateOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiCreateOptimizationRulesRequest | 

try {
    $result = $apiInstance->createOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesApi->createOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiCreateOptimizationRulesRequest**](../Model/OptimizationRulesAPIAmazonAdvertisingApiCreateOptimizationRulesRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse**](../Model/OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spoptimizationrules.v1+json
 - **Accept**: application/vnd.spoptimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **searchOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesResponse searchOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Searches optimization rules based on optional filters.

**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesRequest | 

try {
    $result = $apiInstance->searchOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesApi->searchOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesRequest**](../Model/OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesResponse**](../Model/OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spoptimizationrules.v1+json
 - **Accept**: application/vnd.spoptimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOptimizationRules**
> \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse updateOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more optimization rules.

**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\OptimizationRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiUpdateOptimizationRulesRequest(); // \AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiUpdateOptimizationRulesRequest | 

try {
    $result = $apiInstance->updateOptimizationRules($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OptimizationRulesApi->updateOptimizationRules: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiUpdateOptimizationRulesRequest**](../Model/OptimizationRulesAPIAmazonAdvertisingApiUpdateOptimizationRulesRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse**](../Model/OptimizationRulesAPIAmazonAdvertisingApiOptimizationRulesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spoptimizationrules.v1+json
 - **Accept**: application/vnd.spoptimizationrules.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

