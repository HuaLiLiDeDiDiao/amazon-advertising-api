# AmazonAdvertisingApi\CreateTestAccountApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAccount**](CreateTestAccountApi.md#createaccount) | **POST** /testAccounts | API to create test accounts

# **createAccount**
> \AmazonAdvertisingApi\Model\CreateAccountResponse createAccount($body, $amazon_advertising_api_client_id)

API to create test accounts

Submit a account creation request. You can create up to 1 test account type per marketplace.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CreateTestAccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateAccountRequest(); // \AmazonAdvertisingApi\Model\CreateAccountRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.

try {
    $result = $apiInstance->createAccount($body, $amazon_advertising_api_client_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateTestAccountApi->createAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateAccountRequest**](../Model/CreateAccountRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateAccountResponse**](../Model/CreateAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

