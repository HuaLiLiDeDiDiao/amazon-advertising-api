# AmazonAdvertisingApi\AdGroupsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveAdGroup**](AdGroupsApi.md#archiveadgroup) | **DELETE** /sd/adGroups/{adGroupId} | Sets the ad group status to archived.
[**createAdGroups**](AdGroupsApi.md#createadgroups) | **POST** /sd/adGroups | Creates one or more ad groups.
[**getAdGroup**](AdGroupsApi.md#getadgroup) | **GET** /sd/adGroups/{adGroupId} | Gets a requested ad group.
[**getAdGroupResponseEx**](AdGroupsApi.md#getadgroupresponseex) | **GET** /sd/adGroups/extended/{adGroupId} | Gets extended information for a requested ad group.
[**listAdGroups**](AdGroupsApi.md#listadgroups) | **GET** /sd/adGroups | Gets a list of ad groups.
[**listAdGroupsEx**](AdGroupsApi.md#listadgroupsex) | **GET** /sd/adGroups/extended | Gets a list of ad groups with extended fields.
[**updateAdGroups**](AdGroupsApi.md#updateadgroups) | **PUT** /sd/adGroups | Updates on or more ad groups.

# **archiveAdGroup**
> \AmazonAdvertisingApi\Model\AdGroupResponse archiveAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id)

Sets the ad group status to archived.

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/en-us/info/developer-notes#archiving) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 789; // int | The identifier of the requested ad group.

try {
    $result = $apiInstance->archiveAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->archiveAdGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **int**| The identifier of the requested ad group. |

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupResponse**](../Model/AdGroupResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createAdGroups**
> \AmazonAdvertisingApi\Model\AdGroupResponse[] createAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more ad groups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateAdGroup()); // \AmazonAdvertisingApi\Model\CreateAdGroup[] | An array of AdGroup objects. For each object, specify required fields and their values. Required fields are `campaignId`, `name`, `state`, and `defaultBid`. Maximum length of the array is 100 objects. Note - when using landingPageType of OFF_AMAZON_LINK or STORES within productAds, only 1 adGroup is supported.

try {
    $result = $apiInstance->createAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->createAdGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateAdGroup[]**](../Model/CreateAdGroup.md)| An array of AdGroup objects. For each object, specify required fields and their values. Required fields are &#x60;campaignId&#x60;, &#x60;name&#x60;, &#x60;state&#x60;, and &#x60;defaultBid&#x60;. Maximum length of the array is 100 objects. Note - when using landingPageType of OFF_AMAZON_LINK or STORES within productAds, only 1 adGroup is supported. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupResponse[]**](../Model/AdGroupResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAdGroup**
> \AmazonAdvertisingApi\Model\AdGroup getAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id)

Gets a requested ad group.

Returns an AdGroup object for a requested campaign. Note that the AdGroup object is designed for performance, with a small set of commonly used ad group fields to reduce size. If the extended set of fields is required, use the campaign operations that return the AdGroupResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 789; // int | The identifier of the requested ad group.

try {
    $result = $apiInstance->getAdGroup($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->getAdGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **int**| The identifier of the requested ad group. |

### Return type

[**\AmazonAdvertisingApi\Model\AdGroup**](../Model/AdGroup.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAdGroupResponseEx**
> \AmazonAdvertisingApi\Model\AdGroupResponseEx getAdGroupResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id)

Gets extended information for a requested ad group.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 789; // int | The identifier of the requested ad group.

try {
    $result = $apiInstance->getAdGroupResponseEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->getAdGroupResponseEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **int**| The identifier of the requested ad group. |

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupResponseEx**](../Model/AdGroupResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listAdGroups**
> \AmazonAdvertisingApi\Model\AdGroup[] listAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $campaign_id_filter, $ad_group_id_filter, $name)

Gets a list of ad groups.

Gets an array of AdGroup objects for a requested set of Sponsored Display ad groups. Note that the AdGroup object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use the ad group operations that return the AdGroupResponseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of AdGroup objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten ad groups set `startIndex=0` and `count=10`. To return the next ten ad groups, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only ad groups with state set to one of the values in the specified comma-delimited list.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array is filtered to include only ad groups associated with the campaign identifiers in the specified comma-delimited list.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional. The returned array is filtered to include only ad groups with an identifier specified in the comma-delimited list.
$name = "name_example"; // string | Optional. The returned array includes only ad groups with the specified name.

try {
    $result = $apiInstance->listAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $campaign_id_filter, $ad_group_id_filter, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->listAdGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of campaigns. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of AdGroup objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten ad groups set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten ad groups, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only ad groups with state set to one of the values in the specified comma-delimited list. | [optional] [default to enabled, paused, archived]
 **campaign_id_filter** | **string**| Optional. The returned array is filtered to include only ad groups associated with the campaign identifiers in the specified comma-delimited list. | [optional]
 **ad_group_id_filter** | **string**| Optional. The returned array is filtered to include only ad groups with an identifier specified in the comma-delimited list. | [optional]
 **name** | **string**| Optional. The returned array includes only ad groups with the specified name. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroup[]**](../Model/AdGroup.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listAdGroupsEx**
> \AmazonAdvertisingApi\Model\AdGroupResponseEx[] listAdGroupsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $campaign_id_filter, $ad_group_id_filter, $name)

Gets a list of ad groups with extended fields.

Gets an array of AdGroupResponseEx objects for a set of requested ad groups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. Sets a cursor into the requested set of ad groups. Use in conjunction with the `count` parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0.
$count = 56; // int | Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten campaigns set `startIndex=0` and `count=10`. To return the next ten campaigns, set `startIndex=10` and `count=10`, and so on. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. The returned array is filtered to include only campaigns with state set to one of the values in the comma-delimited list.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. The returned array is filtered to include only ad groups associated with the campaign identifiers in the comma-delimited list.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional. The returned array is filtered to include only ad groups with an identifier specified in the comma-delimited list.
$name = "name_example"; // string | Optional. The returned array includes only ad groups with the specified name.

try {
    $result = $apiInstance->listAdGroupsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $campaign_id_filter, $ad_group_id_filter, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->listAdGroupsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. Sets a cursor into the requested set of ad groups. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. 0-indexed record offset for the result set, defaults to 0. | [optional]
 **count** | **int**| Optional. Sets the number of Campaign objects in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten campaigns set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten campaigns, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. The returned array is filtered to include only campaigns with state set to one of the values in the comma-delimited list. | [optional] [default to enabled, paused, archived]
 **campaign_id_filter** | **string**| Optional. The returned array is filtered to include only ad groups associated with the campaign identifiers in the comma-delimited list. | [optional]
 **ad_group_id_filter** | **string**| Optional. The returned array is filtered to include only ad groups with an identifier specified in the comma-delimited list. | [optional]
 **name** | **string**| Optional. The returned array includes only ad groups with the specified name. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupResponseEx[]**](../Model/AdGroupResponseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAdGroups**
> \AmazonAdvertisingApi\Model\AdGroupResponse[] updateAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates on or more ad groups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\AdGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateAdGroup()); // \AmazonAdvertisingApi\Model\UpdateAdGroup[] | An array of AdGroup objects. For each object, specify an ad group identifier and mutable fields with their updated values. The mutable fields are 'name', 'defaultBid', and 'state'. Maximum length of the array is 100 objects.

try {
    $result = $apiInstance->updateAdGroups($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdGroupsApi->updateAdGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateAdGroup[]**](../Model/UpdateAdGroup.md)| An array of AdGroup objects. For each object, specify an ad group identifier and mutable fields with their updated values. The mutable fields are &#x27;name&#x27;, &#x27;defaultBid&#x27;, and &#x27;state&#x27;. Maximum length of the array is 100 objects. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupResponse[]**](../Model/AdGroupResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

