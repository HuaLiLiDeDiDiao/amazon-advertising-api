# AmazonAdvertisingApi\InvoiceApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAdvertiserInvoices**](InvoiceApi.md#getadvertiserinvoices) | **GET** /invoices | Get invoices for advertiser
[**getInvoice**](InvoiceApi.md#getinvoice) | **GET** /invoices/{invoiceId} | Get invoice data by invoice ID

# **getAdvertiserInvoices**
> \AmazonAdvertisingApi\Model\InlineResponse200 getAdvertiserInvoices($invoice_statuses, $start_date, $end_date, $count, $cursor)

Get invoices for advertiser

**Requires one of these permissions**: [\"nemo_transactions_view\",\"nemo_transactions_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$invoice_statuses = array("invoice_statuses_example"); // string[] | * `ISSUED`: An invoice is issued when its charges are finalized and tax is computed on the total amount.  * `PAID_IN_PART`: When a partial payment is received, the invoice status will change to paid in part. * `PAID_IN_PART`: One full payment has been received, the invoice will be paid in full. * `WRITTEN_OFF`: If an invoice is written off because of an error, the status will be updated to written off.
$start_date = new \AmazonAdvertisingApi\Model\null(); //  | The starting date (inclusive) of the date range for filtering invoices. Please provide the date in ISO-8601 format, representing a UTC date with only the date portion (no time).
$end_date = new \AmazonAdvertisingApi\Model\null(); //  | The ending date (inclusive) of the date range for filtering invoices. Please provide the date in ISO-8601 format, representing a UTC date with only the date portion (no time).
$count = 56; // int | Number of records to include in the paged response. Defaults to 100. Cannot be combined with the cursor parameter.
$cursor = "cursor_example"; // string | A cursor representing how far into a result set this query should begin. In the absence of a cursor the request will default to start index of 0 and page size of 100.

try {
    $result = $apiInstance->getAdvertiserInvoices($invoice_statuses, $start_date, $end_date, $count, $cursor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->getAdvertiserInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_statuses** | [**string[]**](../Model/string.md)| * &#x60;ISSUED&#x60;: An invoice is issued when its charges are finalized and tax is computed on the total amount.  * &#x60;PAID_IN_PART&#x60;: When a partial payment is received, the invoice status will change to paid in part. * &#x60;PAID_IN_PART&#x60;: One full payment has been received, the invoice will be paid in full. * &#x60;WRITTEN_OFF&#x60;: If an invoice is written off because of an error, the status will be updated to written off. | [optional]
 **start_date** | [****](../Model/.md)| The starting date (inclusive) of the date range for filtering invoices. Please provide the date in ISO-8601 format, representing a UTC date with only the date portion (no time). | [optional]
 **end_date** | [****](../Model/.md)| The ending date (inclusive) of the date range for filtering invoices. Please provide the date in ISO-8601 format, representing a UTC date with only the date portion (no time). | [optional]
 **count** | **int**| Number of records to include in the paged response. Defaults to 100. Cannot be combined with the cursor parameter. | [optional]
 **cursor** | **string**| A cursor representing how far into a result set this query should begin. In the absence of a cursor the request will default to start index of 0 and page size of 100. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.invoices.v1.1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInvoice**
> \AmazonAdvertisingApi\Model\Invoice getInvoice($invoice_id)

Get invoice data by invoice ID

**Requires one of these permissions**: [\"nemo_transactions_view\",\"nemo_transactions_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\InvoiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$invoice_id = "invoice_id_example"; // string | ID of invoice to fetch

try {
    $result = $apiInstance->getInvoice($invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InvoiceApi->getInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **string**| ID of invoice to fetch |

### Return type

[**\AmazonAdvertisingApi\Model\Invoice**](../Model/Invoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.invoice.v1.1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

