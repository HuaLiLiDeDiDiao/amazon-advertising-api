# AmazonAdvertisingApi\BillingStatusApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulkGetBillingStatus**](BillingStatusApi.md#bulkgetbillingstatus) | **POST** /billing/statuses | Get the billing status for a list of advertising accounts.

# **bulkGetBillingStatus**
> \AmazonAdvertisingApi\Model\BulkGetBillingStatusResponse bulkGetBillingStatus($body)

Get the billing status for a list of advertising accounts.

Gets the current billing status associated for each advertising account.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"adv_billing_view\",\"adv_billing_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\BillingStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\BulkGetBillingStatusesRequestBody(); // \AmazonAdvertisingApi\Model\BulkGetBillingStatusesRequestBody | 

try {
    $result = $apiInstance->bulkGetBillingStatus($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingStatusApi->bulkGetBillingStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\BulkGetBillingStatusesRequestBody**](../Model/BulkGetBillingStatusesRequestBody.md)|  |

### Return type

[**\AmazonAdvertisingApi\Model\BulkGetBillingStatusResponse**](../Model/BulkGetBillingStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.bulkgetbillingstatusrequestbody.v1+json
 - **Accept**: application/vnd.bulkgetbillingstatusresponse.v1+json, application/vnd.bulkgetbillingstatuserrorresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

