# AmazonAdvertisingApi\SnapshotsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSnapshot**](SnapshotsApi.md#createsnapshot) | **POST** /sd/{recordType}/snapshot | Request a file-based snapshot of all entities of the specified type in the account satisfying the filtering criteria
[**downloadSnapshot**](SnapshotsApi.md#downloadsnapshot) | **GET** /sd/snapshots/{snapshotId}/download | Download previously requested snapshot
[**getSnapshot**](SnapshotsApi.md#getsnapshot) | **GET** /sd/snapshots/{snapshotId} | Retrieve status, metadata, and location of previously requested snapshot

# **createSnapshot**
> \AmazonAdvertisingApi\Model\SnapshotResponse createSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $record_type, $body)

Request a file-based snapshot of all entities of the specified type in the account satisfying the filtering criteria

**To understand the call flow for asynchronous snapshots, see [Getting started with sponsored ads snapshots](/API/docs/en-us/guides/snapshots/get-started).**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\SnapshotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$record_type = "record_type_example"; // string | The type of entity for which the snapshot should be generated. Must be one of: `campaigns`, `adgroups`, `productAds`, or `targets`.
$body = new \AmazonAdvertisingApi\Model\SnapshotRequest(); // \AmazonAdvertisingApi\Model\SnapshotRequest | Request a snapshot file for all entities of a single record type.

try {
    $result = $apiInstance->createSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $record_type, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SnapshotsApi->createSnapshot: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **record_type** | **string**| The type of entity for which the snapshot should be generated. Must be one of: &#x60;campaigns&#x60;, &#x60;adgroups&#x60;, &#x60;productAds&#x60;, or &#x60;targets&#x60;. |
 **body** | [**\AmazonAdvertisingApi\Model\SnapshotRequest**](../Model/SnapshotRequest.md)| Request a snapshot file for all entities of a single record type. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SnapshotResponse**](../Model/SnapshotResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **downloadSnapshot**
> downloadSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $snapshot_id)

Download previously requested snapshot

**To understand the call flow for asynchronous snapshots, see [Getting started with sponsored ads snapshots](/API/docs/en-us/guides/snapshots/overview).**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\SnapshotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$snapshot_id = "snapshot_id_example"; // string | The Snapshot identifier.

try {
    $apiInstance->downloadSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $snapshot_id);
} catch (Exception $e) {
    echo 'Exception when calling SnapshotsApi->downloadSnapshot: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **snapshot_id** | **string**| The Snapshot identifier. |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSnapshot**
> \AmazonAdvertisingApi\Model\SnapshotResponse getSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $snapshot_id)

Retrieve status, metadata, and location of previously requested snapshot

**To understand the call flow for asynchronous snapshots, see [Getting started with sponsored ads snapshots](/API/docs/en-us/guides/snapshots/get-started).**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\SnapshotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$snapshot_id = "snapshot_id_example"; // string | The Snapshot identifier.

try {
    $result = $apiInstance->getSnapshot($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $snapshot_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SnapshotsApi->getSnapshot: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **snapshot_id** | **string**| The Snapshot identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\SnapshotResponse**](../Model/SnapshotResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

