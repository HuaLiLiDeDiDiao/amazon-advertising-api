# AmazonAdvertisingApi\AdvertiserApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountBudgetFeatureFlags**](AdvertiserApi.md#getaccountbudgetfeatureflags) | **GET** /accountBudgets/featureFlags | Gets account budget feature flags information.
[**updateAccountBudgetFeatureFlags**](AdvertiserApi.md#updateaccountbudgetfeatureflags) | **POST** /accountBudgets/featureFlags | Creates or Updates account budget feature flags information.

# **getAccountBudgetFeatureFlags**
> \AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse getAccountBudgetFeatureFlags($amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Gets account budget feature flags information.

Gets account budget feature flags information.  **Requires one of these permissions**: [\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdvertiserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->getAccountBudgetFeatureFlags($amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdvertiserApi->getAccountBudgetFeatureFlags: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\GetAccountBudgetFeatureFlagsResponse**](../Model/GetAccountBudgetFeatureFlagsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.accountBudgetFeatureFlags.v1+json, application/vnd.accountBudgetFeatureFlagsError.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAccountBudgetFeatureFlags**
> \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse updateAccountBudgetFeatureFlags($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Creates or Updates account budget feature flags information.

Creates or Updates account budget feature flags information.  **Requires one of these permissions**: [\"advertiser_campaign_view\",\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\AdvertiserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest(); // \AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->updateAccountBudgetFeatureFlags($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdvertiserApi->updateAccountBudgetFeatureFlags: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsRequest**](../Model/UpdateAccountBudgetFeatureFlagsRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateAccountBudgetFeatureFlagsResponse**](../Model/UpdateAccountBudgetFeatureFlagsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.accountBudgetFeatureFlags.v1+json
 - **Accept**: application/vnd.accountBudgetFeatureFlags.v1+json, application/vnd.accountBudgetFeatureFlagsError.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

