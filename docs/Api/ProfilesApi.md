# AmazonAdvertisingApi\ProfilesApi

All URIs are relative to *https://advertising-api.amazon.com (North America)*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProfileById**](ProfilesApi.md#getprofilebyid) | **GET** /v2/profiles/{profileId} | Gets a profile specified by identifier.
[**listProfiles**](ProfilesApi.md#listprofiles) | **GET** /v2/profiles | Gets a list of profiles.
[**updateProfiles**](ProfilesApi.md#updateprofiles) | **PUT** /v2/profiles | Update the daily budget for one or more profiles.

# **getProfileById**
> \AmazonAdvertisingApi\Model\Profile getProfileById($amazon_advertising_api_client_id, $profile_id)

Gets a profile specified by identifier.

This operation does not return a response unless the current account has created at least one campaign using the advertising console.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProfilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$profile_id = 789; // int | 

try {
    $result = $apiInstance->getProfileById($amazon_advertising_api_client_id, $profile_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProfilesApi->getProfileById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **profile_id** | **int**|  |

### Return type

[**\AmazonAdvertisingApi\Model\Profile**](../Model/Profile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProfiles**
> \AmazonAdvertisingApi\Model\Profile[] listProfiles($amazon_advertising_api_client_id, $api_program, $access_level, $profile_type_filter, $valid_payment_method_filter)

Gets a list of profiles.

Note that this operation does not return a response unless the current account has created at least one campaign using the advertising console.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProfilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$api_program = "campaign"; // string | Filters response to include profiles that have permissions for the specified Advertising API program only. Setting `apiProgram=billing` filters the response to include only profiles to which the user and application associated with the access token have permission to view or edit billing information.
$access_level = "edit"; // string | Filters response to include profiles that have specified permissions for the specified Advertising API program only. Currently, the only supported access level is `view` and `edit`. Setting `accessLevel=view` filters the response to include only profiles to which the user and application associated with the access token have view permission to the provided api program.
$profile_type_filter = "profile_type_filter_example"; // string | Filters response to include profiles that are of the specified types in the comma-delimited list. Default is all types. Note that this filter performs an inclusive AND operation on the types.
$valid_payment_method_filter = "valid_payment_method_filter_example"; // string | Filter response to include profiles that have valid payment methods. Default is to include all profiles. Setting this filter to `true` returns only profiles with either no `validPaymentMethod` field, or the `validPaymentMethod` field set to `true`.  Setting this to `false` returns profiles with the `validPaymentMethod` field set to `false` only.

try {
    $result = $apiInstance->listProfiles($amazon_advertising_api_client_id, $api_program, $access_level, $profile_type_filter, $valid_payment_method_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProfilesApi->listProfiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **api_program** | **string**| Filters response to include profiles that have permissions for the specified Advertising API program only. Setting &#x60;apiProgram&#x3D;billing&#x60; filters the response to include only profiles to which the user and application associated with the access token have permission to view or edit billing information. | [optional] [default to campaign]
 **access_level** | **string**| Filters response to include profiles that have specified permissions for the specified Advertising API program only. Currently, the only supported access level is &#x60;view&#x60; and &#x60;edit&#x60;. Setting &#x60;accessLevel&#x3D;view&#x60; filters the response to include only profiles to which the user and application associated with the access token have view permission to the provided api program. | [optional] [default to edit]
 **profile_type_filter** | **string**| Filters response to include profiles that are of the specified types in the comma-delimited list. Default is all types. Note that this filter performs an inclusive AND operation on the types. | [optional]
 **valid_payment_method_filter** | **string**| Filter response to include profiles that have valid payment methods. Default is to include all profiles. Setting this filter to &#x60;true&#x60; returns only profiles with either no &#x60;validPaymentMethod&#x60; field, or the &#x60;validPaymentMethod&#x60; field set to &#x60;true&#x60;.  Setting this to &#x60;false&#x60; returns profiles with the &#x60;validPaymentMethod&#x60; field set to &#x60;false&#x60; only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Profile[]**](../Model/Profile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProfiles**
> \AmazonAdvertisingApi\Model\ProfileResponse[] updateProfiles($amazon_advertising_api_client_id, $body)

Update the daily budget for one or more profiles.

Note that this operation is only used for Sellers using Sponsored Products. This operation is not enabled for vendor type accounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ProfilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$body = array(new \AmazonAdvertisingApi\Model\Profile()); // \AmazonAdvertisingApi\Model\Profile[] | 

try {
    $result = $apiInstance->updateProfiles($amazon_advertising_api_client_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProfilesApi->updateProfiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **body** | [**\AmazonAdvertisingApi\Model\Profile[]**](../Model/Profile.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProfileResponse[]**](../Model/ProfileResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

