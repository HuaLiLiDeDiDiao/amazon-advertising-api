# AmazonAdvertisingApi\BillingNotificationsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulkGetBillingNotifications**](BillingNotificationsApi.md#bulkgetbillingnotifications) | **POST** /billing/notifications | Get the billing notifications for a list advertising accounts.

# **bulkGetBillingNotifications**
> \AmazonAdvertisingApi\Model\BulkGetBillingNotificationsResponse bulkGetBillingNotifications($body)

Get the billing notifications for a list advertising accounts.

Gets an array of all currently valid billing notifications associated for each advertising account.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"adv_billing_view\",\"adv_billing_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\BillingNotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\BulkGetBillingNotificationsRequestBody(); // \AmazonAdvertisingApi\Model\BulkGetBillingNotificationsRequestBody | 

try {
    $result = $apiInstance->bulkGetBillingNotifications($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingNotificationsApi->bulkGetBillingNotifications: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\BulkGetBillingNotificationsRequestBody**](../Model/BulkGetBillingNotificationsRequestBody.md)|  |

### Return type

[**\AmazonAdvertisingApi\Model\BulkGetBillingNotificationsResponse**](../Model/BulkGetBillingNotificationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.billingnotifications.v1+json
 - **Accept**: application/vnd.bulkgetbillingnotificationsresponse.v1+json, application/vnd.bulkgetbillingnotificationserrorresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

