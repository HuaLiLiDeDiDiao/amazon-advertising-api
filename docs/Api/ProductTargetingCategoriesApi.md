# AmazonAdvertisingApi\ProductTargetingCategoriesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sBTargetingGetRefinementsForCategory**](ProductTargetingCategoriesApi.md#sbtargetinggetrefinementsforcategory) | **GET** /sb/targets/categories/{categoryRefinementId}/refinements | Get refinements for category
[**sBTargetingGetTargetableASINCounts**](ProductTargetingCategoriesApi.md#sbtargetinggettargetableasincounts) | **POST** /sb/targets/products/count | Get number of products in a category
[**sBTargetingGetTargetableCategories**](ProductTargetingCategoriesApi.md#sbtargetinggettargetablecategories) | **GET** /sb/targets/categories | Get targetable categories

# **sBTargetingGetRefinementsForCategory**
> \AmazonAdvertisingApi\Model\SBTargetingGetRefinementsForCategoryResponseContent sBTargetingGetRefinementsForCategory($category_refinement_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $locale, $next_token)

Get refinements for category

Returns refinements according to category input.   Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$category_refinement_id = "category_refinement_id_example"; // string | The category refinement id. Please use /sb/targets/categories or /sb/recommendations/targets/category to retrieve category IDs.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a `Login with Amazon` account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$locale = new \AmazonAdvertisingApi\Model\SBTargetingLocale(); // \AmazonAdvertisingApi\Model\SBTargetingLocale | The locale to which the caller wishes to translate the targetable categories or refinements to. For example, if the caller wishes to receive the targetable categories in Simplified Chinese, the locale parameter should be set to zh_CN. If no locale is provided, the returned tagetable categories will be in the default language of the marketplace.
$next_token = "next_token_example"; // string | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.

try {
    $result = $apiInstance->sBTargetingGetRefinementsForCategory($category_refinement_id, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $locale, $next_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingCategoriesApi->sBTargetingGetRefinementsForCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_refinement_id** | **string**| The category refinement id. Please use /sb/targets/categories or /sb/recommendations/targets/category to retrieve category IDs. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x60;Login with Amazon&#x60; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **locale** | [**\AmazonAdvertisingApi\Model\SBTargetingLocale**](../Model/.md)| The locale to which the caller wishes to translate the targetable categories or refinements to. For example, if the caller wishes to receive the targetable categories in Simplified Chinese, the locale parameter should be set to zh_CN. If no locale is provided, the returned tagetable categories will be in the default language of the marketplace. | [optional]
 **next_token** | **string**| Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingGetRefinementsForCategoryResponseContent**](../Model/SBTargetingGetRefinementsForCategoryResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbtargeting.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sBTargetingGetTargetableASINCounts**
> \AmazonAdvertisingApi\Model\SBTargetingGetTargetableASINCountsResponseContent sBTargetingGetTargetableASINCounts($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Get number of products in a category

Get number of targetable asins based on refinements provided by the user.  Use `/sb/targets/categories` or `/sb/recommendations/targets/category` to retrieve the category ID. Use `/sb/targets/categories/{categoryRefinementId}/refinements` to retrieve refinements data for a category.  Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SBTargetingGetTargetableASINCountsRequestContent(); // \AmazonAdvertisingApi\Model\SBTargetingGetTargetableASINCountsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a `Login with Amazon` account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.

try {
    $result = $apiInstance->sBTargetingGetTargetableASINCounts($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingCategoriesApi->sBTargetingGetTargetableASINCounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SBTargetingGetTargetableASINCountsRequestContent**](../Model/SBTargetingGetTargetableASINCountsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x60;Login with Amazon&#x60; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingGetTargetableASINCountsResponseContent**](../Model/SBTargetingGetTargetableASINCountsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbtargeting.v4+json
 - **Accept**: application/vnd.sbtargeting.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sBTargetingGetTargetableCategories**
> \AmazonAdvertisingApi\Model\SBTargetingGetTargetableCategoriesResponseContent sBTargetingGetTargetableCategories($supply_source, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $locale, $include_only_root_categories, $parent_category_refinement_id, $next_token)

Get targetable categories

Returns all targetable categories by default in a list. List of categories can be used to build and traverse category tree. Set query parameter `includeOnlyRootCategories=true` to return only the root categories, or set `parentCategoryRefinementId` to return children of a specific parent category. Each category node has the fields - category name, category refinement id, parent category refinement id, isTargetable flag, and ASIN count range.   Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ProductTargetingCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$supply_source = new \AmazonAdvertisingApi\Model\SBTargetingSupplySource(); // \AmazonAdvertisingApi\Model\SBTargetingSupplySource | The supply source where the target will be used. Use `AMAZON` for placements on Amazon website. Use `STREAMING_VIDEO` for off-site video placements such as IMDb TV.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a `Login with Amazon` account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$locale = new \AmazonAdvertisingApi\Model\SBTargetingLocale(); // \AmazonAdvertisingApi\Model\SBTargetingLocale | The locale to which the caller wishes to translate the targetable categories or refinements to. For example, if the caller wishes to receive the targetable categories in Simplified Chinese, the locale parameter should be set to zh_CN. If no locale is provided, the returned tagetable categories will be in the default language of the marketplace.
$include_only_root_categories = true; // bool | Indicates whether to only retun root categories or not.
$parent_category_refinement_id = "parent_category_refinement_id_example"; // string | Returns child categories of category.
$next_token = "next_token_example"; // string | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.

try {
    $result = $apiInstance->sBTargetingGetTargetableCategories($supply_source, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $locale, $include_only_root_categories, $parent_category_refinement_id, $next_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductTargetingCategoriesApi->sBTargetingGetTargetableCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supply_source** | [**\AmazonAdvertisingApi\Model\SBTargetingSupplySource**](../Model/.md)| The supply source where the target will be used. Use &#x60;AMAZON&#x60; for placements on Amazon website. Use &#x60;STREAMING_VIDEO&#x60; for off-site video placements such as IMDb TV. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x60;Login with Amazon&#x60; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **locale** | [**\AmazonAdvertisingApi\Model\SBTargetingLocale**](../Model/.md)| The locale to which the caller wishes to translate the targetable categories or refinements to. For example, if the caller wishes to receive the targetable categories in Simplified Chinese, the locale parameter should be set to zh_CN. If no locale is provided, the returned tagetable categories will be in the default language of the marketplace. | [optional]
 **include_only_root_categories** | **bool**| Indicates whether to only retun root categories or not. | [optional]
 **parent_category_refinement_id** | **string**| Returns child categories of category. | [optional]
 **next_token** | **string**| Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingGetTargetableCategoriesResponseContent**](../Model/SBTargetingGetTargetableCategoriesResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbtargeting.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

