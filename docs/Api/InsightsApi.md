# AmazonAdvertisingApi\InsightsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sBInsightsCampaignInsights**](InsightsApi.md#sbinsightscampaigninsights) | **POST** /sb/campaigns/insights | Get insights for campaigns

# **sBInsightsCampaignInsights**
> \AmazonAdvertisingApi\Model\SBInsightsCampaignInsightsResponseContent sBInsightsCampaignInsights($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token)

Get insights for campaigns

Creates campaign level insights. Insights will be provided for passed in campaign parameters.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\InsightsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SBInsightsCampaignInsightsRequestContent(); // \AmazonAdvertisingApi\Model\SBInsightsCampaignInsightsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$next_token = "next_token_example"; // string | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.

try {
    $result = $apiInstance->sBInsightsCampaignInsights($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InsightsApi->sBInsightsCampaignInsights: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SBInsightsCampaignInsightsRequestContent**](../Model/SBInsightsCampaignInsightsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **next_token** | **string**| Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBInsightsCampaignInsightsResponseContent**](../Model/SBInsightsCampaignInsightsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbinsights.v4+json
 - **Accept**: application/vnd.sbinsights.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

