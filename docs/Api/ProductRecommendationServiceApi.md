# AmazonAdvertisingApi\ProductRecommendationServiceApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductRecommendations**](ProductRecommendationServiceApi.md#getproductrecommendations) | **POST** /sp/targets/products/recommendations | Suggested target ASINs for your advertised product

# **getProductRecommendations**
> \AmazonAdvertisingApi\Model\ProductRecommendationsByTheme getProductRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $amazon_advertising_api_advertiser_id)

Suggested target ASINs for your advertised product

Given an advertised ASIN as input, this API returns suggested ASINs to target in a product targeting campaign. We use various methods to generate these suggestions. These include using historical performance of your ad, items that shoppers they frequently view and purchase together, etc. The suggested targets can be retrieved either as a single list, or grouped by ‘theme' – i.e. an accompanying context for why we recommend the items. You can pick the desired format using the Accepts header, please see the response mediaTypes for more information. </br> <h4>Pagination Behavior</h4> The API supports cursor based pagination using encoded cursor values to return next set of records or previously served records. The <b>count</b> parameter in the request body will be used to determine the size of results when requesting the previous page or next page. If no value for <b>count</b> is passed in the request, a default value is assumed. Please refer the range and defaults for these values in the request schema under GetProductRecommendationsRequest. </br> <i><b>Note:</b> The clients should never cache pagination cursor values locally as these values will expire after a certain time period. However a cursor value can be reused to perform retries in case of failures as long as the value has not expired. </br></br> <h4>Themes </h4> Themes provide additional context for why we are recommending a product as a target. See below for an overall list of themes currently available –  </br><b>- Top converting targets</b> – These ASINs generated conversions for the input ASIN in the past 30 days (e.g. your product appeared as an ad on the detail page of these items, and a shopper clicked and purchased your item). The suggested ASINs under this theme are sorted in decreasing order of sales generated for your promoted item. </br><b>- Similar items (frequently viewed together)</b> – Items that shoppers frequently view and click along with your advertised item during a shopping session. </br><b>- Complements</b> – Items that are frequently purchased together as complements. For example, if you are promoting a tennis racquet, you may see tennis balls recommended under this theme. </br><b>- Similar items with low ratings and reviews</b> – Subset of the ‘similar items’ theme containing items that are rated lower than 3 stars and/or with fewer than 5 reviews. </br><b>- Other books read by your readers</b> – Items that shoppers frequently view and click along with your advertised item during a shopping session. </br></br><i><b>Note:</b> Availability of themes differs by input ASIN - some ASINs may not have all above themes available</i>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ProductRecommendationServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$body = new \AmazonAdvertisingApi\Model\GetProductRecommendationsRequest(); // \AmazonAdvertisingApi\Model\GetProductRecommendationsRequest | 
$amazon_advertising_api_advertiser_id = "amazon_advertising_api_advertiser_id_example"; // string | The Advertiser ID associated with the advertiser account.

try {
    $result = $apiInstance->getProductRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $amazon_advertising_api_advertiser_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRecommendationServiceApi->getProductRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **body** | [**\AmazonAdvertisingApi\Model\GetProductRecommendationsRequest**](../Model/GetProductRecommendationsRequest.md)|  | [optional]
 **amazon_advertising_api_advertiser_id** | **string**| The Advertiser ID associated with the advertiser account. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\ProductRecommendationsByTheme**](../Model/ProductRecommendationsByTheme.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spproductrecommendation.v3+json
 - **Accept**: application/vnd.spproductrecommendationresponse.themes.v3+json, application/vnd.spproductrecommendationresponse.asins.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

