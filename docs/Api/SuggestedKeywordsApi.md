# AmazonAdvertisingApi\SuggestedKeywordsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bulkGetAsinSuggestedKeywords**](SuggestedKeywordsApi.md#bulkgetasinsuggestedkeywords) | **POST** /v2/sp/asins/suggested/keywords | Gets suggested keyword for a specified list of ASINs.
[**getAdGroupSuggestedKeywords**](SuggestedKeywordsApi.md#getadgroupsuggestedkeywords) | **GET** /v2/sp/adGroups/{adGroupId}/suggested/keywords | Gets suggested keywords for the specified ad group.
[**getAdGroupSuggestedKeywordsEx**](SuggestedKeywordsApi.md#getadgroupsuggestedkeywordsex) | **GET** /v2/sp/adGroups/{adGroupId}/suggested/keywords/extended | Gets suggested keywords with extended data for the specified ad group.
[**getAsinSuggestedKeywords**](SuggestedKeywordsApi.md#getasinsuggestedkeywords) | **GET** /v2/sp/asins/{asinValue}/suggested/keywords | Gets suggested keywords for the specified ASIN.

# **bulkGetAsinSuggestedKeywords**
> \AmazonAdvertisingApi\Model\BulkGetAsinSuggestedKeywordsResponse bulkGetAsinSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Gets suggested keyword for a specified list of ASINs.

Suggested keywords are returned in an array ordered by descending effectiveness.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\SuggestedKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" developer account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SuggestedKeywordsBody(); // \AmazonAdvertisingApi\Model\SuggestedKeywordsBody | 

try {
    $result = $apiInstance->bulkGetAsinSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuggestedKeywordsApi->bulkGetAsinSuggestedKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; developer account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SuggestedKeywordsBody**](../Model/SuggestedKeywordsBody.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\BulkGetAsinSuggestedKeywordsResponse**](../Model/BulkGetAsinSuggestedKeywordsResponse.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAdGroupSuggestedKeywords**
> \AmazonAdvertisingApi\Model\AdGroupSuggestedKeywordsResponse getAdGroupSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $max_num_suggestions, $ad_state_filter)

Gets suggested keywords for the specified ad group.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\SuggestedKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" developer account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 1.2; // float | The identifier of a valid ad group.
$max_num_suggestions = 100; // int | The maximum number of suggested keywords for the response.
$ad_state_filter = "ad_state_filter_example"; // string | Filters results to ad groups with state matching the comma-delimited list.

try {
    $result = $apiInstance->getAdGroupSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $max_num_suggestions, $ad_state_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuggestedKeywordsApi->getAdGroupSuggestedKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; developer account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **float**| The identifier of a valid ad group. |
 **max_num_suggestions** | **int**| The maximum number of suggested keywords for the response. | [optional] [default to 100]
 **ad_state_filter** | **string**| Filters results to ad groups with state matching the comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupSuggestedKeywordsResponse**](../Model/AdGroupSuggestedKeywordsResponse.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAdGroupSuggestedKeywordsEx**
> \AmazonAdvertisingApi\Model\AdGroupSuggestedKeywordsResponseEx[] getAdGroupSuggestedKeywordsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $max_num_suggestions, $suggest_bids, $ad_state_filter)

Gets suggested keywords with extended data for the specified ad group.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\SuggestedKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" developer account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$ad_group_id = 1.2; // float | The identifier of a valid ad group.
$max_num_suggestions = 100; // int | The maximum number of suggested keywords for the response.
$suggest_bids = "no"; // string | Set to `yes` to include a suggest bid for the suggested keyword in the response. Otherwise, set to `no`.
$ad_state_filter = "ad_state_filter_example"; // string | Filters results to ad groups with state matching the comma-delimited list.

try {
    $result = $apiInstance->getAdGroupSuggestedKeywordsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $ad_group_id, $max_num_suggestions, $suggest_bids, $ad_state_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuggestedKeywordsApi->getAdGroupSuggestedKeywordsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; developer account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **ad_group_id** | **float**| The identifier of a valid ad group. |
 **max_num_suggestions** | **int**| The maximum number of suggested keywords for the response. | [optional] [default to 100]
 **suggest_bids** | **string**| Set to &#x60;yes&#x60; to include a suggest bid for the suggested keyword in the response. Otherwise, set to &#x60;no&#x60;. | [optional] [default to no]
 **ad_state_filter** | **string**| Filters results to ad groups with state matching the comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\AdGroupSuggestedKeywordsResponseEx[]**](../Model/AdGroupSuggestedKeywordsResponseEx.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAsinSuggestedKeywords**
> \AmazonAdvertisingApi\Model\GetAsinSuggestedKeywordsResponse getAsinSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $asin_value, $max_num_suggestions)

Gets suggested keywords for the specified ASIN.

Suggested keywords are returned in an array ordered by descending effectiveness.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\SuggestedKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" developer account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$asin_value = "asin_value_example"; // string | An ASIN.
$max_num_suggestions = 100; // int | The maximum number of suggested keywords for the response.

try {
    $result = $apiInstance->getAsinSuggestedKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $asin_value, $max_num_suggestions);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuggestedKeywordsApi->getAsinSuggestedKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; developer account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **asin_value** | **string**| An ASIN. |
 **max_num_suggestions** | **int**| The maximum number of suggested keywords for the response. | [optional] [default to 100]

### Return type

[**\AmazonAdvertisingApi\Model\GetAsinSuggestedKeywordsResponse**](../Model/GetAsinSuggestedKeywordsResponse.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

