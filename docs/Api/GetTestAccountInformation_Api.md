# AmazonAdvertisingApi\GetTestAccountInformation_Api

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountInformation**](GetTestAccountInformation_Api.md#getaccountinformation) | **GET** /testAccounts | 

# **getAccountInformation**
> \AmazonAdvertisingApi\Model\GetAccountInformationResponse getAccountInformation($amazon_advertising_api_client_id, $request_id)



API to get Account information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\GetTestAccountInformation_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$request_id = "request_id_example"; // string | request id.

try {
    $result = $apiInstance->getAccountInformation($amazon_advertising_api_client_id, $request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GetTestAccountInformation_Api->getAccountInformation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **request_id** | **string**| request id. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\GetAccountInformationResponse**](../Model/GetAccountInformationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

