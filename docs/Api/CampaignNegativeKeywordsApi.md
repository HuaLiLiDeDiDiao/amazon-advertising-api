# AmazonAdvertisingApi\CampaignNegativeKeywordsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSponsoredProductsCampaignNegativeKeywords**](CampaignNegativeKeywordsApi.md#createsponsoredproductscampaignnegativekeywords) | **POST** /sp/campaignNegativeKeywords | 
[**deleteSponsoredProductsCampaignNegativeKeywords**](CampaignNegativeKeywordsApi.md#deletesponsoredproductscampaignnegativekeywords) | **POST** /sp/campaignNegativeKeywords/delete | 
[**listSponsoredProductsCampaignNegativeKeywords**](CampaignNegativeKeywordsApi.md#listsponsoredproductscampaignnegativekeywords) | **POST** /sp/campaignNegativeKeywords/list | 
[**updateSponsoredProductsCampaignNegativeKeywords**](CampaignNegativeKeywordsApi.md#updatesponsoredproductscampaignnegativekeywords) | **PUT** /sp/campaignNegativeKeywords | 

# **createSponsoredProductsCampaignNegativeKeywords**
> \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsResponseContent createSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->createSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeKeywordsApi->createSponsoredProductsCampaignNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsRequestContent**](../Model/SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsResponseContent**](../Model/SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeKeyword.v3+json
 - **Accept**: application/vnd.spCampaignNegativeKeyword.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSponsoredProductsCampaignNegativeKeywords**
> \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsResponseContent deleteSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->deleteSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeKeywordsApi->deleteSponsoredProductsCampaignNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsRequestContent**](../Model/SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsResponseContent**](../Model/SponsoredProductsDeleteSponsoredProductsCampaignNegativeKeywordsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeKeyword.v3+json
 - **Accept**: application/vnd.spCampaignNegativeKeyword.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSponsoredProductsCampaignNegativeKeywords**
> \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsResponseContent listSponsoredProductsCampaignNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



**Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsRequestContent | 

try {
    $result = $apiInstance->listSponsoredProductsCampaignNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeKeywordsApi->listSponsoredProductsCampaignNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsRequestContent**](../Model/SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsRequestContent.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsResponseContent**](../Model/SponsoredProductsListSponsoredProductsCampaignNegativeKeywordsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeKeyword.v3+json
 - **Accept**: application/vnd.spCampaignNegativeKeyword.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSponsoredProductsCampaignNegativeKeywords**
> \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsResponseContent updateSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer)



**Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\CampaignNegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsRequestContent(); // \AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$prefer = "prefer_example"; // string | The \"Prefer\" header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return=representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only.

try {
    $result = $apiInstance->updateSponsoredProductsCampaignNegativeKeywords($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope, $prefer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CampaignNegativeKeywordsApi->updateSponsoredProductsCampaignNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsRequestContent**](../Model/SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **prefer** | **string**| The \&quot;Prefer\&quot; header, as defined in [RFC7240], allows clients to request certain behavior from the service. The service ignores preference values that are either not supported or not known by the service. Either multiple Prefer headers are passed or single one with comma separated values, both forms are equivalent Supported preferences: return&#x3D;representation - return the full object when doing create/update/delete operations instead of ids. Please note that the extendedData field will be part of the full object for /list endpoints only. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsResponseContent**](../Model/SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.spCampaignNegativeKeyword.v3+json
 - **Accept**: application/vnd.spCampaignNegativeKeyword.v3+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

