# AmazonAdvertisingApi\NegativeTargetingApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveNegativeTargetingClause**](NegativeTargetingApi.md#archivenegativetargetingclause) | **DELETE** /sd/negativeTargets/{negativeTargetId} | Sets the &#x60;state&#x60; of a negative targeting clause to &#x60;archived&#x60;.
[**createNegativeTargetingClauses**](NegativeTargetingApi.md#createnegativetargetingclauses) | **POST** /sd/negativeTargets | Creates one or more negative targeting clauses.
[**getNegativeTargets**](NegativeTargetingApi.md#getnegativetargets) | **GET** /sd/negativeTargets/{negativeTargetId} | Gets a negative targeting clause specified by identifier.
[**getNegativeTargetsEx**](NegativeTargetingApi.md#getnegativetargetsex) | **GET** /sd/negativeTargets/extended/{negativeTargetId} | Gets extended information for a negative targeting clause.
[**listNegativeTargetingClauses**](NegativeTargetingApi.md#listnegativetargetingclauses) | **GET** /sd/negativeTargets | Gets a list of negative targeting clauses.
[**listNegativeTargetingClausesEx**](NegativeTargetingApi.md#listnegativetargetingclausesex) | **GET** /sd/negativeTargets/extended | Gets a list of negative targeting clause objects with extended fields.
[**updateNegativeTargetingClauses**](NegativeTargetingApi.md#updatenegativetargetingclauses) | **PUT** /sd/negativeTargets | Updates one or more negative targeting clauses.

# **archiveNegativeTargetingClause**
> \AmazonAdvertisingApi\Model\TargetResponse archiveNegativeTargetingClause($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id)

Sets the `state` of a negative targeting clause to `archived`.

Equivalent to using the updateNegativeTargetingClauses operation to set the `state` property of a targeting clause to `archived`. See [Developer Notes](http://advertising.amazon.com/API/docs/guides/developer_notes#Archiving) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$negative_target_id = 789; // int | The identifier of a negative targeting clause.

try {
    $result = $apiInstance->archiveNegativeTargetingClause($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->archiveNegativeTargetingClause: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **negative_target_id** | **int**| The identifier of a negative targeting clause. |

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\TargetResponse[] createNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more negative targeting clauses.

Successfully created negative targeting clauses associated with an ad group are assigned a unique target identifier. Product negative targeting clause examples: | Negative targeting clause | Description | |---------------------------|-------------| | asinSameAs=B0123456789 | Negatively target this product.| | asinBrandSameAs=12345 | Negatively target products in the brand.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateNegativeTargetingClause()); // \AmazonAdvertisingApi\Model\CreateNegativeTargetingClause[] | A list of up to 100 negative targeting clauses for creation.

try {
    $result = $apiInstance->createNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->createNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateNegativeTargetingClause[]**](../Model/CreateNegativeTargetingClause.md)| A list of up to 100 negative targeting clauses for creation. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse[]**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNegativeTargets**
> \AmazonAdvertisingApi\Model\NegativeTargetingClause getNegativeTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id)

Gets a negative targeting clause specified by identifier.

This call returns the minimal set of negative targeting clause fields, but is more efficient than getNegativeTargetsEx.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$negative_target_id = 789; // int | The negative targeting clause identifier.

try {
    $result = $apiInstance->getNegativeTargets($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->getNegativeTargets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **negative_target_id** | **int**| The negative targeting clause identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\NegativeTargetingClause**](../Model/NegativeTargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNegativeTargetsEx**
> \AmazonAdvertisingApi\Model\NegativeTargetingClauseEx getNegativeTargetsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id)

Gets extended information for a negative targeting clause.

Gets a negative targeting clause with extended fields. Note that this call returns the full set of negative targeting clause extended fields, but is less efficient than getNegativeTarget.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$negative_target_id = 789; // int | The negative targeting clause identifier.

try {
    $result = $apiInstance->getNegativeTargetsEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $negative_target_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->getNegativeTargetsEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **negative_target_id** | **int**| The negative targeting clause identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\NegativeTargetingClauseEx**](../Model/NegativeTargetingClauseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\NegativeTargetingClause[] listNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of negative targeting clauses.

Gets a list of negative targeting clauses objects for a requested set of Sponsored Display negative targets. Note that the Negative Targeting Clause object is designed for performance, and includes a small set of commonly used fields to reduce size. If the extended set of fields is required, use the negative target operations that return the NegativeTargetingClauseEx object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. 0-indexed record offset for the result set. Defaults to 0.
$count = 56; // int | Optional. Number of records to include in the paged response. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. Restricts results to those with state within the specified comma-separated list. Must be one of: `enabled`, `paused`, or `archived`. Default behavior is to include enabled, paused, and archived.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional list of comma separated adGroupIds. Restricts results to negative targeting clauses with the specified `adGroupId`.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. Restricts results to targeting clauses within campaigns specified in comma-separated list.

try {
    $result = $apiInstance->listNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->listNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. 0-indexed record offset for the result set. Defaults to 0. | [optional]
 **count** | **int**| Optional. Number of records to include in the paged response. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. Restricts results to those with state within the specified comma-separated list. Must be one of: &#x60;enabled&#x60;, &#x60;paused&#x60;, or &#x60;archived&#x60;. Default behavior is to include enabled, paused, and archived. | [optional] [default to enabled, paused, archived]
 **ad_group_id_filter** | **string**| Optional list of comma separated adGroupIds. Restricts results to negative targeting clauses with the specified &#x60;adGroupId&#x60;. | [optional]
 **campaign_id_filter** | **string**| Optional. Restricts results to targeting clauses within campaigns specified in comma-separated list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\NegativeTargetingClause[]**](../Model/NegativeTargetingClause.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listNegativeTargetingClausesEx**
> \AmazonAdvertisingApi\Model\NegativeTargetingClauseEx[] listNegativeTargetingClausesEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $target_id_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of negative targeting clause objects with extended fields.

Gets an array of NegativeTargetingClauseEx objects for a set of requested negative targets. Note that this call returns the full set of negative targeting clause extended fields, but is less efficient than getNegativeTargets.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. 0-indexed record offset for the result set. Defaults to 0.
$count = 56; // int | Optional. Number of records to include in the paged response. Defaults to max page size.
$state_filter = "enabled, paused, archived"; // string | Optional. Restricts results to keywords with state within the specified comma-separated list. Must be one of: `enabled`, `paused`, or `archived`. Default behavior is to include `enabled`, `paused`, and `archived`.
$target_id_filter = "target_id_filter_example"; // string | Optional. Restricts results to ads with the specified `tagetId` specified in comma-separated list
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional list of comma separated adGroupIds. Restricts results to negative targeting clauses with the specified `adGroupId`.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional. Restricts results to ads within campaigns specified in the comma-separated list.

try {
    $result = $apiInstance->listNegativeTargetingClausesEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $target_id_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->listNegativeTargetingClausesEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. 0-indexed record offset for the result set. Defaults to 0. | [optional]
 **count** | **int**| Optional. Number of records to include in the paged response. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. Restricts results to keywords with state within the specified comma-separated list. Must be one of: &#x60;enabled&#x60;, &#x60;paused&#x60;, or &#x60;archived&#x60;. Default behavior is to include &#x60;enabled&#x60;, &#x60;paused&#x60;, and &#x60;archived&#x60;. | [optional] [default to enabled, paused, archived]
 **target_id_filter** | **string**| Optional. Restricts results to ads with the specified &#x60;tagetId&#x60; specified in comma-separated list | [optional]
 **ad_group_id_filter** | **string**| Optional list of comma separated adGroupIds. Restricts results to negative targeting clauses with the specified &#x60;adGroupId&#x60;. | [optional]
 **campaign_id_filter** | **string**| Optional. Restricts results to ads within campaigns specified in the comma-separated list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\NegativeTargetingClauseEx[]**](../Model/NegativeTargetingClauseEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateNegativeTargetingClauses**
> \AmazonAdvertisingApi\Model\TargetResponse[] updateNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more negative targeting clauses.

Updates one or more negative targeting clauses. Negative targeting clauses are identified using their targetId. The mutable field is `state`. Maximum length of the array is 100 objects.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\NegativeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\UpdateNegativeTargetingClause()); // \AmazonAdvertisingApi\Model\UpdateNegativeTargetingClause[] | A list of up to 100 negative targeting clauses. Note that the only mutable field is `state`.

try {
    $result = $apiInstance->updateNegativeTargetingClauses($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeTargetingApi->updateNegativeTargetingClauses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\UpdateNegativeTargetingClause[]**](../Model/UpdateNegativeTargetingClause.md)| A list of up to 100 negative targeting clauses. Note that the only mutable field is &#x60;state&#x60;. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\TargetResponse[]**](../Model/TargetResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

