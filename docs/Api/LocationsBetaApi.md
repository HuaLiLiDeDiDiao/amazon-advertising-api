# AmazonAdvertisingApi\LocationsBetaApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLocations**](LocationsBetaApi.md#createlocations) | **POST** /sd/locations | Creates one or more locations associated with an ad group.
[**listLocations**](LocationsBetaApi.md#listlocations) | **GET** /sd/locations | Gets a list of locations associated with ad groups.

# **createLocations**
> \AmazonAdvertisingApi\Model\Location[] createLocations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more locations associated with an ad group.

This resource is not available when productAds have ASIN or SKU fields and only available for advertisers that do not sell products on Amazon.   See [Developer Guide](https://advertising.amazon.com/API/docs/en-us/guides/sponsored-display/non-amazon-sellers/get-started)  Locations optimize Ad Groups for delivery to users that have an association with those locations. For example, an Ad Group might contain the following:  - A Targeting Clause representing an audience of users that viewed a shoe  - A Location representing Seattle, Washington, USA. - A Location representing New York, New York, USA. In this case, delivery of the Targeting Clause will be optimized for New York and Seattle.   You can discover predefined Locations to use in your Ad Groups by calling the GET /locations API. The table below lists  several example Locations. | Location | Description | |---------------------------|-------------| | location=amzn1.ad-geo.XHvCjcKHXsKUwos= | Optimize the AdGroup for the specified location (either a 'city', 'state', 'dma', 'postal code', or 'country').|  If Locations are to be used, they must be created before Targeting Clauses within an Ad Group. Creating a Location after a   Targeting Clause is only permitted if other Locations already exist in the Ad Group. In that case, additional  Locations increase the Targeting Clause's potential reach.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\LocationsBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\CreateLocation()); // \AmazonAdvertisingApi\Model\CreateLocation[] | A list of up to 20 Locations for creation.

try {
    $result = $apiInstance->createLocations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocationsBetaApi->createLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\CreateLocation[]**](../Model/CreateLocation.md)| A list of up to 20 Locations for creation. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Location[]**](../Model/Location.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, a-pplication/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listLocations**
> \AmazonAdvertisingApi\Model\Location[] listLocations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter)

Gets a list of locations associated with ad groups.

Gets a list of Sponsored Display Location objects. This resource is not available when productAds have ASIN or SKU fields and only available for advertisers that do not sell products on Amazon. See [Developer Guide](https://advertising.amazon.com/API/docs/en-us/guides/sponsored-display/non-amazon-sellers/get-started)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\LocationsBetaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 56; // int | Optional. 0-indexed record offset for the result set. Defaults to 0.
$count = 56; // int | Optional. Number of records to include in the paged response. Defaults to max page size.
$state_filter = "enabled"; // string | Optional. Restricts results to those with state within the specified comma-separated list. Must be one of: `enabled`.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | Optional list of comma separated adGroupIds. Restricts results to locations with the specified `adGroupId`.
$campaign_id_filter = "campaign_id_filter_example"; // string | Optional list of comma separated campaignIds. Restricts results to locations with the specified `campaignId`.

try {
    $result = $apiInstance->listLocations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $state_filter, $ad_group_id_filter, $campaign_id_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocationsBetaApi->listLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Optional. 0-indexed record offset for the result set. Defaults to 0. | [optional]
 **count** | **int**| Optional. Number of records to include in the paged response. Defaults to max page size. | [optional]
 **state_filter** | **string**| Optional. Restricts results to those with state within the specified comma-separated list. Must be one of: &#x60;enabled&#x60;. | [optional] [default to enabled]
 **ad_group_id_filter** | **string**| Optional list of comma separated adGroupIds. Restricts results to locations with the specified &#x60;adGroupId&#x60;. | [optional]
 **campaign_id_filter** | **string**| Optional list of comma separated campaignIds. Restricts results to locations with the specified &#x60;campaignId&#x60;. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Location[]**](../Model/Location.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

