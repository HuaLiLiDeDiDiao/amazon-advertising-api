# AmazonAdvertisingApi\ManagerAccountsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createManagerAccount**](ManagerAccountsApi.md#createmanageraccount) | **POST** /managerAccounts | Creates a new Amazon Advertising Manager account.
[**getManagerAccountsForUser**](ManagerAccountsApi.md#getmanageraccountsforuser) | **GET** /managerAccounts | Returns all manager accounts that a given Amazon Ads user has access to.
[**linkAdvertisingAccountsToManagerAccountPublicAPI**](ManagerAccountsApi.md#linkadvertisingaccountstomanageraccountpublicapi) | **POST** /managerAccounts/{managerAccountId}/associate | Link Amazon Advertising accounts or advertisers with a Manager Account.
[**unlinkAdvertisingAccountsToManagerAccountPublicAPI**](ManagerAccountsApi.md#unlinkadvertisingaccountstomanageraccountpublicapi) | **POST** /managerAccounts/{managerAccountId}/disassociate | Unlink Amazon Advertising accounts or advertisers with a Manager Account.

# **createManagerAccount**
> \AmazonAdvertisingApi\Model\ManagerAccount createManagerAccount($body, $amazon_advertising_api_client_id)

Creates a new Amazon Advertising Manager account.

Creates a new Amazon Advertising [Manager account](https://advertising.amazon.com/help?ref_=a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ManagerAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\CreateManagerAccountRequest(); // \AmazonAdvertisingApi\Model\CreateManagerAccountRequest | Request object required to create a new Manager account.
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->createManagerAccount($body, $amazon_advertising_api_client_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManagerAccountsApi->createManagerAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateManagerAccountRequest**](../Model/CreateManagerAccountRequest.md)| Request object required to create a new Manager account. |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\ManagerAccount**](../Model/ManagerAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.createmanageraccountrequest.v1+json
 - **Accept**: application/vnd.manageraccount.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getManagerAccountsForUser**
> \AmazonAdvertisingApi\Model\GetManagerAccountsResponse getManagerAccountsForUser($amazon_advertising_api_client_id)

Returns all manager accounts that a given Amazon Ads user has access to.

Returns all [manager accounts](https://advertising.amazon.com/help?ref_=a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8) that a user has access to, along with metadata for the Amazon Ads accounts that are linked to each manager account. NOTE: A maximum of 50 linked accounts are returned for each manager account.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ManagerAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->getManagerAccountsForUser($amazon_advertising_api_client_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManagerAccountsApi->getManagerAccountsForUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\GetManagerAccountsResponse**](../Model/GetManagerAccountsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.getmanageraccountsresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **linkAdvertisingAccountsToManagerAccountPublicAPI**
> \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountResponse linkAdvertisingAccountsToManagerAccountPublicAPI($body, $amazon_advertising_api_client_id, $manager_account_id)

Link Amazon Advertising accounts or advertisers with a Manager Account.

Link Amazon Advertising accounts or advertisers with a [Manager Account](https://advertising.amazon.com/help?ref_=a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ManagerAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest(); // \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$manager_account_id = "manager_account_id_example"; // string | Id of the Manager Account.

try {
    $result = $apiInstance->linkAdvertisingAccountsToManagerAccountPublicAPI($body, $amazon_advertising_api_client_id, $manager_account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManagerAccountsApi->linkAdvertisingAccountsToManagerAccountPublicAPI: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest**](../Model/UpdateAdvertisingAccountsInManagerAccountRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **manager_account_id** | **string**| Id of the Manager Account. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountResponse**](../Model/UpdateAdvertisingAccountsInManagerAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.updateadvertisingaccountsinmanageraccountrequest.v1+json
 - **Accept**: application/vnd.updateadvertisingaccountsinmanageraccountresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unlinkAdvertisingAccountsToManagerAccountPublicAPI**
> \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountResponse unlinkAdvertisingAccountsToManagerAccountPublicAPI($body, $amazon_advertising_api_client_id, $manager_account_id)

Unlink Amazon Advertising accounts or advertisers with a Manager Account.

Unlink Amazon Advertising accounts or advertisers with a [Manager Account](https://advertising.amazon.com/help?ref_=a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ManagerAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest(); // \AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$manager_account_id = "manager_account_id_example"; // string | Id of the Manager Account.

try {
    $result = $apiInstance->unlinkAdvertisingAccountsToManagerAccountPublicAPI($body, $amazon_advertising_api_client_id, $manager_account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManagerAccountsApi->unlinkAdvertisingAccountsToManagerAccountPublicAPI: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountRequest**](../Model/UpdateAdvertisingAccountsInManagerAccountRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **manager_account_id** | **string**| Id of the Manager Account. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateAdvertisingAccountsInManagerAccountResponse**](../Model/UpdateAdvertisingAccountsInManagerAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.updateadvertisingaccountsinmanageraccountrequest.v1+json
 - **Accept**: application/vnd.updateadvertisingaccountsinmanageraccountresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

