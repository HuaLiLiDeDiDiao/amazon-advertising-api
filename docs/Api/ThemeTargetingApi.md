# AmazonAdvertisingApi\ThemeTargetingApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sbCreateThemes**](ThemeTargetingApi.md#sbcreatethemes) | **POST** /sb/themes | Create one or more theme targets.
[**sbListThemes**](ThemeTargetingApi.md#sblistthemes) | **POST** /sb/themes/list | 
[**sbUpdateThemes**](ThemeTargetingApi.md#sbupdatethemes) | **PUT** /sb/themes | Updates one or more theme targets.

# **sbCreateThemes**
> \AmazonAdvertisingApi\Model\SBCreateThemesResponse sbCreateThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Create one or more theme targets.

Note that this endpoint does not support for **Author profiles**.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ThemeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SbThemesBody1(); // \AmazonAdvertisingApi\Model\SbThemesBody1 | A list of theme targets for creation. <br/>Note that theme targets can be created on multi-adGroups campaigns and where campaign serving status is not one of `archived`, `terminated`, `rejected`, or `ended` and adgroup state is not 'archived'. <br/>Note that this operation supports a maximum list size of 100 theme targets and only one target can be created for each themeType per adGroup.

try {
    $result = $apiInstance->sbCreateThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemeTargetingApi->sbCreateThemes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbThemesBody1**](../Model/SbThemesBody1.md)| A list of theme targets for creation. &lt;br/&gt;Note that theme targets can be created on multi-adGroups campaigns and where campaign serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60; and adgroup state is not &#x27;archived&#x27;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 theme targets and only one target can be created for each themeType per adGroup. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBCreateThemesResponse**](../Model/SBCreateThemesResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbthemescreateresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sbListThemes**
> \AmazonAdvertisingApi\Model\InlineResponse20013 sbListThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)



Note that this endpoint does not support for **Author profiles**. Gets a list of theme targets associated with the client identifier passed in the authorization header, filtered by specified criteria.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ThemeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\ThemesListBody(); // \AmazonAdvertisingApi\Model\ThemesListBody | A set of filters.

try {
    $result = $apiInstance->sbListThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemeTargetingApi->sbListThemes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\ThemesListBody**](../Model/ThemesListBody.md)| A set of filters. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbthemeslistresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sbUpdateThemes**
> \AmazonAdvertisingApi\Model\InlineResponse20014 sbUpdateThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more theme targets.

Note that this endpoint does not support for **Author profiles**.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\ThemeTargetingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SbThemesBody(); // \AmazonAdvertisingApi\Model\SbThemesBody | A list of theme targets with updated values. <br/>Note that theme targets can be updated on multi-adGroups campaigns and where campaign serving status is not one of `archived`, `terminated`, `rejected`, or `ended` and adgroup state is not 'archived'. <br/>Note that this operation supports a maximum list size of 100 theme targets. Also theme target can not be archived.

try {
    $result = $apiInstance->sbUpdateThemes($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemeTargetingApi->sbUpdateThemes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbThemesBody**](../Model/SbThemesBody.md)| A list of theme targets with updated values. &lt;br/&gt;Note that theme targets can be updated on multi-adGroups campaigns and where campaign serving status is not one of &#x60;archived&#x60;, &#x60;terminated&#x60;, &#x60;rejected&#x60;, or &#x60;ended&#x60; and adgroup state is not &#x27;archived&#x27;. &lt;br/&gt;Note that this operation supports a maximum list size of 100 theme targets. Also theme target can not be archived. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbthemesupdateresponse.v3+json, application/vnd.sberror.v3.0+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

