# AmazonAdvertisingApi\BudgetRulesApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBudgetRulesForSDCampaigns**](BudgetRulesApi.md#createbudgetrulesforsdcampaigns) | **POST** /sd/budgetRules | Creates one or more budget rules.
[**getBudgetRuleByRuleIdForSDCampaigns**](BudgetRulesApi.md#getbudgetrulebyruleidforsdcampaigns) | **GET** /sd/budgetRules/{budgetRuleId} | Gets a budget rule specified by identifier.
[**getSDBudgetRulesForAdvertiser**](BudgetRulesApi.md#getsdbudgetrulesforadvertiser) | **GET** /sd/budgetRules | Get all budget rules created by an advertiser
[**updateBudgetRulesForSDCampaigns**](BudgetRulesApi.md#updatebudgetrulesforsdcampaigns) | **PUT** /sd/budgetRules | Update one or more budget rules.

# **createBudgetRulesForSDCampaigns**
> \AmazonAdvertisingApi\Model\CreateBudgetRulesResponse createBudgetRulesForSDCampaigns($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Creates one or more budget rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\CreateSDBudgetRulesRequest(); // \AmazonAdvertisingApi\Model\CreateSDBudgetRulesRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->createBudgetRulesForSDCampaigns($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRulesApi->createBudgetRulesForSDCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\CreateSDBudgetRulesRequest**](../Model/CreateSDBudgetRulesRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\CreateBudgetRulesResponse**](../Model/CreateBudgetRulesResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBudgetRuleByRuleIdForSDCampaigns**
> \AmazonAdvertisingApi\Model\GetSDBudgetRuleResponse getBudgetRuleByRuleIdForSDCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $budget_rule_id)

Gets a budget rule specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$budget_rule_id = "budget_rule_id_example"; // string | The budget rule identifier.

try {
    $result = $apiInstance->getBudgetRuleByRuleIdForSDCampaigns($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $budget_rule_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRulesApi->getBudgetRuleByRuleIdForSDCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **budget_rule_id** | **string**| The budget rule identifier. |

### Return type

[**\AmazonAdvertisingApi\Model\GetSDBudgetRuleResponse**](../Model/GetSDBudgetRuleResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSDBudgetRulesForAdvertiser**
> \AmazonAdvertisingApi\Model\GetSDBudgetRulesForAdvertiserResponse getSDBudgetRulesForAdvertiser($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $page_size, $next_token)

Get all budget rules created by an advertiser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.
$page_size = 1.2; // float | Sets a limit on the number of results returned. Maximum limit of `pageSize` is 30.
$next_token = "next_token_example"; // string | To retrieve the next page of results, call the same operation and specify this token in the request. If the `nextToken` field is empty, there are no further results.

try {
    $result = $apiInstance->getSDBudgetRulesForAdvertiser($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $page_size, $next_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRulesApi->getSDBudgetRulesForAdvertiser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |
 **page_size** | **float**| Sets a limit on the number of results returned. Maximum limit of &#x60;pageSize&#x60; is 30. |
 **next_token** | **string**| To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;nextToken&#x60; field is empty, there are no further results. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\GetSDBudgetRulesForAdvertiserResponse**](../Model/GetSDBudgetRulesForAdvertiserResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBudgetRulesForSDCampaigns**
> \AmazonAdvertisingApi\Model\UpdateBudgetRulesResponse updateBudgetRulesForSDCampaigns($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Update one or more budget rules.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\BudgetRulesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AmazonAdvertisingApi\Model\UpdateSDBudgetRulesRequest(); // \AmazonAdvertisingApi\Model\UpdateSDBudgetRulesRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account. This is a required header for advertisers and integrators using the Advertising API.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API.

try {
    $result = $apiInstance->updateBudgetRulesForSDCampaigns($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetRulesApi->updateBudgetRulesForSDCampaigns: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\UpdateSDBudgetRulesRequest**](../Model/UpdateSDBudgetRulesRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. This is a required header for advertisers and integrators using the Advertising API. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. This is a required header for advertisers and integrators using the Advertising API. |

### Return type

[**\AmazonAdvertisingApi\Model\UpdateBudgetRulesResponse**](../Model/UpdateBudgetRulesResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

