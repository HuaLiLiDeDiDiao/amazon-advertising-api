# AmazonAdvertisingApi\BrandsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBrands**](BrandsApi.md#getbrands) | **GET** /brands | getBrands

# **getBrands**
> \AmazonAdvertisingApi\Model\InlineResponse200[] getBrands($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_type_filter)

getBrands

Gets an array of Brand data objects for the Brand associated with the profile ID passed in the header. For more information about Brands, see [Brand Services](https://brandservices.amazon.com/).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$brand_type_filter = new \AmazonAdvertisingApi\Model\BrandType(); // \AmazonAdvertisingApi\Model\BrandType | The returned array is filtered to include only brands with brand type set to one of the values in the specified comma-delimited list. Returns all brands if not specified.

try {
    $result = $apiInstance->getBrands($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $brand_type_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->getBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **brand_type_filter** | [**\AmazonAdvertisingApi\Model\BrandType**](../Model/.md)| The returned array is filtered to include only brands with brand type set to one of the values in the specified comma-delimited list. Returns all brands if not specified. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse200[]**](../Model/InlineResponse200.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.brand.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

