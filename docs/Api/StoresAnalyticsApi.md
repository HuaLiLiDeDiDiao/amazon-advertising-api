# AmazonAdvertisingApi\StoresAnalyticsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAsinEngagementForStore**](StoresAnalyticsApi.md#getasinengagementforstore) | **POST** /stores/{brandEntityId}/asinMetrics | 
[**getInsightsForStoreAPI**](StoresAnalyticsApi.md#getinsightsforstoreapi) | **POST** /stores/{brandEntityId}/insights | 

# **getAsinEngagementForStore**
> \AmazonAdvertisingApi\Model\GetAsinEngagementForStoreResponse getAsinEngagementForStore($body, $brand_entity_id)



Store asin metrics provides information about your store asin performance, including rendered impressions, viewed impressions, clicks and sales. You can access Stores insights through this API.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\StoresAnalyticsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\GetAsinEngagementForStoreRequest(); // \AmazonAdvertisingApi\Model\GetAsinEngagementForStoreRequest | 
$brand_entity_id = "brand_entity_id_example"; // string | The identifier of the requested store. It can be fetched through calling existing API \"/v2/stores\" listed here https://advertising.amazon.com/API/docs/en-us/reference/2/stores

try {
    $result = $apiInstance->getAsinEngagementForStore($body, $brand_entity_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresAnalyticsApi->getAsinEngagementForStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\GetAsinEngagementForStoreRequest**](../Model/GetAsinEngagementForStoreRequest.md)|  |
 **brand_entity_id** | **string**| The identifier of the requested store. It can be fetched through calling existing API \&quot;/v2/stores\&quot; listed here https://advertising.amazon.com/API/docs/en-us/reference/2/stores |

### Return type

[**\AmazonAdvertisingApi\Model\GetAsinEngagementForStoreResponse**](../Model/GetAsinEngagementForStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.GetAsinEngagementForStoreRequest.v1+json
 - **Accept**: application/vnd.GetAsinEngagementForStoreResponse.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInsightsForStoreAPI**
> \AmazonAdvertisingApi\Model\GetInsightsForStoreResponse getInsightsForStoreAPI($body, $brand_entity_id)



Stores insights provides information about your store's performance, including traffic and sales. You can access Stores insights through this API.  **Requires one of these permissions**: [\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\StoresAnalyticsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\GetInsightsForStoreRequest(); // \AmazonAdvertisingApi\Model\GetInsightsForStoreRequest | 
$brand_entity_id = "brand_entity_id_example"; // string | The identifier of the requested store. It can be fetched through calling existing API \"/v2/stores\" listed here https://advertising.amazon.com/API/docs/en-us/reference/2/stores

try {
    $result = $apiInstance->getInsightsForStoreAPI($body, $brand_entity_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresAnalyticsApi->getInsightsForStoreAPI: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\GetInsightsForStoreRequest**](../Model/GetInsightsForStoreRequest.md)|  |
 **brand_entity_id** | **string**| The identifier of the requested store. It can be fetched through calling existing API \&quot;/v2/stores\&quot; listed here https://advertising.amazon.com/API/docs/en-us/reference/2/stores |

### Return type

[**\AmazonAdvertisingApi\Model\GetInsightsForStoreResponse**](../Model/GetInsightsForStoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.GetInsightsForStoreRequest.v1+json
 - **Accept**: application/vnd.GetInsightsForStoreResponse.v1+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

