# AmazonAdvertisingApi\NegativeKeywordsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveNegativeKeyword**](NegativeKeywordsApi.md#archivenegativekeyword) | **DELETE** /sb/negativeKeywords/{keywordId} | Archives a negative keyword specified by identifier.
[**createNegativeKeywords**](NegativeKeywordsApi.md#createnegativekeywords) | **POST** /sb/negativeKeywords | Creates one or more negative keywords.
[**getNegativeKeyword**](NegativeKeywordsApi.md#getnegativekeyword) | **GET** /sb/negativeKeywords/{keywordId} | Gets a negative keyword specified by identifier.
[**listNegativeKeywords**](NegativeKeywordsApi.md#listnegativekeywords) | **GET** /sb/negativeKeywords | Gets an array of negative keywords, filtered by optional criteria.
[**updateNegativeKeywords**](NegativeKeywordsApi.md#updatenegativekeywords) | **PUT** /sb/negativeKeywords | Updates one or more negative keywords.

# **archiveNegativeKeyword**
> \AmazonAdvertisingApi\Model\SBKeywordResponse archiveNegativeKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id)

Archives a negative keyword specified by identifier.

This operation is equivalent to an update operation that sets the status field to 'archived'. Note that setting the status field to 'archived' is permanent and can't be undone. See [Developer Notes](https://advertising.amazon.com/API/docs/v2/guides/developer_notes) for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$keyword_id = 789; // int | The identifier of an existing campaign.

try {
    $result = $apiInstance->archiveNegativeKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeKeywordsApi->archiveNegativeKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **keyword_id** | **int**| The identifier of an existing campaign. |

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbkeywordresponse.v3+json, application/vnd.error.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createNegativeKeywords**
> \AmazonAdvertisingApi\Model\SBKeywordResponse[] createNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more negative keywords.

Note that `bid` and `state` can't be set at negative keyword creation. <br/>Note that Negative keywords submitted for creation have state set to `pending` while under moderation review. Moderation review may take up to 72 hours. <br/>Note that negative keywords can be created on campaigns one where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 negative keywords. <br>**Note** that negative keywords *can not* be recreated for a campaign if the negative keyword has previously been associated with a campaign and subsequently archived.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SbNegativeKeywordsBody1()); // \AmazonAdvertisingApi\Model\SbNegativeKeywordsBody1[] | An array of negative keywords.

try {
    $result = $apiInstance->createNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeKeywordsApi->createNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbNegativeKeywordsBody1[]**](../Model/SbNegativeKeywordsBody1.md)| An array of negative keywords. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbkeywordresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNegativeKeyword**
> \AmazonAdvertisingApi\Model\InlineResponse2002 getNegativeKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id)

Gets a negative keyword specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$keyword_id = 789; // int | The identifier of an existing negative keyword.

try {
    $result = $apiInstance->getNegativeKeyword($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $keyword_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeKeywordsApi->getNegativeKeyword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **keyword_id** | **int**| The identifier of an existing negative keyword. |

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbnegativekeyword.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listNegativeKeywords**
> \AmazonAdvertisingApi\Model\SBNegativeKeyword[] listNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $match_type_filter, $keyword_text, $state_filter, $campaign_id_filter, $ad_group_id_filter, $keyword_id_filter, $creative_type)

Gets an array of negative keywords, filtered by optional criteria.

**Note**: Negative keywords associated with BrandVideo ad groups are only available in v3.2 version.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$start_index = 0; // int | Sets a zero-based offset into the requested set of negative keywords. Use in conjunction with the `count` parameter to control pagination of the returned array.
$count = 56; // int | Sets the number of negative keywords in the returned array. Use in conjunction with the `startIndex` parameter to control pagination. For example, to return the first ten negative keywords set `startIndex=0` and `count=10`. To return the next ten negative keywords, set `startIndex=10` and `count=10`, and so on.
$match_type_filter = new \AmazonAdvertisingApi\Model\NegativeMatchType(); // \AmazonAdvertisingApi\Model\NegativeMatchType | The returned array is filtered to include only negative keywords with `matchType` set to one of the values in the specified comma-delimited list.
$keyword_text = "keyword_text_example"; // string | The returned array includes only negative keywords with the specified text.
$state_filter = new \AmazonAdvertisingApi\Model\State(); // \AmazonAdvertisingApi\Model\State | The returned array includes only negative keywords with `state` set to the specified value.
$campaign_id_filter = "campaign_id_filter_example"; // string | The returned array includes only negative keywords associated with campaigns matching those specified by identifier in the comma-delimited string.
$ad_group_id_filter = "ad_group_id_filter_example"; // string | The returned array includes only negative keywords associated with ad groups matching those specified by identifier in the comma-delimited string.
$keyword_id_filter = "keyword_id_filter_example"; // string | The returned array includes only negative keywords with identifiers matching those specified in the comma-delimited string.
$creative_type = new \AmazonAdvertisingApi\Model\CreativeType(); // \AmazonAdvertisingApi\Model\CreativeType | Filter by the type of creative the campaign is associated with. To get negative keywords associated with non-video campaigns specify 'productCollection'. To get negative keywords associated with video campaigns, this must be set to 'video'. Returns all negative keywords if not specified.

try {
    $result = $apiInstance->listNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $start_index, $count, $match_type_filter, $keyword_text, $state_filter, $campaign_id_filter, $ad_group_id_filter, $keyword_id_filter, $creative_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeKeywordsApi->listNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **start_index** | **int**| Sets a zero-based offset into the requested set of negative keywords. Use in conjunction with the &#x60;count&#x60; parameter to control pagination of the returned array. | [optional] [default to 0]
 **count** | **int**| Sets the number of negative keywords in the returned array. Use in conjunction with the &#x60;startIndex&#x60; parameter to control pagination. For example, to return the first ten negative keywords set &#x60;startIndex&#x3D;0&#x60; and &#x60;count&#x3D;10&#x60;. To return the next ten negative keywords, set &#x60;startIndex&#x3D;10&#x60; and &#x60;count&#x3D;10&#x60;, and so on. | [optional]
 **match_type_filter** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](../Model/.md)| The returned array is filtered to include only negative keywords with &#x60;matchType&#x60; set to one of the values in the specified comma-delimited list. | [optional]
 **keyword_text** | **string**| The returned array includes only negative keywords with the specified text. | [optional]
 **state_filter** | [**\AmazonAdvertisingApi\Model\State**](../Model/.md)| The returned array includes only negative keywords with &#x60;state&#x60; set to the specified value. | [optional]
 **campaign_id_filter** | **string**| The returned array includes only negative keywords associated with campaigns matching those specified by identifier in the comma-delimited string. | [optional]
 **ad_group_id_filter** | **string**| The returned array includes only negative keywords associated with ad groups matching those specified by identifier in the comma-delimited string. | [optional]
 **keyword_id_filter** | **string**| The returned array includes only negative keywords with identifiers matching those specified in the comma-delimited string. | [optional]
 **creative_type** | [**\AmazonAdvertisingApi\Model\CreativeType**](../Model/.md)| Filter by the type of creative the campaign is associated with. To get negative keywords associated with non-video campaigns specify &#x27;productCollection&#x27;. To get negative keywords associated with video campaigns, this must be set to &#x27;video&#x27;. Returns all negative keywords if not specified. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBNegativeKeyword[]**](../Model/SBNegativeKeyword.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbnegativekeyword.v3.2+json, application/vnd.sbnegativekeyword.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateNegativeKeywords**
> \AmazonAdvertisingApi\Model\SBKeywordResponse[] updateNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more negative keywords.

Negative keywords submitted for update may have state set to `pending` for moderation review. Moderation may take up to 72 hours. <br/>Note that negative keywords can be updated on campaigns where serving status is not one of `archived`, `terminated`, `rejected`, or `ended`. <br/>Note that this operation supports a maximum list size of 100 negative keywords.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AmazonAdvertisingApi\Api\NegativeKeywordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a **Login with Amazon** account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\SbNegativeKeywordsBody()); // \AmazonAdvertisingApi\Model\SbNegativeKeywordsBody[] | An array of negative keywords.

try {
    $result = $apiInstance->updateNegativeKeywords($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NegativeKeywordsApi->updateNegativeKeywords: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a **Login with Amazon** account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SbNegativeKeywordsBody[]**](../Model/SbNegativeKeywordsBody.md)| An array of negative keywords. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](../Model/SBKeywordResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.sbkeywordresponse.v3+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

