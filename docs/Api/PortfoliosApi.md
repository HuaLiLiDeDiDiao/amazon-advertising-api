# AmazonAdvertisingApi\PortfoliosApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPortfolios**](PortfoliosApi.md#createportfolios) | **POST** /v2/portfolios | Creates one or more portfolios.
[**getPortfolio**](PortfoliosApi.md#getportfolio) | **GET** /v2/portfolios/{portfolioId} | Gets a requested portfolio.
[**listPortfolios**](PortfoliosApi.md#listportfolios) | **GET** /v2/portfolios | Gets a list of portfolios.
[**updatePortfolios**](PortfoliosApi.md#updateportfolios) | **PUT** /v2/portfolios | Updates one or more portfolios.

# **createPortfolios**
> \AmazonAdvertisingApi\Model\InlineResponse200[] createPortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Creates one or more portfolios.

The request body is a list of portfolio resources to be created. Note that this operation is limited to the creation of 100 portfolios. Also note that the only valid `state` for Portfolio creation is `enabled`. Portfolios can't be created with `state` set to `paused`, this will result in an `INVALID_ARGUMENT` error.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\Portfolio()); // \AmazonAdvertisingApi\Model\Portfolio[] | A list of portfolio resources with updated values.

try {
    $result = $apiInstance->createPortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosApi->createPortfolios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\Portfolio[]**](../Model/Portfolio.md)| A list of portfolio resources with updated values. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse200[]**](../Model/InlineResponse200.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPortfolio**
> \AmazonAdvertisingApi\Model\Portfolio getPortfolio($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id)

Gets a requested portfolio.

Returns a Portfolio object for a requested portfolio.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$portfolio_id = 1.2; // float | The identifier of an existing portfolio.

try {
    $result = $apiInstance->getPortfolio($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosApi->getPortfolio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **portfolio_id** | **float**| The identifier of an existing portfolio. |

### Return type

[**\AmazonAdvertisingApi\Model\Portfolio**](../Model/Portfolio.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listPortfolios**
> \AmazonAdvertisingApi\Model\Portfolio[] listPortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id_filter, $portfolio_name_filter, $portfolio_state_filter)

Gets a list of portfolios.

Retrieves a list of portfolios, optionally filtered by identifier, name, or state. Note that this operation returns a maximum of 100 portfolios.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$portfolio_id_filter = "portfolio_id_filter_example"; // string | The returned list includes portfolios with identifiers matching those in the specified comma-delimited list. There is a maximum of 100 identifiers allowed.
$portfolio_name_filter = "portfolio_name_filter_example"; // string | The returned list includes portfolios with names matching those in the specified comma-delimited list. There is a maximum of 100 names allowed.
$portfolio_state_filter = "portfolio_state_filter_example"; // string | The returned list includes portfolios with states matching those in the specified comma-delimited list.

try {
    $result = $apiInstance->listPortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id_filter, $portfolio_name_filter, $portfolio_state_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosApi->listPortfolios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **portfolio_id_filter** | **string**| The returned list includes portfolios with identifiers matching those in the specified comma-delimited list. There is a maximum of 100 identifiers allowed. | [optional]
 **portfolio_name_filter** | **string**| The returned list includes portfolios with names matching those in the specified comma-delimited list. There is a maximum of 100 names allowed. | [optional]
 **portfolio_state_filter** | **string**| The returned list includes portfolios with states matching those in the specified comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\Portfolio[]**](../Model/Portfolio.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePortfolios**
> \AmazonAdvertisingApi\Model\InlineResponse200[] updatePortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Updates one or more portfolios.

The request body is a list of portfolio resources with updated values.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = array(new \AmazonAdvertisingApi\Model\Portfolio()); // \AmazonAdvertisingApi\Model\Portfolio[] | A list of portfolio resources with updated values.

try {
    $result = $apiInstance->updatePortfolios($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosApi->updatePortfolios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\Portfolio[]**](../Model/Portfolio.md)| A list of portfolio resources with updated values. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\InlineResponse200[]**](../Model/InlineResponse200.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

