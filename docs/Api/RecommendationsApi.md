# AmazonAdvertisingApi\RecommendationsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBudgetRecommendations**](RecommendationsApi.md#getbudgetrecommendations) | **POST** /sb/campaigns/budgetRecommendations | Get budget recommendations
[**getHeadlineRecommendations**](RecommendationsApi.md#getheadlinerecommendations) | **POST** /sb/recommendations/creative/headline | Get recommendations for creative headline
[**sBTargetingGetNegativeBrands**](RecommendationsApi.md#sbtargetinggetnegativebrands) | **GET** /sb/negativeTargets/brands/recommendations | Get brand recommendations for negative targeting

# **getBudgetRecommendations**
> \AmazonAdvertisingApi\Model\GetBudgetRecommendationsResponseContent getBudgetRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Get budget recommendations

Provides daily budget recommendations for a list of requested Sponsored Brands campaigns, with context on estimated historical missed opportunities.  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\RecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\GetBudgetRecommendationsRequestContent(); // \AmazonAdvertisingApi\Model\GetBudgetRecommendationsRequestContent | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a `Login with Amazon` account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->getBudgetRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendationsApi->getBudgetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\GetBudgetRecommendationsRequestContent**](../Model/GetBudgetRecommendationsRequestContent.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x60;Login with Amazon&#x60; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\GetBudgetRecommendationsResponseContent**](../Model/GetBudgetRecommendationsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/vnd.sbbudgetrecommendation.v4+json
 - **Accept**: application/vnd.sbbudgetrecommendation.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getHeadlineRecommendations**
> \AmazonAdvertisingApi\Model\HeadlineSuggestionResponse getHeadlineRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope)

Get recommendations for creative headline

API to receive creative headline suggestions.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\RecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \AmazonAdvertisingApi\Model\HeadlineSuggestionRequest(); // \AmazonAdvertisingApi\Model\HeadlineSuggestionRequest | 
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.

try {
    $result = $apiInstance->getHeadlineRecommendations($body, $amazon_advertising_api_client_id, $amazon_advertising_api_scope);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendationsApi->getHeadlineRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AmazonAdvertisingApi\Model\HeadlineSuggestionRequest**](../Model/HeadlineSuggestionRequest.md)|  |
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |

### Return type

[**\AmazonAdvertisingApi\Model\HeadlineSuggestionResponse**](../Model/HeadlineSuggestionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sBTargetingGetNegativeBrands**
> \AmazonAdvertisingApi\Model\SBTargetingGetNegativeBrandsResponseContent sBTargetingGetNegativeBrands($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token)

Get brand recommendations for negative targeting

Returns brands recommended for negative targeting. Only available for Sellers and Vendors. These recommendations include your own brands because targeting your own brands usually results in lower performance than targeting competitors' brands.   Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP  **Requires one of these permissions**: [\"advertiser_campaign_edit\",\"advertiser_campaign_view\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\RecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a `Login with Amazon` account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$next_token = "next_token_example"; // string | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the `NextToken` field is empty, there are no further results.

try {
    $result = $apiInstance->sBTargetingGetNegativeBrands($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendationsApi->sBTargetingGetNegativeBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a &#x60;Login with Amazon&#x60; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **next_token** | **string**| Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SBTargetingGetNegativeBrandsResponseContent**](../Model/SBTargetingGetNegativeBrandsResponseContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.sbtargeting.v4+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

