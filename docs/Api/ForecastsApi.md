# AmazonAdvertisingApi\ForecastsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSDForecast**](ForecastsApi.md#createsdforecast) | **POST** /sd/forecasts | Return forecasts for an ad group that may or may not exist.

# **createSDForecast**
> \AmazonAdvertisingApi\Model\SDForecastResponse createSDForecast($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body)

Return forecasts for an ad group that may or may not exist.

Returns forecasts for a given ad group specified in SD forecast request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\ForecastsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDForecastRequest(); // \AmazonAdvertisingApi\Model\SDForecastRequest | 

try {
    $result = $apiInstance->createSDForecast($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ForecastsApi->createSDForecast: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDForecastRequest**](../Model/SDForecastRequest.md)|  | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDForecastResponse**](../Model/SDForecastResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdforecasts.v3.1+json
 - **Accept**: application/vnd.sdforecasts.v3.1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

