# AmazonAdvertisingApi\TargetingRecommendationsApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTargetRecommendations**](TargetingRecommendationsApi.md#gettargetrecommendations) | **POST** /sd/targets/recommendations | Returns a set of recommended products and categories to target

# **getTargetRecommendations**
> \AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34 getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale)

Returns a set of recommended products and categories to target

This API provides product, category and standard audience recommendations to target based on the list of input ASINs. Allow 1 week for our systems to process data for any new ASINs listed on Amazon before using this service. Note -  recommendations are only available for productAds with SKU or ASIN.  For API v3.0, the API returns up to 100 recommendations for contextual targeting.  For API v3.1, the API returns up to 100 recommendations for both product and category targeting.  For API v3.2, the API introduces contextual targeting themes in the request and returns product recommendations based on different targeting themes.  For API v3.3, the API introduces standard audience recommendations and translated category recommendations based on locale.  For API v3.4, the API includes the theme expression used in contextual targeting recommendations in the response.  The currently available tactic identifiers are:  |Tactic Name|Type|Description| |-----------|----|-----------| |T00020&nbsp;|Contextual Targeting|Products: Choose individual products to show your ads in placements related to those products.| |T00030&nbsp;|Audience Targeting|Audiences: Select individual audiences to show your ads.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34 | 
$locale = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale | The requested locale from query parameter to return translated category recommendations.

try {
    $result = $apiInstance->getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingRecommendationsApi->getTargetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34**](../Model/SDTargetingRecommendationsRequestV34.md)|  | [optional]
 **locale** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale**](../Model/.md)| The requested locale from query parameter to return translated category recommendations. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34**](../Model/SDTargetingRecommendationsResponseV34.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json
 - **Accept**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetRecommendations**
> \AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34 getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale)

Returns a set of recommended products and categories to target

This API provides product, category and standard audience recommendations to target based on the list of input ASINs. Allow 1 week for our systems to process data for any new ASINs listed on Amazon before using this service. Note -  recommendations are only available for productAds with SKU or ASIN.  For API v3.0, the API returns up to 100 recommendations for contextual targeting.  For API v3.1, the API returns up to 100 recommendations for both product and category targeting.  For API v3.2, the API introduces contextual targeting themes in the request and returns product recommendations based on different targeting themes.  For API v3.3, the API introduces standard audience recommendations and translated category recommendations based on locale.  For API v3.4, the API includes the theme expression used in contextual targeting recommendations in the response.  The currently available tactic identifiers are:  |Tactic Name|Type|Description| |-----------|----|-----------| |T00020&nbsp;|Contextual Targeting|Products: Choose individual products to show your ads in placements related to those products.| |T00030&nbsp;|Audience Targeting|Audiences: Select individual audiences to show your ads.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34 | 
$locale = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale | The requested locale from query parameter to return translated category recommendations.

try {
    $result = $apiInstance->getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingRecommendationsApi->getTargetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34**](../Model/SDTargetingRecommendationsRequestV34.md)|  | [optional]
 **locale** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale**](../Model/.md)| The requested locale from query parameter to return translated category recommendations. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34**](../Model/SDTargetingRecommendationsResponseV34.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json
 - **Accept**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetRecommendations**
> \AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34 getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale)

Returns a set of recommended products and categories to target

This API provides product, category and standard audience recommendations to target based on the list of input ASINs. Allow 1 week for our systems to process data for any new ASINs listed on Amazon before using this service. Note -  recommendations are only available for productAds with SKU or ASIN.  For API v3.0, the API returns up to 100 recommendations for contextual targeting.  For API v3.1, the API returns up to 100 recommendations for both product and category targeting.  For API v3.2, the API introduces contextual targeting themes in the request and returns product recommendations based on different targeting themes.  For API v3.3, the API introduces standard audience recommendations and translated category recommendations based on locale.  For API v3.4, the API includes the theme expression used in contextual targeting recommendations in the response.  The currently available tactic identifiers are:  |Tactic Name|Type|Description| |-----------|----|-----------| |T00020&nbsp;|Contextual Targeting|Products: Choose individual products to show your ads in placements related to those products.| |T00030&nbsp;|Audience Targeting|Audiences: Select individual audiences to show your ads.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34 | 
$locale = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale | The requested locale from query parameter to return translated category recommendations.

try {
    $result = $apiInstance->getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingRecommendationsApi->getTargetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34**](../Model/SDTargetingRecommendationsRequestV34.md)|  | [optional]
 **locale** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale**](../Model/.md)| The requested locale from query parameter to return translated category recommendations. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34**](../Model/SDTargetingRecommendationsResponseV34.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json
 - **Accept**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetRecommendations**
> \AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34 getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale)

Returns a set of recommended products and categories to target

This API provides product, category and standard audience recommendations to target based on the list of input ASINs. Allow 1 week for our systems to process data for any new ASINs listed on Amazon before using this service. Note -  recommendations are only available for productAds with SKU or ASIN.  For API v3.0, the API returns up to 100 recommendations for contextual targeting.  For API v3.1, the API returns up to 100 recommendations for both product and category targeting.  For API v3.2, the API introduces contextual targeting themes in the request and returns product recommendations based on different targeting themes.  For API v3.3, the API introduces standard audience recommendations and translated category recommendations based on locale.  For API v3.4, the API includes the theme expression used in contextual targeting recommendations in the response.  The currently available tactic identifiers are:  |Tactic Name|Type|Description| |-----------|----|-----------| |T00020&nbsp;|Contextual Targeting|Products: Choose individual products to show your ads in placements related to those products.| |T00030&nbsp;|Audience Targeting|Audiences: Select individual audiences to show your ads.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34 | 
$locale = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale | The requested locale from query parameter to return translated category recommendations.

try {
    $result = $apiInstance->getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingRecommendationsApi->getTargetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34**](../Model/SDTargetingRecommendationsRequestV34.md)|  | [optional]
 **locale** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale**](../Model/.md)| The requested locale from query parameter to return translated category recommendations. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34**](../Model/SDTargetingRecommendationsResponseV34.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json
 - **Accept**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTargetRecommendations**
> \AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34 getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale)

Returns a set of recommended products and categories to target

This API provides product, category and standard audience recommendations to target based on the list of input ASINs. Allow 1 week for our systems to process data for any new ASINs listed on Amazon before using this service. Note -  recommendations are only available for productAds with SKU or ASIN.  For API v3.0, the API returns up to 100 recommendations for contextual targeting.  For API v3.1, the API returns up to 100 recommendations for both product and category targeting.  For API v3.2, the API introduces contextual targeting themes in the request and returns product recommendations based on different targeting themes.  For API v3.3, the API introduces standard audience recommendations and translated category recommendations based on locale.  For API v3.4, the API includes the theme expression used in contextual targeting recommendations in the response.  The currently available tactic identifiers are:  |Tactic Name|Type|Description| |-----------|----|-----------| |T00020&nbsp;|Contextual Targeting|Products: Choose individual products to show your ads in placements related to those products.| |T00030&nbsp;|Audience Targeting|Audiences: Select individual audiences to show your ads.|

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\TargetingRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$body = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34 | 
$locale = new \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale(); // \AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale | The requested locale from query parameter to return translated category recommendations.

try {
    $result = $apiInstance->getTargetRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $body, $locale);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TargetingRecommendationsApi->getTargetRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **body** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsRequestV34**](../Model/SDTargetingRecommendationsRequestV34.md)|  | [optional]
 **locale** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsLocale**](../Model/.md)| The requested locale from query parameter to return translated category recommendations. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsResponseV34**](../Model/SDTargetingRecommendationsResponseV34.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json
 - **Accept**: application/vnd.sdtargetingrecommendations.v3.4+json, application/vnd.sdtargetingrecommendations.v3.3+json, application/vnd.sdtargetingrecommendations.v3.2+json, application/vnd.sdtargetingrecommendations.v3.1+json, application/vnd.sdtargetingrecommendations.v3.0+json, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

