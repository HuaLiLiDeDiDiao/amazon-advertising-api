# AmazonAdvertisingApi\ConsolidatedRecommendationsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCampaignRecommendations**](ConsolidatedRecommendationsApi.md#getcampaignrecommendations) | **GET** /sp/campaign/recommendations | 

# **getCampaignRecommendations**
> \AmazonAdvertisingApi\Model\GetCampaignRecommendationsResponse getCampaignRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token, $max_results)



Gets the top consolidated recommendations across bid, budget, targeting for SP campaigns given an advertiser profile id. The recommendations are refreshed everyday.  **Requires one of these permissions**: [\"advertiser_campaign_view\",\"advertiser_campaign_edit\"]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new AmazonAdvertisingApi\Api\ConsolidatedRecommendationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.
$next_token = "next_token_example"; // string | Optional. Token to retrieve subsequent page of results.
$max_results = "max_results_example"; // string | Optional. Limits the number of items to return in the response.

try {
    $result = $apiInstance->getCampaignRecommendations($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $next_token, $max_results);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConsolidatedRecommendationsApi->getCampaignRecommendations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id &#x60;profileId&#x60; from the response to pass it as input. |
 **next_token** | **string**| Optional. Token to retrieve subsequent page of results. | [optional]
 **max_results** | **string**| Optional. Limits the number of items to return in the response. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\GetCampaignRecommendationsResponse**](../Model/GetCampaignRecommendationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.spgetcampaignrecommendationsresponse.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

