# AmazonAdvertisingApi\PortfoliosExtendedApi

All URIs are relative to *https://advertising-api.amazon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listPortfolioEx**](PortfoliosExtendedApi.md#listportfolioex) | **GET** /v2/portfolios/extended/{portfolioId} | Gets an extended set of properties for a portfolio specified by identifier.
[**listPortfoliosEx**](PortfoliosExtendedApi.md#listportfoliosex) | **GET** /v2/portfolios/extended | Gets a list of portfolios with an extended set of properties.

# **listPortfolioEx**
> \AmazonAdvertisingApi\Model\PortfolioEx listPortfolioEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id)

Gets an extended set of properties for a portfolio specified by identifier.

Gets an extended set of properties for a portfolio specified by identifier.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosExtendedApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$portfolio_id = 1.2; // float | The identifier of an existing portfolio.

try {
    $result = $apiInstance->listPortfolioEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosExtendedApi->listPortfolioEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **portfolio_id** | **float**| The identifier of an existing portfolio. |

### Return type

[**\AmazonAdvertisingApi\Model\PortfolioEx**](../Model/PortfolioEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listPortfoliosEx**
> \AmazonAdvertisingApi\Model\PortfolioEx[] listPortfoliosEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id_filter, $portfolio_name_filter, $portfolio_state_filter)

Gets a list of portfolios with an extended set of properties.

Retrieves a list of portfolios with an extended set of properties, optionally filtered by identifier, name, or state. Note that this operation returns a maximum of 100 portfolios.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oauth2AuthorizationCode
$config = AmazonAdvertisingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new AmazonAdvertisingApi\Api\PortfoliosExtendedApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amazon_advertising_api_client_id = "amazon_advertising_api_client_id_example"; // string | The identifier of a client associated with a \"Login with Amazon\" account.
$amazon_advertising_api_scope = "amazon_advertising_api_scope_example"; // string | The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.
$portfolio_id_filter = "portfolio_id_filter_example"; // string | The returned list includes portfolios with identifiers matching those in the specified comma-delimited list. There is a maximum of 100 identifiers allowed.
$portfolio_name_filter = "portfolio_name_filter_example"; // string | The returned list includes portfolios with names matching those in the specified comma-delimited list. There is a maximum of 100 names allowed.
$portfolio_state_filter = "portfolio_state_filter_example"; // string | The returned list includes portfolios with states matching those in the specified comma-delimited list.

try {
    $result = $apiInstance->listPortfoliosEx($amazon_advertising_api_client_id, $amazon_advertising_api_scope, $portfolio_id_filter, $portfolio_name_filter, $portfolio_state_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PortfoliosExtendedApi->listPortfoliosEx: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amazon_advertising_api_client_id** | **string**| The identifier of a client associated with a \&quot;Login with Amazon\&quot; account. |
 **amazon_advertising_api_scope** | **string**| The identifier of a profile associated with the advertiser account. Use &#x60;GET&#x60; method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header. |
 **portfolio_id_filter** | **string**| The returned list includes portfolios with identifiers matching those in the specified comma-delimited list. There is a maximum of 100 identifiers allowed. | [optional]
 **portfolio_name_filter** | **string**| The returned list includes portfolios with names matching those in the specified comma-delimited list. There is a maximum of 100 names allowed. | [optional]
 **portfolio_state_filter** | **string**| The returned list includes portfolios with states matching those in the specified comma-delimited list. | [optional]

### Return type

[**\AmazonAdvertisingApi\Model\PortfolioEx[]**](../Model/PortfolioEx.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth), [oauth2AuthorizationCode](../../README.md#oauth2AuthorizationCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

