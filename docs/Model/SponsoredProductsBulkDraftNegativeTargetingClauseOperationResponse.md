# SponsoredProductsBulkDraftNegativeTargetingClauseOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftNegativeTargetingClauseSuccessResponseItem[]**](SponsoredProductsDraftNegativeTargetingClauseSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftNegativeTargetingClauseFailureResponseItem[]**](SponsoredProductsDraftNegativeTargetingClauseFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

