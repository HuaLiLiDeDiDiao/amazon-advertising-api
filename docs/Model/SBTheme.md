# SBTheme

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_id** | **string** | The theme target identifier. | [optional] 
**ad_group_id** | **string** | The identifier of the ad group associated with the theme target. | [optional] 
**campaign_id** | **string** | The identifier of the campaign associated with the theme target. | [optional] 
**theme_type** | [**\AmazonAdvertisingApi\Model\ThemeType**](ThemeType.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBThemeState**](SBThemeState.md) |  | [optional] 
**bid** | **float** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. For more information, see [supported features](https://advertising.amazon.com/API/docs/v2/guides/supported_features). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

