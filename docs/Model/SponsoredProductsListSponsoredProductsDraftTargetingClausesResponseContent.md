# SponsoredProductsListSponsoredProductsDraftTargetingClausesResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **int** | The total number of entities | [optional] 
**next_token** | **string** | token value allowing to navigate to the next response page | [optional] 
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftTargetingClause[]**](SponsoredProductsDraftTargetingClause.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

