# SponsoredProductsDeleteSponsoredProductsKeywordsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkKeywordOperationResponse**](SponsoredProductsBulkKeywordOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

