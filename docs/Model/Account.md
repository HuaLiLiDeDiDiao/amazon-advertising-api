# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_id** | **string** | The identifier of the marketplace to which the account is associated. See [this table](https://docs.developer.amazonservices.com/en_US/dev_guide/DG_Endpoints.html) for &#x60;marketplaceId&#x60; mappings. | [optional] 
**account_id** | **string** | Id of the Amazon Advertising account. | [optional] 
**account_name** | **string** | The name given to the Amazon Advertising account. | [optional] 
**profile_id** | **string** | The identifier of a profile associated with the advertiser account. Note that this value is only populated for a subset of account types: &#x60;[ SELLER, VENDOR, MARKETING_CLOUD ]&#x60;. It will be &#x60;null&#x60; for accounts of other types. | [optional] 
**account_type** | [**\AmazonAdvertisingApi\Model\AccountType**](AccountType.md) |  | [optional] 
**dsp_advertiser_id** | **string** | The identifier of a DSP advertiser. Note that this value is only populated for accounts with type &#x60;DSP_ADVERTISING_ACCOUNT&#x60;. It will be &#x60;null&#x60; for accounts of other types. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

