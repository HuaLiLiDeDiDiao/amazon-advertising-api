# InlineResponse20016

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_recommendation_results** | [**\AmazonAdvertisingApi\Model\SBCategoryResponse[]**](SBCategoryResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

