# SponsoredProductsCampaignNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingExpressionPredicate[]**](SponsoredProductsNegativeTargetingExpressionPredicate.md) | The CampaignNegativeTargetingClause expression. | 
**target_id** | **string** | The target identifier | 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingExpressionPredicate[]**](SponsoredProductsNegativeTargetingExpressionPredicate.md) | The resolved CampaignNegativeTargetingClause expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityState**](SponsoredProductsEntityState.md) |  | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignNegativeTargetingClauseExtendedData**](SponsoredProductsCampaignNegativeTargetingClauseExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

