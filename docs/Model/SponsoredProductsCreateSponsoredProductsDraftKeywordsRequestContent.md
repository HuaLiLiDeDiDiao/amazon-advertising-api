# SponsoredProductsCreateSponsoredProductsDraftKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftKeyword[]**](SponsoredProductsCreateDraftKeyword.md) | An array of draft keywords. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

