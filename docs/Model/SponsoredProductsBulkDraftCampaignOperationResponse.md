# SponsoredProductsBulkDraftCampaignOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignMutationSuccessResponseItem[]**](SponsoredProductsDraftCampaignMutationSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignMutationFailureResponseItem[]**](SponsoredProductsDraftCampaignMutationFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

