# BulkGetBillingStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\BulkGetBillingStatusSuccess[]**](BulkGetBillingStatusSuccess.md) |  | 
**error** | [**\AmazonAdvertisingApi\Model\BulkGetBillingStatusError[]**](BulkGetBillingStatusError.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

