# BrandLogo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_logo_crop** | [**\AmazonAdvertisingApi\Model\AssetCrop**](AssetCrop.md) |  | [optional] 
**brand_logo_url** | **string** |  | [optional] 
**brand_logo_asset_id** | **string** | The identifier of image/video asset from the store&#x27;s asset library | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

