# PreviewCreativeModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeRequest**](CreativeTypeInCreativeRequest.md) |  | [optional] 
**properties** | [**\AmazonAdvertisingApi\Model\CreativeProperties**](CreativeProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

