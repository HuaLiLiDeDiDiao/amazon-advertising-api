# AssociatedCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The campaign identifier. | 
**rule_status** | **string** | The budget rule evaluation status for this campaign. Read-only. | 
**campaign_name** | **string** | The campaign name. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

