# SponsoredProductsCreateGlobalTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The targeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**name** | **string** | Name for the targeting clause | [optional] 
**expression_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateExpressionType**](SponsoredProductsCreateExpressionType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | [optional] 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

