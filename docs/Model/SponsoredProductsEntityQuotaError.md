# SponsoredProductsEntityQuotaError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**\AmazonAdvertisingApi\Model\SponsoredProductsQuotaErrorReason**](SponsoredProductsQuotaErrorReason.md) |  | 
**quota_scope** | [**\AmazonAdvertisingApi\Model\SponsoredProductsQuotaScope**](SponsoredProductsQuotaScope.md) |  | [optional] 
**entity_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityType**](SponsoredProductsEntityType.md) |  | 
**quota** | **string** | optional current quota | [optional] 
**cause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsErrorCause**](SponsoredProductsErrorCause.md) |  | [optional] 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

