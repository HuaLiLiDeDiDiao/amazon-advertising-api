# CreateAccountRequestAccountMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendor_code** | **string** | Vendor code  that needs to  be associated with the vendor  account. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

