# SBDraftCampaignBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**draft_campaign_id** | **int** | The identifier of the draft campaign. | [optional] 
**name** | **string** | The name of the draft campaign. Maximum 128 characters. Duplicate campaign names are not allowed. | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**budget** | **float** | The budget associated with the draft campaign. | [optional] 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | [optional] 
**start_date** | **string** | The YYYYMMDD start date for the campaign. If this field is not set to a value, the current date is used. | [optional] 
**end_date** | **string** | The YYYYMMDD end date for the campaign. Must be greater than the value for &#x60;startDate&#x60;. If not specified, the campaign has no end date and runs continuously. | [optional] 
**brand_entity_id** | **string** | The brand entity identifier. Note that this field is required for sellers. For more information, see the [Stores reference](https://advertising.amazon.com/API/docs/v2/reference/stores) or [Brands reference](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Brands). | [optional] 
**bid_optimization** | **bool** | Set to &#x60;true&#x60; to allow Amazon to automatically optimize bids for placements below top of search. | [optional] [default to true]
**bid_multiplier** | **float** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 
**portfolio_id** | **int** | The identifier of the Portfolio to which the draft campaign is associated. | [optional] 
**ad_format** | [**\AmazonAdvertisingApi\Model\AdFormat**](AdFormat.md) |  | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBCreative**](SBCreative.md) |  | [optional] 
**landing_page** | [**OneOfSBDraftCampaignBaseLandingPage**](OneOfSBDraftCampaignBaseLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

