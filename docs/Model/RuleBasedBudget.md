# RuleBasedBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_processing** | **bool** |  | [optional] 
**applicable_rule_name** | **string** |  | [optional] 
**value** | **double** |  | [optional] 
**applicable_rule_id** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

