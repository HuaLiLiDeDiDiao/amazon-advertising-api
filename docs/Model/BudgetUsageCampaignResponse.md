# BudgetUsageCampaignResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\BudgetUsageCampaign[]**](BudgetUsageCampaign.md) | List of budget usage percentages that were successfully pulled | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\BudgetUsageCampaignBatchError[]**](BudgetUsageCampaignBatchError.md) | List of budget usage percentages that failed to pull | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

