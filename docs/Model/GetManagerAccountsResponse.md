# GetManagerAccountsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manager_accounts** | [**\AmazonAdvertisingApi\Model\ManagerAccount[]**](ManagerAccount.md) | List of Manager Accounts that the user has access to | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

