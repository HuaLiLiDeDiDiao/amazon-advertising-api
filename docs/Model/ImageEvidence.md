# ImageEvidence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top_left_y** | **int** | The top left Y-coordinate of the content that violates the specfied policy within the image. | [optional] 
**top_left_x** | **int** | The top left X-coordinate of the content that violates the specfied policy within the image. | [optional] 
**width** | **int** | The width of the content that violates the specfied policy within the image. | [optional] 
**height** | **int** | The height of the content that violates the specfied policy within the image. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

