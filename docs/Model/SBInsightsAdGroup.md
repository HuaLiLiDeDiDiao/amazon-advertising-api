# SBInsightsAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SBInsightsKeyword[]**](SBInsightsKeyword.md) |  | [optional] 
**ad_format** | [**\AmazonAdvertisingApi\Model\SBInsightsAdFormat**](SBInsightsAdFormat.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

