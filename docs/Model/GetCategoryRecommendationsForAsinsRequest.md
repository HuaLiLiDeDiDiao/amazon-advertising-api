# GetCategoryRecommendationsForAsinsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | List of input ASINs. This API does not check if the ASINs are valid ASINs. | [optional] 
**include_ancestor** | **bool** | Enable this if you would like to retrieve categories which are ancestor nodes of the original recommended categories. This may increase the number of categories returned, but decrease the relevancy of those categories. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

