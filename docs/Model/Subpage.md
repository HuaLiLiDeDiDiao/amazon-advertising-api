# Subpage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_title** | **string** |  | [optional] 
**asin** | **string** |  | [optional] 
**url** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

