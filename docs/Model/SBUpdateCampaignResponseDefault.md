# SBUpdateCampaignResponseDefault

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The campaign identifier. | [optional] 
**code** | **string** | An enumerated response code. | [optional] 
**description** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | [optional] 
**errors** | [**\AmazonAdvertisingApi\Model\ErrorEntry[]**](ErrorEntry.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

