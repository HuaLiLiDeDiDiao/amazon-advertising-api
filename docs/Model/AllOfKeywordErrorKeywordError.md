# AllOfKeywordErrorKeywordError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** |  | [optional] 
**match_type** | **string** |  | [optional] 
**bid** | **string** |  | [optional] 
**note** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

