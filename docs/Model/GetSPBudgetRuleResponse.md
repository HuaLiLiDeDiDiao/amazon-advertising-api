# GetSPBudgetRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_rule** | [**\AmazonAdvertisingApi\Model\SPBudgetRule**](SPBudgetRule.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

