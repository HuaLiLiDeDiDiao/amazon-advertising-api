# UpdateSPBudgetRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_rules_details** | [**\AmazonAdvertisingApi\Model\SPBudgetRule[]**](SPBudgetRule.md) | A list of budget rule details. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

