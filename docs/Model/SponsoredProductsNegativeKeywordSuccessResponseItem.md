# SponsoredProductsNegativeKeywordSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | the index of the negativeKeyword in the array from the request body | 
**negative_keyword** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeKeyword**](SponsoredProductsNegativeKeyword.md) |  | [optional] 
**negative_keyword_id** | **string** | the negativeKeyword ID | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

