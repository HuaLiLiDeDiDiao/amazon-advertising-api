# SponsoredProductsInternalServerExceptionResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\SponsoredProductsInternalErrorErrorCode**](SponsoredProductsInternalErrorErrorCode.md) |  | 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

