# CreativeModerationViolatingHeadlineContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reviewed_text** | **string** | The specific text reviewed during moderation. | [optional] 
**text_evidence** | [**\AmazonAdvertisingApi\Model\CreativeModerationTextEvidence[]**](CreativeModerationTextEvidence.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

