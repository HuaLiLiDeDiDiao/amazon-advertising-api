# SbNegativeKeywordsBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **int** | The identifier of the ad group to which the negative keyword is associated. | [optional] 
**campaign_id** | **int** | The identifier of the campaign to which the negative keyword is associated. | [optional] 
**keyword_text** | **string** | The keyword text. Maximum length is ten words if &#x27;matchType&#x27; is &#x27;negativeExact&#x27;. Maximum length is 4 words if &#x27;matchType&#x27; is &#x27;negativePhrase&#x27; | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](NegativeMatchType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

