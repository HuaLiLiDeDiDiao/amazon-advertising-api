# PolicyViolation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**policy_description** | **string** | A human-readable description of the policy. | [optional] 
**violating_text_contents** | [**\AmazonAdvertisingApi\Model\ViolatingTextContent[]**](ViolatingTextContent.md) |  | [optional] 
**violating_image_contents** | [**\AmazonAdvertisingApi\Model\ViolatingImageContent[]**](ViolatingImageContent.md) |  | [optional] 
**policy_link_url** | **string** | Address of the policy documentation. Follow the link to learn more about the specified policy. | [optional] 
**violating_video_contents** | [**\AmazonAdvertisingApi\Model\ViolatingVideoContent[]**](ViolatingVideoContent.md) |  | [optional] 
**violating_asin_contents** | [**\AmazonAdvertisingApi\Model\ViolatingAsinContent[]**](ViolatingAsinContent.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

