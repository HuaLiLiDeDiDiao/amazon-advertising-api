# SponsoredProductsCreateTargetPromotionGroupTargetsFailureResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetError[]**](SponsoredProductsCreateTargetError.md) | Response object of failed target promotion group target. | [optional] 
**target** | **string** | The target that was requested to be created. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

