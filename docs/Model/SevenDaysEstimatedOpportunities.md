# SevenDaysEstimatedOpportunities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**estimated_incremental_clicks_lower** | **int** | Lower bound of the estimated incremental clicks that could be gained if all optimizations are made. | [optional] 
**estimated_incremental_clicks_upper** | **int** | Upper bound of the estimated incremental clicks that could be gained if all optimizations are made. | [optional] 
**end_date** | **string** | End date of the opportunities date range in YYYY-MM-DDTHH:mm:ssZ format. | [optional] 
**start_date** | **string** | Start date of the opportunities date range in YYYY-MM-DDTHH:mm:ssZ format. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

