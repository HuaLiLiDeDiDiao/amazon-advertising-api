# SponsoredProductsUpdateSponsoredProductsDraftCampaignsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateDraftCampaign[]**](SponsoredProductsUpdateDraftCampaign.md) | An array of drafts with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

