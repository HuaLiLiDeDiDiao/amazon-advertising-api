# DSPAudienceFieldsV1Fees

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **int** | Fee amount in base currency units, multiplied by scaling factor (&#x27;scale&#x27;). | [optional] 
**scale** | **int** | Scale of the amount relative to the base currency unit. For instance, if the scale is 1000, the currency is USD, and the amount is 500, the human-readable fee is $0.50. | [optional] 
**currency** | **string** | Base currency, such as US Dollar. | [optional] 
**fee_calculation_type** | **string** | How the fee is applied. | [optional] 
**impression_supply_type** | **string** | To which supply types this fee applies to. The fee may be different for different supply types. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

