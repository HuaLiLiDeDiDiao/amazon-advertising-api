# ForecastBucketV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lower_bound_inclusive** | **int** | The inclusive lower bound for the bucket.  If not specified, the bucket captures all values below the upper bound. | [optional] 
**upper_bound_exclusive** | **int** | The exclusive upper bound for the bucket.  If not specified, the bucket captures all values above the lower bound. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

