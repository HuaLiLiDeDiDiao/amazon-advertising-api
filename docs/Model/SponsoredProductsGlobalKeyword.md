# SponsoredProductsGlobalKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword. | 
**campaign_id** | **string** | The identifier of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMatchType**](SponsoredProductsMatchType.md) |  | 
**name** | **string** | Name for the Keyword | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityState**](SponsoredProductsGlobalEntityState.md) |  | 
**bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this keyword is associated. | 
**keyword_text** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalKeywordText**](SponsoredProductsGlobalKeywordText.md) |  | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalKeywordExtendedData**](SponsoredProductsGlobalKeywordExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

