# DspAudienceErrorItemError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_name** | **string** |  | [optional] 
**error_type** | [**\AmazonAdvertisingApi\Model\ErrorType**](ErrorType.md) |  | 
**message** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

