# BrandMetricsReturnOnEngagementMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**viewed_detail_page_only** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 
**high_value_customers** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 
**brand_customers** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 
**branded_searches_and_detail_page_views** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 
**total_brand_purchasers** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 
**add_to_carts** | [**\AmazonAdvertisingApi\Model\BrandMetricsCurrencyAmount**](BrandMetricsCurrencyAmount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

