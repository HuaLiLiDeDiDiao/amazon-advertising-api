# SponsoredProductsProductAdMutationExceptionResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\SponsoredProductsInvalidArgumentErrorCode**](SponsoredProductsInvalidArgumentErrorCode.md) |  | 
**message** | **string** | Human readable error message | 
**errors** | [**\AmazonAdvertisingApi\Model\SponsoredProductsProductAdMutationError[]**](SponsoredProductsProductAdMutationError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

