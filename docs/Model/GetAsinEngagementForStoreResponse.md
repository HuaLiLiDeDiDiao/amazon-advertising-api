# GetAsinEngagementForStoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metrics_details** | [**\AmazonAdvertisingApi\Model\AsinEngagementDetail[]**](AsinEngagementDetail.md) |  | [optional] 
**dimension** | [**\AmazonAdvertisingApi\Model\AsinEngagementDimension**](AsinEngagementDimension.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

