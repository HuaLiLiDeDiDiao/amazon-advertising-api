# SBForecastingAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBForecastingProductTarget[]**](SBForecastingProductTarget.md) |  | [optional] 
**negative_targets** | [**\AmazonAdvertisingApi\Model\SBForecastingNegativeProductTarget[]**](SBForecastingNegativeProductTarget.md) |  | [optional] 
**landing_pages** | [**\AmazonAdvertisingApi\Model\SBForecastingLandingPageObject[]**](SBForecastingLandingPageObject.md) |  | [optional] 
**themes** | [**\AmazonAdvertisingApi\Model\SBForecastingTheme[]**](SBForecastingTheme.md) |  | [optional] 
**keywords** | [**\AmazonAdvertisingApi\Model\SBForecastingKeyword[]**](SBForecastingKeyword.md) |  | [optional] 
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SBForecastingNegativeKeyword[]**](SBForecastingNegativeKeyword.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

