# CategoryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | The category identifier. | [optional] 
**name** | **string** | The category name. | [optional] 
**is_targetable** | **string** | If set to &#x60;true&#x60;, indicates this category can be targeted in a targeting expression. Otherwise, set to false. | [optional] 
**path** | **string** | The path of this category within the category catalog. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

