# SponsoredProductsUpdateSponsoredProductsCampaignNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateCampaignNegativeKeyword[]**](SponsoredProductsUpdateCampaignNegativeKeyword.md) | An array of campaignNegativeKeywords with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

