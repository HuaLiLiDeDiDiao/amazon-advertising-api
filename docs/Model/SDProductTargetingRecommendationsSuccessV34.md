# SDProductTargetingRecommendationsSuccessV34

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | HTTP status code 200 indicating a successful response for product recommendations. | [optional] 
**name** | **string** | The theme name specified in the request. | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\SDProductTargetingThemeExpression[]**](SDProductTargetingThemeExpression.md) | A list of expressions defining the product targeting theme. The list will define an AND operator on different expressions. For example, asinPriceGreaterThan and asinReviewRatingLessThan can be used to request product recommendations which are both with greater price and less review rating compared to the goal products. Note: currently the service only support one item in the array. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\SDProductRecommendationV32[]**](SDProductRecommendationV32.md) | A list of recommended products. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

