# SponsoredProductsGlobalProductAdServingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductAServingStatusReason[]**](SponsoredProductsGlobalProductAServingStatusReason.md) | Serving status details of Product Ad | [optional] 
**marketplace_ad_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceProductAdServingStatus[]**](SponsoredProductsMarketplaceProductAdServingStatus.md) | The marketplace serving statuses. | [optional] 
**ad_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityServingStatus**](SponsoredProductsGlobalEntityServingStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

