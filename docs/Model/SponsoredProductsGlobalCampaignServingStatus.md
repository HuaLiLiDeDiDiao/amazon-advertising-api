# SponsoredProductsGlobalCampaignServingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalCampaignServingStatusReason[]**](SponsoredProductsGlobalCampaignServingStatusReason.md) | Serving status details of campaign | [optional] 
**marketplace_campaign_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceCampaignServingStatus[]**](SponsoredProductsMarketplaceCampaignServingStatus.md) | The marketplace serving statuses. | [optional] 
**campaign_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityServingStatus**](SponsoredProductsGlobalEntityServingStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

