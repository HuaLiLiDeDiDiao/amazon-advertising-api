# SponsoredProductsGlobalBid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_settings** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceBid[]**](SponsoredProductsMarketplaceBid.md) | marketplace bid settings. | [optional] 
**currency** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCurrency**](SponsoredProductsCurrency.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

