# ListSponsoredBrandsAdsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 
**state_filter** | [**\AmazonAdvertisingApi\Model\EntityStateFilter**](EntityStateFilter.md) |  | [optional] 
**max_results** | **float** | Number of records to include in the paginated response. Defaults to max page size for given API. | [optional] 
**next_token** | **string** | Token value allowing to navigate to the next response page. | [optional] 
**ad_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 
**name_filter** | [**\AmazonAdvertisingApi\Model\NameFilter**](NameFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

