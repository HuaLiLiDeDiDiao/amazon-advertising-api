# CreateCreative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | Unqiue identifier for the ad group associated with the creative. | 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeRequest**](CreativeTypeInCreativeRequest.md) |  | [optional] 
**properties** | [**\AmazonAdvertisingApi\Model\CreativeProperties**](CreativeProperties.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

