# GetOptimizationRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rule** | [**\AmazonAdvertisingApi\Model\OptimizationRule**](OptimizationRule.md) |  | [optional] 
**ad_group_ids** | [**\AmazonAdvertisingApi\Model\AdGroupId[]**](AdGroupId.md) | A list of adGroup identifiers that the optimization rule associates with. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

