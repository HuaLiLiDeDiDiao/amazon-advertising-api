# ProductAdExServingStatusDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The status identifier. | [optional] 
**severity** | **string** | An enumerated advertising eligibility severity status. If set to &#x60;INELIGIBLE&#x60;, the product cannot be included in an advertisement. If set to &#x60;ELIGIBLE_WITH_WARNING&#x60;, the product may not receive impressions when included in an advertisement. | [optional] 
**message** | **string** | A human-readable description of the status identifier specified in the &#x60;name&#x60; field. | [optional] 
**help_url** | **string** | A URL with additional information about the status identifier. May not be present for all status identifiers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

