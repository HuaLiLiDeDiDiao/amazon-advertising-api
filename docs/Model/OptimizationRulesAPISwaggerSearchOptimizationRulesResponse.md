# OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated error code for machine use. | [optional] 
**next_token** | **string** |  | [optional] 
**optimization_rules** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule[]**](OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

