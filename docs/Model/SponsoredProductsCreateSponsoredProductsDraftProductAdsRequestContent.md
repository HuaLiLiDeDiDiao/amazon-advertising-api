# SponsoredProductsCreateSponsoredProductsDraftProductAdsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_ads** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftProductAd[]**](SponsoredProductsCreateDraftProductAd.md) | An array of ads. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

