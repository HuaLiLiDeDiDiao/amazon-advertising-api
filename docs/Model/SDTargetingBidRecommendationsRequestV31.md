# SDTargetingBidRecommendationsRequestV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\AmazonAdvertisingApi\Model\SDGoalProduct[]**](SDGoalProduct.md) | A list of products to tailor bid recommendations for category and audience based targeting clauses. | [optional] 
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SDTargetingBidRecommendationsRequestV31TargetingClauses[]**](SDTargetingBidRecommendationsRequestV31TargetingClauses.md) | A list of targeting clauses to receive bid recommendations for. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

