# Refinements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_ranges** | [**\AmazonAdvertisingApi\Model\AgeRanges**](AgeRanges.md) |  | [optional] 
**brands** | [**\AmazonAdvertisingApi\Model\Brands**](Brands.md) |  | [optional] 
**genres** | [**\AmazonAdvertisingApi\Model\Genres**](Genres.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

