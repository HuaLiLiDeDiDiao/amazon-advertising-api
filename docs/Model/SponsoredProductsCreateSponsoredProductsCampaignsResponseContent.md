# SponsoredProductsCreateSponsoredProductsCampaignsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkCampaignOperationResponse**](SponsoredProductsBulkCampaignOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

