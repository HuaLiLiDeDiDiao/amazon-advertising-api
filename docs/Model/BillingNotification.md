# BillingNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**severity** | [**\AmazonAdvertisingApi\Model\BillingNotificationSeverity**](BillingNotificationSeverity.md) |  | 
**payment_due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**suspension_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | [**\AmazonAdvertisingApi\Model\BillingNotificationNames**](BillingNotificationNames.md) |  | 
**description** | **string** |  | 
**priority** | **int** |  | 
**title** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

