# SBVideoUpdateDraftCampaignRequestWithProductTargets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBTarget[]**](SBTarget.md) |  | [optional] 
**negative_targets** | [**\AmazonAdvertisingApi\Model\SBNegativeTarget[]**](SBNegativeTarget.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

