# SDRuleBasedBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**execution_time** | **float** | Epoch time of budget rule execution. | [optional] 
**applied_rule** | [**\AmazonAdvertisingApi\Model\SDBudgetRule**](SDBudgetRule.md) |  | [optional] 
**rule_based_budget_value** | **float** | The budget value. | [optional] 
**daily_budget_value** | **float** | The daily budget value. | [optional] 
**performance_metric** | [**\AmazonAdvertisingApi\Model\PerformanceMetricValue**](PerformanceMetricValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

