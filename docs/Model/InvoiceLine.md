# InvoiceLine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_tags** | **map[string,string]** | Campaign tags in the form of string key-value pairs. | [optional] 
**cost_event_type** | **string** | Type of event charged (clicks or impressions) | 
**commission_rate** | **float** |  | [optional] 
**fees** | [**\AmazonAdvertisingApi\Model\Fee[]**](Fee.md) | Charges can include different fees (see feeType below). | [optional] 
**cost** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**campaign_id** | **int** |  | [optional] 
**price_type** | **string** | Metric used for performance measurement. | 
**supply_cost** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 
**portfolio_id** | **int** | Sponsored Ads only. This identifier maps to one of the portfolios listed in the portfolios section. | [optional] 
**cost_per_event_type** | **float** | Ad spends cost (Cost exclusive of adjustments/promotions/fees/etc) per unit (thousand impressions/clicks). | [optional] 
**program_name** | [**\AmazonAdvertisingApi\Model\AdProgram**](AdProgram.md) |  | [optional] 
**cost_event_count** | **int** | Number of clicks/impressions charged | 
**purchase_order_number** | **string** |  | [optional] 
**name** | **string** |  | 
**campaign_name** | **string** |  | [optional] 
**promotion_amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 
**cost_per_unit** | **float** |  | 
**commission_amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

