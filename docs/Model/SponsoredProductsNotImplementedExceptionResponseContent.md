# SponsoredProductsNotImplementedExceptionResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNotImplementedExceptionCode**](SponsoredProductsNotImplementedExceptionCode.md) |  | [optional] 
**message** | **string** | A human-readable description of the response. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

