# KeywordBidRecommendationsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The identifier of the ad group that the keywords are associated with. | [optional] 
**keywords** | [**\AmazonAdvertisingApi\Model\KeywordBidRecommendationsDataKeywords[]**](KeywordBidRecommendationsDataKeywords.md) | An array of keyword data objects. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

