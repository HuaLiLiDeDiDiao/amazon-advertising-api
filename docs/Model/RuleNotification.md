# RuleNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule_state** | [**\AmazonAdvertisingApi\Model\RuleState**](RuleState.md) |  | [optional] 
**campaign_optimization_id** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationId**](CampaignOptimizationId.md) |  | [optional] 
**campaign_id** | [**\AmazonAdvertisingApi\Model\RuleCampaignId**](RuleCampaignId.md) |  | [optional] 
**notification_string** | **string** | Explains why the rule state is disabled | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

