# SponsoredProductsNegativeTargetMutationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | **string** | The type of the error | 
**error_value** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetMutationErrorSelector**](SponsoredProductsNegativeTargetMutationErrorSelector.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

