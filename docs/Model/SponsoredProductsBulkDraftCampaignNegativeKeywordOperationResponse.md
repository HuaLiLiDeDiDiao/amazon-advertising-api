# SponsoredProductsBulkDraftCampaignNegativeKeywordOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignNegativeKeywordSuccessResponseItem[]**](SponsoredProductsDraftCampaignNegativeKeywordSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignNegativeKeywordFailureResponseItem[]**](SponsoredProductsDraftCampaignNegativeKeywordFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

