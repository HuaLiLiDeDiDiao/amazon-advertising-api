# SDProductRecommendationV32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asin** | [**\AmazonAdvertisingApi\Model\SDASIN**](SDASIN.md) |  | [optional] 
**rank** | **int** | A rank to signify which recommendations are weighed more heavily, with a lower rank signifying a stronger recommendation | [optional] 
**advertised_asins** | [**\AmazonAdvertisingApi\Model\SDASIN[]**](SDASIN.md) | The top advertised products this recommendation is made for. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

