# SponsoredProductsUpdateSponsoredProductsGlobalKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalKeyword[]**](SponsoredProductsUpdateGlobalKeyword.md) | An array of keywords with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

