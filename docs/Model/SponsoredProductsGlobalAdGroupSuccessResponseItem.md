# SponsoredProductsGlobalAdGroupSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalAdGroup**](SponsoredProductsGlobalAdGroup.md) |  | [optional] 
**index** | **int** | the index of the adGroup in the array from the request body | 
**ad_group_id** | **string** | the adGroup ID | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

