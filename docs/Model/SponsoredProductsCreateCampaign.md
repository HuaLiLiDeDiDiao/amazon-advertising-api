# SponsoredProductsCreateCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **string** | The identifier of an existing portfolio to which the campaign is associated. | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**name** | **string** | The name of the campaign. | 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDynamicBidding**](SponsoredProductsCreateOrUpdateDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | Default: today&#x27;s date. The format of the date is YYYY-MM-DD. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateBudget**](SponsoredProductsCreateOrUpdateBudget.md) |  | 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

