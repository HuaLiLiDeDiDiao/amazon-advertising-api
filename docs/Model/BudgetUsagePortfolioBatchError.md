# BudgetUsagePortfolioBatchError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **string** | ID of requested resource | [optional] 
**code** | **string** | An enumerated error code for machine use. | [optional] 
**index** | **float** | An index to maintain order of the portfolioIds | [optional] 
**details** | **string** | A human-readable description of the response. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

