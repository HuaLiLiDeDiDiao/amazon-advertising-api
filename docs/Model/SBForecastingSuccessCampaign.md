# SBForecastingSuccessCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forecasts** | [**\AmazonAdvertisingApi\Model\SBForecastingMetric[]**](SBForecastingMetric.md) |  | [optional] 
**forecast_timestamp** | **string** | The forecast timestamp. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

