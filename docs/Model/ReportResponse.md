# ReportResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The identifier of the report. | [optional] 
**record_type** | **string** | The type of report requested. | [optional] 
**status** | **string** | The build status of the report. | [optional] 
**status_details** | **string** | A human-readable description of the current status. | [optional] 
**location** | **string** | The URI location of the report. | [optional] 
**file_size** | **int** | The size of the report file, in bytes. | [optional] 
**expiration** | **int** | Epoch date of the expiration of the URI in the &#x60;location&#x60; property. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

