# BrandSafetyGetResponsePagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | The total number of deny list domains created by the advertiser | [optional] 
**limit** | **int** | The maximum number of deny list domains returned from GET request | [optional] 
**offset** | **int** | The number of deny list domains skipped | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

