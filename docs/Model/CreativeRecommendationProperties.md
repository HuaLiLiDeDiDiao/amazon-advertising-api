# CreativeRecommendationProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | ----------------------------------------------- List types ----------------------------------------------- A list of ASINs | [optional] 
**brand_name** | **string** | The displayed brand name in the ad headline. Maximum length is 30 characters. See [the policy](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#headlines) for headline requirements. | [optional] 
**subpages** | [**\AmazonAdvertisingApi\Model\Subpage[]**](Subpage.md) | An array of subpages | [optional] 
**landing_page** | [**\AmazonAdvertisingApi\Model\CreativeLandingPageV2**](CreativeLandingPageV2.md) |  | [optional] 
**custom_images** | [**\AmazonAdvertisingApi\Model\CustomImage[]**](CustomImage.md) | An array of customImages associated with the creative. | [optional] 
**video_asset_ids** | **string[]** | An array of videoAssetIds associated with the creative. Advertisers can get video assetIds from Asset Library /assets/search API. | [optional] 
**recommended_creative_id** | **string** | a Unique Id identifying the creative Recommendation | [optional] 
**brand_logo** | [**\AmazonAdvertisingApi\Model\BrandLogo**](BrandLogo.md) |  | [optional] 
**headline** | **string** | The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. See [the policy](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies#headlines) for headline requirements. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

