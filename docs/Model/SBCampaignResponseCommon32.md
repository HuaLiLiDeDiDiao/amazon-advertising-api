# SBCampaignResponseCommon32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_optimization** | **bool** | Set to &#x60;true&#x60; to allow Amazon to automatically optimize bids for placements below top of search. | [optional] 
**bid_multiplier** | **float** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. For example, if set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 
**creative** | [****](.md) |  | [optional] 
**landing_page** | [****](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

