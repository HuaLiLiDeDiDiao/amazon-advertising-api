# BudgetRecommendationNewCampaignsException

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | [**\AmazonAdvertisingApi\Model\BudgetRecommendationNewCampaignsErrorMessage**](BudgetRecommendationNewCampaignsErrorMessage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

