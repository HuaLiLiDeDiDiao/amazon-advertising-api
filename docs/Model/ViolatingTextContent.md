# ViolatingTextContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reviewed_text** | **string** | The actual text on which the moderation was done. | [optional] 
**violating_text_evidences** | [**\AmazonAdvertisingApi\Model\ViolatingTextEvidence[]**](ViolatingTextEvidence.md) |  | [optional] 
**moderated_component** | **string** | Moderation component which marked the policy violation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

