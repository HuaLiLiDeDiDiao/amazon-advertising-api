# SDProductRecommendationsV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\AmazonAdvertisingApi\Model\SDProductRecommendation[]**](SDProductRecommendation.md) | List of recommended product targets | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

