# ListAudiencesRequestBodyV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_type** | **string** |  | [optional] 
**filters** | [**\AmazonAdvertisingApi\Model\AudienceFilterV1[]**](AudienceFilterV1.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

