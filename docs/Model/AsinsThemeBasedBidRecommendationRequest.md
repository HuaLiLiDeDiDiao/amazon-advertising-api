# AsinsThemeBasedBidRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | The list of ad ASINs in the ad group. | 
**targeting_expressions** | [**\AmazonAdvertisingApi\Model\TargetingExpressionList**](TargetingExpressionList.md) |  | 
**bidding** | [**\AmazonAdvertisingApi\Model\AsinsThemeBasedBidRecommendationRequestBidding**](AsinsThemeBasedBidRecommendationRequestBidding.md) |  | 
**recommendation_type** | **string** | The bid recommendation type. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

