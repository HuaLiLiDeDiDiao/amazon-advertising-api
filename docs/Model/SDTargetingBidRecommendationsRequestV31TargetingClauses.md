# SDTargetingBidRecommendationsRequestV31TargetingClauses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clause** | [**\AmazonAdvertisingApi\Model\SDTargetingClauseV31**](SDTargetingClauseV31.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

