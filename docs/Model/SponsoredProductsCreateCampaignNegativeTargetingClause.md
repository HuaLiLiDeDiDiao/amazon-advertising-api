# SponsoredProductsCreateCampaignNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate[]**](SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate.md) | The NegativeTargeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. CampaignNegativeTargetingClauses are only available for AUTO campaigns | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

