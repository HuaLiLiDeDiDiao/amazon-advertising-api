# SBUpdateDraftCampaignRequestWithKeywordsKeywords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\Bid**](Bid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

