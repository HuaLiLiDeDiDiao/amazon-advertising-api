# DSPReportsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | A unique identifier of the request. | [optional] 
**message** | **string** | A human-readable description of the response. | 
**errors** | [**\AmazonAdvertisingApi\Model\DSPReportsSubError[]**](DSPReportsSubError.md) | A list of errors. Please check the values in this field for report validation errors. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

