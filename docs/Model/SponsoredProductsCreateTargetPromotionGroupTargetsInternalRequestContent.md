# SponsoredProductsCreateTargetPromotionGroupTargetsInternalRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_promotion_group_id** | **string** | Entity object identifier | 
**api_gateway_context** | [**\AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext**](SponsoredProductsApiGatewayContext.md) |  | 
**targets** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetRequest[]**](SponsoredProductsCreateTargetRequest.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

