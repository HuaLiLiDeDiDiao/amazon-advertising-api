# BidRecommendationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **string** | The identifier of the ad group that the recommendations are associated with. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\BidRecommendationsResponseRecommendations[]**](BidRecommendationsResponseRecommendations.md) | An array of bid recommendation objects. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

