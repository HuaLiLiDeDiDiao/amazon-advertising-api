# NegativeKeywordResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **float** | The identifier of the keyword. | [optional] 
**code** | **string** | The success or error code for the operation. | [optional] 
**details** | **string** | The human-readable description of the error. | [optional] 
**description** | **string** | The human-readable description of the error. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

