# SBBudgetRuleDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | [**\AmazonAdvertisingApi\Model\RuleDuration**](RuleDuration.md) |  | [optional] 
**recurrence** | [**\AmazonAdvertisingApi\Model\Recurrence**](Recurrence.md) |  | [optional] 
**rule_type** | [**\AmazonAdvertisingApi\Model\SBRuleType**](SBRuleType.md) |  | [optional] 
**budget_increase_by** | [**\AmazonAdvertisingApi\Model\BudgetIncreaseBy**](BudgetIncreaseBy.md) |  | [optional] 
**name** | **string** | The budget rule name. Required to be unique within a campaign. | [optional] 
**performance_measure_condition** | [**\AmazonAdvertisingApi\Model\PerformanceMeasureConditionForSB**](PerformanceMeasureConditionForSB.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

