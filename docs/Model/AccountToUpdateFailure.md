# AccountToUpdateFailure

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | [**\AmazonAdvertisingApi\Model\ErrorDetail**](ErrorDetail.md) |  | [optional] 
**account** | [**\AmazonAdvertisingApi\Model\AccountToUpdate**](AccountToUpdate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

