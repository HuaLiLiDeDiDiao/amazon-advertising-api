# SponsoredProductsCopyCampaignResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | Id of the request to be passed in GET /copy/{requestId}. | [optional] 
**error_detail** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaignErrorDetail**](SponsoredProductsCopyCampaignErrorDetail.md) |  | [optional] 
**copy_campaign_item** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaign**](SponsoredProductsCopyCampaign.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

