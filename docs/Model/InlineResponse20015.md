# InlineResponse20015

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommended_products** | [**\AmazonAdvertisingApi\Model\InlineResponse20015RecommendedProducts[]**](InlineResponse20015RecommendedProducts.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

