# BudgetUsageCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_usage_percent** | **float** | Budget usage percentage (spend / available budget) for the given budget policy. | [optional] 
**campaign_id** | **string** | ID of requested resource | [optional] 
**usage_updated_timestamp** | [**\DateTime**](\DateTime.md) | Last evaluation time for budget usage | [optional] 
**index** | **float** | An index to maintain order of the campaignIds | [optional] 
**budget** | **float** | Budget amount of resource requested | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

