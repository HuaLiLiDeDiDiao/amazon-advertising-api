# SevenDaysMissedOpportunities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**estimated_missed_sales_lower** | **double** | Lower bound of the estimated Missed Sales. This will be in local currency. | [optional] 
**estimated_missed_sales_upper** | **double** | Upper bound of the estimated Missed Sales. This will be in local currency. | [optional] 
**end_date** | **string** | End date of the Missed Opportunities date range (YYYY-MM-DD) in local time. | [optional] 
**estimated_missed_impressions_lower** | **float** | Lower bound of the estimated Missed Impressions. | [optional] 
**estimated_missed_clicks_lower** | **float** | Lower bound of the estimated Missed Clicks. | [optional] 
**estimated_missed_clicks_upper** | **float** | Upper bound of the estimated Missed Clicks. | [optional] 
**estimated_missed_impressions_upper** | **float** | Upper bound of the estimated Missed Impressions. | [optional] 
**start_date** | **string** | Start date of the Missed Opportunities date range (YYYY-MM-DD) in local time. | [optional] 
**percent_time_in_budget** | **double** | Percentage of time the campaign is active with a budget. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

