# RecommendedBid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**range_end** | **double** |  | [optional] 
**range_start** | **double** |  | [optional] 
**recommended** | **double** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

