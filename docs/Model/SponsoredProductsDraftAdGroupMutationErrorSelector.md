# SponsoredProductsDraftAdGroupMutationErrorSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_not_found_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityNotFoundError**](SponsoredProductsEntityNotFoundError.md) |  | [optional] 
**entity_state_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityStateError**](SponsoredProductsEntityStateError.md) |  | [optional] 
**missing_value_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMissingValueError**](SponsoredProductsMissingValueError.md) |  | [optional] 
**bidding_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBiddingError**](SponsoredProductsBiddingError.md) |  | [optional] 
**malformed_value_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMalformedValueError**](SponsoredProductsMalformedValueError.md) |  | [optional] 
**duplicate_value_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDuplicateValueError**](SponsoredProductsDuplicateValueError.md) |  | [optional] 
**billing_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBillingError**](SponsoredProductsBillingError.md) |  | [optional] 
**entity_quota_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityQuotaError**](SponsoredProductsEntityQuotaError.md) |  | [optional] 
**internal_server_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsInternalServerError**](SponsoredProductsInternalServerError.md) |  | [optional] 
**range_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsRangeError**](SponsoredProductsRangeError.md) |  | [optional] 
**other_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsOtherError**](SponsoredProductsOtherError.md) |  | [optional] 
**throttled_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsThrottledError**](SponsoredProductsThrottledError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

