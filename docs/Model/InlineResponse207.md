# InlineResponse207

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The campaign identifier. | [optional] 
**ad_group_responses** | [**\AmazonAdvertisingApi\Model\InlineResponse207AdGroupResponses[]**](InlineResponse207AdGroupResponses.md) | An array of ad groups associated with the campaign. | [optional] 
**keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) | An array of keywords associated with the campaign. | [optional] 
**negative_keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) | An array of negative keywords associated with the campaign. | [optional] 
**code** | **string** | An enumerated response code. | [optional] 
**details** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

