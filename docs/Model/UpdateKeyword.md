# UpdateKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **float** | The identifier of the keyword. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**bid** | **float** | Bid associated with this keyword. Applicable to biddable match types only. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

