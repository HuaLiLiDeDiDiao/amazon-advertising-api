# ProductRecommendationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_result_count** | **float** | The total number of recommendations in the response. | [optional] 
**recommended_products** | [**\AmazonAdvertisingApi\Model\ProductRecommendationsResponseRecommendedProducts[]**](ProductRecommendationsResponseRecommendedProducts.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

