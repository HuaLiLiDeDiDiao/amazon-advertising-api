# SponsoredProductsEntityNotFoundError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityNotFoundErrorReason**](SponsoredProductsEntityNotFoundErrorReason.md) |  | 
**entity_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityType**](SponsoredProductsEntityType.md) |  | 
**cause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsErrorCause**](SponsoredProductsErrorCause.md) |  | [optional] 
**entity_id** | **string** | The entity id in the request | 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

