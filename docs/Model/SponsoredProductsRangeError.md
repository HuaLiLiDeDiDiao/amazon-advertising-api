# SponsoredProductsRangeError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**\AmazonAdvertisingApi\Model\SponsoredProductsValueLimitErrorReason**](SponsoredProductsValueLimitErrorReason.md) |  | 
**marketplace** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplace**](SponsoredProductsMarketplace.md) |  | [optional] 
**allowed** | **string[]** | allowed values | [optional] 
**cause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsErrorCause**](SponsoredProductsErrorCause.md) |  | [optional] 
**upper_limit** | **string** | optional upper limit | [optional] 
**lower_limit** | **string** | optional lower limit | [optional] 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

