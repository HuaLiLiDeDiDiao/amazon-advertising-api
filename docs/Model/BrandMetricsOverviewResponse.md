# BrandMetricsOverviewResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | **string** | Token  to fetch additional results (if any). Subsequent calls must be made with same  parameters as in the previous requests. | [optional] 
**overview_metrics** | [**\AmazonAdvertisingApi\Model\BrandMetricsOverviewResponseOverviewMetrics[]**](BrandMetricsOverviewResponseOverviewMetrics.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

