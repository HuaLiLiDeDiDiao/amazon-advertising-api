# BudgetUsageCampaignBatchError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated error code for machine use. | [optional] 
**campaign_id** | **string** | ID of requested resource | [optional] 
**index** | **float** | An index to maintain order of the campaignIds | [optional] 
**details** | **string** | A human-readable description of the response. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

