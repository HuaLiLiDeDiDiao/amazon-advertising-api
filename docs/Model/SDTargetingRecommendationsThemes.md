# SDTargetingRecommendationsThemes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\AmazonAdvertisingApi\Model\SDProductTargetingTheme[]**](SDProductTargetingTheme.md) | A list of themes for product targeting recommendations. If this list is empty, the service will return all the current available theme recommendations. Recommendations will be returned for each theme. If specified, each theme should only include unique expressions. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

