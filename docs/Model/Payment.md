# Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_payment_attempt_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | [optional] 
**reason** | **string** | Provides additional details and reason for the payment status | [optional] 
**amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**payment_method** | [**\AmazonAdvertisingApi\Model\PaymentMethod**](PaymentMethod.md) |  | 
**current_payment_attempt_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | [optional] 
**id** | **int** |  | 
**last_payment_attempt_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | [optional] 
**refunded_amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 
**status** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

