# AdGroupSuggestedKeywordsResponseEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The ad group identifier. | [optional] 
**campaign_id** | **float** | The campaign identifier. | [optional] 
**keyword_text** | **string** | The suggested keyword. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**state** | **string** | The state of the ad for which the keyword is suggested. | [optional] 
**bid** | **float** | The suggested bid for the suggested keyword. Note that this field will not be included in the response if the &#x60;suggestBids&#x60; query parameter is set to &#x60;no&#x60; in the request. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

