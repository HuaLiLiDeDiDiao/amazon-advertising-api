# SponsoredProductsCreateSponsoredProductsGlobalCampaignNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalCampaignNegativeKeyword[]**](SponsoredProductsCreateGlobalCampaignNegativeKeyword.md) | An array of campaignNegativeKeywords. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

