# SponsoredProductsUpdateNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

