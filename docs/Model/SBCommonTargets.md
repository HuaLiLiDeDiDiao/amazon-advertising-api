# SBCommonTargets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBCommonTargetsTargets[]**](SBCommonTargetsTargets.md) |  | [optional] 
**negative_targets** | [**\AmazonAdvertisingApi\Model\SBCommonTargetsNegativeTargets[]**](SBCommonTargetsNegativeTargets.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

