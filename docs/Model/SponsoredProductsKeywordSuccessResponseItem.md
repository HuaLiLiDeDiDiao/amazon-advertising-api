# SponsoredProductsKeywordSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | the keyword ID | [optional] 
**index** | **int** | the index of the keyword in the array from the request body | 
**keyword** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeyword**](SponsoredProductsKeyword.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

