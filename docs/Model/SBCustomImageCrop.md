# SBCustomImageCrop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top** | **int** | The highest pixel from which to begin cropping | [optional] 
**left** | **int** | The leftmost pixel from which to begin cropping | [optional] 
**width** | **int** | The number of pixels to crop rightwards from the value specified as &#x60;left&#x60; | [optional] 
**height** | **int** | The number of pixels to crop down from the value specified as &#x60;top&#x60; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

