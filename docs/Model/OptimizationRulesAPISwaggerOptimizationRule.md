# OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rule_id** | **string** | The rule identifier. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

