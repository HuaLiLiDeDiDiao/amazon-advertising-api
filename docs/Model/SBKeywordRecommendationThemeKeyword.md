# SBKeywordRecommendationThemeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommendation_id** | **string** | Unique ID for each recommendation. | [optional] 
**value** | **string** | Recommended keyword value. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

