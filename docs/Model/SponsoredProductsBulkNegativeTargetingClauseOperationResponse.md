# SponsoredProductsBulkNegativeTargetingClauseOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingClauseSuccessResponseItem[]**](SponsoredProductsNegativeTargetingClauseSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingClauseFailureResponseItem[]**](SponsoredProductsNegativeTargetingClauseFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

