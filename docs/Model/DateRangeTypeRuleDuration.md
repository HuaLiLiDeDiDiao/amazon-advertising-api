# DateRangeTypeRuleDuration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | **string** | The end date of the budget rule in YYYYMMDD format. The end date is inclusive. Required to be equal or greater than &#x60;startDate&#x60;. | [optional] 
**start_date** | **string** | The start date of the budget rule in YYYYMMDD format. The start date is inclusive. Required to be greater than or equal to current date. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

