# SponsoredProductsCreateSponsoredProductsNegativeKeywordsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkNegativeKeywordOperationResponse**](SponsoredProductsBulkNegativeKeywordOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

