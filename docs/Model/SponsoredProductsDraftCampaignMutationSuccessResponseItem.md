# SponsoredProductsDraftCampaignMutationSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | the draft ID | [optional] 
**index** | **int** | the index of the draft in the array from the request body | 
**campaign** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaign**](SponsoredProductsDraftCampaign.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

