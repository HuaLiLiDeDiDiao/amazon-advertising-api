# CreateVideoAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the ad. | 
**state** | [**\AmazonAdvertisingApi\Model\CreateOrUpdateEntityState**](CreateOrUpdateEntityState.md) |  | 
**ad_group_id** | **string** | The adGroup identifier. | 
**creative** | [**\AmazonAdvertisingApi\Model\CreateVideoCreative**](CreateVideoCreative.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

