# InvoiceSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fees** | [**\AmazonAdvertisingApi\Model\Fee[]**](Fee.md) | Regulatory Advertising Fees. | [optional] 
**payment_terms_days** | **int** |  | [optional] 
**tax_amount_due** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 
**remaining_tax_amount_due** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | [optional] 
**remaining_fees** | [**\AmazonAdvertisingApi\Model\Fee[]**](Fee.md) | Remaining Regulatory Advertising Fees. | [optional] 
**to_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**due_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | [optional] 
**billing_aggregation** | [**\AmazonAdvertisingApi\Model\BillingAggregation**](BillingAggregation.md) |  | [optional] 
**invoice_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**remaining_amount_due** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**from_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**amount_due** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**tax_rate** | **float** |  | [optional] 
**purchase_order_number** | **string** |  | [optional] 
**payment_terms_type** | **string** |  | [optional] 
**payment_method** | [**\AmazonAdvertisingApi\Model\PaymentMethod**](PaymentMethod.md) |  | [optional] 
**id** | **string** |  | 
**downloadable_documents** | [**\AmazonAdvertisingApi\Model\DocumentType[]**](DocumentType.md) | List of downloadable documents associated with this invoice and accessible from the advertising console. | [optional] 
**status** | [**\AmazonAdvertisingApi\Model\InvoiceStatus**](InvoiceStatus.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

