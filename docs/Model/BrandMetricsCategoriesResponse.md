# BrandMetricsCategoriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_node_ids** | [**\AmazonAdvertisingApi\Model\BrandMetricsCategoriesResponseCategoryNodeIds[]**](BrandMetricsCategoriesResponseCategoryNodeIds.md) | List of category Node IDs owned by the calling advertiser. Returns all the categories if brandName and BrandId are not passed. | 
**next_token** | **string** | Token  to fetch additional results (if any). Subsequent calls must be made with same  parameters as in the previous requests. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

