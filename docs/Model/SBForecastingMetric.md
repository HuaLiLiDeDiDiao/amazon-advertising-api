# SBForecastingMetric

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metric** | **string** | The forecast metric name. Currently only IMPRESSIONS is supported. | [optional] 
**value** | [**\AmazonAdvertisingApi\Model\SBForecastingMetricValue**](SBForecastingMetricValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

