# CreativeModerationViolatingBrandLogoContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reviewed_image_url** | **string** | Address of the image reviewed during moderation. | [optional] 
**image_evidences** | [**\AmazonAdvertisingApi\Model\CreativeModerationImageEvidences[]**](CreativeModerationImageEvidences.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

