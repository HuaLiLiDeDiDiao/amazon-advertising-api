# Bidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_optimization** | **bool** | Whether to use automatic placement level bid optimization. If set to true, Amazon will automatically set the right placement adjustment and the bidAdjustmentsByPlacement field is ignored. If set to false, the bidAdjustmentsByPlacement field will be used to adjust bid on different placements. If this field is changed from false to true, the bidAdjustmentsByPlacement field will be reset to null. | [optional] 
**bid_adjustments_by_shopper_segment** | [**\AmazonAdvertisingApi\Model\BidAdjustmentByShopperSegment[]**](BidAdjustmentByShopperSegment.md) | DEPRECATED [PLANNED SHUTOFF DATE 3/31/2024] **Note: This feature has been deprecated and planned to shutoff on 03/31/2024. After the shut off date, we will ignore this field in the request and treat it as null. You will not get an error if you supply this field in the request. Based on customer feedback, we are rethinking this feature in context to Goal based campaigns to help advertiser reach NTB customers at scale with transparent reporting. Meanwhile, if you have any feedback or suggestion related to this feature then please reach out to our customer support teams.  Shopper segment level bid adjustment. When both bidAdjustmentsByPlacement and bidAdjustmentsByShopperSegment are specified, the adjustment will be multiplicative. | [optional] 
**bid_adjustments_by_placement** | [**\AmazonAdvertisingApi\Model\BidAdjustmentByPlacement[]**](BidAdjustmentByPlacement.md) | Placement level bid adjustment. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. | [optional] 
**bid_optimization_strategy** | [**\AmazonAdvertisingApi\Model\BidOptimizationStrategy**](BidOptimizationStrategy.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

