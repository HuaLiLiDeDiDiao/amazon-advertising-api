# Recurrence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\RecurrenceType**](RecurrenceType.md) |  | [optional] 
**days_of_week** | [**\AmazonAdvertisingApi\Model\DayOfWeek[]**](DayOfWeek.md) | Object representing days of the week for weekly type rule. It is not required for daily recurrence type | [optional] 
**intra_day_schedule** | [**\AmazonAdvertisingApi\Model\TimeOfDay[]**](TimeOfDay.md) | List of objects representing start and end time of desired intra-day budget rule window | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

