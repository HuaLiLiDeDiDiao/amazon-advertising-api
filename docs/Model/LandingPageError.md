# LandingPageError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**landing_page_error** | [**AllOfLandingPageErrorLandingPageError**](AllOfLandingPageErrorLandingPageError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

