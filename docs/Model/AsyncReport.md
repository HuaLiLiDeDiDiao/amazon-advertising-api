# AsyncReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The identifier of the requested report. | 
**end_date** | **string** | The end date for the reporting period in YYYY-mm-dd format. | 
**configuration** | [**\AmazonAdvertisingApi\Model\AsyncReportConfiguration**](AsyncReportConfiguration.md) |  | 
**url_expires_at** | **string** | The date at which the download URL for the generated report expires. urlExpires at this time defaults to 3600 seconds but may vary in the future. | [optional] 
**url** | **string** | URL of the generated report. | [optional] 
**created_at** | **string** | The date at which the report was created in ISO 8601 date time format. | 
**file_size** | **float** | The size of the report file, in bytes. | [optional] 
**failure_reason** | **string** | Present for failed reports only. The reason why a report failed to generate. | [optional] 
**name** | **string** | Optional. The name of the generated report. | [optional] 
**generated_at** | **string** | The date at which the report was generated in ISO 8601 date time format. | [optional] 
**start_date** | **string** | The start date for the reporting period in YYYY-mm-dd format. | 
**status** | **string** | The build status of the report.   - &#x60;PENDING&#x60; - Report is created and awaiting processing.   - &#x60;PROCESSING&#x60; - Report is processing. Please wait.   - &#x60;COMPLETED&#x60; - Report has completed.  Check the &#x60;url&#x60; for the output file.   - &#x60;FAILED&#x60; - Report generation failed.  Check the &#x60;failureReason&#x60; for details. | 
**updated_at** | **string** | The date at which the report was last updated in ISO 8601 date time format. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

