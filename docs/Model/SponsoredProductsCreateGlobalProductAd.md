# SponsoredProductsCreateGlobalProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The campaign identifier. | 
**custom_text** | **string** | The custom text to use for creating a custom text ad for the associated ASIN. Defined for only KDP Authors and Book Vendors | [optional] 
**name** | **string** | Name for the product Ad | [optional] 
**asin** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**sku** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 
**ad_group_id** | **string** | The ad group identifier. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

