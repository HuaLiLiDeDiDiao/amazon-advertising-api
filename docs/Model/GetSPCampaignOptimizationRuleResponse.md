# GetSPCampaignOptimizationRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_optimization_rule** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationRule**](CampaignOptimizationRule.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

