# SuggestedKeywordsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | A list of ASINs. | [optional] 
**max_num_suggestions** | **int** | The maximum number of suggested keywords in the response. | [optional] [default to 100]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

