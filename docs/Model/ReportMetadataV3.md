# ReportMetadataV3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The identifier of the report. | [optional] 
**format** | **string** | The data format of the report. | [optional] 
**status_details** | **string** | A human-readable description of the current status. | [optional] 
**location** | **string** | The URI address of the report. | [optional] 
**expiration** | [**\DateTime**](\DateTime.md) | The expiration time of the URI in the location property in date-time format(yyyy-MM-ddTHH:mm:ss). The expiration time is the time when the download link expires. | [optional] 
**type** | **string** | The type of report. | [optional] 
**status** | **string** | The build status of the report. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

