# SponsoredProductsTargetingClauseSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingClause**](SponsoredProductsTargetingClause.md) |  | [optional] 
**target_id** | **string** | the targetingClause ID | [optional] 
**index** | **int** | the index of the targetingClause in the array from the request body | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

