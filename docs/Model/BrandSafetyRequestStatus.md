# BrandSafetyRequestStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | Request ID | [optional] 
**timestamp** | **string** | Request timestamp | [optional] 
**status** | **string** | The status of the request | [optional] 
**status_details** | **string** | Details related to the request status | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

