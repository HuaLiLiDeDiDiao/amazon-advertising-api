# SponsoredProductsGlobalNegativeKeywordText

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_settings** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeywordTextMarketplaceSettings[]**](SponsoredProductsKeywordTextMarketplaceSettings.md) | The marketplace settings for keyword text to be overridden for marketplace. | [optional] 
**value** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

