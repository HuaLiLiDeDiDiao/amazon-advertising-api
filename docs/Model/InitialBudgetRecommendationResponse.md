# InitialBudgetRecommendationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**special_events** | [**\AmazonAdvertisingApi\Model\SpecialEvent[]**](SpecialEvent.md) | A list of special events around the start and end date of the campaign. | 
**daily_budget** | **float** | Recommended daily budget for the new campaign. Note: value -1 means we don’t have enough information to provide a recommendation. | 
**recommendation_id** | **string** | Unique identifier for each recommendation. | [optional] 
**benchmark** | [**\AmazonAdvertisingApi\Model\Benchmark**](Benchmark.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

