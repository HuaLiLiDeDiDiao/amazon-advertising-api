# OptimizationRulesAPIAmazonAdvertisingApiRequestFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rule_filter** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleFilter**](OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleFilter.md) |  | [optional] 
**campaign_filter** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiCampaignFilter**](OptimizationRulesAPIAmazonAdvertisingApiCampaignFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

