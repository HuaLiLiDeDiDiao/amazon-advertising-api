# SBKeywordRecommendationThemes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_type** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationThemeType**](SBKeywordRecommendationThemeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

