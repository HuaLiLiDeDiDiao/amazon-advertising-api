# SponsoredProductsGlobalTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The targeting expression. | 
**target_id** | **string** | The target identifier | 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The resolved targeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**name** | **string** | Name for the targeting clause | [optional] 
**expression_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExpressionType**](SponsoredProductsExpressionType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityState**](SponsoredProductsGlobalEntityState.md) |  | 
**bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingClauseExtendedData**](SponsoredProductsGlobalTargetingClauseExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

