# InsightFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_ids** | **string[]** | List of pages to be fetched for insight metrics. Users can first make request to the API with the same parameters but without the filter to retrieve all the available page ids. | [optional] 
**sources** | [**\AmazonAdvertisingApi\Model\TrafficSource[]**](TrafficSource.md) | List of sources to be fetched for insight metrics. | [optional] 
**tags** | **string[]** | List of tags to be fetched for insight metrics. Users can first make request to the API with the same parameters but without the filter to retrieve all the available tag names. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

