# CreativeModerationVideoEvidences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_video_position** | [**\AmazonAdvertisingApi\Model\CreativeModerationViolatingVideoPosition**](CreativeModerationViolatingVideoPosition.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

