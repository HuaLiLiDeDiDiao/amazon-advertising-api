# GetTargetableAsinCountsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_ranges** | [**\AmazonAdvertisingApi\Model\AgeRanges**](AgeRanges.md) |  | [optional] 
**brands** | [**\AmazonAdvertisingApi\Model\Brands**](Brands.md) |  | [optional] 
**genres** | [**\AmazonAdvertisingApi\Model\Genres**](Genres.md) |  | [optional] 
**is_prime_shipping** | **bool** | Indicates if products have prime shipping | [optional] 
**rating_range** | [**\AmazonAdvertisingApi\Model\RatingRange**](RatingRange.md) |  | [optional] 
**category** | **string** | The category node id. Please use the GetTargetableCategories API or GetCategoryRecommendationsForASINs API to retrieve category IDs. | 
**price_range** | [**\AmazonAdvertisingApi\Model\PriceRange**](PriceRange.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

