# TextEvidence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_text** | **string** | The specific text determined to violate the specified policy in reviewedText. | [optional] 
**position** | [**\AmazonAdvertisingApi\Model\TextEvidencePosition**](TextEvidencePosition.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

