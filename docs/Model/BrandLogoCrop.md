# BrandLogoCrop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top** | **float** |  | [optional] 
**left** | **float** |  | [optional] 
**width** | **float** |  | [optional] 
**height** | **float** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

