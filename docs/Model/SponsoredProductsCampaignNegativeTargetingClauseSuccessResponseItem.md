# SponsoredProductsCampaignNegativeTargetingClauseSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_targeting_clause_id** | **string** | the CampaignNegativeTargets ID | [optional] 
**campaign_negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignNegativeTargetingClause**](SponsoredProductsCampaignNegativeTargetingClause.md) |  | [optional] 
**index** | **int** | the index of the CampaignNegativeTargets in the array from the request body | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

