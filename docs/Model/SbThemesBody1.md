# SbThemesBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**themes** | [**\AmazonAdvertisingApi\Model\SBCreateThemesRequest[]**](SBCreateThemesRequest.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

