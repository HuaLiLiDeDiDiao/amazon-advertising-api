# SBCreateDraftCampaignPositiveKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** | The keyword text. Maximum length is ten words. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**bid** | **float** | The bid associated with the keyword. For information on the maximum allowable bid, see the &#x27;keyword bid constraints by marketplace&#x27; section of the &#x27;supported features&#x27; document in the &#x27;guides&#x27; section. Note that the bid cannot not be larger than the budget associated with the campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

