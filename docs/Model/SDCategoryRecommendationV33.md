# SDCategoryRecommendationV33

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**\AmazonAdvertisingApi\Model\SDCategory**](SDCategory.md) |  | [optional] 
**name** | **string** | The category name | [optional] 
**translated_name** | **string** | The translated category name by requested locale, field will not be provided if locale is not provided or campaign localization service is down. | [optional] 
**path** | **string[]** | The path of the category within the category catalogue. | [optional] 
**translated_path** | **string[]** | The translated path of the category within the category catalogue by requested locale, field will not be provided if locale is not provided or campaign localization is down. | [optional] 
**targetable_asin_count_range** | [**\AmazonAdvertisingApi\Model\SDCategoryRecommendationV33TargetableAsinCountRange**](SDCategoryRecommendationV33TargetableAsinCountRange.md) |  | [optional] 
**rank** | **int** | A rank to signify which recommendations are weighed more heavily, with a lower rank signifying a stronger recommendation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

