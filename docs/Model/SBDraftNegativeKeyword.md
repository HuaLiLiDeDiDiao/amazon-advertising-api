# SBDraftNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **int** | The keyword identifier. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group associated with the keyword. | [optional] 
**campaign_id** | **int** | The identifier of the campaign associated with the keyword. | [optional] 
**keyword_text** | **string** | The keyword text. The maximum number of words for this string is 10. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](NegativeMatchType.md) |  | [optional] 
**state** | **string** | Newly created SB keywords are in a default state of &#x27;draft&#x27; before transitioning to a &#x27;pending&#x27; state for moderation. After moderation, the keyword will be in an enabled state. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

