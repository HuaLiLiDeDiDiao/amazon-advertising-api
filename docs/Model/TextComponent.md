# TextComponent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component_type** | **string** | Type of text component. | 
**id** | **string** | Id of the component. The same will be returned as part of the response as well. This can be used to uniquely identify the component from the pre moderation response. | 
**text** | **string** | Text which needs to be moderated. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

