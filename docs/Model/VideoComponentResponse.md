# VideoComponentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pre_moderation_status** | **string** | The pre moderation status of the component. | [optional] 
**component_type** | **string** | Type of the video component. | [optional] 
**landing_page** | [**\AmazonAdvertisingApi\Model\LandingPage**](LandingPage.md) |  | [optional] 
**policy_violations** | [**\AmazonAdvertisingApi\Model\VideoPolicyViolation[]**](VideoPolicyViolation.md) | A list of policy violations for the component that were detected during pre moderation. Note that this field is present in the response only when preModerationStatus is set to REJECTED. | [optional] 
**id** | **string** | Id of the component. This is the same id sent as part of the request. This can be used to uniquely identify the component. | [optional] 
**url** | **string** | Publicly accessible url of the video that got pre moderated. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

