# ThemeRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **string** | A theme name representing the context around the recommended list of ASINs. | [optional] 
**recommended_asins** | **string[]** | List of recommended ASINs under current theme. | [optional] 
**theme** | **string** | A theme name representing the context around the recommended list of ASINs. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

