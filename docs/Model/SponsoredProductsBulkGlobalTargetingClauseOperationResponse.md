# SponsoredProductsBulkGlobalTargetingClauseOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingClauseSuccessResponseItem[]**](SponsoredProductsGlobalTargetingClauseSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingClauseFailureResponseItem[]**](SponsoredProductsGlobalTargetingClauseFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

