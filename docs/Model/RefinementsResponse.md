# RefinementsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_id** | **float** | The category identifier. | [optional] 
**brands** | [**\AmazonAdvertisingApi\Model\RefinementsResponseBrands[]**](RefinementsResponseBrands.md) | The brands found in this category. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

