# NegativeTargetsListBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**null[]**](.md) | Restricts results to negative targets with the specified filters.  Filters are inclusive. Filters are joined using &#x27;and&#x27; logic. Specify one each type of filter. Specifying multiples of the same type of filter results in an error. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

