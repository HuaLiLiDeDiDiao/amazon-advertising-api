# SponsoredProductsCreateSponsoredProductsGlobalNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalNegativeKeyword[]**](SponsoredProductsCreateGlobalNegativeKeyword.md) | An array of negativeKeywords. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

