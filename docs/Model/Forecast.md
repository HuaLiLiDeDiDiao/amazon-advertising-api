# Forecast

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metric** | **string** | Describes which metric is forecasted. |Name|Description| |-----------|------------------------| |IMPRESSIONS| Available impressions| |REACH      | Delivered viewable impressions| |CLICKS     | Delivered page visits| |CONVERSIONS| [Preview only] Delivered conversions| | [optional] 
**value** | [**\AmazonAdvertisingApi\Model\ForecastRange**](ForecastRange.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

