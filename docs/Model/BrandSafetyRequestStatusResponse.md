# BrandSafetyRequestStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_status** | [**\AmazonAdvertisingApi\Model\BrandSafetyRequestStatus**](BrandSafetyRequestStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

