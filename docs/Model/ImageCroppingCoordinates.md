# ImageCroppingCoordinates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top** | **int** | Pixel distance from the top edge of the cropping zone to the top edge of the original image. | 
**left** | **int** | Pixel distance from the left edge of the cropping zone to the left edge of the original image. | 
**width** | **int** | Pixel width of the cropping zone. | 
**height** | **int** | Pixel height of the cropping zone. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

