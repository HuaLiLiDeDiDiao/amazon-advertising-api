# SBTargetingBrand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_refinement_id** | **string** | Id of brand. Use /sb/targets/categories/{categoryRefinementId}/refinements to retrieve Brand Refinement IDs. | 
**name** | **string** | Name of brand. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

