# BulkGetBillingStatusError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | 
**error_code** | [**\AmazonAdvertisingApi\Model\BulkGetBillingStatusErrorCodes**](BulkGetBillingStatusErrorCodes.md) |  | [optional] 
**description** | **string** |  | [optional] 
**advertiser_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

