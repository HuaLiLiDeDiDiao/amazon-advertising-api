# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **int** | The identifier of the negative keyword. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group to which the negative keyword is associated. | [optional] 
**campaign_id** | **int** | The identifier of the campaign to which the negative keyword is associated. | [optional] 
**keyword_text** | **string** | The keyword text. Maximum length of string is ten words if &#x60;matchType&#x60; is set to &#x60;negativeExact&#x60;. Maximum length is 4 words if &#x60;matchType&#x60; is set to &#x60;negativePhrase&#x60;. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](NegativeMatchType.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBNegativeKeywordState**](SBNegativeKeywordState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

