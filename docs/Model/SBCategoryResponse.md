# SBCategoryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The category identifier. | [optional] 
**name** | **string** | The category name. | [optional] 
**translated_name** | **string** | The category name translated to the specified locale in the request. | [optional] 
**is_targetable** | **bool** | Set to &#x60;true&#x60; if the category can be targeted in a targeting expression, and &#x60;false&#x60; if not. | [optional] 
**path** | **string** | The path of the category within the category catalogue. | [optional] 
**translated_path** | **string** | The path of the category within the category catalogue translated to the specified locale in the request. | [optional] 
**estimated_reach** | **string** | The estimated daily reach range of the category. Only set when supply source is &#x60;STREAMING_VIDEO&#x60; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

