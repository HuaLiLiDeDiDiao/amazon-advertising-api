# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_or_region** | **string** |  | 
**attention_name** | **string** |  | [optional] 
**city** | **string** |  | 
**country_code** | [**\AmazonAdvertisingApi\Model\CountryCode**](CountryCode.md) |  | 
**company_name** | **string** |  | 
**postal_code** | **string** |  | 
**address_line1** | **string** |  | 
**address_line2** | **string** |  | 
**address_line3** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

