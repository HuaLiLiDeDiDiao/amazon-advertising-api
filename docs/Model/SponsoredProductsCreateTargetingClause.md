# SponsoredProductsCreateTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetingExpressionPredicate[]**](SponsoredProductsCreateTargetingExpressionPredicate.md) | The targeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**expression_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateExpressionType**](SponsoredProductsCreateExpressionType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 
**bid** | **double** | The bid for ads sourced using the target. Targets that do not have bid values in listTargetingClauses will inherit the defaultBid from the adGroup level. For more information about bid constraints by marketplace, see [bid limits](https://advertising.amazon.com/API/docs/en-us/concepts/limits#bid-constraints-by-marketplace). | [optional] 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

