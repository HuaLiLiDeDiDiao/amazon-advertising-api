# SBInsightsCampaignInsightsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**insights** | [**\AmazonAdvertisingApi\Model\SBInsightsObject[]**](SBInsightsObject.md) |  | [optional] 
**next_token** | **string** | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

