# SBVideoDraftCampaignWithKeywords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SBDraftKeyword[]**](SBDraftKeyword.md) | An array of keywords associated with the Draft campaign. | [optional] 
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SBDraftNegativeKeyword[]**](SBDraftNegativeKeyword.md) | An array of negative keywords associated with the Draft campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

