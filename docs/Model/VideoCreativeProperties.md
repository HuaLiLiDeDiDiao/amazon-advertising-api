# VideoCreativeProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**\AmazonAdvertisingApi\Model\Video**](Video.md) |  | [optional] 
**square_videos** | [**\AmazonAdvertisingApi\Model\Video[]**](Video.md) | An optional collection of 1:1 square videos which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 
**landscape_videos** | [**\AmazonAdvertisingApi\Model\Video[]**](Video.md) | An optional collection of 16:9 landscape videos which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 
**portrait_videos** | [**\AmazonAdvertisingApi\Model\Video[]**](Video.md) | An optional collection of 9:16 portrait videos which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

