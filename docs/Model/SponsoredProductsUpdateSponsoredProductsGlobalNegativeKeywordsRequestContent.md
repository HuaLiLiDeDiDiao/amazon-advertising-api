# SponsoredProductsUpdateSponsoredProductsGlobalNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalNegativeKeyword[]**](SponsoredProductsUpdateGlobalNegativeKeyword.md) | An array of negativeKeywords with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

