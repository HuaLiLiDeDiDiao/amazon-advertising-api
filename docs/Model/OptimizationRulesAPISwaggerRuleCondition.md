# OptimizationRulesAPIAmazonAdvertisingApiRuleCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**criteria** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleCriteria**](OptimizationRulesAPIAmazonAdvertisingApiRuleCriteria.md) |  | 
**attribute_name** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleAttribute**](OptimizationRulesAPIAmazonAdvertisingApiRuleAttribute.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

