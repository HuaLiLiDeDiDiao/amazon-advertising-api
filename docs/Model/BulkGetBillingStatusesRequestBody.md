# BulkGetBillingStatusesRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**advertiser_marketplaces** | [**\AmazonAdvertisingApi\Model\AdvertiserMarketplace[]**](AdvertiserMarketplace.md) |  | 
**locale** | [**\AmazonAdvertisingApi\Model\Locale**](Locale.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

