# KDPError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kdp_error** | [**AllOfKDPErrorKdpError**](AllOfKDPErrorKdpError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

