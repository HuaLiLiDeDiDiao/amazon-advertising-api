# BidRecommendationsForTargetsResponseRecommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_bid** | [**\AmazonAdvertisingApi\Model\SuggestedBid**](SuggestedBid.md) |  | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

