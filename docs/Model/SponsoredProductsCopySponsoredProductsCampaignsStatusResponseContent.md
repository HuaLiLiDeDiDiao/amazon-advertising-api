# SponsoredProductsCopySponsoredProductsCampaignsStatusResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**async_task_detail** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaignTaskDetails**](SponsoredProductsCopyCampaignTaskDetails.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

