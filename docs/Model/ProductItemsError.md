# ProductItemsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_items_error** | [**AllOfProductItemsErrorProductItemsError**](AllOfProductItemsErrorProductItemsError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

