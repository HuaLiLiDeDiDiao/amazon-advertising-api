# ViolatingImageContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_image_evidences** | [**\AmazonAdvertisingApi\Model\ViolatingImageEvidence[]**](ViolatingImageEvidence.md) |  | [optional] 
**moderated_component** | **string** | Moderation component which marked the policy violation. | [optional] 
**reviewed_image_url** | **string** | URL of the image which has the ad policy violation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

