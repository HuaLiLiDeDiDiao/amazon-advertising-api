# SponsoredProductsUpdateGlobalCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**campaign_id** | **string** | The identifier of the campaign. | 
**name** | **string** | The name of the campaign. | [optional] 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDynamicBidding**](SponsoredProductsCreateOrUpdateDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBudget**](SponsoredProductsGlobalBudget.md) |  | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

