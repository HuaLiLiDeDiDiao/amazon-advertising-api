# SponsoredProductsBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBudgetType**](SponsoredProductsBudgetType.md) |  | 
**budget** | **double** | Monetary value | 
**effective_budget** | **double** | Monetary value | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

