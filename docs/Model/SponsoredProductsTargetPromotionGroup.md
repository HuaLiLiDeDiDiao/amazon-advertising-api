# SponsoredProductsTargetPromotionGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_campaign_ad_group_ids** | **string[]** | The Ids of the manual product-targeting AdGroups associated with the target promotion group | [optional] 
**target_promotion_group_id** | **string** | The id of the target promotion group. | [optional] 
**auto_targeting_campaign_ad_ids** | **string[]** | The list of Product Ad Ids in the Auto-Targeting campaign&#x27;s Ad Group that&#x27;s tied to the Target Promotion Group. | [optional] 
**keyword_campaign_ad_group_ids** | **string[]** | The Ids of the manual keyword-targeting AdGroups associated with the target promotion group | [optional] 
**state** | **string** | The state of the target promotion group. | [optional] 
**target_promotion_group_name** | **string** | The name of the target promotion group. | [optional] 
**auto_targeting_campaign_ad_group_id** | **string** | The Id of the auto-targeting AdGroup associated with the target promotion group | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

