# ErrorCause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | **string** | Error location, JSON Path expression specifying element of API payload causing error. | 
**trigger** | **string** | Optional value causing error. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

