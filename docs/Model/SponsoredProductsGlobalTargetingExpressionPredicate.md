# SponsoredProductsGlobalTargetingExpressionPredicate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_settings** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingExpressionPredicateMarketValue[]**](SponsoredProductsTargetingExpressionPredicateMarketValue.md) | The marketplace settings for expression to be overridden for marketplace. | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingExpressionPredicateType**](SponsoredProductsTargetingExpressionPredicateType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

