# BrandSafetyGetResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagination** | [**\AmazonAdvertisingApi\Model\BrandSafetyGetResponsePagination**](BrandSafetyGetResponsePagination.md) |  | [optional] 
**domains** | [**\AmazonAdvertisingApi\Model\BrandSafetyDenyListProcessedDomain[]**](BrandSafetyDenyListProcessedDomain.md) | List of Brand Safety Deny List Domains | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

