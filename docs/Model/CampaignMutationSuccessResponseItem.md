# CampaignMutationSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The campaign ID. | [optional] 
**index** | **float** | The index of the campaign in the array from the request body. | 
**campaign** | [**\AmazonAdvertisingApi\Model\Campaign**](Campaign.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

