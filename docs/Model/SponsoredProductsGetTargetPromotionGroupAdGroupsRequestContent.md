# SponsoredProductsGetTargetPromotionGroupAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_ids** | **string[]** | List of ad Ids for ASIN/SKU matches. | [optional] 
**max_results** | **int** | The maximum number of results requested. | [optional] 
**next_token** | **string** | Token value allowing to navigate to the next or previous response page | [optional] 
**ad_group_id** | **string** | The id of an adGroup. ASIN/SKU match will be based on all the ads this adGroup id contains if adIds are not provided. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

