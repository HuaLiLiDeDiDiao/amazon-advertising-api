# NameFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_term_match_type** | [**\AmazonAdvertisingApi\Model\QueryTermMatchType**](QueryTermMatchType.md) |  | [optional] 
**include** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

