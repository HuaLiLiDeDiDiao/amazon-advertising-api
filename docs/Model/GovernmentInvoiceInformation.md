# GovernmentInvoiceInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_type** | **string** |  | [optional] 
**country_code** | [**\AmazonAdvertisingApi\Model\CountryCode**](CountryCode.md) |  | [optional] 
**government_document_s3_link** | **string** | PreSigned URL to grant time-limited download access for govt invoice pdf | [optional] 
**government_xml_document_s3_link** | **string** | PreSigned URL to grant time-limited download access for govt invoice XML | [optional] 
**government_invoice_id** | **string** | Government generated ID | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

