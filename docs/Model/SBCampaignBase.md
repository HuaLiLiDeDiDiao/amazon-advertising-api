# SBCampaignBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The campaign identifier. | [optional] 
**name** | [**\AmazonAdvertisingApi\Model\SBCampaignName**](SBCampaignName.md) |  | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**budget** | **float** |  | [optional] 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | [optional] 
**start_date** | [**\AmazonAdvertisingApi\Model\StartDate**](StartDate.md) |  | [optional] 
**end_date** | [**\AmazonAdvertisingApi\Model\EndDate**](EndDate.md) |  | [optional] 
**state** | [**AllOfSBCampaignBaseState**](AllOfSBCampaignBaseState.md) |  | [optional] 
**serving_status** | **string** | |Status|Description| |------|-----------| |ASIN_NOT_BUYABLE| The ASIN can&#x27;t be purchased due to eligibility or availability.| |BILLING_ERROR| Billing information requires correction.| |ENDED| THe &#x60;endDate&#x60; specified in the campaign object occurs in the past.| |LANDING_PAGE_NOT_AVAILABLE| The specified landing page is not available. This may be caused by an incorrect address or a landing page with less than three ASINs.| |OUT_OF_BUDGET| The campaign has run out of budget.| |PAUSED| The campaign state set to &#x60;paused&#x60;.| |PENDING_REVIEW|: A newly created campaign that has not passed moderation review. Note that moderation review may take up to 72 hours.| |READY| The campaign is scheduled for a future date.| |REJECTED| The campaign failed moderation review.| |RUNNING| The campaign is enabled and serving.| |SCHEDULED| A transitive state between &#x60;ready&#x60; and &#x60;running&#x60;, as child entities associated with the campaign move to a running state.| |TERMINATED|The state of the campaign is set to &#x60;archived&#x60;.| | [optional] 
**brand_entity_id** | **string** | The brand entity identifier. Note that this field is required for sellers. For more information, see the [Stores reference](https://advertising.amazon.com/API/docs/v2/reference/stores) or [Brands reference](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Brands). | [optional] 
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 
**ad_format** | [**\AmazonAdvertisingApi\Model\AdFormat**](AdFormat.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

