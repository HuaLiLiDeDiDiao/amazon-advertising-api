# SbCampaignsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The identifier of an existing campaign. | [optional] 
**name** | **string** | The name of the campaign. | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**budget** | **float** | The budget associated with the campaign. | [optional] 
**end_date** | **string** | The YYYYMMDD end date for the campaign. Note that a value must be specified if the &#x60;budgetType&#x60; for the campaign is set to &#x60;lifetime&#x60;. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**bid_optimization** | **bool** | Set to true to allow Amazon to automatically optimize bids for placements below top of search. &#x60;Not supported for video campaigns&#x60; | [optional] 
**bid_multiplier** | **float** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00. &#x60;Not supported for video campaigns&#x60; | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

