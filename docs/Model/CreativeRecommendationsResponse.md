# CreativeRecommendationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary_headlines** | [**\AmazonAdvertisingApi\Model\PrimaryHeadlineRecommendationGroups**](PrimaryHeadlineRecommendationGroups.md) |  | [optional] 
**secondary_headlines** | [**\AmazonAdvertisingApi\Model\SecondaryHeadlineRecommendationGroups**](SecondaryHeadlineRecommendationGroups.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

