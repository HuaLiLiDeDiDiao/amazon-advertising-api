# StoreQualityRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommended_action** | **string** | description of the recommendation. | [optional] 
**observed_average_dwell_time_increase** | **string** | The percentage by which store quality could improve by this recommendation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

