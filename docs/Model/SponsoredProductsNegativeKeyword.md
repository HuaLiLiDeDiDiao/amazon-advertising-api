# SponsoredProductsNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword. | 
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. | [optional] 
**campaign_id** | **string** | The identifier of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeMatchType**](SponsoredProductsNegativeMatchType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityState**](SponsoredProductsEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this keyword is associated. | 
**keyword_text** | **string** | The keyword text. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeKeywordExtendedData**](SponsoredProductsNegativeKeywordExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

