# FetchTaxonomyNodeV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audience_count** | **int** |  | [optional] 
**category** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

