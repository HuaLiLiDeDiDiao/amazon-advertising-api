# SponsoredProductsGlobalBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBudgetType**](SponsoredProductsBudgetType.md) |  | 
**marketplace_settings** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceBudget[]**](SponsoredProductsMarketplaceBudget.md) | marketplace budget settings. | [optional] 
**currency** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCurrency**](SponsoredProductsCurrency.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

