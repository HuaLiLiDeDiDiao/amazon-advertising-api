# SPCampaignOptimizationRecommendationsAPIRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_ids** | [**\AmazonAdvertisingApi\Model\RuleCampaignId[]**](RuleCampaignId.md) | A list of campaign ids | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

