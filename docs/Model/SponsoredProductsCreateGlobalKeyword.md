# SponsoredProductsCreateGlobalKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifer of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateMatchType**](SponsoredProductsCreateOrUpdateMatchType.md) |  | 
**name** | **string** | Name for the Keyword | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | [optional] 
**ad_group_id** | **string** | The identifier of the ad group to which this keyword is associated. | 
**keyword_text** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalKeywordText**](SponsoredProductsGlobalKeywordText.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

