# ProductRecommendationsByASIN

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_cursor** | **string** | An identifier to fetch next set of &#x60;ProductRecommendation&#x60; records in the result set if available. This will be null when at the end of result set. | [optional] 
**previous_cursor** | **string** | Optional parameter that links to the previous result set served. This parameter will be null on the first request. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\ProductRecommendation[]**](ProductRecommendation.md) | An array of &#x60;ProductRecommendation&#x60; objects. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

