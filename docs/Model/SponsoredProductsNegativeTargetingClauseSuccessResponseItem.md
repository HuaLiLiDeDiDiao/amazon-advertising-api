# SponsoredProductsNegativeTargetingClauseSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **string** | the NegativeTargetingClause ID | [optional] 
**index** | **int** | the index of the NegativeTargetingClause in the array from the request body | 
**negative_targeting_clause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingClause**](SponsoredProductsNegativeTargetingClause.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

