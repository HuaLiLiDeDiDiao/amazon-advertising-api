# NegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | [**\AmazonAdvertisingApi\Model\TargetId**](TargetId.md) |  | [optional] 
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | [optional] 
**expression_type** | **string** |  | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\NegativeTargetingExpression[]**](NegativeTargetingExpression.md) | The expression to negatively match against. * Only one brand may be specified per targeting expression. * Only one asin may be specified per targeting expression. * To exclude a brand from a targeting expression, you must create a negative targeting expression in the same ad group as the positive targeting expression. | [optional] 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\NegativeTargetingExpression[]**](NegativeTargetingExpression.md) | The resolved negative targeting expression. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

