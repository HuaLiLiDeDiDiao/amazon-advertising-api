# ContactInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**\AmazonAdvertisingApi\Model\Address**](Address.md) |  | 
**email** | [**\AmazonAdvertisingApi\Model\Email**](Email.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

