# SponsoredProductsAdGroupServingStatusDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**\AmazonAdvertisingApi\Model\SponsoredProductsAdGroupServingStatusReason**](SponsoredProductsAdGroupServingStatusReason.md) |  | [optional] 
**help_url** | **string** | A URL with additional information about the status identifier. | [optional] 
**message** | **string** | A human-readable description of the status identifier specified in the name field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

