# SBVideoCampaignResponseCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBVideoCreative**](SBVideoCreative.md) |  | [optional] 
**landing_page** | [**\AmazonAdvertisingApi\Model\SBDetailPageLandingPage**](SBDetailPageLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

