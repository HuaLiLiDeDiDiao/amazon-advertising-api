# SponsoredProductsDraftKeywordSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | the draft keyword ID | [optional] 
**index** | **int** | the index of the draft keyword in the array from the request body | 
**keyword** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftKeyword**](SponsoredProductsDraftKeyword.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

