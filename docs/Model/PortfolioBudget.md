# PortfolioBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | The budget amount associated with the portfolio. Cannot be &#x60;null&#x60;. | [optional] 
**currency_code** | **string** | The currency used for all monetary values for entities under this profile. Cannot be &#x60;null&#x60;. |Region|&#x60;countryCode&#x60;|Country Name|&#x60;currencyCode&#x60;| |-----|------|------|------| |NA|US|United States|USD| |NA|CA|Canada|CAD| |NA|MX|Mexico|MXN| |NA|BR|Brazil|BRL| |EU|UK|United Kingdom|GBP| |EU|DE|Germany|EUR| |EU|FR|France|EUR| |EU|ES|Spain|EUR| |EU|IT|Italy|EUR| |EU|NL|The Netherlands|EUR| |EU|SE|Sweden|SEK| |EU|PL|Poland|PLN| |EU|AE|United Arab Emirates|AED| |EU|TR|Turkey|TRY| |FE|JP|Japan|JPY| |FE|AU|Australia|AUD| |FE|SG|Singapore|SGD| | [optional] 
**policy** | **string** | The budget policy. Set to &#x60;dateRange&#x60; to specify a budget for a specific period of time. Set to &#x60;monthlyRecurring&#x60; to specify a budget that is automatically renewed at the beginning of each month. Cannot be &#x60;null&#x60;. | [optional] 
**start_date** | **string** | The starting date in &#x60;YYYYMMDD&#x60; format to which the budget is applied. Required if &#x60;policy&#x60; is set to &#x60;dateRange&#x60;. Not specified if &#x60;policy&#x60; is set to &#x60;monthlyRecurring&#x60;. Note that the starting date for &#x60;monthlyRecurring&#x60; is the date when the policy is set. | [optional] 
**end_date** | **string** | The end date after which the budget is no longer applied. Optional if &#x60;policy&#x60; is set to &#x60;dateRange&#x60; or &#x60;monthlyRecurring&#x60;. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

