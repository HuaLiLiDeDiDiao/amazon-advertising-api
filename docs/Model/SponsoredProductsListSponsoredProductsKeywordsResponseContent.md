# SponsoredProductsListSponsoredProductsKeywordsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **int** | The total number of entities | [optional] 
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeyword[]**](SponsoredProductsKeyword.md) |  | [optional] 
**next_token** | **string** | token value allowing to navigate to the next response page | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

