# InlineResponse20014Success

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_id** | [**\AmazonAdvertisingApi\Model\SBThemeId**](SBThemeId.md) |  | [optional] 
**index** | [**\AmazonAdvertisingApi\Model\SBTargetRequestIndex**](SBTargetRequestIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

