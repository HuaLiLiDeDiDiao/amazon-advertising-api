# UpdateAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The product ad identifier. | 
**name** | **string** | The name of the ad. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\CreateOrUpdateEntityState**](CreateOrUpdateEntityState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

