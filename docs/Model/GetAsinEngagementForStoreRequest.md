# GetAsinEngagementForStoreRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | [**\DateTime**](\DateTime.md) | The end date (inclusive) in YYYY-MM-DD format for the time period from when to fetch the insights. | 
**order_by** | [**\AmazonAdvertisingApi\Model\SortOrder**](SortOrder.md) |  | [optional] 
**sort_by** | [****](.md) | Nullable metric to sort on. If a value is provided, it must also appear in the metrics list. If no value is provided, the result is not guaranteed to be sorted. This field is only valid when the dimension is ASIN. | [optional] 
**metrics** | [**\AmazonAdvertisingApi\Model\AsinEngagementMetric[]**](AsinEngagementMetric.md) | List of the engagement metrics to be fetched. At least one metric should be specified. | 
**dimension** | [**\AmazonAdvertisingApi\Model\AsinEngagementDimension**](AsinEngagementDimension.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The start date (inclusive) in YYYY-MM-DD format for the time period from when to fetch the insights. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

