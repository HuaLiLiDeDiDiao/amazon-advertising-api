# AllOfCampaignErrorCampaignError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_length** | **string** |  | [optional] 
**invalid_characters** | **string** |  | [optional] 
**note** | **string** |  | [optional] 
**max_campaigns** | **string** |  | [optional] 
**supported_states** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

