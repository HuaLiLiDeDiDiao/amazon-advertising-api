# AdGroupSuggestedKeywordsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The ad group identifier. | [optional] 
**suggesteded_keywords** | **string[]** | An array of suggested keywords. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

