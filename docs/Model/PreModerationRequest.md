# PreModerationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**record_id** | **string** | Id of the brand/advertiser. | [optional] 
**asin_components** | [**\AmazonAdvertisingApi\Model\AsinComponent[]**](AsinComponent.md) | Asin components which needs to be pre moderated. | [optional] 
**ad_program** | **string** | Type of Ad program to which this pre moderation components belong to. | 
**locale** | **string** | Specifying locale will translate the premoderation message into that locale&#x27;s associated language.     | Locale | Language (ISO 639) | Country (ISO 3166) |   |-----|-----|-------|   | ar-AE | Arabic (ar) | United Arab Emirates (AE) |   | zh-CN | Chinese (zh) | China (CN) |   | nl-NL | Dutch (nl) | Netherlands (NL) |   | en-AU | English (en) | Australia (AU) |   | en-CA | English (en) | Canada (CA) |   | en-IN | English (en) | India (IN) |   | en-GB | English (en) | United Kingdom (GB) |   | en-US | English (en) | United States (US) |   | fr-CA | French (fr) | Canada (CA) |   | fr-FR | French (fr) | France (FR) |   | de-DE | German (de) | Germany (DE) |   | it-IT | Italian (it) | Italy (IT) |   | ja-JP | Japanese (ja) | Japan (JP) |   | ko-KR | Korean (ko) | South Korea (KR) |   | pt-BR | Portuguese (pt) | Brazil (BR) |   | es-ES | Spanish (es) | Spain (ES) |   | es-US | Spanish (es) | United States (US) |   | es-MX | Spanish (es) | Mexico (MX) |   | tr-TR | Turkish (tr) | Turkey (TR) | | 
**image_components** | [**\AmazonAdvertisingApi\Model\ImageComponent[]**](ImageComponent.md) | Image components which needs to be pre moderated. | [optional] 
**date_components** | [**\AmazonAdvertisingApi\Model\DateComponent[]**](DateComponent.md) | Date components which needs to be pre moderated. | [optional] 
**text_components** | [**\AmazonAdvertisingApi\Model\TextComponent[]**](TextComponent.md) | Text components which needs to be pre moderated. | [optional] 
**video_components** | [**\AmazonAdvertisingApi\Model\VideoComponent[]**](VideoComponent.md) | Video components which needs to be pre moderated. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

