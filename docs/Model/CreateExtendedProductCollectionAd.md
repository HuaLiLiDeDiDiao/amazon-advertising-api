# CreateExtendedProductCollectionAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**landing_page** | [**\AmazonAdvertisingApi\Model\LandingPage**](LandingPage.md) |  | 
**name** | **string** | The name of the ad. | 
**state** | [**\AmazonAdvertisingApi\Model\CreateOrUpdateEntityState**](CreateOrUpdateEntityState.md) |  | 
**ad_group_id** | **string** | The adGroup identifier. | 
**creative** | [**\AmazonAdvertisingApi\Model\CreateExtendedProductCollectionCreative**](CreateExtendedProductCollectionCreative.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

