# SDProductTargetingThemeExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The contextual targeting grammar used to define the targeting theme. Note asinAsBestSeller is currently not supported. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

