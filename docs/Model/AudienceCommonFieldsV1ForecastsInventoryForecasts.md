# AudienceCommonFieldsV1ForecastsInventoryForecasts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all** | [**\AmazonAdvertisingApi\Model\InventoryForecastV1**](InventoryForecastV1.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

