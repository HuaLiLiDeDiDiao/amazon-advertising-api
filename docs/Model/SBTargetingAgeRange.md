# SBTargetingAgeRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_range_refinement_id** | **string** | Id of Age Range. Use /sb/targets/categories/{categoryRefinementId}/refinements to retrieve Age Range Refinement IDs. | 
**name** | **string** | Name of Age Range. | [optional] 
**translated_name** | **string** | Translated name of Age Range based off locale sent in request. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

