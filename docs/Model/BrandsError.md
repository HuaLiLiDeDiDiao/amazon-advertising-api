# BrandsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brands_error** | [**AllOfBrandsErrorBrandsError**](AllOfBrandsErrorBrandsError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

