# SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicateType**](SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicateType.md) |  | 
**value** | **string** | The expression value | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

