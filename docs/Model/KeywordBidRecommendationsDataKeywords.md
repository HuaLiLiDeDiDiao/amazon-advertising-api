# KeywordBidRecommendationsDataKeywords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword** | **string** | The keyword text. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

