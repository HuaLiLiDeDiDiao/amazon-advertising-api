# SponsoredProductsUpdateSponsoredProductsGlobalTargetingClausesResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkGlobalTargetingClauseOperationResponse**](SponsoredProductsBulkGlobalTargetingClauseOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

