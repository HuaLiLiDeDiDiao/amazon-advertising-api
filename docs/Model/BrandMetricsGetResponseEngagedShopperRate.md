# BrandMetricsGetResponseEngagedShopperRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**this_brand** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseEngagedShopperRateThisBrand**](BrandMetricsGetResponseEngagedShopperRateThisBrand.md) |  | [optional] 
**top_performers** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseEngagedShopperRateTopPerformers**](BrandMetricsGetResponseEngagedShopperRateTopPerformers.md) |  | [optional] 
**peer_median** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseEngagedShopperRatePeerMedian**](BrandMetricsGetResponseEngagedShopperRatePeerMedian.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

