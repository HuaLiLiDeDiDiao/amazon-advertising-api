# SponsoredProductsDynamicBidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**placement_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsPlacementBidding[]**](SponsoredProductsPlacementBidding.md) |  | [optional] 
**strategy** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBiddingStrategy**](SponsoredProductsBiddingStrategy.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

