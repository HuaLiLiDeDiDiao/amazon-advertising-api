# RangeError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **string** |  | 
**allowed** | **string[]** | Allowed values. | [optional] 
**cause** | [**\AmazonAdvertisingApi\Model\ErrorCause**](ErrorCause.md) |  | 
**upper_limit** | **string** | Optional upper limit. | [optional] 
**lower_limit** | **string** | Optional lower limit. | [optional] 
**message** | **string** | Human readable error message. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

