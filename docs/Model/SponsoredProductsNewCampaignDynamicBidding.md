# SponsoredProductsNewCampaignDynamicBidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**placement_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignPlacementBidding[]**](SponsoredProductsNewCampaignPlacementBidding.md) | The product placement. | [optional] 
**strategy** | **string** | One of LEGACY_FOR_SALES, AUTO_FOR_SALES, MANUAL, RULE_BASED. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

