# SBUpdateThemesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_id** | **string** | The identifier of the theme target. | 
**ad_group_id** | **string** | The identifier of the ad group to which the target is associated. | 
**campaign_id** | **string** | The identifier of the campaign to which the target is associated. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBThemeState**](SBThemeState.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\Bid**](Bid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

