# CreativeModerationPolicyViolations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**policy_description** | **string** | A human-readable description of the policy. | [optional] 
**policy_link_url** | **string** | Address of the policy documentation. Follow the link to learn more about the specified policy. | [optional] 
**violating_headline_contents** | [**\AmazonAdvertisingApi\Model\CreativeModerationViolatingHeadlineContents[]**](CreativeModerationViolatingHeadlineContents.md) | Information about the headline text that violates the specified policy. | [optional] 
**violating_brand_logo_contents** | [**\AmazonAdvertisingApi\Model\CreativeModerationViolatingBrandLogoContents[]**](CreativeModerationViolatingBrandLogoContents.md) | Information about the brand logo that violates the specified policy. | [optional] 
**violating_custom_image_contents** | [**\AmazonAdvertisingApi\Model\CreativeModerationViolatingBrandLogoContents[]**](CreativeModerationViolatingBrandLogoContents.md) | Information about the custom image that violates the specified policy. | [optional] 
**violating_video_contents** | [**\AmazonAdvertisingApi\Model\CreativeModerationViolatingVideoContents[]**](CreativeModerationViolatingVideoContents.md) | Information about the video that violates the specified policy. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

