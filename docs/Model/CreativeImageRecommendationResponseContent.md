# CreativeImageRecommendationResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **float** | The total number of results returned by an operation. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\CreativeImageRecommendationEntry[]**](CreativeImageRecommendationEntry.md) | Recommendations are sorted on relevancy score, i.e. more relevant image has lesser array index value | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

