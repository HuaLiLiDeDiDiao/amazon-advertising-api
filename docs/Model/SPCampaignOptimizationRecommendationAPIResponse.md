# SPCampaignOptimizationRecommendationAPIResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_optimization_recommendations** | [**\AmazonAdvertisingApi\Model\RuleRecommendation[]**](RuleRecommendation.md) | List of successful campaign optimization recomendation for campaigns. | [optional] 
**campaign_optimization_recommendations_error** | [**\AmazonAdvertisingApi\Model\RuleRecommendationError[]**](RuleRecommendationError.md) | List of errors that occured when generating campaign optimization recommendation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

