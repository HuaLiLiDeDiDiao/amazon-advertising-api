# SponsoredProductsBulkCampaignNegativeTargetingClauseOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignNegativeTargetingClauseSuccessResponseItem[]**](SponsoredProductsCampaignNegativeTargetingClauseSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignNegativeTargetingClauseFailureResponseItem[]**](SponsoredProductsCampaignNegativeTargetingClauseFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

