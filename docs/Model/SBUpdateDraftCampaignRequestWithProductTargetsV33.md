# SBUpdateDraftCampaignRequestWithProductTargetsV33

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_optimization_strategy** | [**\AmazonAdvertisingApi\Model\BidOptimizationStrategy**](BidOptimizationStrategy.md) |  | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustmentV33[]**](BidAdjustmentV33.md) | List of bid adjustments for placement group and shopper segments. BidMultiplier cannot be specified when bidAdjustments are present. &#x60;Not supported for video campaigns&#x60; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

