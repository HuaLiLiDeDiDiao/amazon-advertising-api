# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_id** | **string** | The Brand identifier. | [optional] 
**brand_entity_id** | **string** | The Brand entity identifier. | [optional] 
**brand_registry_name** | **string** | The Brand name. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

