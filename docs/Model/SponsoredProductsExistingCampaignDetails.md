# SponsoredProductsExistingCampaignDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_campaign_ad_group_ids** | **string[]** | AdGroupIds of existing manual campaigns to be used as part of the Target Promotion Group for     promoting product targets. | [optional] 
**keyword_campaign_ad_group_ids** | **string[]** | AdGroupIds of existing manual campaigns to be used as part of the Target Promotion Group for     promoting keyword targets. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

