# SBKeywordResponseKeywordError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** | Description of error message. | [optional] 
**location** | **string** | Location of the error in the request body. | [optional] 
**trigger** | **string** | The specific value on where the error is occuring. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

