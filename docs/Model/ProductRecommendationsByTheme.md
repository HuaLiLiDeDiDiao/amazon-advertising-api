# ProductRecommendationsByTheme

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_cursor** | **string** | An identifier to fetch next set of &#x60;ThemeRecommendation&#x60; records in the result set if available. This will be null when at the end of result set. | [optional] 
**previous_cursor** | **string** | Optional parameter that links to the previous result set served to the requester. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\ThemeRecommendation[]**](ThemeRecommendation.md) | An array of &#x60;ThemeRecommendation&#x60; objects | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

