# AllOfPaginationErrorPaginationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**upper_limit** | **string** |  | [optional] 
**expected** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

