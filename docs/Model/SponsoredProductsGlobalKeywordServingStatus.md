# SponsoredProductsGlobalKeywordServingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalKeywordServingStatusReason[]**](SponsoredProductsGlobalKeywordServingStatusReason.md) | Serving status details of Keyword | [optional] 
**marketplace_keyword_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceLevelKeywordServingStatus[]**](SponsoredProductsMarketplaceLevelKeywordServingStatus.md) | The marketplace serving statuses. | [optional] 
**keyword_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityServingStatus**](SponsoredProductsGlobalEntityServingStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

