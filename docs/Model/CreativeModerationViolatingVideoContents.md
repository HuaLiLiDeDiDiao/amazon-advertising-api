# CreativeModerationViolatingVideoContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reviewed_video_url** | **string** | Address of the video reviewed during moderation. | [optional] 
**video_evidences** | [**\AmazonAdvertisingApi\Model\CreativeModerationVideoEvidences[]**](CreativeModerationVideoEvidences.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

