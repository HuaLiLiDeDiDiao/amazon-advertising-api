# UpdateProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | [**\AmazonAdvertisingApi\Model\AdId**](AdId.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

