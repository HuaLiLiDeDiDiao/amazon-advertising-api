# ProductListBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**null[]**](.md) | Restricts recommendations to the criteria specified in the filters. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

