# InlineResponse20018ViolatingAsinContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**moderated_component** | **string** | The ad component that includes the ASIN that violates the specified policy. | [optional] 
**asin_evidences** | [**\AmazonAdvertisingApi\Model\InlineResponse20018AsinEvidences[]**](InlineResponse20018AsinEvidences.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

