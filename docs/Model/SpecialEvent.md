# SpecialEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_modifier** | **float** | Deprecated. The factor used to boost the recommended budget. | [optional] 
**end_date** | **string** | The end date of the special event in YYYYMMDD format. | [optional] 
**daily_budget** | **float** | Recommended daily budget for the new campaign during the special event period. | [optional] 
**event_key** | **string** | The key of the special event. | [optional] 
**event_name** | **string** | The name of the special event. | [optional] 
**start_date** | **string** | The start date of the special event in YYYYMMDD format. | [optional] 
**benchmark** | [**\AmazonAdvertisingApi\Model\Benchmark**](Benchmark.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

