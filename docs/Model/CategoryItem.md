# CategoryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | **string** | The category id of the parent node | [optional] 
**path** | **string** | The path of the category, which contains the current category and all parent categories | [optional] 
**can_be_targeted** | **bool** | A flag which indicates if the current node may be targeted | [optional] 
**name** | **string** | The name of the category | [optional] 
**id** | **string** | The category id of the current node | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

