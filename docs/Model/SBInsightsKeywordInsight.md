# SBInsightsKeywordInsight

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alerts** | [**\AmazonAdvertisingApi\Model\SBInsightsKeywordAlertType[]**](SBInsightsKeywordAlertType.md) |  | [optional] 
**search_term_impression_share** | **double** | The account-level ad-attributed impression share for the search-term / keyword. Provides percentage share of all ad impressions the advertiser has for the keyword in the last 7 days. This metric helps advertisers identify potential opportunities based on their share of relevant keywords. This information is only available for keywords the advertiser targeted with ad impressions. Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\SBInsightsMatchType**](SBInsightsMatchType.md) |  | [optional] 
**ad_group_index** | **int** | Correlates the ad group to the ad group array index specified in the request. Zero-based. | [optional] 
**search_term_impression_rank** | **int** | The account-level ad-attributed impression rank for the search-term / keyword. Provides the [1:N] place the advertiser ranks among all advertisers for the keyword by ad impressions in a marketplace in the last 7 days. It tells an advertiser how many advertisers had higher share of ad impressions. This information is only available for keywords the advertiser targeted with ad impressions. Only available in the following marketplaces: US, CA, MX, UK, DE, FR, ES, IT, IN, JP. | [optional] 
**bid** | **double** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. For more information, see [supported features](https://advertising.amazon.com/API/docs/v2/guides/supported_features). | [optional] 
**keyword_index** | **int** | Correlates the keyword to the keyword array index specified in the request. Zero-based. | [optional] 
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

