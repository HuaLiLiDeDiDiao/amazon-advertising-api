# AdGroupResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | The HTTP status code of the response. | [optional] 
**description** | **string** | A human-readable description of the response. | [optional] 
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

