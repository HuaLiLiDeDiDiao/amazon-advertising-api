# BidAdjustmentByPlacement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percentage** | **float** |  | [optional] 
**placement** | [**\AmazonAdvertisingApi\Model\Placement**](Placement.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

