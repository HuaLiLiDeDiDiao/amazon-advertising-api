# SBForecastingProductExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The expression type associated with the target. Valid value: ASIN_CATEGORY_SAME_AS, ASIN_BRAND_SAME_AS, ASIN_PRICE_LESS_THAN, ASIN_PRICE_BETWEEN, ASIN_PRICE_GREATER_THAN, ASIN_REVIEW_RATING_LESS_THAN, ASIN_REVIEW_RATING_BETWEEN, ASIN_REVIEW_RATING_GREATER_THAN, ASIN_SAME_AS. | [optional] 
**value** | **string** | The expression value associated with targets. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

