# SponsoredProductsTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The ID of an ad for which the targets are recommended | [optional] 
**campaign_id** | **string** | The ID of a campaign for which the targets are recommended | [optional] 
**target_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetType**](SponsoredProductsTargetType.md) |  | [optional] 
**ad_asin** | **string** | The ASIN of the product being advertised | [optional] 
**recommended_target** | **string** | The keyword or ASIN that is being targeted | [optional] 
**ad_group_id** | **string** | The ID of an ad group for which the targets are recommended | [optional] 
**recommendation_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsRecommendationReason[]**](SponsoredProductsRecommendationReason.md) | Provides a list of reasons for why this target is being recommended for harvesting | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

