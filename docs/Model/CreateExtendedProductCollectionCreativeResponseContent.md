# CreateExtendedProductCollectionCreativeResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The unique ID of a Sponsored Brands ad. | [optional] 
**creative_version** | **string** | The version identifier that helps you keep track of multiple versions of a submitted (non-draft) Sponsored Brands creative. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

