# SBKeywordsNegativeKeywords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\Keyword[]**](Keyword.md) | An array of keywords associated with the campaign. | [optional] 
**negative_keywords** | [**\AmazonAdvertisingApi\Model\NegativeKeyword[]**](NegativeKeyword.md) | An array of negative keywords associated with the campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

