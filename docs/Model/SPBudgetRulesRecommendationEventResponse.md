# SPBudgetRulesRecommendationEventResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommended_budget_rule_events** | [**\AmazonAdvertisingApi\Model\SPBudgetRulesRecommendationEvent[]**](SPBudgetRulesRecommendationEvent.md) | A list of recommended special events with date range and suggested budget increase. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

