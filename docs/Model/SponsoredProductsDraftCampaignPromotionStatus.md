# SponsoredProductsDraftCampaignPromotionStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion_state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignPromotionState**](SponsoredProductsDraftCampaignPromotionState.md) |  | 
**campaign_id** | **string** | entity object identifier | 
**destination_id** | **string** | entity object identifier | 
**errors** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignPromotionError[]**](SponsoredProductsDraftCampaignPromotionError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

