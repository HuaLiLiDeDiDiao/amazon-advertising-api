# ListSponsoredBrandsAdGroupsBetaResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **float** | The total number of entities. | [optional] 
**ad_groups** | [**\AmazonAdvertisingApi\Model\AdGroup[]**](AdGroup.md) |  | [optional] 
**next_token** | **string** | Token value allowing to navigate to the next response page. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

