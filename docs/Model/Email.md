# Email

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **string** |  | 
**display_name** | **string** | Customer name used in email communication. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

