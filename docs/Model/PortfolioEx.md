# PortfolioEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **float** | Date that the portfolio was created, in epoch time. | [optional] 
**last_updated_date** | **float** | Date at least one property value of the portfolio was updated, in epoch time. | [optional] 
**serving_status** | **string** | The current serving status of the portfolio: | Portfolio serving status | Description | |--------------------------|-------------| | PORTFOLIO_STATUS_ENABLED | The portfolio&#x27;s status is ENABLED. | | PORTFOLIO_STATUS_PAUSED  | The portfolio&#x27;s status is PAUSED. | | PORTFOLIO_ARCHIVED | The portfolio is archived. | | PORTFOLIO_OUT_OF_BUDGET | The maximum budget cap at the portfolio level has been reached. | | PENDING_START_DATE | The portfolio&#x27;s start date is in the future. | | ENDED | The portfolio&#x27;s end date is in the past. | | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

