# TaxDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permanent_account_number** | **string** | **IN only** field that represents the tax account number of the billed entity entered on AMS portal. | [optional] 
**tax_calculation_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**tax_breakups** | [**\AmazonAdvertisingApi\Model\TaxBreakup[]**](TaxBreakup.md) | List of taxes applied on the transaction for this invoice. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

