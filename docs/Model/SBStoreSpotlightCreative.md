# SBStoreSpotlightCreative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_logo_crop** | [**\AmazonAdvertisingApi\Model\SBBrandLogoCrop**](SBBrandLogoCrop.md) |  | [optional] 
**brand_logo_url** | **string** | The address of the hosted image. | [optional] 
**brand_name** | **string** | A brand name. Maximum length is 30 characters. | [optional] 
**subpages** | [**\AmazonAdvertisingApi\Model\SBStoreSpotlightCreativeSubpages[]**](SBStoreSpotlightCreativeSubpages.md) |  | [optional] 
**brand_logo_asset_id** | **string** | The identifier of the brand logo image from the Store assets library. See [listAssets](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Stores) for more information. Note that for campaigns created in the Amazon Advertising console prior to release of the Store assets library, responses will not include a value for the brandLogoAssetID field. | [optional] 
**headline** | **string** | The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

