# SDAudienceRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audience** | [**\AmazonAdvertisingApi\Model\SDAudience**](SDAudience.md) |  | [optional] 
**name** | **string** | The Amazon audience name | [optional] 
**rank** | **int** | A rank to signify which recommendations are weighed more heavily, with a lower rank signifying a stronger recommendation | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

