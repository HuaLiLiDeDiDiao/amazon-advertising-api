# AdGroupThemeBasedBidRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_expressions** | [**\AmazonAdvertisingApi\Model\TargetingExpressionList**](TargetingExpressionList.md) |  | 
**campaign_id** | **string** | The campaign identifier. | 
**recommendation_type** | **string** | The bid recommendation type. | 
**ad_group_id** | **string** | The ad group identifier. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

