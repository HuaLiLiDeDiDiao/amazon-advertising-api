# CreateAsyncReportRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | **string** | YYYY-MM-DD format. The maximum lookback window supported depends on the selection of reportTypeId. Most report types support &#x60;95 days&#x60; as lookback window. | 
**configuration** | [**\AmazonAdvertisingApi\Model\AsyncReportConfiguration**](AsyncReportConfiguration.md) |  | 
**name** | **string** | The name of the report. | [optional] 
**start_date** | **string** | YYYY-MM-DD format. The maximum lookback window supported depends on the selection of reportTypeId. Most report types support &#x60;95 days&#x60; as lookback window. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

