# OptimizationRulesAPIAmazonAdvertisingApiUpdateOptimizationRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rules** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule[]**](OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

