# SponsoredProductsUpdateCampaignNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate[]**](SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate.md) | The NegativeTargeting expression. | [optional] 
**target_id** | **string** | The target identifier | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

