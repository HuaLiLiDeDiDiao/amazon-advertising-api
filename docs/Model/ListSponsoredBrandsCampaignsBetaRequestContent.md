# ListSponsoredBrandsCampaignsBetaRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 
**portfolio_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 
**state_filter** | [**\AmazonAdvertisingApi\Model\EntityStateFilter**](EntityStateFilter.md) |  | [optional] 
**max_results** | **float** | Number of records to include in the paginated response. Defaults to max page size for given API. | [optional] 
**next_token** | **string** | Token value allowing to navigate to the next response page. | [optional] 
**goal_type_filter** | [**\AmazonAdvertisingApi\Model\GoalTypeFilter**](GoalTypeFilter.md) |  | [optional] 
**include_extended_data_fields** | **bool** | Setting to true will slow down performance because the API needs to retrieve extra information for each campaign. | [optional] 
**name_filter** | [**\AmazonAdvertisingApi\Model\NameFilter**](NameFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

