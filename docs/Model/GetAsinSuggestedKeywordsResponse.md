# GetAsinSuggestedKeywordsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asin** | **string** | The ASIN for which keywords are suggested. | [optional] 
**suggested_keywords** | [**\AmazonAdvertisingApi\Model\GetAsinSuggestedKeywordsResponseSuggestedKeywords[]**](GetAsinSuggestedKeywordsResponseSuggestedKeywords.md) | The list of suggested keywords. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

