# DraftsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drafts_error** | [**AllOfDraftsErrorDraftsError**](AllOfDraftsErrorDraftsError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

