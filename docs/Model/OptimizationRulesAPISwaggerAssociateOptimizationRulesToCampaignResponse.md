# OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated error code for machine use. | [optional] 
**responses** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSingleOptimizationRuleAssociationResponse[]**](OptimizationRulesAPIAmazonAdvertisingApiSingleOptimizationRuleAssociationResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

