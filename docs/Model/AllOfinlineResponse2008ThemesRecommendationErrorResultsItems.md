# AllOfinlineResponse2008ThemesRecommendationErrorResultsItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_type** | [**\AmazonAdvertisingApi\Model\ThemeType**](ThemeType.md) |  | [optional] 
**theme_index** | [**\AmazonAdvertisingApi\Model\SBBidRecommendationThemeIndex**](SBBidRecommendationThemeIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

