# SponsoredProductsDateError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDateErrorReason**](SponsoredProductsDateErrorReason.md) |  | 
**cause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsErrorCause**](SponsoredProductsErrorCause.md) |  | [optional] 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

