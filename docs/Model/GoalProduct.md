# GoalProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asin** | [**\AmazonAdvertisingApi\Model\ASIN**](ASIN.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

