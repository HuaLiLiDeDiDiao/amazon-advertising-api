# ModerationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version_id** | [**\AmazonAdvertisingApi\Model\VersionId**](VersionId.md) |  | [optional] 
**id_type** | [**\AmazonAdvertisingApi\Model\IdType**](IdType.md) |  | [optional] 
**moderation_status** | [**\AmazonAdvertisingApi\Model\ModerationStatus**](ModerationStatus.md) |  | [optional] 
**policy_violations** | [**\AmazonAdvertisingApi\Model\PolicyViolation[]**](PolicyViolation.md) | A list of policy violations for a campaign that has failed moderation. Note that this field is present in the response only when moderationStatus is set to REJECTED. | [optional] 
**eta_for_moderation** | **string** | Expected date and time by which moderation will be complete. The format is ISO 8601 in UTC time zone. Note that this field is present in the response only when moderationStatus is set to IN_PROGRESS. | [optional] 
**id** | [**\AmazonAdvertisingApi\Model\Id**](Id.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

