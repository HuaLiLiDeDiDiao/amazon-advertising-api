# SponsoredProductsCampaignNegativeKeywordSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keyword** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignNegativeKeyword**](SponsoredProductsCampaignNegativeKeyword.md) |  | [optional] 
**campaign_negative_keyword_id** | **string** | the campaignNegativeKeyword ID | [optional] 
**index** | **int** | the index of the campaign in the array from the request body | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

