# CreateVideoCreativeRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The unique ID of a Sponsored Brands ad. | 
**creative** | [**\AmazonAdvertisingApi\Model\VideoCreative**](VideoCreative.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

