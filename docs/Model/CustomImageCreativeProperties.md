# CustomImageCreativeProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rect_custom_image** | [**\AmazonAdvertisingApi\Model\Image**](Image.md) |  | [optional] 
**square_custom_image** | [**\AmazonAdvertisingApi\Model\Image**](Image.md) |  | [optional] 
**square_images** | [**\AmazonAdvertisingApi\Model\Image[]**](Image.md) | An optional collection of 1:1 square images which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 
**landscape_images** | [**\AmazonAdvertisingApi\Model\Image[]**](Image.md) | An optional collection of 16:9 landscape images which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 
**portrait_images** | [**\AmazonAdvertisingApi\Model\Image[]**](Image.md) | An optional collection of 9:16 portrait images which are displayed on the ad. This operation is a PREVIEW ONLY. This note will be removed once this functionality becomes available. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

