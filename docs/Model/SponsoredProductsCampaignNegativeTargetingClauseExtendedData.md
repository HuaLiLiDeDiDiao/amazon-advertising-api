# SponsoredProductsCampaignNegativeTargetingClauseExtendedData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_update_date_time** | [**\DateTime**](\DateTime.md) | Last updated date in ISO 8601. | [optional] 
**serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeywordServingStatus**](SponsoredProductsKeywordServingStatus.md) |  | [optional] 
**serving_status_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeywordServingStatusDetail[]**](SponsoredProductsKeywordServingStatusDetail.md) | The serving status reasons of the CampaignNegativeTargetingClause | [optional] 
**creation_date_time** | [**\DateTime**](\DateTime.md) | Creation date in ISO 8601. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

