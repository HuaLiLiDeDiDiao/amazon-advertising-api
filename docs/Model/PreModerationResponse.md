# PreModerationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**record_id** | **string** | Id of the brand/advertiser. | [optional] 
**asin_components** | [**\AmazonAdvertisingApi\Model\AsinComponentResponse[]**](AsinComponentResponse.md) | Pre moderation result of the asin components. It will have information regarding the policy violations present if any. | [optional] 
**pre_moderation_id** | **string** | Unique Id for the moderation Request. | [optional] 
**ad_program** | **string** | Type of Ad program to which the pre moderation components belong to. | [optional] 
**locale** | **string** | Locale value that was passed in request. | [optional] 
**image_components** | [**\AmazonAdvertisingApi\Model\ImageComponentResponse[]**](ImageComponentResponse.md) | Pre moderation result of the image components. It will have information regarding the policy violations present if any. | [optional] 
**date_components** | [**\AmazonAdvertisingApi\Model\DateComponentResponse[]**](DateComponentResponse.md) | Pre moderation result of the date components. It will have information regarding the policy violations present if any. | [optional] 
**text_components** | [**\AmazonAdvertisingApi\Model\TextComponentResponse[]**](TextComponentResponse.md) | Pre moderation result of the text components. It will have information regarding the policy violations present if any. | [optional] 
**video_components** | [**\AmazonAdvertisingApi\Model\VideoComponentResponse[]**](VideoComponentResponse.md) | Pre moderation result of the video components. It will have information regarding the policy violations present if any. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

