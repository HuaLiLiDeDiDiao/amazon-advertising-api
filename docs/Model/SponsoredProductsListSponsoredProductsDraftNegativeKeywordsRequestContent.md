# SponsoredProductsListSponsoredProductsDraftNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsReducedObjectIdFilter**](SponsoredProductsReducedObjectIdFilter.md) |  | [optional] 
**negative_keyword_text_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeywordTextFilter**](SponsoredProductsKeywordTextFilter.md) |  | [optional] 
**max_results** | **int** | Number of records to include in the paginated response. Defaults to max page size for given API | [optional] 
**next_token** | **string** | token value allowing to navigate to the next response page | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsReducedObjectIdFilter**](SponsoredProductsReducedObjectIdFilter.md) |  | [optional] 
**include_extended_data_fields** | **bool** | Whether to get entity with extended data fields such as creationDate, lastUpdateDate, servingStatus | [optional] 
**locale** | **string** | The locale preference of the advertiser. | [optional] 
**negative_keyword_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**match_type_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeMatchType[]**](SponsoredProductsNegativeMatchType.md) | Restricts results to resources with the selected matchType | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

