# DSPReportsSubError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **string** | Request body field which is cause of the error. | [optional] 
**error_type** | **string** | Enumerated error type. | 
**message** | **string** | Detailed error description | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

