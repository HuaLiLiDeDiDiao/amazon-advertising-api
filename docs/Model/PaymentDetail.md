# PaymentDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**payment_method** | [**\AmazonAdvertisingApi\Model\PaymentMethod**](PaymentMethod.md) |  | [optional] 
**received_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**status** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

