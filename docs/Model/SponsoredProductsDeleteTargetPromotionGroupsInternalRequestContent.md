# SponsoredProductsDeleteTargetPromotionGroupsInternalRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_gateway_context** | [**\AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext**](SponsoredProductsApiGatewayContext.md) |  | 
**target_promotion_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetPromotionGroup[]**](SponsoredProductsTargetPromotionGroup.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

