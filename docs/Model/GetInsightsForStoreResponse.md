# GetInsightsForStoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**\AmazonAdvertisingApi\Model\InsightFilter**](InsightFilter.md) |  | [optional] 
**pagination_token** | **string** | The token can be directly used to fetch next page of the result. The token can only been used when the token is been created less than 24 hours and the request input is same as last request | [optional] 
**metrics_details** | [**\AmazonAdvertisingApi\Model\InsightMetricsDetail[]**](InsightMetricsDetail.md) |  | [optional] 
**dimension** | [**\AmazonAdvertisingApi\Model\InsightDimension**](InsightDimension.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

