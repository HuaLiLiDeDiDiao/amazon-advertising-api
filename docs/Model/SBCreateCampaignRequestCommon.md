# SBCreateCampaignRequestCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the campaign. This name must be unique to the Amazon Ads account to which the campaign is associated. Maximum length of the string is 128 characters. Note that idempotency for this field works different for sellers and vendors. Sellers aren&#x27;t allowed to have duplicate campaign names, but vendors can have duplicate campaign names. | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**budget** | **float** | The budget amount associated with the campaign. | [optional] 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | [optional] 
**start_date** | [**\AmazonAdvertisingApi\Model\StartDate**](StartDate.md) |  | [optional] 
**end_date** | [**\AmazonAdvertisingApi\Model\EndDate**](EndDate.md) |  | [optional] 
**ad_format** | [**\AmazonAdvertisingApi\Model\AdFormat**](AdFormat.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**brand_entity_id** | **string** | The brand entity identifier. Note that this field is required for sellers. For more information, see the [Stores reference](https://advertising.amazon.com/API/docs/v2/reference/stores) or [Brands reference](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Brands). | [optional] 
**bid_optimization** | **bool** | Set to &#x60;true&#x60; to allow Amazon to automatically optimize bids for placements below top of search. | [optional] [default to true]
**bid_multiplier** | **float** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBCreative**](SBCreative.md) |  | [optional] 
**landing_page** | [**\AmazonAdvertisingApi\Model\SBLandingPage**](SBLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

