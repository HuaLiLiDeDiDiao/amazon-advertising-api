# KeywordTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**match_type** | **string** | Keyword match type. The default value will be BROAD. | [optional] 
**keyword** | **string** | The keyword value | [optional] 
**bid** | **double** | The bid value for the keyword, in minor currency units (example: cents). The default value will be the suggested bid. | [optional] 
**user_selected_keyword** | **bool** | Flag that tells if keyword was selected by the user or was recommended by KRS | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

