# InitialBudgetRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bidding** | [**\AmazonAdvertisingApi\Model\Bidding**](Bidding.md) |  | 
**ad_groups** | [**\AmazonAdvertisingApi\Model\AdGroup[]**](AdGroup.md) | The ad group information for this new campaign. | 
**end_date** | **string** | The end date of the campaign in YYYYMMDD format. | [optional] 
**targeting_type** | **string** | Specifies the targeting type. | 
**start_date** | **string** | The start date of the campaign in YYYYMMDD format. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

