# SponsoredProductsMissingValueError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMissingValueErrorReason**](SponsoredProductsMissingValueErrorReason.md) |  | 
**marketplace** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplace**](SponsoredProductsMarketplace.md) |  | [optional] 
**cause** | [**\AmazonAdvertisingApi\Model\SponsoredProductsErrorCause**](SponsoredProductsErrorCause.md) |  | [optional] 
**message** | **string** | Human readable error message | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

