# CreateProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | 
**campaign_id** | [**\AmazonAdvertisingApi\Model\CampaignId**](CampaignId.md) |  | 
**landing_page_url** | [**\AmazonAdvertisingApi\Model\LandingPageURL**](LandingPageURL.md) |  | [optional] 
**landing_page_type** | [**\AmazonAdvertisingApi\Model\LandingPageType**](LandingPageType.md) |  | [optional] 
**ad_name** | [**\AmazonAdvertisingApi\Model\AdName**](AdName.md) |  | [optional] 
**asin** | **string** | The ASIN of the product advertised by the product ad. | 
**sku** | **string** | The SKU of the product advertised by the product ad. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

