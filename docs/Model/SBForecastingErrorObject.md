# SBForecastingErrorObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | Correlates the campaign to the campaign list index specified in the request. Zero-based. | [optional] 
**code** | **string** | The forecast error code. | [optional] 
**description** | **string** | The forecast error description. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

