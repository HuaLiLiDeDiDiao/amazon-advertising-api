# CreativePreviewRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative** | [**\AmazonAdvertisingApi\Model\PreviewCreativeModel**](PreviewCreativeModel.md) |  | 
**preview_configuration** | [**\AmazonAdvertisingApi\Model\CreativePreviewConfiguration**](CreativePreviewConfiguration.md) |  | 
**preview_configurations** | [**\AmazonAdvertisingApi\Model\CreativePreviewConfigurations**](CreativePreviewConfigurations.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

