# OptimizationRulesAPIAmazonAdvertisingApiAssociateOptimizationRulesToCampaignRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rule_ids** | **string[]** | An array of rule identifiers. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

