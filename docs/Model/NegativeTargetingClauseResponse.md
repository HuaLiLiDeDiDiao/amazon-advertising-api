# NegativeTargetingClauseResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **float** | The negative target identifier. | [optional] 
**code** | **string** | An enumerated response code. | [optional] 
**details** | **string** | A human-readable description of the value in the &#x60;code&#x60; field. | [optional] 
**description** | **string** | A human-readable description of the value in the &#x60;code&#x60; field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

