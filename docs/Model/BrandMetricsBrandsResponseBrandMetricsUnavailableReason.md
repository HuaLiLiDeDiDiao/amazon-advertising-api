# BrandMetricsBrandsResponseBrandMetricsUnavailableReason

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **string** | Reason for why access to the brand or data for the brand is not available | [optional] 
**code** | **int** | Reason code for why access to the brand or data for the brand is not available | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

