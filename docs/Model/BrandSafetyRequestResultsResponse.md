# BrandSafetyRequestResultsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**\AmazonAdvertisingApi\Model\BrandSafetyRequestResult[]**](BrandSafetyRequestResult.md) | A list of results for the given requestId | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

