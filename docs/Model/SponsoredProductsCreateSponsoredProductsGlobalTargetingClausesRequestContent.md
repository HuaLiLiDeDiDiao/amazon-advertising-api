# SponsoredProductsCreateSponsoredProductsGlobalTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalTargetingClause[]**](SponsoredProductsCreateGlobalTargetingClause.md) | An array of targetingClauses. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

