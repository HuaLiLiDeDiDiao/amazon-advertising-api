# AudienceErrorV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | A value created by Amazon API Gateway that uniquely identifies your request. | [optional] 
**message** | **string** | A human-readable description of the response. | [optional] 
**errors** | [**\AmazonAdvertisingApi\Model\AudienceSubErrorV1[]**](AudienceSubErrorV1.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

