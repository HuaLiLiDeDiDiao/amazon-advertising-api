# BulkAdGroupOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\AdGroupSuccessResponseItem[]**](AdGroupSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\AdGroupFailureResponseItem[]**](AdGroupFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

