# ImageCrop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top_left_y** | **int** | Policy violated region&#x27;s top left Y-axis pixel value. | [optional] 
**top_left_x** | **int** | Policy violated region&#x27;s top left X-axis pixel value. | [optional] 
**width** | **int** | Policy violated region&#x27;s width in pixel. | [optional] 
**height** | **int** | Policy violated region&#x27;s height in pixel. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

