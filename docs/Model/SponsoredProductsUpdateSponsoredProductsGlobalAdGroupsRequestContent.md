# SponsoredProductsUpdateSponsoredProductsGlobalAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalAdGroup[]**](SponsoredProductsUpdateGlobalAdGroup.md) | An array of adGroups with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

