# SBTargetingGetTargetableASINCountsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_ranges** | **string[]** | List of Age Range Refinement Ids. | [optional] 
**brands** | **string[]** | List of Brand Refinement Ids. | [optional] 
**genres** | **string[]** | List of Genre Refinement Ids. | [optional] 
**is_prime_shipping** | **bool** | Indicates if products have prime shipping. Leave empty to include both prime shipping and non-prime shipping products. | [optional] 
**rating_range** | [**\AmazonAdvertisingApi\Model\SBTargetingRatingRange**](SBTargetingRatingRange.md) |  | [optional] 
**category** | **string** | The category refinement id. Please use /sb/targets/categories or /sb/recommendations/targets/category to retrieve category IDs. | 
**price_range** | [**\AmazonAdvertisingApi\Model\SBTargetingPriceRange**](SBTargetingPriceRange.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

