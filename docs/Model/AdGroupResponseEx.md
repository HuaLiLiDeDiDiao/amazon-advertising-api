# AdGroupResponseEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The identifier of the ad group. | [optional] 
**name** | **string** | The name of the ad group. | [optional] 
**campaign_id** | **float** | The identifier of the campaign that this ad group is associated with. | [optional] 
**default_bid** | **double** | The amount of the default bid associated with the ad group. Used if no bid is specified. | [optional] 
**state** | **string** | The delivery state of the ad group. | [optional] 
**tactic** | [**\AmazonAdvertisingApi\Model\Tactic**](Tactic.md) |  | [optional] 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeResponse**](CreativeTypeInCreativeResponse.md) |  | [optional] 
**serving_status** | **string** | The status of the ad group. | [optional] 
**bid_optimization** | **string** | Bid optimization type for the Adgroup. Default behavior is to optimize for clicks. Note, reach and clicks are only accepted with productAds that include landingPageURL OFF_AMAZON_LINK. |Name|CostType|Description| |----|--------|-----------| |reach|vcpm|Optimize for viewable impressions. $1 is the minimum bid for vCPM.| |clicks [Default]|cpc|Optimize for page visits.| |conversions|cpc|Optimize for conversion.| |leads |cpc| [PREVIEW ONLY] Optimize for lead generation.| | [optional] 
**creation_date** | **int** | Epoch time the ad group was created. | [optional] 
**last_updated_date** | **int** | Epoch time any property in the ad group was last updated. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

