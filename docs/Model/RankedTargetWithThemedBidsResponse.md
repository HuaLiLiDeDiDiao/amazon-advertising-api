# RankedTargetWithThemedBidsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_target_list** | [**\AmazonAdvertisingApi\Model\RankedTargetWithThemedBidsList**](RankedTargetWithThemedBidsList.md) |  | [optional] 
**impact_metrics** | [**\AmazonAdvertisingApi\Model\ImpactMetrics[]**](ImpactMetrics.md) | A list of impact metrics which anticipates the number of clicks and orders you will receive if you target all targeting expressions using the low, medium, and high suggested bid. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

