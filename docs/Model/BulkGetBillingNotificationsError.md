# BulkGetBillingNotificationsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | 
**error_code** | [**\AmazonAdvertisingApi\Model\BulkGetBillingNotificationsErrorCodes**](BulkGetBillingNotificationsErrorCodes.md) |  | 
**description** | **string** |  | 
**advertiser_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

