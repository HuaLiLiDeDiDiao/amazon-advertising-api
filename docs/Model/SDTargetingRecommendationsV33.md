# SDTargetingRecommendationsV33

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categories** | [**\AmazonAdvertisingApi\Model\SDCategoryRecommendationV33[]**](SDCategoryRecommendationV33.md) | List of recommended category targets. | [optional] 
**audiences** | [**\AmazonAdvertisingApi\Model\SDAudienceCategoryRecommendations[]**](SDAudienceCategoryRecommendations.md) | List of recommended audience targets, broken down by audience category | [optional] 
**themes** | [**\AmazonAdvertisingApi\Model\SDThemeRecommendationsThemes**](SDThemeRecommendationsThemes.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

