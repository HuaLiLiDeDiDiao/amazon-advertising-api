# InlineResponse20018PolicyViolations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**policy_description** | **string** | A human-readable description of the policy. | [optional] 
**policy_link_url** | **string** | Address of the policy documentation. Follow the link to learn more about the specified policy. | [optional] 
**violating_text_contents** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ViolatingTextContents[]**](InlineResponse20018ViolatingTextContents.md) | Information about the specific text that violates the specified policy in the campaign. | [optional] 
**violating_image_contents** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ViolatingImageContents[]**](InlineResponse20018ViolatingImageContents.md) | Information about the specific image that violates the specified policy. | [optional] 
**violating_asin_contents** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ViolatingAsinContents[]**](InlineResponse20018ViolatingAsinContents.md) | Information about the specific ASIN in the campaign that violates the specified policy. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

