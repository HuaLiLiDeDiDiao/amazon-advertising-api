# SponsoredProductsExpressionTypeFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExpressionType[]**](SponsoredProductsExpressionType.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

