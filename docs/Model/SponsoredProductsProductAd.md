# SponsoredProductsProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The product ad identifier. | 
**campaign_id** | **string** | The campaign identifier. | 
**custom_text** | **string** | The custom text that is associated with this ad. Defined for custom text ads only. | [optional] 
**asin** | **string** | The ASIN associated with the product. Defined for vendors only. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityState**](SponsoredProductsEntityState.md) |  | 
**sku** | **string** | The SKU associated with the product. Defined for seller accounts only. | [optional] 
**ad_group_id** | **string** | The ad group identifier. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsProductAdExtendedData**](SponsoredProductsProductAdExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

