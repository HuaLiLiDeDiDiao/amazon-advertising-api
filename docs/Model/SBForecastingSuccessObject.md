# SBForecastingSuccessObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | Correlates the campaign to the campaign list index specified in the request. Zero-based. | [optional] 
**campaign** | [**\AmazonAdvertisingApi\Model\SBForecastingSuccessCampaign**](SBForecastingSuccessCampaign.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

