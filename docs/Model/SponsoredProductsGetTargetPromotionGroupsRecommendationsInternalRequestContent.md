# SponsoredProductsGetTargetPromotionGroupsRecommendationsInternalRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**client_id** | **string** |  | [optional] 
**api_scope** | **string** |  | [optional] 
**max_results** | **int** |  | [optional] 
**next_token** | **string** | Token for fetching different pages in the response | [optional] 
**ad_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**api_gateway_context** | [**\AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext**](SponsoredProductsApiGatewayContext.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

