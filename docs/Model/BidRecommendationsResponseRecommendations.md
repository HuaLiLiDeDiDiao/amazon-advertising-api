# BidRecommendationsResponseRecommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | The resulting status code for retrieving the bid. | [optional] 
**keyword** | **string** | The keyword text. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**suggested_bid** | [**\AmazonAdvertisingApi\Model\SuggestedBid**](SuggestedBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

