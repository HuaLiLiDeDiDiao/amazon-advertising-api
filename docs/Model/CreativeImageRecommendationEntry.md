# CreativeImageRecommendationEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **double** | Recommendations with higher values are more relevant | [optional] 
**size_in_bytes** | **float** | The asset size in bytes | [optional] 
**asset_id** | **string** | The identifier of image/video asset from the store&#x27;s asset library | [optional] 
**image_url** | **string** | The URL of the asset | [optional] 
**width** | **float** | The width of the asset in pixels | [optional] 
**name** | **string** | The fileName of the asset | [optional] 
**content_type** | [**\AmazonAdvertisingApi\Model\MediaType**](MediaType.md) |  | [optional] 
**height** | **float** | The height of the asset in pixels | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

