# SponsoredProductsUpdateDraftCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **string** | The identifier of an existing portfolio to which the draft is associated. | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**campaign_id** | **string** | The identifier of the draft. | 
**name** | **string** | The name of the draft. | [optional] 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | [optional] 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDraftCampaignDynamicBidding**](SponsoredProductsCreateOrUpdateDraftCampaignDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDraftCampaignBudget**](SponsoredProductsCreateOrUpdateDraftCampaignBudget.md) |  | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

