# BulkGetBillingNotificationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\BulkGetBillingNotificationsSuccess[]**](BulkGetBillingNotificationsSuccess.md) |  | 
**error** | [**\AmazonAdvertisingApi\Model\BulkGetBillingNotificationsError[]**](BulkGetBillingNotificationsError.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

