# SDTargetingRecommendationsResponseV32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommendations** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsV32**](SDTargetingRecommendationsV32.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

