# SponsoredProductsServiceUnavailableExceptionResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\SponsoredProductsServiceUnavailableExceptionErrorCode**](SponsoredProductsServiceUnavailableExceptionErrorCode.md) |  | 
**message** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

