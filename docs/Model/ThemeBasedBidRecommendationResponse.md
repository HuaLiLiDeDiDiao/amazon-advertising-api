# ThemeBasedBidRecommendationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_recommendations** | [**\AmazonAdvertisingApi\Model\ThemeBasedBidRecommendation[]**](ThemeBasedBidRecommendation.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

