# SBVideoListCampaignItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**landing_page** | [**\AmazonAdvertisingApi\Model\SBDetailPageLandingPage**](SBDetailPageLandingPage.md) |  | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBVideoCreative**](SBVideoCreative.md) |  | [optional] 
**supply_source** | [**\AmazonAdvertisingApi\Model\SupplySource**](SupplySource.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

