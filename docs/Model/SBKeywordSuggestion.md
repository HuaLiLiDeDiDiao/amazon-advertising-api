# SBKeywordSuggestion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**search_term_impression_share** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationImpressionShare**](SBKeywordRecommendationImpressionShare.md) |  | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationMatchType**](SBKeywordRecommendationMatchType.md) |  | [optional] 
**translation** | **string** | Localized keyword value if locale was specified. | [optional] 
**search_term_impression_rank** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationImpressionRank**](SBKeywordRecommendationImpressionRank.md) |  | [optional] 
**recommendation_id** | **string** | Unique ID for each recommendation. | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationType**](SBKeywordRecommendationType.md) |  | [optional] 
**value** | **string** | Recommended keyword value. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

