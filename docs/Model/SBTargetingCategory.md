# SBTargetingCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asin_count_range** | [**\AmazonAdvertisingApi\Model\SBTargetingIntegerRange**](SBTargetingIntegerRange.md) |  | [optional] 
**is_targetable** | **bool** | If the category is targetable or not. | [optional] 
**parent_category_refinement_id** | **string** | The category refinement id of the parent category. Missing parentCategoryRefinementId signifies this is a root category. | [optional] 
**estimated_reach** | [**\AmazonAdvertisingApi\Model\SBTargetingEstimatedReachRange**](SBTargetingEstimatedReachRange.md) |  | [optional] 
**name** | **string** | Name of category. | [optional] 
**translated_name** | **string** | Translated name of the category. | [optional] 
**category_refinement_id** | **string** | The category refinement id. Please use /sb/targets/categories or /sb/recommendations/targets/category to retrieve category IDs. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

