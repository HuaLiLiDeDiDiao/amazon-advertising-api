# BidAdjustmentByShopperSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percentage** | **float** |  | [optional] 
**shopper_segment** | [**\AmazonAdvertisingApi\Model\ShopperSegment**](ShopperSegment.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

