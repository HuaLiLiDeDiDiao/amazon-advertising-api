# Theme

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_for_display** | **string** |  | 
**theme_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

