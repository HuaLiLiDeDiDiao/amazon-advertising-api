# AllOfTargetErrorTargetError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid_states** | **string** |  | [optional] 
**match_type** | **string** |  | [optional] 
**target_type** | **string** |  | [optional] 
**note** | **string** |  | [optional] 
**max_targets** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

