# GetCampaignRecommendationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | **string** | An identifier to fetch next set of campaign recommendations records in the result set if available. This will be null when at the end of result set. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\CampaignRecommendation[]**](CampaignRecommendation.md) | List of campaign recommendations. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

