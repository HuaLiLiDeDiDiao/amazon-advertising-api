# BidRecommendationsForTargetsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The identifier of the ad group that the recommended bid are associated with. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\BidRecommendationsForTargetsResponseRecommendations[]**](BidRecommendationsForTargetsResponseRecommendations.md) | An array of recommendation objects. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

