# CreateAssociatedOptimizationRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optimization_rule_ids** | [**\AmazonAdvertisingApi\Model\RuleId[]**](RuleId.md) | A list of optimization rule identifiers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

