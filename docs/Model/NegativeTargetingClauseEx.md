# NegativeTargetingClauseEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **float** |  | [optional] 
**ad_group_id** | **float** |  | [optional] 
**state** | **string** |  | [optional] 
**expression_type** | **string** |  | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\NegativeTargetingClauseExExpression[]**](NegativeTargetingClauseExExpression.md) | The expression to negatively match against. * Only one brand may be specified per targeting expression. * Only one asin may be specified per targeting expression. * To exclude a brand from a targeting expression, you must create a negative targeting expression in the same ad group as the positive targeting expression. | [optional] 
**serving_status** | **string** | The status of the target. | [optional] 
**creation_date** | **int** | Epoch date the target was created. | [optional] 
**last_updated_date** | **int** | Epoch date of the last update to any property associated with the target. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

