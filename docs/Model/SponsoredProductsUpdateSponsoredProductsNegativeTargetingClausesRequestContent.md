# SponsoredProductsUpdateSponsoredProductsNegativeTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateNegativeTargetingClause[]**](SponsoredProductsUpdateNegativeTargetingClause.md) | An array of negativeTargeting with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

