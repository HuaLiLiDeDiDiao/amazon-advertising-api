# SponsoredProductsCreateSponsoredProductsCampaignNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateCampaignNegativeKeyword[]**](SponsoredProductsCreateCampaignNegativeKeyword.md) | An array of campaignNegativeKeywords. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

