# SponsoredProductsCreateSponsoredProductsDraftNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftNegativeKeyword[]**](SponsoredProductsCreateDraftNegativeKeyword.md) | An array of negativeKeywords. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

