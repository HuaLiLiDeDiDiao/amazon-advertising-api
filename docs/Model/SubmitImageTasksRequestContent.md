# SubmitImageTasksRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_task_metadata_list** | [**\AmazonAdvertisingApi\Model\ImageTaskMetadata[]**](ImageTaskMetadata.md) | Advertiser provided information to generate AI images. Max size of the list is 4, each element will be executed as an individual image task | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

