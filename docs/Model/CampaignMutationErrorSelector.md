# CampaignMutationErrorSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_error** | [**\AmazonAdvertisingApi\Model\DateError**](DateError.md) |  | [optional] 
**bidding_error** | [**\AmazonAdvertisingApi\Model\BiddingError**](BiddingError.md) |  | [optional] 
**budget_error** | [**\AmazonAdvertisingApi\Model\BudgetError**](BudgetError.md) |  | [optional] 
**billing_error** | [**\AmazonAdvertisingApi\Model\BillingError**](BillingError.md) |  | [optional] 
**range_error** | [**\AmazonAdvertisingApi\Model\RangeError**](RangeError.md) |  | [optional] 
**other_error** | [**\AmazonAdvertisingApi\Model\OtherError**](OtherError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

