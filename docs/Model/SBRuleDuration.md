# SBRuleDuration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_range_type_rule_duration** | [**\AmazonAdvertisingApi\Model\DateRangeTypeRuleDuration**](DateRangeTypeRuleDuration.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

