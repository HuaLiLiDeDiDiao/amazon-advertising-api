# BaseOptimizationRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | **string** | The state of the optimization rule. | [optional] 
**rule_name** | **string** | The name of the optimization rule. | [optional] 
**rule_conditions** | [**\AmazonAdvertisingApi\Model\RuleCondition[]**](RuleCondition.md) | A list of rule conditions that define the advertiser&#x27;s intent for the outcome of the rule. The rule uses &#x27;AND&#x27; logic to combine every condition in this list, and will validate the combination when the rule is created or updated. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

