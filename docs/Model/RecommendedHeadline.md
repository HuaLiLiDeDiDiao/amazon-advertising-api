# RecommendedHeadline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headline_id** | **string** | Unique Id of Recommended headline. | [optional] 
**headline** | **string** | String that contains Recommended headline. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

