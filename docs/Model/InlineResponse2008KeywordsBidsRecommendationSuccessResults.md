# InlineResponse2008KeywordsBidsRecommendationSuccessResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommendation_id** | **string** | The identifier of the keyword bid recommendation. | [optional] 
**recommended_bid** | [**\AmazonAdvertisingApi\Model\RecommendedBid**](RecommendedBid.md) |  | [optional] 
**keyword** | [**\AmazonAdvertisingApi\Model\InlineResponse2008Keyword**](InlineResponse2008Keyword.md) |  | [optional] 
**keyword_index** | [**\AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex**](SBBidRecommendationKeywordIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

