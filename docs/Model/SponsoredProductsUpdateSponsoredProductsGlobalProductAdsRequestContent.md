# SponsoredProductsUpdateSponsoredProductsGlobalProductAdsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_ads** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalProductAd[]**](SponsoredProductsUpdateGlobalProductAd.md) | An array of ads with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

