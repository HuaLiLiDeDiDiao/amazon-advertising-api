# Values

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conversions** | [**\AmazonAdvertisingApi\Model\Conversions**](Conversions.md) |  | [optional] 
**clicks** | [**\AmazonAdvertisingApi\Model\Clicks**](Clicks.md) |  | [optional] 
**impressions** | [**\AmazonAdvertisingApi\Model\Impressions**](Impressions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

