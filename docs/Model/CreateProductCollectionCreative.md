# CreateProductCollectionCreative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_logo_crop** | [**\AmazonAdvertisingApi\Model\BrandLogoCrop**](BrandLogoCrop.md) |  | [optional] 
**asins** | **string[]** |  | [optional] 
**brand_name** | **string** |  | [optional] 
**custom_image_asset_id** | **string** |  | [optional] 
**custom_image_crop** | [**\AmazonAdvertisingApi\Model\CustomImageCrop**](CustomImageCrop.md) |  | [optional] 
**brand_logo_asset_id** | **string** |  | [optional] 
**headline** | **string** | The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

