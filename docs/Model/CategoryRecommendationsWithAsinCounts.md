# CategoryRecommendationsWithAsinCounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categories** | [**\AmazonAdvertisingApi\Model\CategoryItemWithAsinCounts[]**](CategoryItemWithAsinCounts.md) | List of category recommendations | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

