# AllOfCreativeErrorCreativeError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_length** | **string** |  | [optional] 
**emoji** | **string** |  | [optional] 
**invalid_characters** | **string** |  | [optional] 
**note** | **string** |  | [optional] 
**expected_type** | **string** |  | [optional] 
**actual_type** | **string** |  | [optional] 
**max_size** | **string** |  | [optional] 
**asins** | **string** |  | [optional] 
**crop_field** | **string** |  | [optional] 
**min** | **string** |  | [optional] 
**top** | **string** |  | [optional] 
**crop_height** | **string** |  | [optional] 
**image_height** | **string** |  | [optional] 
**left** | **string** |  | [optional] 
**crop_width** | **string** |  | [optional] 
**image_width** | **string** |  | [optional] 
**aspect_ratio** | **string** |  | [optional] 
**required_aspect_ratio** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

