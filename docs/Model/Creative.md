# Creative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_id** | **float** | Unique identifier of the creative. | 
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeResponse**](CreativeTypeInCreativeResponse.md) |  | 
**properties** | [**\AmazonAdvertisingApi\Model\CreativeProperties**](CreativeProperties.md) |  | 
**moderation_status** | **string** | The moderation status of the creative | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

