# VideoComponent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component_type** | **string** | Type of the video component. | 
**landing_page** | [**\AmazonAdvertisingApi\Model\LandingPage**](LandingPage.md) |  | [optional] 
**id** | **string** | Id of the component. The same will be returned as part of the response as well. This can be used to uniquely identify the component from the pre moderation response. | 
**url** | **string** | Url of the video to be pre moderated. The url must be publicly accessible. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

