# BudgetRecommendationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_recommendations_success_results** | [**\AmazonAdvertisingApi\Model\BudgetRecommendation[]**](BudgetRecommendation.md) | List of successful budget recomendation for campagins. | 
**budget_recommendations_error_results** | [**\AmazonAdvertisingApi\Model\BudgetRecommendationError[]**](BudgetRecommendationError.md) | List of errors that occured when generating bduget recommendation. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

