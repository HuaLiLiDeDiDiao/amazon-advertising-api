# PerformanceMeasureConditionForSB

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metric_name** | [**\AmazonAdvertisingApi\Model\PerformanceMetricForSB**](PerformanceMetricForSB.md) |  | 
**comparison_operator** | [**\AmazonAdvertisingApi\Model\ComparisonOperator**](ComparisonOperator.md) |  | 
**threshold** | **double** | The performance threshold value. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

