# ProfileError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile_error** | [**AllOfProfileErrorProfileError**](AllOfProfileErrorProfileError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

