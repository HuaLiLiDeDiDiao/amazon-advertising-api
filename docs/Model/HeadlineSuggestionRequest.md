# HeadlineSuggestionRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | An array of ASINs associated with the creative. Note do not pass an empty array, this results in an error. | [optional] 
**store_pages** | [**\AmazonAdvertisingApi\Model\StorePage[]**](StorePage.md) | An array of Store Pages associated with SB Spotlight Creative. | [optional] 
**max_num_suggestions** | **float** | Maximum number of suggestions that API should return. Response will [0, maxNumSuggestions] suggestions (suggestions are not guaranteed). | [optional] 
**ad_format** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

