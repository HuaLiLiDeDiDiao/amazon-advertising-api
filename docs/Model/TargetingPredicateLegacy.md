# TargetingPredicateLegacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**value** | **string** | The value to be targeted. | [optional] 
**event_type** | **string** | The type of event that the value applies to. Only available for similarProduct and exactProduct currently. * views event type corresponds to a customer who viewed the detail page of the product(s). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

