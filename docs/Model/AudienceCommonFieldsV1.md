# AudienceCommonFieldsV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audience_name** | **string** | Audience name | 
**sub_category** | **string** | Audience segment sub-category | [optional] 
**update_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**description** | **string** | Audience description | 
**audience_id** | **string** | Audience segment identifier | 
**category** | **string** | Audience segment category | 
**create_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | **string** |  | 
**forecasts** | [**\AmazonAdvertisingApi\Model\AudienceCommonFieldsV1Forecasts**](AudienceCommonFieldsV1Forecasts.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

