# SponsoredProductsCreateTargetPromotionGroupTargetsInternalResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsSuccessResponseItem[]**](SponsoredProductsCreateTargetPromotionGroupTargetsSuccessResponseItem.md) | List of successfully created targets. | [optional] 
**errors** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetPromotionGroupTargetsFailureResponseItem[]**](SponsoredProductsCreateTargetPromotionGroupTargetsFailureResponseItem.md) | List of targets that failed to create. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

