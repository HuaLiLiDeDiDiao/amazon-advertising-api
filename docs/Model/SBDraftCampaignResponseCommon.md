# SBDraftCampaignResponseCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**draft_campaign_id** | **int** | The identifier of the draft campaign. | [optional] 
**code** | **string** | The draft campaign response code. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

