# SponsoredProductsDraftCampaignNegativeKeywordSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keyword** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignNegativeKeyword**](SponsoredProductsDraftCampaignNegativeKeyword.md) |  | [optional] 
**campaign_negative_keyword_id** | **string** | the negativeKeyword ID | [optional] 
**index** | **int** | the index of the draft in the array from the request body | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

