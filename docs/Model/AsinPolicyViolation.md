# AsinPolicyViolation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**policy_description** | **string** | A human-readable description of the policy. | [optional] 
**name** | **string** | A policy violation code. | [optional] 
**type** | **string** | Type of policy violation. | [optional] 
**policy_link_url** | **string** | Address of the policy documentation. Follow the link to learn more about the specified policy. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

