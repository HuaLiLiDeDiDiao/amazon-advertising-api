# SBCreateCampaignsResponseFullV33

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign** | [**\AmazonAdvertisingApi\Model\SBCampaignResponseV33**](SBCampaignResponseV33.md) |  | [optional] 
**all_of** | [**\AmazonAdvertisingApi\Model\SBCreateCampaignsResponseDefault**](SBCreateCampaignsResponseDefault.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

