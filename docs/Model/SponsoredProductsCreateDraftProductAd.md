# SponsoredProductsCreateDraftProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The campaign identifier. | 
**custom_text** | **string** | The custom text to use for creating a custom text ad for the associated ASIN. Defined only for KDP Authors and Book Vendors in US marketplace. | [optional] 
**asin** | **string** | The ASIN associated with the product. Defined for vendors only. | [optional] 
**sku** | **string** | The SKU associated with the product. Defined for seller accounts only. | [optional] 
**ad_group_id** | **string** | The ad group identifier. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

