# SDForecastResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_optimization** | **string** |  | [optional] 
**lifetime_forecasts** | [**\AmazonAdvertisingApi\Model\Forecast[]**](Forecast.md) | Forecasts for campaign start date and end date. Default end date is start date plus 7 days. | [optional] 
**weekly_forecasts** | [**\AmazonAdvertisingApi\Model\Forecast[]**](Forecast.md) | Weekly average forecasts. | [optional] 
**daily_forecasts** | [**\AmazonAdvertisingApi\Model\Forecast[]**](Forecast.md) | Daily average forecasts. | [optional] 
**forecast_status** | [**\AmazonAdvertisingApi\Model\ForecastStatus**](ForecastStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

