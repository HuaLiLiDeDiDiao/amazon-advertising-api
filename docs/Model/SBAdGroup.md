# SBAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The identifier of the campaign associated with the ad group. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group. | [optional] 
**name** | **string** | The name of the ad group. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

