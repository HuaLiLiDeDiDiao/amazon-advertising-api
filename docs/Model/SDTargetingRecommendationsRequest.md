# SDTargetingRecommendationsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tactic** | [**\AmazonAdvertisingApi\Model\SDTactic**](SDTactic.md) |  | 
**products** | [**\AmazonAdvertisingApi\Model\SDGoalProduct[]**](SDGoalProduct.md) | A list of products for which to get targeting recommendations | 
**type_filter** | [**\AmazonAdvertisingApi\Model\SDRecommendationType[]**](SDRecommendationType.md) | A filter to indicate which types of recommendations to request. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

