# ImpactMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clicks** | [**\AmazonAdvertisingApi\Model\ImpactMetric**](ImpactMetric.md) |  | [optional] 
**orders** | [**\AmazonAdvertisingApi\Model\ImpactMetric**](ImpactMetric.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

