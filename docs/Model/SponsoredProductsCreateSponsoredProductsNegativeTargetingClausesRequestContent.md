# SponsoredProductsCreateSponsoredProductsNegativeTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateNegativeTargetingClause[]**](SponsoredProductsCreateNegativeTargetingClause.md) | An array of negativeTargeting. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

