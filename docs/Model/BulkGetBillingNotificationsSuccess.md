# BulkGetBillingNotificationsSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_notifications** | [**\AmazonAdvertisingApi\Model\BillingNotification[]**](BillingNotification.md) |  | 
**index** | **int** |  | 
**advertiser_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

