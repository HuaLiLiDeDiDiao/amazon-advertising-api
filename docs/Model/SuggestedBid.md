# SuggestedBid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested** | **float** | The bid recommendation. | [optional] 
**range_start** | **float** | The lower bound bid recommendation. | [optional] 
**range_end** | **float** | The upper bound bid recommendation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

