# SponsoredProductsCreateSponsoredProductsDraftProductAdsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**draft_product_ads** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkDraftProductAdOperationResponse**](SponsoredProductsBulkDraftProductAdOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

