# SponsoredProductsUpdateSponsoredProductsGlobalTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalTargetingClause[]**](SponsoredProductsUpdateGlobalTargetingClause.md) | An array of targetingClauses with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

