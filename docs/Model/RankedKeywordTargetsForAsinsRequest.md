# RankedKeywordTargetsForAsinsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | An array list of Asins | 
**bidding_strategy** | **string** | The bid recommendations returned will depend on the bidding strategy. &lt;br&gt; LEGACY_FOR_SALES - Dynamic Bids (Down only) &lt;br&gt; AUTO_FOR_SALES - Dynamic Bids (Up or down) &lt;br&gt; MANUAL - Fixed Bids | [optional] [default to 'LEGACY_FOR_SALES']
**recommendation_type** | **string** | The recommendationType to retrieve recommended keyword targets for a list of ASINs. | 
**bids_enabled** | **bool** | Set this parameter to false if you do not want to retrieve bid suggestions for your keyword targets. Defaults to true. | [optional] [default to true]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

