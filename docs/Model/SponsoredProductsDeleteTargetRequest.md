# SponsoredProductsDeleteTargetRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_promotion_group_id** | **string** | Entity object identifier | 
**target_id** | **string** | Entity object identifier | 
**manual_targeting_ad_group_id** | **string** | Entity object identifier | 
**auto_targeting_campaign_ad_group_id** | **string** | Entity object identifier | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

