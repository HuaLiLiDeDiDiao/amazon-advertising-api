# SponsoredProductsCreateOrUpdateBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateBudgetType**](SponsoredProductsCreateOrUpdateBudgetType.md) |  | 
**budget** | **double** | Monetary value | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

