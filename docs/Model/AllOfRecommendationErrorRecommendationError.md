# AllOfRecommendationErrorRecommendationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_count** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

