# SBKeywordResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated response code. | [optional] 
**details** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | [optional] 
**errors** | [**\AmazonAdvertisingApi\Model\SBKeywordResponseErrors[]**](SBKeywordResponseErrors.md) | Array of KeywordError object. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

