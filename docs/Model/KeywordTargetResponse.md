# KeywordTargetResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_bid** | [**AllOfKeywordTargetResponseSuggestedBid**](AllOfKeywordTargetResponseSuggestedBid.md) |  | [optional] 
**translation** | **string** | The translation of keyword if a locale is passed in | [optional] 
**rank** | **float** | The keyword target rank | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

