# AllOfSBCreateTargetsResponseCreateTargetErrorResultsItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_request_index** | [**\AmazonAdvertisingApi\Model\SBTargetRequestIndex**](SBTargetRequestIndex.md) |  | [optional] 
**target_id** | [**\AmazonAdvertisingApi\Model\SBTargetId**](SBTargetId.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

