# BiddingAdjustments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predicate** | **string** | You can enable controls to adjust your bid based on the placement location. Specify a location where you want to use bid controls. The percentage value set is the percentage of the original bid for which you want to have your bid adjustment increased. For example, a 50% adjustment on a $1.00 bid would increase the bid to $1.50 for the opportunity to win a specified placement. | Predicate |  Placement | |-----------|------------| | &#x60;placementTop&#x60; | Top of search (first page) | | &#x60;placementProductPage&#x60; | Product pages | | [optional] 
**percentage** | **float** | The bid adjustment percentage value. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

