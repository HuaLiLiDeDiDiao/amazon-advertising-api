# AsinComponent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component_type** | **string** | Type of the asin component. | 
**asin** | **string** | Asin id to be pre moderated. | 
**id** | **string** | Id of the component. The same will be returned as part of the response as well. This can be used to uniquely identify the component from the pre moderation response. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

