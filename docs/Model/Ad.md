# Ad

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The ad identifier. Note: Ads created using version 3/non-multi ad group campaigns do not have an associated adId. [Learn more](https://advertising.amazon.com/API/docs/en-us/sponsored-brands/campaigns/managing-multi-ad-group-campaigns#ads). | [optional] 
**campaign_id** | **string** | The campaign identifier. | 
**landing_page** | [**\AmazonAdvertisingApi\Model\LandingPage**](LandingPage.md) |  | [optional] 
**name** | **string** | The name of the ad. Note: Ads created using version 3/non-multi ad group campaigns do not have an associated name. [Learn more](https://advertising.amazon.com/API/docs/en-us/sponsored-brands/campaigns/managing-multi-ad-group-campaigns#ads). | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\EntityState**](EntityState.md) |  | 
**ad_group_id** | **string** | The adGroup identifier. | 
**creative** | [**\AmazonAdvertisingApi\Model\Creative**](Creative.md) |  | [optional] 
**extended_data** | [**\AmazonAdvertisingApi\Model\AdExtendedData**](AdExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

