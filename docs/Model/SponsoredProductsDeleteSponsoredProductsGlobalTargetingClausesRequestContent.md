# SponsoredProductsDeleteSponsoredProductsGlobalTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

