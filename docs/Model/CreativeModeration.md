# CreativeModeration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_id** | **float** | Unique identifier of the creative. | 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeResponse**](CreativeTypeInCreativeResponse.md) |  | 
**moderation_status** | **string** | The moderation status of the creative. |Status|Description| |------|-----------| |APPROVED|Moderation for the creative is complete.| |IN_PROGRESS|Moderation for the creative is in progress. The expected date and time for completion are specfied in the &#x60;etaForModeration&#x60; field.| |REJECTED|The creative has failed moderation. Specific information about the content that violated policy is available in &#x60;policyViolations&#x60;.| | 
**eta_for_moderation** | [**\DateTime**](\DateTime.md) | Expected date and time by which moderation will be complete. | 
**policy_violations** | [**\AmazonAdvertisingApi\Model\CreativeModerationPolicyViolations[]**](CreativeModerationPolicyViolations.md) | A list of policy violations for a creative that has failed moderation. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

