# InlineResponse20018ViolatingImageContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**moderated_component** | **string** | The ad component that includes the image that violates the specified policy. | [optional] 
**reviewed_image_url** | **string** | Address of the image reviewed during moderation. | [optional] 
**image_evidences** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ImageEvidences[]**](InlineResponse20018ImageEvidences.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

