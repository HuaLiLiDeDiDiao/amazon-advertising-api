# BudgetRuleRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_budget_increase_percent** | **float** | suggested increase percent | [optional] 
**rule_name** | **string** | rule name for the recomemendation | [optional] 
**rule_id** | **string** | rule id for the recomemendation | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

