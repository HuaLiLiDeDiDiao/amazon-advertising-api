# SnapshotRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state_filter** | **string** | Optional. Restricts results to entities with state within the specified comma-separated list. Default behavior is to include &#x27;enabled&#x27; and &#x27;paused&#x27;. You can include &#x27;enabled&#x27;, &#x27;paused&#x27;, and &#x27;archived&#x27; or any combination. | [optional] 
**tactic_filter** | [**\AmazonAdvertisingApi\Model\TacticFilter**](TacticFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

