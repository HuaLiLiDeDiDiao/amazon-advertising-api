# SBForecastingResponseCampaignObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**successes** | [**\AmazonAdvertisingApi\Model\SBForecastingSuccessObject[]**](SBForecastingSuccessObject.md) |  | [optional] 
**errors** | [**\AmazonAdvertisingApi\Model\SBForecastingErrorObject[]**](SBForecastingErrorObject.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

