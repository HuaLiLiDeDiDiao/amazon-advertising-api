# SponsoredProductsUpdateSponsoredProductsGlobalCampaignNegativeKeywordsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkGlobalCampaignNegativeKeywordOperationResponse**](SponsoredProductsBulkGlobalCampaignNegativeKeywordOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

