# RuleDuration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_type_rule_duration** | [**\AmazonAdvertisingApi\Model\EventTypeRuleDuration**](EventTypeRuleDuration.md) |  | [optional] 
**date_range_type_rule_duration** | [**\AmazonAdvertisingApi\Model\DateRangeTypeRuleDuration**](DateRangeTypeRuleDuration.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

