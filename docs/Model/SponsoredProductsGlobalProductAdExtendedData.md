# SponsoredProductsGlobalProductAdExtendedData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductAdServingStatus**](SponsoredProductsGlobalProductAdServingStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

