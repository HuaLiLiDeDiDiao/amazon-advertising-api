# FilterError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter_error** | [**AllOfFilterErrorFilterError**](AllOfFilterErrorFilterError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

