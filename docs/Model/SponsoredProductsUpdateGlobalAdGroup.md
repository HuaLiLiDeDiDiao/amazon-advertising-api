# SponsoredProductsUpdateGlobalAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicable_marketplaces** | **string[]** |  | [optional] 
**name** | **string** | The name of the ad group. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 
**ad_group_id** | **string** | The identifier of the keyword. | 
**default_bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

