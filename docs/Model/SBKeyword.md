# SBKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **int** | The keyword identifier. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group associated with the keyword. | [optional] 
**campaign_id** | **int** | The identifier of the campaign associated with the keyword. | [optional] 
**keyword_text** | **string** | The keyword text. The maximum number of words for this string is 10. | [optional] 
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBKeywordState**](SBKeywordState.md) |  | [optional] 
**bid** | **float** | The bid associated with the keyword. Note that this value must be less than the budget associated with the Advertiser account. For more information, see the **Keyword bid constraints by marketplace** section of the [supported features](https://advertising.amazon.com/API/docs/v2/guides/supported_features) article. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

