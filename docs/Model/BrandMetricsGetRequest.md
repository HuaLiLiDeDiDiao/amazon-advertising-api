# BrandMetricsGetRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_name** | **string** | Brand Name | [optional] 
**report_start_date** | [**\DateTime**](\DateTime.md) | Beginning of the data range (inclusive) in YYYY-MM-DD format . The date will be in the Coordinated Universal Time (UTC) timezone. If no date is passed, it will fetch the reports from the begining date till the reportEndDate. if no date is passed for both reportStartDate and reportEndDate, latest report will be fetched | [optional] 
**category_tree_name** | **string** | The node at the top of a browse tree. It is the start node of a tree | 
**look_back_period** | **string** | Currently supported values: \&quot;1w\&quot; (one week), \&quot;1m\&quot; (one month) and \&quot;1cm\&quot; (one calendar month). This defines the period of time used to determine the number of shoppers in the metrics computation. | [optional] [default to '1w']
**category_path** | **string[]** | The hierarchical path that leads to a node starting with the root node | [optional] 
**next_token** | **string** | Token  to fetch additional results (if any). Subsequent calls must be made with same  parameters as in the previous requests. | [optional] 
**brand_id** | **string** | Brand Id from BrandAid | 
**category_id** | **string** | Category Node ID represents the catalog node that can be used in product targeting with categories | 
**report_end_date** | [**\DateTime**](\DateTime.md) | End of the data range (inclusive) in YYYY-MM-DD format . The date will be in the Coordinated Universal Time (UTC) timezone. If no date is passed, it will fetch the reports from the reportStartDate to the latest date. if no date is passed for both reportStartDate and reportEndDate, latest report will be fetched | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

