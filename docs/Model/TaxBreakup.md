# TaxBreakup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payer_jurisdiction** | **string** | Tax jurisdiction of payer (billed customer) | [optional] 
**tax_rate** | **float** |  | 
**issuer_tax_information** | [**\AmazonAdvertisingApi\Model\IssuerTaxRegistrationInfo**](IssuerTaxRegistrationInfo.md) |  | 
**third_party_tax_information** | [**\AmazonAdvertisingApi\Model\ThirdPartyTaxRegistrationInfo**](ThirdPartyTaxRegistrationInfo.md) |  | [optional] 
**issuer_jurisdiction** | **string** | Tax jurisdiction of issuer (Amazon billing entity) | 
**payer_tax_information** | [**\AmazonAdvertisingApi\Model\PayerTaxRegistrationInfo**](PayerTaxRegistrationInfo.md) |  | 
**tax_amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**tax_name** | **string** |  | 
**taxed_jurisdiction_name** | **string** | Tax jurisdiction for which tax applies, this can be at the country, state or local level. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

