# SDTargetingRecommendationsFailureV34

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | HTTP status code indicating a failure response for targeting recomendations. | [optional] 
**name** | **string** | The theme name specified in the request. If the themes field is not provided in the request, the value of this field will be set to default. | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\SDProductTargetingThemeExpression[]**](SDProductTargetingThemeExpression.md) | A list of expressions that failed to be applied in the product targeting theme. | [optional] 
**error_message** | **string** | A human friendly error message indicating the failure reasons. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

