# BrandMetricsOverviewResponseOverviewMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**overview_metadata** | [**\AmazonAdvertisingApi\Model\BrandMetricsOverviewResponseOverviewMetadata**](BrandMetricsOverviewResponseOverviewMetadata.md) |  | [optional] 
**metrics** | [**\AmazonAdvertisingApi\Model\BrandMetricsOverviewResponseMetrics**](BrandMetricsOverviewResponseMetrics.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

