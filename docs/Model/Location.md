# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_expression_id** | [**\AmazonAdvertisingApi\Model\LocationExpressionId**](LocationExpressionId.md) |  | [optional] 
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\LocationExpression[]**](LocationExpression.md) | The Location definition. | [optional] 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\ResolvedLocationExpression[]**](ResolvedLocationExpression.md) | The human-readable location definition. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

