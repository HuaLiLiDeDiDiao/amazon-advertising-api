# SponsoredProductsCreateGlobalCampaignNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeMatchType**](SponsoredProductsCreateOrUpdateNegativeMatchType.md) |  | 
**name** | **string** | Name for the keyword | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**keyword_text** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalNegativeKeywordText**](SponsoredProductsGlobalNegativeKeywordText.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

