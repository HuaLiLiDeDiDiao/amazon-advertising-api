# SponsoredProductsCreateSponsoredProductsGlobalCampaignsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalCampaign[]**](SponsoredProductsCreateGlobalCampaign.md) | An array of campaigns. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

