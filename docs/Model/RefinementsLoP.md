# RefinementsLoP

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**age_ranges** | [**\AmazonAdvertisingApi\Model\AgeRangesLoP**](AgeRangesLoP.md) |  | [optional] 
**brands** | [**\AmazonAdvertisingApi\Model\BrandsLoP**](BrandsLoP.md) |  | [optional] 
**genres** | [**\AmazonAdvertisingApi\Model\GenresLoP**](GenresLoP.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

