# SBTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expressions** | [**\AmazonAdvertisingApi\Model\SBExpression[]**](SBExpression.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\SBAPIBid**](SBAPIBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

