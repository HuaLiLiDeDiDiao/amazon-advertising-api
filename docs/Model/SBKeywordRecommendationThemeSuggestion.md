# SBKeywordRecommendationThemeSuggestion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationThemeKeyword[]**](SBKeywordRecommendationThemeKeyword.md) |  | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationThemeType**](SBKeywordRecommendationThemeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

