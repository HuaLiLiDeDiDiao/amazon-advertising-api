# SponsoredProductsUpdateGlobalCampaignNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword. | 
**name** | **string** | Name for the keyword | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 
**keyword_text** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalNegativeKeywordText**](SponsoredProductsGlobalNegativeKeywordText.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

