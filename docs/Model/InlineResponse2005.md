# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media_id** | [**\AmazonAdvertisingApi\Model\MediaId**](MediaId.md) |  | [optional] 
**status** | [**\AmazonAdvertisingApi\Model\MediaStatus**](MediaStatus.md) |  | [optional] 
**status_metadata** | [**\AmazonAdvertisingApi\Model\InlineResponse2005StatusMetadata[]**](InlineResponse2005StatusMetadata.md) |  | [optional] 
**published_media_url** | **string** | The preview URL of the media. It is only available when status is &#x60;Available&#x60;. | [optional] 
**original_media_url** | **string** | This is a signed URL which returns the original media in .mp4 file extension. The URL is only active for 7 days and requires to be regenerated if the video is not downloaded within 7 days. If you try to upload the downloaded video using the Asset Library API and get an error, then please retry upload after changing the file extension to .mov. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

