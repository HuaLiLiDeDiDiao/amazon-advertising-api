# SponsoredProductsCreateKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser. | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. For example, if the advertiser’s preferred language is Simplified Chinese, set the locale to zh_CN. Supported locales include: Simplified Chinese (locale: zh_CN) for US, UK and CA. English (locale: en_GB) for DE, FR, IT and ES. | [optional] 
**campaign_id** | **string** | The identifer of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateMatchType**](SponsoredProductsCreateOrUpdateMatchType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 
**bid** | **double** | Bid associated with this keyword. Applicable to biddable match types only. For more information about bid constraints by marketplace, see [bid limits](https://advertising.amazon.com/API/docs/en-us/concepts/limits#bid-constraints-by-marketplace). | [optional] 
**ad_group_id** | **string** | The identifier of the ad group to which this keyword is associated. | 
**keyword_text** | **string** | The keyword text. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

