# BidSuggestion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested** | **double** | The suggested bid | [optional] 
**range_start** | **double** | The bid range start | [optional] 
**bid_rec_id** | **string** | The bid recommendation id | [optional] 
**range_end** | **double** | The bid range end | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

