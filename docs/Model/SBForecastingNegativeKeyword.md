# SBForecastingNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | [optional] 
**match_type** | **string** | The negative match type. Valid value: NEGATIVE_EXACT, NEGATIVE_PHRASE. For more information, see [negative keyword match types](https://advertising.amazon.com/help#GHTRFDZRJPW6764R) in the Amazon Advertising support center. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

