# AllOfinlineResponse20012UpdateTargetErrorResultsItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | [**\AmazonAdvertisingApi\Model\SBTargetId**](SBTargetId.md) |  | [optional] 
**target_request_index** | [**\AmazonAdvertisingApi\Model\SBNegativeTargetRequestIndex**](SBNegativeTargetRequestIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

