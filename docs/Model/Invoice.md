# Invoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotions** | [**\AmazonAdvertisingApi\Model\Promotions**](Promotions.md) |  | 
**government_invoice_information** | [**\AmazonAdvertisingApi\Model\GovernmentInvoiceInformation**](GovernmentInvoiceInformation.md) |  | [optional] 
**payer_contact_info** | [**\AmazonAdvertisingApi\Model\ContactInfo**](ContactInfo.md) |  | 
**tax_detail** | [**\AmazonAdvertisingApi\Model\TaxDetail**](TaxDetail.md) |  | 
**adjustments** | [**\AmazonAdvertisingApi\Model\Adjustments**](Adjustments.md) |  | 
**invoice_lines** | [**\AmazonAdvertisingApi\Model\InvoiceLines**](InvoiceLines.md) |  | 
**invoice_summary** | [**\AmazonAdvertisingApi\Model\InvoiceSummary**](InvoiceSummary.md) |  | 
**issuer_contact_info** | [**\AmazonAdvertisingApi\Model\ContactInfo**](ContactInfo.md) |  | 
**third_party_contact_info** | [**\AmazonAdvertisingApi\Model\ThirdPartyContactInformation**](ThirdPartyContactInformation.md) |  | 
**payments** | [**\AmazonAdvertisingApi\Model\Payments**](Payments.md) |  | 
**portfolios** | [**\AmazonAdvertisingApi\Model\Portfolios**](Portfolios.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

