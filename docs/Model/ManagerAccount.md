# ManagerAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manager_account_name** | **string** | The name given to a Manager Account. | [optional] 
**linked_accounts** | [**\AmazonAdvertisingApi\Model\Account[]**](Account.md) |  | [optional] 
**manager_account_id** | **string** | Id of the Manager Account. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

