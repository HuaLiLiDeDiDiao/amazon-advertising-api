# OptimizationRulesAPIAmazonAdvertisingApiGetAssociatedCampaignsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**associated_campaigns** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiSingleCampaignRuleAssociationStatus[]**](OptimizationRulesAPIAmazonAdvertisingApiSingleCampaignRuleAssociationStatus.md) |  | [optional] 
**code** | **string** | An enumerated error code for machine use. | [optional] 
**next_token** | **string** | To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

