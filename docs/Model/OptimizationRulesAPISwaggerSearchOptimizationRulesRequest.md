# OptimizationRulesAPIAmazonAdvertisingApiSearchOptimizationRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | **string** |  | [optional] 
**optimization_rule_filter** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleFilter**](OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleFilter.md) |  | [optional] 
**page_size** | **float** |  | [optional] 
**campaign_filter** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiCampaignFilter**](OptimizationRulesAPIAmazonAdvertisingApiCampaignFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

