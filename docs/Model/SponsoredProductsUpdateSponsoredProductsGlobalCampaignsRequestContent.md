# SponsoredProductsUpdateSponsoredProductsGlobalCampaignsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateGlobalCampaign[]**](SponsoredProductsUpdateGlobalCampaign.md) | An array of campaigns with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

