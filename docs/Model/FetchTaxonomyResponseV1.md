# FetchTaxonomyResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_path** | **string[]** |  | [optional] 
**next_token** | **string** |  | [optional] 
**categories** | [**\AmazonAdvertisingApi\Model\FetchTaxonomyNodeV1[]**](FetchTaxonomyNodeV1.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

