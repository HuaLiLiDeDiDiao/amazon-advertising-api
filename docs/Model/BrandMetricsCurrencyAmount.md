# BrandMetricsCurrencyAmount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **double** |  | [optional] 
**currency_code** | **string** | The currency used for all monetary values for entities under this profile | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

