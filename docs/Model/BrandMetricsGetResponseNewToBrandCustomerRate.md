# BrandMetricsGetResponseNewToBrandCustomerRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**this_brand** | **double** | share of customers that had not purchased [Brand Name] products in the last 12 months, but did so in the [lookBackPeriod] | [optional] 
**top_performers** | **double** | share of customers that had not purchased average of the top 95th-99th percent of peers products in the last 12 months, but did so in the [lookBackPeriod] | [optional] 
**peer_median** | **double** | share of customers that had not purchased peer median products in the last 12 months, but did so in the [lookBackPeriod] | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

