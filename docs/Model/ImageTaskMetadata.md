# ImageTaskMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_spec** | [**\AmazonAdvertisingApi\Model\ImageSpec**](ImageSpec.md) |  | [optional] 
**max_results** | **float** | Optional. An upper bound for number of image results for this set of metadata. Default value is 4. | [optional] 
**theme_id** | **string** | Optional. | [optional] 
**asin** | **string** | Required. The product that is shown in AI image. | 
**prompt** | **string** | Optional. Open text prompt | [optional] 
**product_image_asset_id** | **string** | Optional. Source image provided by advertiser and they are registered in Asset Library | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

