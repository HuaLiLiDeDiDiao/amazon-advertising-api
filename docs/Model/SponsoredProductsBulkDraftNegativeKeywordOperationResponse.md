# SponsoredProductsBulkDraftNegativeKeywordOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftNegativeKeywordSuccessResponseItem[]**](SponsoredProductsDraftNegativeKeywordSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftNegativeKeywordFailureResponseItem[]**](SponsoredProductsDraftNegativeKeywordFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

