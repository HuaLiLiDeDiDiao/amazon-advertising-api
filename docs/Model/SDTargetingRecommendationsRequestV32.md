# SDTargetingRecommendationsRequestV32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tactic** | [**\AmazonAdvertisingApi\Model\SDTacticV31**](SDTacticV31.md) |  | 
**products** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsProducts**](SDTargetingRecommendationsProducts.md) |  | 
**type_filter** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsTypeFilterV31**](SDTargetingRecommendationsTypeFilterV31.md) |  | 
**themes** | [**\AmazonAdvertisingApi\Model\SDTargetingRecommendationsThemes**](SDTargetingRecommendationsThemes.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

