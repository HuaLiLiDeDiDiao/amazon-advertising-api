# Keyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | [optional] 
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser. | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. For example, if the advertiser’s preferred language is Simplified Chinese, set the locale to &#x60;zh_CN&#x60;. Supported locales include: Simplified Chinese (locale: zh_CN) for US, UK and CA. English (locale: en_GB) for DE, FR, IT and ES. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\SBMatchType**](SBMatchType.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\SBAPIBid**](SBAPIBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

