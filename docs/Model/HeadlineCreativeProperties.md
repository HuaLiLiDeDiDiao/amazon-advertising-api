# HeadlineCreativeProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headline** | **string** | A marketing phrase to display on the ad. This field is optional and mutable. Maximum number of characters allowed is 50. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

