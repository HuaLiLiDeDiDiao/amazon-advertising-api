# OptimizationRulesAPIAmazonAdvertisingApiActionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action_unit** | **string** |  | 
**value** | **double** |  | 
**action_operator** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleActionOperator**](OptimizationRulesAPIAmazonAdvertisingApiRuleActionOperator.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

