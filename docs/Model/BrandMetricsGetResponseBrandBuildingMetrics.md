# BrandMetricsGetResponseBrandBuildingMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseMetadata**](BrandMetricsGetResponseMetadata.md) |  | [optional] 
**return_on_engagement_metrics** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseReturnOnEngagementMetrics**](BrandMetricsGetResponseReturnOnEngagementMetrics.md) |  | [optional] 
**abi_metrics** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseAbiMetrics**](BrandMetricsGetResponseAbiMetrics.md) |  | [optional] 
**engaged_shopper_rate** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseEngagedShopperRate**](BrandMetricsGetResponseEngagedShopperRate.md) |  | [optional] 
**shoppers_metrics** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseShoppersMetrics**](BrandMetricsGetResponseShoppersMetrics.md) |  | [optional] 
**customer_conversion_rate** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseCustomerConversionRate**](BrandMetricsGetResponseCustomerConversionRate.md) |  | [optional] 
**new_to_brand_customer_rate** | [**\AmazonAdvertisingApi\Model\BrandMetricsGetResponseNewToBrandCustomerRate**](BrandMetricsGetResponseNewToBrandCustomerRate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

