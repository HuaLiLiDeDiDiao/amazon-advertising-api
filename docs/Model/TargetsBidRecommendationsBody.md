# TargetsBidRecommendationsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The ad group identifier. | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\TargetingExpressionPredicate[][]**](array.md) | The list of targeting expressions. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

