# OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrenceTimesOfDay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_time** | **string** | Time of the day in HH:MM. | 
**end_time** | **string** | Time of the day in HH:MM. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

