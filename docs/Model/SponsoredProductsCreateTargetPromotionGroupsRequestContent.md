# SponsoredProductsCreateTargetPromotionGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_ids** | **string[]** | The list of adIds (optional) of the Ad Group of the Auto-Targeting campaign, that will be part of the Target Promotion Group. If this     list is empty, all the Product Ads under the Ad Group will be part of the Target Promotion Group. | [optional] 
**existing_campaign_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExistingCampaignDetails**](SponsoredProductsExistingCampaignDetails.md) |  | [optional] 
**ad_group_id** | **string** | The adGroupId of the Ad Group of an Auto-Targeting campaign that will be part of the Target Promotion Group. | 
**new_campaign_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignDetails**](SponsoredProductsNewCampaignDetails.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

