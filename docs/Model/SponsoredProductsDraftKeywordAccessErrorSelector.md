# SponsoredProductsDraftKeywordAccessErrorSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_not_found_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityNotFoundError**](SponsoredProductsEntityNotFoundError.md) |  | [optional] 
**missing_value_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMissingValueError**](SponsoredProductsMissingValueError.md) |  | [optional] 
**locale_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsLocaleError**](SponsoredProductsLocaleError.md) |  | [optional] 
**malformed_value_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMalformedValueError**](SponsoredProductsMalformedValueError.md) |  | [optional] 
**internal_server_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsInternalServerError**](SponsoredProductsInternalServerError.md) |  | [optional] 
**range_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsRangeError**](SponsoredProductsRangeError.md) |  | [optional] 
**other_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsOtherError**](SponsoredProductsOtherError.md) |  | [optional] 
**throttled_error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsThrottledError**](SponsoredProductsThrottledError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

