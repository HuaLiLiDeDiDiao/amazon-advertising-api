# SDForecastRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign** | [**\AmazonAdvertisingApi\Model\Campaign**](Campaign.md) |  | 
**ad_group** | [**\AmazonAdvertisingApi\Model\AdGroup**](AdGroup.md) |  | 
**optimization_rules** | [**\AmazonAdvertisingApi\Model\OptimizationRule[]**](OptimizationRule.md) | A list of SD optimization rules. Forecast will be affected by the optimization strategy rules.  Currently, supported rule metrics by forecast are &#x60;COST_PER_CLICK&#x60;, &#x60;COST_PER_THOUSAND_VIEWABLE_IMPRESSIONS&#x60; and &#x60;COST_PER_ORDER&#x60;. | [optional] 
**product_ads** | [**\AmazonAdvertisingApi\Model\ProductAd[]**](ProductAd.md) |  | 
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SDForecastRequestTargetingClause[]**](SDForecastRequestTargetingClause.md) | A list of SD targeting clauses. | 
**negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\NegativeTargetingClause[]**](NegativeTargetingClause.md) | A list of SD negative targeting clauses. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

