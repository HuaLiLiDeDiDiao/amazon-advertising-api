# CreativeModerationViolatingTextPosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **int** | Zero-based index into the text in reviewedText where the text specified in violatingText starts | [optional] 
**end** | **int** | Zero-based index into the text in reviewedText where the text specified in violatingText ends | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

