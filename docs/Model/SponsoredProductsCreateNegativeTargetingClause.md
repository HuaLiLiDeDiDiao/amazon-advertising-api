# SponsoredProductsCreateNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate[]**](SponsoredProductsCreateOrUpdateNegativeTargetingExpressionPredicate.md) | The NegativeTargeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

