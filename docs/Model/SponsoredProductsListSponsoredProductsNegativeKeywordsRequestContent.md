# SponsoredProductsListSponsoredProductsNegativeKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsReducedObjectIdFilter**](SponsoredProductsReducedObjectIdFilter.md) |  | [optional] 
**state_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityStateFilter**](SponsoredProductsEntityStateFilter.md) |  | [optional] 
**negative_keyword_text_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsKeywordTextFilter**](SponsoredProductsKeywordTextFilter.md) |  | [optional] 
**max_results** | **int** | Number of records to include in the paginated response. Defaults to max page size for given API | [optional] 
**next_token** | **string** | token value allowing to navigate to the next response page | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsReducedObjectIdFilter**](SponsoredProductsReducedObjectIdFilter.md) |  | [optional] 
**include_extended_data_fields** | **bool** | Whether to get entity with extended data fields such as creationDate, lastUpdateDate, servingStatus | [optional] 
**locale** | **string** | Restricts results to negativeKeywords that match the specified locale. | [optional] 
**negative_keyword_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**match_type_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeMatchType[]**](SponsoredProductsNegativeMatchType.md) | Only the negativeKeyword with the match type that is in this list will be listed | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

