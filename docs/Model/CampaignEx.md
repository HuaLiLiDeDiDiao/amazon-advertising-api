# CampaignEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **float** | The identifier of an existing portfolio to which the campaign is associated. | [optional] 
**campaign_id** | **float** | The identifier of the campaign. | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**name** | **string** | The name of the campaign. | [optional] 
**campaign_type** | **string** | The advertising product managed by this campaign. | [optional] 
**targeting_type** | **string** | The type of targeting of the campaign. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**daily_budget** | **float** | The daily budget of the campaign. | [optional] 
**start_date** | **string** | The starting date of the campaign. The format of the date is YYYYMMDD. | [optional] 
**end_date** | **string** | The ending date of the campaign to stop running. The format of the date is YYYYMMDD. | [optional] 
**premium_bid_adjustment** | **bool** | If set to true, Amazon increases the default bid for ads that are eligible to appear in this placement. See developer notes for more information. | [optional] 
**bidding** | [**\AmazonAdvertisingApi\Model\Bidding**](Bidding.md) |  | [optional] 
**creation_date** | **float** | The creation date of the campaign, in epoch time. | [optional] 
**last_updated_date** | **float** | The date that any value associated with the campaign was last changed, in epoch time. | [optional] 
**serving_status** | **string** | The computed status of the campaign. See developer notes for more information. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

