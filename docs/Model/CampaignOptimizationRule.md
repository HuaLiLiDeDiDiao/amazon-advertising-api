# CampaignOptimizationRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | [**\AmazonAdvertisingApi\Model\RecurrenceType**](RecurrenceType.md) |  | [optional] 
**rule_action** | [**\AmazonAdvertisingApi\Model\RuleAction**](RuleAction.md) |  | [optional] 
**campaign_optimization_id** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationId**](CampaignOptimizationId.md) |  | 
**created_date** | [**\AmazonAdvertisingApi\Model\RuleCreationDate**](RuleCreationDate.md) |  | [optional] 
**rule_condition** | [**\AmazonAdvertisingApi\Model\RuleConditionList**](RuleConditionList.md) |  | [optional] 
**rule_type** | [**\AmazonAdvertisingApi\Model\RuleType**](RuleType.md) |  | [optional] 
**rule_name** | [**\AmazonAdvertisingApi\Model\RuleName**](RuleName.md) |  | [optional] 
**campaign_ids** | [**\AmazonAdvertisingApi\Model\RuleCampaignId[]**](RuleCampaignId.md) |  | [optional] 
**rule_status** | [**\AmazonAdvertisingApi\Model\RuleStatus**](RuleStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

