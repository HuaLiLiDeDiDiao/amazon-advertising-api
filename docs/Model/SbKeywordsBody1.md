# SbKeywordsBody1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **int** | The identifier of an existing ad group to which the keyword is associated. | [optional] 
**campaign_id** | **int** | The identifier of an existing campaign to which the keyword is associated. | [optional] 
**keyword_text** | **string** | The keyword text. The maximum number of words for this string is 10. | [optional] 
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser. | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. For example, if the advertiser’s preferred language is Simplified Chinese, set the locale to &#x60;zh_CN&#x60;. Supported locales include: Simplified Chinese (locale: zh_CN) for US, UK and CA. English (locale: en_GB) for DE, FR, IT and ES. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**bid** | **float** | The bid associated with the keyword. Note that this value must be less than the budget associated with the Advertiser account. For more information, see [Keyword bid constraints by marketplace](https://advertising.amazon.com/API/docs/en-us/reference/concepts/limits#bid-constraints-by-marketplace). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

