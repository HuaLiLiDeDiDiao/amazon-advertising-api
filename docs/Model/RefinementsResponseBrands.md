# RefinementsResponseBrands

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | The brand identifier. Note that this is not the same identifier returned from the [getBrands](sponsored-brands/3-0/openapi#/Brands/getBrands) operation. | [optional] 
**name** | **string** | The brand name. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

