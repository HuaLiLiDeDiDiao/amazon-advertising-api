# SponsoredProductsGlobalTargetingClauseServingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalKeywordServingStatusReason[]**](SponsoredProductsGlobalKeywordServingStatusReason.md) | Serving status details of adgroup | [optional] 
**targeting_clause_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityServingStatus**](SponsoredProductsGlobalEntityServingStatus.md) |  | [optional] 
**marketplace_targeting_clause_serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplaceTargetingClauseServingStatus[]**](SponsoredProductsMarketplaceTargetingClauseServingStatus.md) | The marketplace serving statuses. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

