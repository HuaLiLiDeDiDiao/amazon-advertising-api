# SDTargetingClauseV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression_type** | **string** | Tactic T00020 ad groups only allow manual targeting. | 
**expression** | [**\AmazonAdvertisingApi\Model\SDTargetingExpressionV31**](SDTargetingExpressionV31.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

