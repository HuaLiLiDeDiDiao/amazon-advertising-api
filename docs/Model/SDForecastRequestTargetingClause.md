# SDForecastRequestTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | [**\AmazonAdvertisingApi\Model\TargetId**](TargetId.md) |  | [optional] 
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | [optional] 
**expression_type** | **string** | Tactic T00020 &amp; T00030 ad groups should use &#x27;manual&#x27; targeting. | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

