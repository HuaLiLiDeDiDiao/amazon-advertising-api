# SponsoredProductsUpdateGlobalProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The product ad identifier. | 
**name** | **string** | Name for the product Ad | [optional] 
**asin** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 
**sku** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

