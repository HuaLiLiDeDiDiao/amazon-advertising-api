# InlineResponse20019

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The unique ID for your requested report. | [optional] 
**record_type** | **string** | The record type of the report. It can be &#x60;campaign&#x60;, &#x60;adGroup&#x60;, &#x60;keyword&#x60;, or &#x60;targets&#x60;. | [optional] 
**status** | **string** | The status of the report. Status is one of &#x60;IN_PROGRESS&#x60;, &#x60;SUCCESS&#x60;, or &#x60;FAILURE&#x60;. | [optional] 
**status_details** | **string** | Description of the status. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

