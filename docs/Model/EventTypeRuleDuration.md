# EventTypeRuleDuration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **string** | The event identifier. This value is available from the budget rules recommendation API. | 
**end_date** | **string** | The event end date in YYYYMMDD format. Read-only. | [optional] 
**event_name** | **string** | The event name. Read-only. | [optional] 
**start_date** | **string** | The event start date in YYYYMMDD format. Read-only. Note that this field is present only for announced events. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

