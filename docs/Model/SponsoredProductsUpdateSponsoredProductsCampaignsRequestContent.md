# SponsoredProductsUpdateSponsoredProductsCampaignsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateCampaign[]**](SponsoredProductsUpdateCampaign.md) | An array of campaigns with updated values. Note: targetingType cannot be updated | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

