# DspAudienceErrorItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idempotency_key** | **string** | unique request token for this Audience Edit request. | 
**index** | **int** | Index of the DspAudienceEditRequestItem from the request. e.g. 1st item in the request will correspond to index 0 in the response. | 
**audience_id** | **string** | Identifier of audience for which edit was attempted. | 
**message** | **string** | A human-readable description of the response. | 
**errors** | [**\AmazonAdvertisingApi\Model\DspAudienceErrorItemError[]**](DspAudienceErrorItemError.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

