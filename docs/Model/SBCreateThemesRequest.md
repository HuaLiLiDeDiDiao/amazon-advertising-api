# SBCreateThemesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **string** | The identifier of the ad group to which the target is associated. | 
**campaign_id** | **string** | The identifier of the campaign to which the target is associated. | [optional] 
**theme_type** | [**\AmazonAdvertisingApi\Model\ThemeType**](ThemeType.md) |  | 
**bid** | [**\AmazonAdvertisingApi\Model\Bid**](Bid.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

