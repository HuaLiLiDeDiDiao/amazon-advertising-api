# SDProductTargetingRecommendationsSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | HTTP status code 200 indicating a successful response for product recomendations. | [optional] 
**name** | **string** | The theme name specified in the request. | [optional] 
**recommendations** | [**\AmazonAdvertisingApi\Model\SDProductRecommendationV32[]**](SDProductRecommendationV32.md) | A list of recommended products. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

