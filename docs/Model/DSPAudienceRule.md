# DSPAudienceRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clause** | [**\AmazonAdvertisingApi\Model\Clause**](Clause.md) |  | 
**attribute_type** | [**\AmazonAdvertisingApi\Model\AttributeType**](AttributeType.md) |  | 
**attribute_values** | **string[]** | For a given audienceType and attributeType combination, the attribute values being supplied. | 
**operator** | [**\AmazonAdvertisingApi\Model\Operator**](Operator.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

