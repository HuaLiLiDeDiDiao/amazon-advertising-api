# SponsoredProductsCreateOrUpdateMarketplaceState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplace**](SponsoredProductsMarketplace.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

