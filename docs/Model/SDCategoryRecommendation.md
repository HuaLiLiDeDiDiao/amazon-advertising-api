# SDCategoryRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**\AmazonAdvertisingApi\Model\SDCategory**](SDCategory.md) |  | [optional] 
**name** | **string** | The category name | [optional] 
**path** | **string[]** | The path of the category within the category catalogue. | [optional] 
**targetable_asin_count_range** | [**\AmazonAdvertisingApi\Model\SDCategoryRecommendationTargetableAsinCountRange**](SDCategoryRecommendationTargetableAsinCountRange.md) |  | [optional] 
**rank** | **int** | A rank to signify which recommendations are weighed more heavily, with a lower rank signifying a stronger recommendation | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

