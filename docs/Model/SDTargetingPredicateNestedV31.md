# SDTargetingPredicateNestedV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | 
**value** | [**\AmazonAdvertisingApi\Model\SDTargetingPredicateBaseV31[]**](SDTargetingPredicateBaseV31.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

