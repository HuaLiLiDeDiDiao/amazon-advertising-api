# BrandMetricsGetResponseEngagedShopperRateTopPerformers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**upper_bound** | [****](.md) |  | [optional] 
**lower_bound** | [****](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

