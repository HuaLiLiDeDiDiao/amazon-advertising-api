# SponsoredProductsCreateSponsoredProductsGlobalAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalAdGroup[]**](SponsoredProductsCreateGlobalAdGroup.md) | An array of adGroups. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

