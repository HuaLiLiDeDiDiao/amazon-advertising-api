# LocationExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\LocationPredicate**](LocationPredicate.md) |  | [optional] 
**value** | **string** | The location identifier. Currently, this can correspond to either a &#x27;city&#x27;, &#x27;state&#x27;, &#x27;dma&#x27;, &#x27;postal code&#x27;, or &#x27;country&#x27;. Its value is discoverable using the GET /locations API. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

