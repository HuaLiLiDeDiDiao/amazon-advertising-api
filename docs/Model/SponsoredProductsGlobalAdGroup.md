# SponsoredProductsGlobalAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of the campaign to which the keyword is associated. | 
**applicable_marketplaces** | **string[]** |  | [optional] 
**name** | **string** | The name of the ad group. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityState**](SponsoredProductsGlobalEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the keyword. | 
**default_bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalAdGroupExtendedData**](SponsoredProductsGlobalAdGroupExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

