# ThemeBasedBidRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme** | [**\AmazonAdvertisingApi\Model\Theme**](Theme.md) |  | 
**bid_recommendations_for_targeting_expressions** | [**\AmazonAdvertisingApi\Model\BidRecommendationPerTargetingExpression[]**](BidRecommendationPerTargetingExpression.md) | The bid recommendations for targeting expressions listed in the request. | 
**impact_metrics** | [**\AmazonAdvertisingApi\Model\ImpactMetrics**](ImpactMetrics.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

