# BrandSafetyRequestResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**\AmazonAdvertisingApi\Model\BrandSafetyDenyListDomainUpdateResultStatus**](BrandSafetyDenyListDomainUpdateResultStatus.md) |  | [optional] 
**details** | **string** | A human-readable description of the response. | [optional] 
**domain_id** | **int** | The identifier of the Brand Safety Deny List Domain. | [optional] 
**name** | **string** | The website or app identifier. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

