# SBCampaignBudgetRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule_state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**last_updated_date** | **float** | Epoch time of budget rule update. Read-only. | [optional] 
**created_date** | **float** | Epoch time of budget rule creation. Read-only. | [optional] 
**rule_details** | [**\AmazonAdvertisingApi\Model\SBBudgetRuleDetails**](SBBudgetRuleDetails.md) |  | [optional] 
**rule_id** | **string** | The budget rule identifier. | 
**rule_status** | **string** | The budget rule evaluation status. Read-only. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

