# SnapshotResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snapshot_id** | **string** | The identifier of the snapshot that was requested. | [optional] 
**record_type** | **string** | The record type of the snapshot file. | [optional] 
**status** | **string** | The status of the generation of the snapshot. | [optional] 
**status_details** | **string** | Optional description of the status. | [optional] 
**location** | **string** | The URI for the snapshot. It&#x27;s only available if status is SUCCESS. | [optional] 
**file_size** | **float** | The size of the snapshot file in bytes. It&#x27;s only available if status is SUCCESS. | [optional] 
**expiration** | **float** | The epoch time for expiration of the snapshot file and each snapshot file will be expired in 30 mins after generated. It&#x27;s only available if status is SUCCESS. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

