# SponsoredProductsMarketplaceCampaignServingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_reasons** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalCampaignMarketplaceServingReason[]**](SponsoredProductsGlobalCampaignMarketplaceServingReason.md) |  | [optional] 
**marketplace** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplace**](SponsoredProductsMarketplace.md) |  | [optional] 
**serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalCampaignMarketplaceServingStatus**](SponsoredProductsGlobalCampaignMarketplaceServingStatus.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

