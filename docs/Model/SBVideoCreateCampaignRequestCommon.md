# SBVideoCreateCampaignRequestCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the campaign. This name must be unique to the Amazon Ads account to which the campaign is associated. Maximum length of the string is 128 characters. | [optional] 
**budget** | **float** | The budget amount associated with the campaign. | [optional] 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | [optional] 
**start_date** | [**\AmazonAdvertisingApi\Model\StartDate**](StartDate.md) |  | [optional] 
**end_date** | [**\AmazonAdvertisingApi\Model\EndDate**](EndDate.md) |  | [optional] 
**ad_format** | [**\AmazonAdvertisingApi\Model\AdFormat**](AdFormat.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**brand_entity_id** | **string** | The brand entity identifier. Note that this field is required for sellers. For more information, see the [Stores reference](https://advertising.amazon.com/API/docs/v2/reference/stores) or [Brands reference](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Brands). | [optional] 
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBVideoCreative**](SBVideoCreative.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

