# CreativeRecommendationByIdResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_type** | **string** | Supported are PRODUCT_COLLECTION, STORE_SPOTLIGHT, VIDEO, BRAND_VIDEO. More could be added in future. | [optional] 
**creative_properties** | [**\AmazonAdvertisingApi\Model\CreativeRecommendationProperties**](CreativeRecommendationProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

