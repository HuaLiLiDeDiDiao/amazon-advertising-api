# ProductAdResponseEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **float** | The identifier of the ad. | [optional] 
**ad_group_id** | **float** | The identifier of the ad group associated with the ad. | [optional] 
**campaign_id** | **float** | The identifier of the campaign associated with the ad. | [optional] 
**landing_page_url** | [**\AmazonAdvertisingApi\Model\LandingPageURL**](LandingPageURL.md) |  | [optional] 
**landing_page_type** | [**\AmazonAdvertisingApi\Model\LandingPageType**](LandingPageType.md) |  | [optional] 
**ad_name** | [**\AmazonAdvertisingApi\Model\AdName**](AdName.md) |  | [optional] 
**asin** | **string** | The ASIN of the product being advertised. | [optional] 
**sku** | **string** | The SKU of the product being advertised. | [optional] 
**state** | **string** | The state of the product ad. | [optional] 
**serving_status** | **string** | The status of the product ad. | [optional] 
**creation_date** | **int** | Epoch date the product ad was created. | [optional] 
**last_updated_date** | **int** | Epoch date of the last update to any property associated with the product ad. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

