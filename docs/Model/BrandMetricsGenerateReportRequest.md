# BrandMetricsGenerateReportRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_node_tree_name** | **string** | Optional. The node at the top of a browse tree. It is the start node of a tree | [optional] 
**category_node_path** | **string[]** | Optional. The hierarchical path that leads to a node starting with the root node. If no Category Node Name is passed, then all data available for all brands belonging to the entity are retrieved. | [optional] 
**brand_name** | **string** | Optional. Brand Name. If no Brand Name is passed, then all data available for all brands belonging to the entity are retrieved. | [optional] 
**report_start_date** | [**\DateTime**](\DateTime.md) | Optional. Retrieves metrics with metricsComputationDate between reportStartDate and reportEndDate  (inclusive). The date will be in the Coordinated Universal Time (UTC) timezone in YYYY-MM-DD format. If no date is passed in reportStartDate, all available metrics with metricsComputationDate till the reportEndDate will be provided. If no date is passed for either reportStartDate or reportEndDate, the metrics with the most receont metricsComputationDate will be returned. | [optional] 
**look_back_period** | **string** | Currently supported values: \&quot;1w\&quot; (one week), \&quot;1m\&quot; (one month) and  \&quot;1cm\&quot; (one calendar month). This defines the period of time used to determine the number of shoppers in the metrics computation. | [optional] [default to '1w']
**format** | **string** | Format of the report | [optional] [default to 'JSON']
**metrics** | **string[]** | Optional. Specify an array of string of metrics field names to include in the report. If no metric field names are specified, all metrics are returned. | [optional] 
**report_end_date** | [**\DateTime**](\DateTime.md) | Optional. Retrieves metrics with metricsComputationDate between reportStartDate and reportEndDate  (inclusive). The date will be in the Coordinated Universal Time (UTC) timezone in YYYY-MM-DD format. If no date is passed in reportEndDate, all available metrics with metricsComputationDate from the reportStartDate will be provided. If no date is passed for either reportStartDate or reportEndDate, the metrics with the most receont metricsComputationDate will be returned. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

