# BrandMetricsGetResponseAbiMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consideration_index** | **double** | Consideration Index measures actual on-Amazon audience signals reflecting consideration for your brand in the short and long term, across the whole on-Amazon audience. The percentile lets you know how the consideration activity for your brand compares to that of your peers. | [optional] 
**sales_index** | **double** | Sales Index represents actual on-Amazon sales in the short term and long term, across the whole on-Amazon audience. The percentile lets you know how your sales compare to that of your peers | [optional] 
**awareness_index** | **double** | Awareness Index measures actual on-Amazon audience signals reflecting awareness for your brand in the short and long term, across the whole on-Amazon audience. The percentile lets you know how the awareness activity for your brand compares to that of your peers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

