# SDAudienceCategoryRecommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**\AmazonAdvertisingApi\Model\SDAudienceCategory**](SDAudienceCategory.md) |  | [optional] 
**audiences** | [**\AmazonAdvertisingApi\Model\SDAudienceRecommendation[]**](SDAudienceRecommendation.md) | List of recommended standard Amazon audience targets | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

