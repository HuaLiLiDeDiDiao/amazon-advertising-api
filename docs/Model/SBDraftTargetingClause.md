# SBDraftTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **int** | The target identifier. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group to which the target is associated. | [optional] 
**campaign_id** | **int** | The identifier of the campaign to which the target is associated. | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\SBExpression**](SBExpression.md) |  | [optional] 
**resolved_expressions** | [**\AmazonAdvertisingApi\Model\SBResolvedExpression**](SBResolvedExpression.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\Bid**](Bid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

