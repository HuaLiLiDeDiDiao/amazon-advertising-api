# SBCreateCampaignsResponseDefault

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The campaign identifier. | [optional] 
**ad_group_responses** | [**\AmazonAdvertisingApi\Model\SBCreateCampaignsResponseDefaultAdGroupResponses[]**](SBCreateCampaignsResponseDefaultAdGroupResponses.md) | An array of ad groups associated with the campaign. | [optional] 
**keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) | An array of keywords associated with the campaign. | [optional] 
**negative_keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) | An array of negative keywords associated with the campaign. | [optional] 
**targeting_clause_responses** | [**\AmazonAdvertisingApi\Model\SBTargetingClauseResponse[]**](SBTargetingClauseResponse.md) | An array of targets associated with the campaign. | [optional] 
**negative_targeting_clause_responses** | [**\AmazonAdvertisingApi\Model\SBTargetingClauseResponse[]**](SBTargetingClauseResponse.md) | An array of negative targets associated with the campaign. | [optional] 
**code** | **string** | An enumerated response code. | [optional] 
**description** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

