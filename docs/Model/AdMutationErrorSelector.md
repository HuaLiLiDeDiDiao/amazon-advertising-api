# AdMutationErrorSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**range_error** | [**\AmazonAdvertisingApi\Model\RangeError**](RangeError.md) |  | [optional] 
**other_error** | [**\AmazonAdvertisingApi\Model\OtherError**](OtherError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

