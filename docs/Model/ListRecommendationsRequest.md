# ListRecommendationsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | [**\AmazonAdvertisingApi\Model\NextToken**](NextToken.md) |  | [optional] 
**max_results** | [**\AmazonAdvertisingApi\Model\MaxResultsRecommendations**](MaxResultsRecommendations.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

