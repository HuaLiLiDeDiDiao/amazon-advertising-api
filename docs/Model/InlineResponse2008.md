# InlineResponse2008

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords_bids_recommendation_success_results** | [**\AmazonAdvertisingApi\Model\InlineResponse2008KeywordsBidsRecommendationSuccessResults[]**](InlineResponse2008KeywordsBidsRecommendationSuccessResults.md) | Lists the bid recommendations for the keywords specified in the request. | [optional] 
**keywords_bids_recommendation_error_results** | [**\AmazonAdvertisingApi\Model\AllOfinlineResponse2008KeywordsBidsRecommendationErrorResultsItems[]**](.md) | Lists errors that occurred during creation of keyword bid recommendations. | [optional] 
**targets_bids_recommendation_success_results** | [**\AmazonAdvertisingApi\Model\InlineResponse2008TargetsBidsRecommendationSuccessResults[]**](InlineResponse2008TargetsBidsRecommendationSuccessResults.md) | Lists the bid recommendations for the keywords or targets specified in the request. | [optional] 
**targets_bids_recommendation_error_results** | [**\AmazonAdvertisingApi\Model\AllOfinlineResponse2008TargetsBidsRecommendationErrorResultsItems[]**](.md) | Lists errors that occurred during creation of target bid recommendations. | [optional] 
**themes_recommendation_success_results** | [**\AmazonAdvertisingApi\Model\InlineResponse2008ThemesRecommendationSuccessResults[]**](InlineResponse2008ThemesRecommendationSuccessResults.md) | Lists the theme targets recommendations for the theme targets specified in the request. | [optional] 
**themes_recommendation_error_results** | [**\AmazonAdvertisingApi\Model\AllOfinlineResponse2008ThemesRecommendationErrorResultsItems[]**](.md) | Lists errors that occurred during creation of theme targets bid recommendations. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

