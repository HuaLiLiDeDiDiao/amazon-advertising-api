# SBNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **int** | The target identifier. | [optional] 
**ad_group_id** | **int** | The identifier of an existing ad group. The newly created target is associated to this ad group | [optional] 
**campaign_id** | **int** | The identifier of an existing campaign. The newly created target is associated to this campaign | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\SBNegativeTargetingExpressions**](SBNegativeTargetingExpressions.md) |  | [optional] 
**resolved_expressions** | [**\AmazonAdvertisingApi\Model\SBResolvedExpression**](SBResolvedExpression.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBProductTargetState**](SBProductTargetState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

