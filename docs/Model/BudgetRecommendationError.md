# BudgetRecommendationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | 
**campaign_id** | **string** | The identifier of a campaign. | 
**index** | **float** | Correlate the recommendation to the campaign index in the request. Zero-based. | 
**details** | **string** | An enumerated response code. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

