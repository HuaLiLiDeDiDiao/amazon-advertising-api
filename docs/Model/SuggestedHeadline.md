# SuggestedHeadline

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headline_id** | **string** | Unique Id of suggested headline. | [optional] 
**headline** | **string** | String that contains suggested headline. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

