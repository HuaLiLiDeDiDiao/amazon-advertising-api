# BaseAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the ad group. | [optional] 
**campaign_id** | [**\AmazonAdvertisingApi\Model\CampaignId**](CampaignId.md) |  | [optional] 
**default_bid** | **double** | The amount of the default bid associated with the ad group. Used if no bid is specified. | [optional] 
**bid_optimization** | **string** | Bid Optimization for the Adgroup. Default behavior is to optimize for clicks. |Name|CostType|Description| |----|--------|-----------| |reach |vcpm|Optimize for viewable impressions. $1 is the minimum bid for vCPM.| |clicks |cpc|[Default] Optimize for page visits.| |conversions |cpc|Optimize for conversion.| | [optional] 
**state** | **string** | The state of the ad group. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

