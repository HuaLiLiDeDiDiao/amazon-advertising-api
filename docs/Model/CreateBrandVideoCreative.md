# CreateBrandVideoCreative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** |  | [optional] 
**brand_logo_crop** | [**\AmazonAdvertisingApi\Model\BrandLogoCrop**](BrandLogoCrop.md) |  | [optional] 
**brand_name** | **string** |  | [optional] 
**consent_to_translate** | **bool** | If set to true and the headline and/or video are not in the marketplace&#x27;s default language, Amazon will attempt to translate them to the marketplace&#x27;s default language. If Amazon is unable to translate them, the ad will be rejected by moderation. We only support translating headlines and videos from English to German, French, Italian, Spanish, Japanese, and Dutch. See developer notes for more information. | [optional] 
**video_asset_ids** | **string[]** |  | [optional] 
**brand_logo_asset_id** | **string** |  | [optional] 
**headline** | **string** | The headline text. Maximum length of the string is 50 characters for all marketplaces other than Japan, which has a maximum length of 35 characters. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

