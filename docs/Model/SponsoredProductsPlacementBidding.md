# SponsoredProductsPlacementBidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percentage** | **int** |  | [optional] 
**placement** | [**\AmazonAdvertisingApi\Model\SponsoredProductsPlacement**](SponsoredProductsPlacement.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

