# SDTargetingBidRecommendationsResponseV32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_optimization** | [**\AmazonAdvertisingApi\Model\SDBidOptimizationV32**](SDBidOptimizationV32.md) |  | 
**cost_type** | [**\AmazonAdvertisingApi\Model\SDCostTypeV31**](SDCostTypeV31.md) |  | 
**bid_recommendations** | [**\AmazonAdvertisingApi\Model\OneOfSDTargetingBidRecommendationsResponseV32BidRecommendationsItems[]**](.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

