# InlineResponse20018ViolatingTextContents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**moderated_component** | **string** | The ad component that includes the text that violates the specified policy. | [optional] 
**reviewed_text** | **string** | The specific text reviewed during moderation. | [optional] 
**text_evidences** | [**\AmazonAdvertisingApi\Model\InlineResponse20018TextEvidences[]**](InlineResponse20018TextEvidences.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

