# BrandMetricsOverviewResponseMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**engaged_shopper_rate_lower_bound** | **double** | Lower bound on the Percentage of unique shoppers in a category that your brand has driven an engagement with or purchase from int the selected time frame divided by the total unique shoppers with 1+ detail page view in the selected category | [optional] 
**engaged_shopper_rate_upper_bound** | **double** | Upper bound on the Percentage of unique shoppers in a category that your brand has driven an engagement with or purchase from int the selected time frame divided by the total unique shoppers with 1+ detail page view in the selected category | [optional] 
**customer_conversion_rate** | **double** | Percentage of shoppers moving from \\\&quot;considering\\\&quot; [Brand Name] in the [categoryPath] to \\\&quot;purchased\\\&quot; in the [lookBackPeriod] | [optional] 
**total_shoppers** | **int** | Total number of shoppers that interacted with the brand in the given lookback period. | [optional] 
**new_to_brand_customer_rate** | **double** | share of customers that had not purchased [Brand Name] products in the last 12 months, but did so in the [lookBackPeriod] | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

