# ListThemesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_results** | **float** | Optional. The max limit for the number of themes it can return. | [optional] 
**next_token** | **string** | Optional. The pagination token to retrieve the next page of results. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

