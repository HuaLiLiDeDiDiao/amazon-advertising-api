# InlineResponse20015RecommendedProducts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommended_target_asin** | **string** | The recommended target ASIN. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

