# SponsoredProductsCopyCampaignTaskDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_marketplace_id** | **string** | The target marketplace in obfuscated format. | [optional] 
**target_campaign_id** | **string** | The identifier of the campaign in the target marketplace. | [optional] 
**percentage_completed** | **int** | Percent of copy operation that is complete | [optional] 
**source_marketplace_id** | **string** | The source marketplace in obfuscated format. | [optional] 
**source_campaign_id** | **string** | The identifier of the campaign in the source marketplace. | [optional] 
**source_advertiser_id** | **string** | The identifier of the advertiser in source marketplace. | [optional] 
**target_advertiser_id** | **string** | The identifier of the advertiser in the target marketplace. | [optional] 
**status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsAsyncStatus**](SponsoredProductsAsyncStatus.md) |  | [optional] 
**error_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaignErrorDetail[]**](SponsoredProductsCopyCampaignErrorDetail.md) | Errors that could occur during async process (up to 10) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

