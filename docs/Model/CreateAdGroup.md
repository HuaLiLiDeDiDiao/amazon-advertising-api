# CreateAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeType**](CreativeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

