# PatchDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**op** | **string** | The JSONPatch operation type. | 
**path** | **string** | A path constructed from the JSON object to be updated. | 
**value** | [**AnyOfPatchDocumentValue**](AnyOfPatchDocumentValue.md) | The value used by the operation specified in the &#x60;op&#x60; field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

