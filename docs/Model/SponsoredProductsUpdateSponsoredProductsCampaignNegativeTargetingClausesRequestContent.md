# SponsoredProductsUpdateSponsoredProductsCampaignNegativeTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsUpdateCampaignNegativeTargetingClause[]**](SponsoredProductsUpdateCampaignNegativeTargetingClause.md) | An array of Campaign Negative TargetingClauses with updated values. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

