# InlineResponse200Recommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_bid** | [**\AmazonAdvertisingApi\Model\SuggestedBid**](SuggestedBid.md) |  | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\TargetingExpressionPredicate**](TargetingExpressionPredicate.md) |  | [optional] 
**code** | **string** | The response code. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

