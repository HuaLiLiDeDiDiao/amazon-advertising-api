# SponsoredProductsUpdateGlobalNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The NegativeTargeting expression. | [optional] 
**target_id** | **string** | The target identifier | 
**name** | **string** | Name for the negative targeting clause | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

