# SponsoredProductsBulkNegativeKeywordOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeKeywordSuccessResponseItem[]**](SponsoredProductsNegativeKeywordSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeKeywordFailureResponseItem[]**](SponsoredProductsNegativeKeywordFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

