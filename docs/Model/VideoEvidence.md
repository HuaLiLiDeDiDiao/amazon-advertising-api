# VideoEvidence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **int** | The start position (in seconds) of the content that violates the specified policy within the video. | [optional] 
**end** | **int** | The end position (in seconds) of the content that violates the specified policy within the video. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

