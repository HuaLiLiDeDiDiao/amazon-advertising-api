# FetchTaxonomyRequestBodyV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_type** | **string** |  | [optional] 
**category_path** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

