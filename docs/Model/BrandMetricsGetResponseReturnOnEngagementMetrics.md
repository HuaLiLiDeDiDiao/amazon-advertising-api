# BrandMetricsGetResponseReturnOnEngagementMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**this_brand** | [**\AmazonAdvertisingApi\Model\BrandMetricsReturnOnEngagementMetrics**](BrandMetricsReturnOnEngagementMetrics.md) |  | [optional] 
**top_performers** | [**\AmazonAdvertisingApi\Model\BrandMetricsReturnOnEngagementMetrics**](BrandMetricsReturnOnEngagementMetrics.md) |  | [optional] 
**peer_median** | [**\AmazonAdvertisingApi\Model\BrandMetricsReturnOnEngagementMetrics**](BrandMetricsReturnOnEngagementMetrics.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

