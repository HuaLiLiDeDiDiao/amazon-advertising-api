# OptimizationRulesAPIAmazonAdvertisingApiRuleAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action_type** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiActionType**](OptimizationRulesAPIAmazonAdvertisingApiActionType.md) |  | [optional] 
**action_details** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiActionDetails**](OptimizationRulesAPIAmazonAdvertisingApiActionDetails.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

