# CreateNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **float** | The identifier of the campaign to which the negative keyword is associated. | [optional] 
**ad_group_id** | **float** | The identifier of the ad group to which the negative keyword is associated. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**keyword_text** | **string** | The text of the expression to match against a search query. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](NegativeMatchType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

