# SponsoredProductsCopySponsoredProductsCampaignsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**copy_campaigns_items** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaign[]**](SponsoredProductsCopyCampaign.md) | An array of campaigns. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

