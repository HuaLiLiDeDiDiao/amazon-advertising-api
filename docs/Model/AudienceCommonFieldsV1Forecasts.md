# AudienceCommonFieldsV1Forecasts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inventory_forecasts** | [**\AmazonAdvertisingApi\Model\AudienceCommonFieldsV1ForecastsInventoryForecasts**](AudienceCommonFieldsV1ForecastsInventoryForecasts.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

