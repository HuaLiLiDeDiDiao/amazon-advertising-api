# SDBidRecommendationV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**range_lower** | **float** | The lowest recommended bid to use to win an ad placement for this target. | 
**range_upper** | **float** | The highest recommended bid to use to win an ad placement for this target. | 
**recommended** | **float** | The recommended bid to use to win an ad placement for this target. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

