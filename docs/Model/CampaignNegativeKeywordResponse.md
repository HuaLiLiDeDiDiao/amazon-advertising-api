# CampaignNegativeKeywordResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **float** | The identifier of the campaign negative keyword. | [optional] 
**code** | **string** | An enumerated success or error code for machine use. | [optional] 
**details** | **string** | A human-readable description of the code. | [optional] 
**description** | **string** | A human-readable description of the code. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

