# SBKeywordRecommendationRequestUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**goal** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationGoal**](SBKeywordRecommendationGoal.md) |  | [optional] 
**creative_type** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationCreativeType**](SBKeywordRecommendationCreativeType.md) |  | [optional] 
**max_num_suggestions** | **int** | Maximum number of suggestions to return. Max value is 1000. If not provided, default to 100. | [optional] 
**creative_asins** | **string[]** |  | [optional] 
**locale** | **string** | Optional locale to request keyword suggestion translations. For example, to request Simplified Chinese translations in US, provide locale “zh_CN”. Response will include both keyword suggestions and their translations. Supported locales include: Simplified Chinese (locale: “zh_CN”) for US, UK and CA. English (locale: “en_GB”) for DE, FR, IT and ES. | [optional] 
**url** | **string** | The URL of the Stores page, or, Vendors may also specify the URL of a custom landing page. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

