# SBResolvedExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\ProductPredicateType**](ProductPredicateType.md) |  | [optional] 
**value** | **string** | The human-readable target text. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

