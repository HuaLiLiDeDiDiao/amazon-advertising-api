# SponsoredProductsCreateSponsoredProductsAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateAdGroup[]**](SponsoredProductsCreateAdGroup.md) | An array of adGroups. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

