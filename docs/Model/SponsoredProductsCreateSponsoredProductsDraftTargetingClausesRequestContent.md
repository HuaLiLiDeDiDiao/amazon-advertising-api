# SponsoredProductsCreateSponsoredProductsDraftTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftTargetingClause[]**](SponsoredProductsCreateDraftTargetingClause.md) | An array of draftTargetingClauses. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

