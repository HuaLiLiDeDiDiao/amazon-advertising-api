# SDTargetingRecommendationsV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categories** | [**\AmazonAdvertisingApi\Model\SDCategoryRecommendation[]**](SDCategoryRecommendation.md) | List of recommended category targets | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

