# SponsoredProductsGlobalAdGroupFailureResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | the index of the adGroup in the array from the request body | 
**errors** | [**\AmazonAdvertisingApi\Model\SponsoredProductsAdGroupMutationError[]**](SponsoredProductsAdGroupMutationError.md) | A list of validation errors | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

