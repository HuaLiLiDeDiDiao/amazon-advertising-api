# InlineResponse20018ViolatingTextPosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **int** | Zero-based index into the text in &#x60;reviewedText&#x60; where the text specified in &#x60;violatingText&#x60; starts. | [optional] 
**end** | **int** | Zero-based index into the text in &#x60;reviewedText&#x60; where the text specified in &#x60;violatingText&#x60; ends. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

