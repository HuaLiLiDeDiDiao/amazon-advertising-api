# ResolvedLocationExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\LocationPredicate**](LocationPredicate.md) |  | [optional] 
**value** | **string** | The human-readable location name. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

