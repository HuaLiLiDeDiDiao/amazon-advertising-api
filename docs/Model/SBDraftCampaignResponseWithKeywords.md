# SBDraftCampaignResponseWithKeywords

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) |  | [optional] 
**negative_keyword_responses** | [**\AmazonAdvertisingApi\Model\SBKeywordResponse[]**](SBKeywordResponse.md) |  | [optional] 
**details** | **string** | A human-readable description of the &#x27;code&#x27; field value. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

