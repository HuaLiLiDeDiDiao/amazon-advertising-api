# TargetingClauseEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id** | **float** |  | [optional] 
**ad_group_id** | **float** |  | [optional] 
**campaign_id** | **float** |  | [optional] 
**state** | **string** |  | [optional] 
**expression_type** | **string** |  | [optional] 
**bid** | **float** | If a value for &#x60;bid&#x60; is specified, it overrides the current adGroup bid. When using vcpm costType. $1 is the minimum bid for vCPM. Note that this field is ignored for negative targeting clauses. | [optional] 
**expression** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 
**serving_status** | **string** | The status of the target. | [optional] 
**creation_date** | **int** | Epoch date the target was created. | [optional] 
**last_updated_date** | **int** | Epoch date of the last update to any property associated with the target. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

