# CreateExtendedProductCollectionCreative

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_logo_crop** | [**\AmazonAdvertisingApi\Model\BrandLogoCrop**](BrandLogoCrop.md) |  | [optional] 
**asins** | **string[]** |  | [optional] 
**brand_name** | **string** |  | [optional] 
**custom_images** | [**\AmazonAdvertisingApi\Model\CustomImage[]**](CustomImage.md) |  | [optional] 
**consent_to_translate** | **bool** | If set to true and the headline and/or video are not in the marketplace&#x27;s default language, Amazon will attempt to translate them to the marketplace&#x27;s default language. If Amazon is unable to translate them, the ad will be rejected by moderation. We only support translating headlines and videos from English to German, French, Italian, Spanish, Japanese, and Dutch. See developer notes for more information. | [optional] 
**brand_logo_asset_id** | **string** |  | [optional] 
**headline** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

