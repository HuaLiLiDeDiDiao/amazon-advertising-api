# DeleteSPCampaignOptimizationRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_optimization_id** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationId**](CampaignOptimizationId.md) |  | [optional] 
**code** | **string** | An enumerated success or error code for machine use. | [optional] 
**details** | **string** | A human-readable description of the error, if unsuccessful | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

