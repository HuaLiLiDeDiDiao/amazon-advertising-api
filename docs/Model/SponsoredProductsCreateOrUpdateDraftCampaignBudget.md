# SponsoredProductsCreateOrUpdateDraftCampaignBudget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDraftCampaignBudgetType**](SponsoredProductsCreateOrUpdateDraftCampaignBudgetType.md) |  | 
**budget** | **double** | Monetary value | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

