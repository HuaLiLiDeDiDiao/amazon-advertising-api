# GetBudgetRecommendationsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\BudgetRecommendation[]**](BudgetRecommendation.md) | List of successful budget recommendations for campaigns. | 
**error** | [**\AmazonAdvertisingApi\Model\BudgetRecommendationError[]**](BudgetRecommendationError.md) | List of errors that occurred when generating budget recommendations. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

