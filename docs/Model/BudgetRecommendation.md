# BudgetRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of a campaign. | 
**suggested_budget** | **double** | Recommended budget for the campaign. | 
**index** | **float** | Correlate the recommendation to the campaign index in the request. Zero-based. | 
**seven_days_missed_opportunities** | [**\AmazonAdvertisingApi\Model\SevenDaysMissedOpportunities**](SevenDaysMissedOpportunities.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

