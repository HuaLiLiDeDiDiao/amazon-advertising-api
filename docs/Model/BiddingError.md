# BiddingError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **string** | Exact error reason. | 
**cause** | [**\AmazonAdvertisingApi\Model\ErrorCause**](ErrorCause.md) |  | 
**upper_limit** | **string** |  | [optional] 
**lower_limit** | **string** |  | [optional] 
**message** | **string** | Human readable error message. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

