# CreateNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | 
**expression** | [**\AmazonAdvertisingApi\Model\NegativeTargetingExpression[]**](NegativeTargetingExpression.md) | The expression to negatively match against. * Only one brand may be specified per targeting expression. * Only one asin may be specified per targeting expression. * To exclude a brand from a targeting expression, you must create a negative targeting expression in the same ad group as the positive targeting expression. | 
**expression_type** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

