# CustomImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_id** | **string** |  | [optional] 
**crop** | [**\AmazonAdvertisingApi\Model\CustomImageCrop**](CustomImageCrop.md) |  | [optional] 
**url** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

