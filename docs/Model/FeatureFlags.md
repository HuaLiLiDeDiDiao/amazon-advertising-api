# FeatureFlags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_opted_out_for_average_daily_budget_increase** | **bool** | Denotes the opt in/out decision for AverageDailyBudgetIncrease feature. If the entity spends less than your daily budget, the unspent amount can be used to increase your daily budget to 100% (if opted in) or 25% (if opted out) in other days of calendar month. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

