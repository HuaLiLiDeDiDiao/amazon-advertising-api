# TargetingExpressionExpressions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string[]** | The type. | [optional] 
**value** | **string** | The expression value | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

