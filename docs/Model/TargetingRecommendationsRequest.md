# TargetingRecommendationsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tactic** | [**\AmazonAdvertisingApi\Model\Tactic**](Tactic.md) |  | 
**products** | [**\AmazonAdvertisingApi\Model\GoalProduct[]**](GoalProduct.md) | A list of products for which to get targeting recommendations | 
**type_filter** | [**\AmazonAdvertisingApi\Model\RecommendationType[]**](RecommendationType.md) | A filter to indicate which types of recommendations to request. T00030 only allow \&quot;CATEGORY\&quot;. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

