# KeywordBidInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_bid** | [**\AmazonAdvertisingApi\Model\BidSuggestion**](BidSuggestion.md) |  | [optional] 
**match_type** | **string** | Keyword match type. The default value will be BROAD. | [optional] 
**rank** | **float** | The keyword target rank | [optional] 
**bid** | **float** | The bid value for the keyword, in minor currency units (example: cents). The default value will be the suggested bid. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

