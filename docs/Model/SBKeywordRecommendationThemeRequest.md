# SBKeywordRecommendationThemeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**themes** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationThemes[]**](SBKeywordRecommendationThemes.md) |  | [optional] 
**max_num_suggestions** | **int** | Maximum number of suggestions to return for each theme. Max value is 1000. If not provided, default to 100. | [optional] 
**landing_pages** | [**\AmazonAdvertisingApi\Model\SBKeywordRecommendationLandingPage[]**](SBKeywordRecommendationLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

