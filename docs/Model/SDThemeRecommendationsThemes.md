# SDThemeRecommendationsThemes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\AmazonAdvertisingApi\Model\OneOfSDThemeRecommendationsThemesProductsItems[]**](.md) | A list of contextual targeting theme recommendations. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

