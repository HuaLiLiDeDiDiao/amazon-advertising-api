# DSPAudienceFieldsV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fees** | [**\AmazonAdvertisingApi\Model\DSPAudienceFieldsV1Fees[]**](DSPAudienceFieldsV1Fees.md) | Fees that will apply to this segment. Not all segments have fees. Fees may differ depending on the supply type the segment is attached to. In this case, multiple fee objects will be present. | [optional] 
**provider_id** | **string** | The Data Management Platform provider identifier. Only applicable to Third party audience segments. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

