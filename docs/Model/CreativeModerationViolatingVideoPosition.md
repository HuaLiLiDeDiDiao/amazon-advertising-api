# CreativeModerationViolatingVideoPosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **int** | Time at which policy violation within video asset starts. | [optional] 
**end** | **int** | Time at which policy violation within the video asset ends. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

