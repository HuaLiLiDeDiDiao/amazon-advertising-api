# AdGroupKeywordTargetRankRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of the campaign | 
**recommendation_type** | **string** | The recommendationType to retrieve recommended keyword targets for an existing ad group. | 
**ad_group_id** | **string** | The identifier of the ad group | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

