# Clicks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lower** | **int** | lower bound. | [optional] 
**upper** | **int** | upper bound. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

