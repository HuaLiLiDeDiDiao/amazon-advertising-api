# ImageTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_url_expiration** | **double** | The timestamp after which the imageUrl will be invalid. The number represents Unix epoch seconds with optional millisecond precision. | [optional] 
**image_results** | [**\AmazonAdvertisingApi\Model\ImageResult[]**](ImageResult.md) |  | [optional] 
**message** | **string** | Image task status details. | [optional] 
**task_id** | **string** |  | [optional] 
**status** | **string** | Image task status. Valid values are PENDING, COMPLETED and FAILED | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

