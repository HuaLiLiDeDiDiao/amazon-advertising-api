# LandingPageInvalidArgumentExceptionResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\LandingPageInvalidArgumentErrorCode**](LandingPageInvalidArgumentErrorCode.md) |  | 
**details** | **string** | A human-readable description of the code field. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

