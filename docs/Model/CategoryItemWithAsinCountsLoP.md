# CategoryItemWithAsinCountsLoP

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_path** | **string** | The path of the category, which contains the current category and all parent categories | [optional] 
**name** | **string** | The name of the category | [optional] 
**translated_category_path** | **string** | The translated path of the category, which contains the current category and all parent categories. | [optional] 
**asin_counts** | [**\AmazonAdvertisingApi\Model\IntegerRange**](IntegerRange.md) |  | [optional] 
**parent_category_id** | **string** | The category id of the parent node | [optional] 
**id** | **string** | The category id of the current node | [optional] 
**translated_name** | **string** | The translated name of the category. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

