# DspAudienceSuccessItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idempotency_key** | **string** | unique request token for this Audience Edit request. | 
**index** | **int** | index of the DspAudienceEditRequestItem from the request. e.g. 1st item in the request will correspond to index 0 in the response. | 
**audience_id** | **string** | The audience identifier of the audience to be edited. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

