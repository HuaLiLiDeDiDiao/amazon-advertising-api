# InlineResponse20010

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**update_target_success_results** | [**\AmazonAdvertisingApi\Model\InlineResponse20010UpdateTargetSuccessResults[]**](InlineResponse20010UpdateTargetSuccessResults.md) | Lists the successfully updated targets. Note that targets in the response are correlated to targets in the request using the &#x60;targetRequestIndex&#x60; field. For example, if &#x60;targetRequestIndex&#x60; is set to &#x60;2&#x60;, the values correlate to the third target object in the request. | [optional] 
**update_target_error_results** | [**\AmazonAdvertisingApi\Model\AllOfinlineResponse20010UpdateTargetErrorResultsItems[]**](.md) | Lists errors that occured during target update. Note that errors are correlated to target update requests by the &#x60;targetRequestIndex&#x60; field. This field corresponds to the order of the target in the request. For example, if &#x60;targetRequestIndex&#x60; is set to &#x60;2&#x60;, the values correlate to the third target object in the request array. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

