# SponsoredProductsBulkGlobalAdGroupOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalAdGroupSuccessResponseItem[]**](SponsoredProductsGlobalAdGroupSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalAdGroupFailureResponseItem[]**](SponsoredProductsGlobalAdGroupFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

