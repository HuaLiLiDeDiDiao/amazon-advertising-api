# SBCreateNegativeTargetingClauseRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **int** | The identifier of an existing ad group. The newly created target is associated to this ad group. | [optional] 
**campaign_id** | **int** | The identifier of an existing campaign. The newly created target is associated to this campaign. | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\SBNegativeTargetingExpressions**](SBNegativeTargetingExpressions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

