# SponsoredProductsCreateTargetError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | **string** | The type of the error. | [optional] 
**error_value** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetErrorSelector**](SponsoredProductsCreateTargetErrorSelector.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

