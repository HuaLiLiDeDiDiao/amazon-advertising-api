# ViolatingAsinContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_asin_evidences** | [**\AmazonAdvertisingApi\Model\ViolatingAsinEvidence[]**](ViolatingAsinEvidence.md) |  | [optional] 
**moderated_component** | **string** | Moderation component which marked the policy violation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

