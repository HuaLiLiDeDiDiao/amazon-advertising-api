# SponsoredProductsCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **string** | The identifier of an existing portfolio to which the campaign is associated. | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**campaign_id** | **string** | The identifier of the campaign. | 
**name** | **string** | The name of the campaign. | 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityState**](SponsoredProductsEntityState.md) |  | 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDynamicBidding**](SponsoredProductsDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBudget**](SponsoredProductsBudget.md) |  | 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignExtendedData**](SponsoredProductsCampaignExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

