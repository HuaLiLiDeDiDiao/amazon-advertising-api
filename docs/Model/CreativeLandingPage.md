# CreativeLandingPage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | The list of asins on the landingPage If type is PRODUCT_LIST. | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\CreativeLandingPageType**](CreativeLandingPageType.md) |  | [optional] 
**value** | **string** | The url of the landingPage. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

