# SponsoredProductsGetTargetPromotionGroupsRecommendationsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **int** | Total number of records available | 
**next_token** | **string** | Token for fetching the next page | [optional] 
**targets** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTarget[]**](SponsoredProductsTarget.md) | List of optimized targets for the request, as recommended by Amazon heuristics | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

