# Portfolio

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **float** | The portfolio identifier. | [optional] 
**name** | **string** | The portfolio name. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\PortfolioBudget**](PortfolioBudget.md) |  | [optional] 
**in_budget** | **bool** | Indicates the current budget status of the portfolio. Set to &#x60;true&#x60; if the portfolio is in budget, set to &#x60;false&#x60; if the portfolio is out of budget. | [optional] 
**state** | **string** | The current state of the portfolio. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

