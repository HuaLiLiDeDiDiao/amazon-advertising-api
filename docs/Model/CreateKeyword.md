# CreateKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **float** | The identifer of the campaign to which the keyword is associated. | [optional] 
**ad_group_id** | **float** | The identifier of the ad group to which this keyword is associated. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**keyword_text** | **string** | The keyword text. | [optional] 
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser. | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. For example, if the advertiser’s preferred language is Simplified Chinese, set the locale to &#x60;zh_CN&#x60;. Supported locales include: Simplified Chinese (locale: zh_CN) for US, UK and CA. English (locale: en_GB) for DE, FR, IT and ES. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**bid** | **float** | Bid associated with this keyword. Applicable to biddable match types only. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

