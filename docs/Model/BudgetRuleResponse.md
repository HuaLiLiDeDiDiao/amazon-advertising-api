# BudgetRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated success or error code for machine use. | [optional] 
**details** | **string** | A human-readable description of the error, if unsuccessful | [optional] 
**rule_id** | **string** | The rule identifier. | [optional] 
**associated_campaign_ids** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

