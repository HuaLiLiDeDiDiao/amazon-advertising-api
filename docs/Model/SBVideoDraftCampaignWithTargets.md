# SBVideoDraftCampaignWithTargets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBDraftTargetingClause[]**](SBDraftTargetingClause.md) | An array of targets associated with the draft campaign. | [optional] 
**negative_targets** | [**\AmazonAdvertisingApi\Model\SBDraftNegativeTargetingClause[]**](SBDraftNegativeTargetingClause.md) | An array of negative keywords associated with the draft campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

