# SponsoredProductsDraftProductAdSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | the DraftProductAd ID | [optional] 
**index** | **int** | The index in the original list from the request. | 
**product_ad** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftProductAd**](SponsoredProductsDraftProductAd.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

