# DspAudienceEditResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\DspAudienceSuccessItem[]**](DspAudienceSuccessItem.md) |  | 
**failed** | [**\AmazonAdvertisingApi\Model\DspAudienceErrorItem[]**](DspAudienceErrorItem.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

