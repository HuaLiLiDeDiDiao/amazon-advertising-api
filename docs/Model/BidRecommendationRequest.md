# BidRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The identifier of the ad group that is associated with the targeting expression used to generate the bid recommendation. | [optional] 
**expressions** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

