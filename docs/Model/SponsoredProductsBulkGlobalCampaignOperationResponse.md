# SponsoredProductsBulkGlobalCampaignOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalCampaignMutationSuccessResponseItem[]**](SponsoredProductsGlobalCampaignMutationSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalCampaignMutationFailureResponseItem[]**](SponsoredProductsGlobalCampaignMutationFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

