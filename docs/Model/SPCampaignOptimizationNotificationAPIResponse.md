# SPCampaignOptimizationNotificationAPIResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_optimization_recommendations_error** | [**\AmazonAdvertisingApi\Model\RuleNotificationError[]**](RuleNotificationError.md) | List of errors that occured when generating campaign optimization notifications. | [optional] 
**campaign_optimization_notifications** | [**\AmazonAdvertisingApi\Model\RuleNotification[]**](RuleNotification.md) | List of successful campaign optimization notifications for campaigns. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

