# ThemesListBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_id_filter** | [**\AmazonAdvertisingApi\Model\SbthemeslistThemeIdFilter**](SbthemeslistThemeIdFilter.md) |  | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\SbthemeslistAdGroupIdFilter**](SbthemeslistAdGroupIdFilter.md) |  | [optional] 
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\SbthemeslistCampaignIdFilter**](SbthemeslistCampaignIdFilter.md) |  | [optional] 
**state_filter** | [**\AmazonAdvertisingApi\Model\SbthemeslistStateFilter**](SbthemeslistStateFilter.md) |  | [optional] 
**theme_type_filter** | [**\AmazonAdvertisingApi\Model\SbthemeslistThemeTypeFilter**](SbthemeslistThemeTypeFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

