# CreativeRecommendationsEligibilityRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | [**\AmazonAdvertisingApi\Model\Source**](Source.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

