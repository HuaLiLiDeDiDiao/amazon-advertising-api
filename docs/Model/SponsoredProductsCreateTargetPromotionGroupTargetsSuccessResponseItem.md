# SponsoredProductsCreateTargetPromotionGroupTargetsSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTarget**](SponsoredProductsCreateTarget.md) |  | [optional] 
**target** | **string** | The target that was requested to be created. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

