# ViolatingVideoContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_video_evidences** | [**\AmazonAdvertisingApi\Model\ViolatingVideoEvidence[]**](ViolatingVideoEvidence.md) |  | [optional] 
**moderated_component** | **string** | Moderation component which marked the policy violation. | [optional] 
**reviewed_video_url** | **string** | URL of the video which has the ad policy violation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

