# AllOfBudgetErrorBudgetError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_type** | **string** |  | [optional] 
**min_budget** | **string** |  | [optional] 
**max_budget** | **string** |  | [optional] 
**precision** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

