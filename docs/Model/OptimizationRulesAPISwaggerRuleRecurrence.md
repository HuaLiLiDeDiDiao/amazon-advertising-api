# OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiDuration**](OptimizationRulesAPIAmazonAdvertisingApiDuration.md) |  | 
**times_of_day** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrenceTimesOfDay[]**](OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrenceTimesOfDay.md) | List of times of the day. | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrenceType**](OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrenceType.md) |  | 
**days_of_week** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiDayOfTheWeek[]**](OptimizationRulesAPIAmazonAdvertisingApiDayOfTheWeek.md) | A list of days of the week. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

