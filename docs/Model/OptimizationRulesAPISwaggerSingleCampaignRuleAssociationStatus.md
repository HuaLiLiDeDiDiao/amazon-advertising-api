# OptimizationRulesAPIAmazonAdvertisingApiSingleCampaignRuleAssociationStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule_state** | **string** |  | [optional] 
**campaign_id** | **string** | Sp campaign identifier. | [optional] 
**optimization_rule_id** | **string** | The rule identifier. | [optional] 
**notification_string** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

