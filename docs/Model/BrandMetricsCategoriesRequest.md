# BrandMetricsCategoriesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand_name** | **string** | Name of the brand. | [optional] 
**next_token** | **string** | Token  to fetch additional results (if any). Subsequent calls must be made with same  parameters as in the previous requests. | [optional] 
**brand_id** | **string** | Brand Id from BrandAid | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

