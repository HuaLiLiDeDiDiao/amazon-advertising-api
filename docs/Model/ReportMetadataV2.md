# ReportMetadataV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The identifier of the report. | [optional] 
**format** | **string** | The data format of the report. | [optional] 
**status_details** | **string** | A human-readable description of the current status. | [optional] 
**location** | **string** | The URI address of the report. | [optional] 
**expiration** | **int** | The expiration time of the URI in the location property in milliseconds. The expiration time is the interval between the time the response was generated and the time the URI expires. | [optional] 
**type** | **string** | The type of report. | [optional] 
**status** | **string** | The build status of the report. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

