# InlineResponse2008TargetsBidsRecommendationSuccessResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recommendation_id** | [****](.md) | The identifier of the target bid recommendation. | [optional] 
**recommended_bid** | [**\AmazonAdvertisingApi\Model\RecommendedBid**](RecommendedBid.md) |  | [optional] 
**targets** | [**\AmazonAdvertisingApi\Model\SBTargetingExpressions**](SBTargetingExpressions.md) |  | [optional] 
**targets_index** | [**\AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex**](SBBidRecommendationKeywordIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

