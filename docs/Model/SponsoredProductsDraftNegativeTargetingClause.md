# SponsoredProductsDraftNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingExpressionPredicate[]**](SponsoredProductsNegativeTargetingExpressionPredicate.md) | The DraftNegativeTargeting expression. | 
**target_id** | **string** | The target identifier | 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeTargetingExpressionPredicate[]**](SponsoredProductsNegativeTargetingExpressionPredicate.md) | The resolved DraftNegativeTargeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftNegativeTargetingClauseExtendedData**](SponsoredProductsDraftNegativeTargetingClauseExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

