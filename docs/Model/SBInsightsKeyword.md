# SBInsightsKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**match_type** | [**\AmazonAdvertisingApi\Model\SBInsightsMatchType**](SBInsightsMatchType.md) |  | 
**bid** | **double** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. For more information, see [supported features](https://advertising.amazon.com/API/docs/v2/guides/supported_features). | 
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

