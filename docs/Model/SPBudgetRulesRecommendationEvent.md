# SPBudgetRulesRecommendationEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **string** | The event identifier. | [optional] 
**end_date** | **string** | The end date in YYYYMMDD format. | [optional] 
**suggested_budget_increase_percent** | **float** | The suggested budget increase expressed as a percent. | [optional] 
**event_name** | **string** | The event name. | [optional] 
**start_date** | **string** | The start date in YYYYMMDD format. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

