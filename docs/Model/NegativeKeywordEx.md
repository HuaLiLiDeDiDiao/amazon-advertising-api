# NegativeKeywordEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **float** | The identifier of the keyword. | [optional] 
**campaign_id** | **float** | The identifer of the campaign to which the keyword is associated. | [optional] 
**ad_group_id** | **float** | The identifier of the ad group to which this keyword is associated. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**keyword_text** | **string** | The text of the expression to match against a search query. | [optional] 
**match_type** | [**\AmazonAdvertisingApi\Model\NegativeMatchType**](NegativeMatchType.md) |  | [optional] 
**creation_date** | **float** | Creation date in epoch time. | [optional] 
**last_updated_date** | **float** | Date of last update in epoch time. | [optional] 
**serving_status** | **string** | The serving status of the keyword. See the **computed status** section of the [developer notes](https://advertising.amazon.com/API/docs/en-us/reference/concepts/developer-notes) for definitions. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

