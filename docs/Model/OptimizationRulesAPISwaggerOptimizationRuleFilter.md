# OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule_category** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter**](OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter.md) |  | [optional] 
**rule_sub_category** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter**](OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter.md) |  | [optional] 
**optimization_rule_id** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter**](OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

