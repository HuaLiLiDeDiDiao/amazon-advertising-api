# FeeIdentifiers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_code** | [**\AmazonAdvertisingApi\Model\CountryCode**](CountryCode.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

