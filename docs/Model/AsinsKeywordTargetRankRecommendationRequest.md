# AsinsKeywordTargetRankRecommendationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | An array list of Asins | 
**recommendation_type** | **string** | The recommendationType to retrieve recommended keyword targets for a list of ASINs. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

