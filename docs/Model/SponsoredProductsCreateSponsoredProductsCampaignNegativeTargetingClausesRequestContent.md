# SponsoredProductsCreateSponsoredProductsCampaignNegativeTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateCampaignNegativeTargetingClause[]**](SponsoredProductsCreateCampaignNegativeTargetingClause.md) | An array of Campaign Negative TargetingClauses. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

