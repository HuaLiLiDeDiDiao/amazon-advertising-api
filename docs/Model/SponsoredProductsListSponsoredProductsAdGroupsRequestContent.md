# SponsoredProductsListSponsoredProductsAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsReducedObjectIdFilter**](SponsoredProductsReducedObjectIdFilter.md) |  | [optional] 
**state_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityStateFilter**](SponsoredProductsEntityStateFilter.md) |  | [optional] 
**max_results** | **int** | Number of records to include in the paginated response. Defaults to max page size for given API | [optional] 
**next_token** | **string** | token value allowing to navigate to the next response page | [optional] 
**ad_group_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | [optional] 
**include_extended_data_fields** | **bool** | Whether to get entity with extended data fields such as creationDate, lastUpdateDate, servingStatus | [optional] 
**name_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNameFilter**](SponsoredProductsNameFilter.md) |  | [optional] 
**campaign_targeting_type_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

