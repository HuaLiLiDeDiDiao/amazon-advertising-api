# BidAdjustment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_adjustment_predicate** | **string** | The enum for placement group name | [optional] 
**bid_adjustment_percent** | **float** | Bid adjustment for placement group. Value is a percentage to two decimal places. Example: If this is set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

