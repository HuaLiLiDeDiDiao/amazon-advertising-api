# SponsoredProductsGlobalNegativeTargetingClauseExtendedData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serving_status** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingClauseServingStatus**](SponsoredProductsGlobalTargetingClauseServingStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

