# CreateTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | 
**expression_type** | **string** | Tactic T00020 ad groups only allow manual targeting. | 
**expression** | [**\AmazonAdvertisingApi\Model\CreateTargetingExpression**](CreateTargetingExpression.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

