# DeleteSponsoredBrandsAdsBetaRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id_filter** | [**\AmazonAdvertisingApi\Model\ObjectIdFilter**](ObjectIdFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

