# SponsoredProductsBulkGlobalNegativeKeywordOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalNegativeKeywordSuccessResponseItem[]**](SponsoredProductsGlobalNegativeKeywordSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalNegativeKeywordFailureResponseItem[]**](SponsoredProductsGlobalNegativeKeywordFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

