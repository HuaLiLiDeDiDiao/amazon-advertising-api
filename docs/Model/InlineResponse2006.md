# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_id** | **string** | The identifier of an asset associated with a store. | [optional] 
**url** | **string** | The address where the asset is stored. | [optional] 
**media_type** | [**\AmazonAdvertisingApi\Model\MediaType**](MediaType.md) |  | [optional] 
**name** | **string** | The name of the asset. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

