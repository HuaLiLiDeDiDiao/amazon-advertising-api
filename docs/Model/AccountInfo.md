# AccountInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_string_id** | **string** | The identifier of the marketplace to which the account is associated. | [optional] 
**id** | **string** | Identifier for sellers and vendors. Note that this value is not unique and may be the same across marketplace. | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\AccountType**](AccountType.md) |  | [optional] 
**name** | **string** | Account name. | [optional] 
**sub_type** | **string** | The account subtype. | [optional] 
**valid_payment_method** | **bool** | Only present for Vendors, this returns whether the Advertiser has set up a valid payment method or not. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

