# PayerTaxRegistrationInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax_id** | **string** | Tax registration with government (Ex: VAT ID, GST ID) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

