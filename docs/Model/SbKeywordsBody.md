# SbKeywordsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **int** | The identifier of the keyword. | 
**ad_group_id** | **int** | The identifier of an existing ad group to which the keyword is associated. | 
**campaign_id** | **int** | The identifier of an existing campaign to which the keyword is associated. | 
**state** | [**\AmazonAdvertisingApi\Model\SBKeywordState**](SBKeywordState.md) |  | [optional] 
**bid** | **float** | The bid associated with the keyword. Note that this value must be less than the budget associated with the Advertiser account. For more information, see the **Keyword bid constraints by marketplace** section of the [supported features](https://advertising.amazon.com/API/docs/v2/guides/supported_features) article. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

