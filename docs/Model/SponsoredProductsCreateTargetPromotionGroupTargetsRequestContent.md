# SponsoredProductsCreateTargetPromotionGroupTargetsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_promotion_group_id** | **string** | The id of the target promotion group where the targets are being added. | 
**targets** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateTargetRequest[]**](SponsoredProductsCreateTargetRequest.md) | List of targets to be added to the target promotion group. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

