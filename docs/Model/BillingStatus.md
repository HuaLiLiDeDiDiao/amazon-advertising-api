# BillingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_status_code** | [**\AmazonAdvertisingApi\Model\BillingStatusCode**](BillingStatusCode.md) |  | 
**message** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

