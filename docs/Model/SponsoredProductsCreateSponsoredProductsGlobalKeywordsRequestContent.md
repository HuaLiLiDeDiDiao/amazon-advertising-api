# SponsoredProductsCreateSponsoredProductsGlobalKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateGlobalKeyword[]**](SponsoredProductsCreateGlobalKeyword.md) | An array of keywords. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

