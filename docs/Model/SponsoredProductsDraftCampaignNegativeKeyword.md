# SponsoredProductsDraftCampaignNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword. | 
**campaign_id** | **string** | The identifier of the draft to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNegativeMatchType**](SponsoredProductsNegativeMatchType.md) |  | 
**keyword_text** | **string** | The keyword text. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignNegativeKeywordExtendedData**](SponsoredProductsDraftCampaignNegativeKeywordExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

