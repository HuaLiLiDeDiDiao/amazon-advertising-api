# SponsoredProductsDraftCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portfolio_id** | **string** | The identifier of an existing portfolio to which the draft is associated. | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**campaign_id** | **string** | The identifier of the draft. | 
**name** | **string** | The name of the draft. | 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignDynamicBidding**](SponsoredProductsDraftCampaignDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignBudget**](SponsoredProductsDraftCampaignBudget.md) |  | 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignExtendedData**](SponsoredProductsDraftCampaignExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

