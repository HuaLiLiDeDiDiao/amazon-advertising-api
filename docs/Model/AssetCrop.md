# AssetCrop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**top** | **float** | The highest pixel from which to begin cropping | [optional] 
**left** | **float** | The leftmost pixel from which to begin cropping | [optional] 
**width** | **float** | The number of pixels to crop rightwards from the value specified as left | [optional] 
**height** | **float** | The number of pixels to crop down from the value specified as top | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

