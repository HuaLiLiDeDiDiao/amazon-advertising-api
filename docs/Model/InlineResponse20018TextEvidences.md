# InlineResponse20018TextEvidences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_text** | **string** | The specific text determined to violate the specified policy in &#x60;reviewedText&#x60;. | [optional] 
**violating_text_position** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ViolatingTextPosition**](InlineResponse20018ViolatingTextPosition.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

