# SBForecastingTheme

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme_type** | **string** | The theme target type. Valid value: KEYWORDS_RELATED_TO_YOUR_BRAND, KEYWORDS_RELATED_TO_YOUR_LANDING_PAGES.   KEYWORDS_RELATED_TO_YOUR_BRAND - keywords related to brands.   KEYWORDS_RELATED_TO_YOUR_LANDING_PAGES - keywords related to your landing pages. | [optional] 
**bid** | **float** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

