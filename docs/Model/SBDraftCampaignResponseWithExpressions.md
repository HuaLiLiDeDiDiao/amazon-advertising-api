# SBDraftCampaignResponseWithExpressions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targeting_clause_responses** | [**\AmazonAdvertisingApi\Model\SBExpressionResponse[]**](SBExpressionResponse.md) |  | [optional] 
**negative_targeting_clause_responses** | [**\AmazonAdvertisingApi\Model\SBExpressionResponse[]**](SBExpressionResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

