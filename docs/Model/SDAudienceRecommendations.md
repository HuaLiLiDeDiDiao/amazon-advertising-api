# SDAudienceRecommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audiences** | [**\AmazonAdvertisingApi\Model\SDAudienceCategoryRecommendations[]**](SDAudienceCategoryRecommendations.md) | List of recommended audience targets, broken down by audience category | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

