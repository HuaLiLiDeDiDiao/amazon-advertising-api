# SBTargetingGenre

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**genre_refinement_id** | **string** | Id of Genre. Use /sb/targets/categories/{categoryRefinementId}/refinements to retrieve Genre Refinement IDs. | 
**name** | **string** | Name of Genre. | [optional] 
**translated_name** | **string** | Translated name of Genre based off locale sent in request. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

