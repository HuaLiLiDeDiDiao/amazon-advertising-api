# SponsoredProductsBulkDraftKeywordOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftKeywordSuccessResponseItem[]**](SponsoredProductsDraftKeywordSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftKeywordFailureResponseItem[]**](SponsoredProductsDraftKeywordFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

