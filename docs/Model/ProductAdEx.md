# ProductAdEx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **float** | The product ad identifier. | [optional] 
**campaign_id** | **float** | The campaign identifier. | [optional] 
**ad_group_id** | **float** | The ad group identifier. | [optional] 
**sku** | **string** | The SKU associated with the product. Defined for seller accounts only. | [optional] 
**asin** | **string** | The ASIN associated with the product. Defined for vendors only. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**creation_date** | **float** | The epoch date the product ad was created. | [optional] 
**last_updated_date** | **float** | The epoch date the product ad was last updated. | [optional] 
**serving_status** | **string** | The computed status of the product ad. See the [developer notes](https://advertising.amazon.com/API/docs/en-us/reference/concepts/developer-notes) for more information. | [optional] 
**serving_status_details** | [**\AmazonAdvertisingApi\Model\ProductAdExServingStatusDetails[]**](ProductAdExServingStatusDetails.md) | Details of serving status. Only statuses related to moderation according to the ad policy are currently included. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

