# Video

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_id** | **string** | The unique identifier of the video asset. This assetId comes from the Creative Asset Library. | 
**asset_version** | **string** | The identifier of the particular video assetversion. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

