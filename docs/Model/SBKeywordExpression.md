# SBKeywordExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\MatchType**](MatchType.md) |  | [optional] 
**value** | **string** | The text of the keyword. Maximum of 10 words. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

