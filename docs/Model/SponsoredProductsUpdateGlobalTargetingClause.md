# SponsoredProductsUpdateGlobalTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The targeting expression. | [optional] 
**target_id** | **string** | The target identifier | 
**name** | **string** | Name for the targeting clause | [optional] 
**expression_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExpressionTypeWithoutOther**](SponsoredProductsExpressionTypeWithoutOther.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

