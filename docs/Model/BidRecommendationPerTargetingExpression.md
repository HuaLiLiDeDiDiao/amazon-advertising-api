# BidRecommendationPerTargetingExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bid_values** | [**\AmazonAdvertisingApi\Model\BidValue[]**](BidValue.md) |  | 
**targeting_expression** | [**\AmazonAdvertisingApi\Model\TargetingExpression**](TargetingExpression.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

