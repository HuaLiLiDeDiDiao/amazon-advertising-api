# OptimizationRulesAPIAmazonAdvertisingApiEntityFieldFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | **string[]** |  | [optional] 
**filter_type** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiFilterType**](OptimizationRulesAPIAmazonAdvertisingApiFilterType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

