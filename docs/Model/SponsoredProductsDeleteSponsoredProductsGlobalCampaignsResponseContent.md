# SponsoredProductsDeleteSponsoredProductsGlobalCampaignsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkGlobalCampaignOperationResponse**](SponsoredProductsBulkGlobalCampaignOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

