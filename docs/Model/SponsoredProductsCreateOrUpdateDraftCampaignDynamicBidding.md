# SponsoredProductsCreateOrUpdateDraftCampaignDynamicBidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**placement_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsDraftCampaignPlacementBidding[]**](SponsoredProductsDraftCampaignPlacementBidding.md) |  | [optional] 
**strategy** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateBiddingStrategy**](SponsoredProductsCreateOrUpdateBiddingStrategy.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

