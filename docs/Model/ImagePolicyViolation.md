# ImagePolicyViolation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**policy_description** | **string** | A human-readable description of the policy. | [optional] 
**image_evidences** | [**\AmazonAdvertisingApi\Model\ImageEvidence[]**](ImageEvidence.md) | List of evidences for the policy violations detected on the image component. | [optional] 
**name** | **string** | A policy violation code. | [optional] 
**type** | **string** | Type of policy violation. | [optional] 
**policy_link_url** | **string** | Address of the policy documentation. Follow the link to learn more about the specified policy. | [optional] 
**text_evidences** | [**\AmazonAdvertisingApi\Model\TextEvidence[]**](TextEvidence.md) | Policy violation on an image can be detected on the ocr detected text on the image as well. This list of text evidences will have the policy violations detected on the text on top of the image. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

