# CreateManagerAccountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manager_account_name** | **string** | Name of the Manager account. | [optional] 
**manager_account_type** | **string** | Type of the Manager account, which indicates how the Manager account will be used. Use &#x60;Advertiser&#x60; if the Manager account will be used for **your own** products and services, or &#x60;Agency&#x60; if you are managing accounts **on behalf of your clients**. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

