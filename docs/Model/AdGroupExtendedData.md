# AdGroupExtendedData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serving_status** | [**\AmazonAdvertisingApi\Model\AdGroupServingStatus**](AdGroupServingStatus.md) |  | [optional] 
**last_update_date** | **float** | Date of last update in epoch time. | [optional] 
**serving_status_details** | **string[]** | The serving status reasons of the Ad Group. | [optional] 
**creation_date** | **float** | Creation date in epoch time. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

