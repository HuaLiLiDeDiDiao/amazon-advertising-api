# GetAccountInformationResponseInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | List of ASIN belonging to Author account. | [optional] 
**country_code** | **string** | Country code of a test account. | [optional] 
**account_type** | **string** | Type of test account. | [optional] 
**id** | **string** | Identifier for the account. | [optional] 
**status** | **string** | Status  of test account creation request. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

