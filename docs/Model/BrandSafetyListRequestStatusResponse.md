# BrandSafetyListRequestStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_status_list** | [**\AmazonAdvertisingApi\Model\BrandSafetyRequestStatus[]**](BrandSafetyRequestStatus.md) | List of all requests&#x27; status. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

