# SBCommonTargetsTargets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expressions** | [**\AmazonAdvertisingApi\Model\SBExpression[]**](SBExpression.md) | An array of targets associated with the campaign. | [optional] 
**bid** | [**\AmazonAdvertisingApi\Model\Bid**](Bid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

