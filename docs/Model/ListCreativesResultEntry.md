# ListCreativesResultEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The unique ID of a Sponsored Brands ad. | [optional] 
**creation_time** | **double** |  | [optional] 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeType**](CreativeType.md) |  | [optional] 
**creative_version** | **string** | The version identifier that helps you keep track of multiple versions of a submitted (non-draft) Sponsored Brands creative. | [optional] 
**creative_status** | [**\AmazonAdvertisingApi\Model\CreativeStatus**](CreativeStatus.md) |  | [optional] 
**creative_properties** | [**\AmazonAdvertisingApi\Model\CreativeProperties**](CreativeProperties.md) |  | [optional] 
**last_update_time** | **double** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

