# Benchmark

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**benchmark_status** | **string** | Specifies the processing status of the benchmark. Success - If all fields in values property (impressions, clicks, conversions) have all non-null values. Failed - If all fields in values property have all null values. Partial - If some of the fields (impressions, clicks, or conversions) in values property have null values. | [optional] 
**values** | [**\AmazonAdvertisingApi\Model\Values**](Values.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

