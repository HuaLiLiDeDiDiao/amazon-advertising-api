# AdSuccessResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | the Ad ID. | [optional] 
**ad** | [**\AmazonAdvertisingApi\Model\MultiAdGroupAd**](MultiAdGroupAd.md) |  | [optional] 
**index** | **float** | The index in the original list from the request. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

