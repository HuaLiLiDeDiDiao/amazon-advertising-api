# SponsoredProductsCreateTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_promotion_group_id** | **string** | The id of the target promotion group. | [optional] 
**target_id** | **string** | The id of the target that got created. | [optional] 
**manual_targeting_ad_group_id** | **string** | The adGroupId of the manual-targeting campaign where the target belongs. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

