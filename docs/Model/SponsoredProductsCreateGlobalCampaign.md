# SponsoredProductsCreateGlobalCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**name** | **string** | The name of the campaign. | 
**targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingType**](SponsoredProductsTargetingType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**dynamic_bidding** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateDynamicBidding**](SponsoredProductsCreateOrUpdateDynamicBidding.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) | The format of the date is YYYY-MM-DD. | [optional] 
**budget** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBudget**](SponsoredProductsGlobalBudget.md) |  | 
**tags** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTags**](SponsoredProductsTags.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

