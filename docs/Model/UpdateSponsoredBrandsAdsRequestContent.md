# UpdateSponsoredBrandsAdsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ads** | [**\AmazonAdvertisingApi\Model\UpdateAd[]**](UpdateAd.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

