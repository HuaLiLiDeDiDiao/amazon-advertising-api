# BrandMetricsGetResponseShoppersMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**this_brand** | [**\AmazonAdvertisingApi\Model\BrandMetricsShoppersMetrics**](BrandMetricsShoppersMetrics.md) |  | [optional] 
**top_performers** | [**\AmazonAdvertisingApi\Model\BrandMetricsShoppersMetrics**](BrandMetricsShoppersMetrics.md) |  | [optional] 
**peer_median** | [**\AmazonAdvertisingApi\Model\BrandMetricsShoppersMetrics**](BrandMetricsShoppersMetrics.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

