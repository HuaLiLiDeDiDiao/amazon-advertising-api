# AccountToUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roles** | [**\AmazonAdvertisingApi\Model\AccountRelationshipRole[]**](AccountRelationshipRole.md) | The types of role that will exist with the Amazon Advertising account. Depending on account type, the default role will be ENTITY_USER or SELLER_USER. Only one role at a time is currently supported | [optional] 
**id** | **string** | Id of the Amazon Advertising account. | [optional] 
**type** | **string** | The type of the Id | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

