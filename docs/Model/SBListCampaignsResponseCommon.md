# SBListCampaignsResponseCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **int** | The campaign identifier. | [optional] 
**name** | **string** | The name of the campaign. This name must be unique to the Amazon Ads account to which the campaign is associated. Maximum length of the string is 128 characters. | [optional] 
**tags** | [**\AmazonAdvertisingApi\Model\CampaignTags**](CampaignTags.md) |  | [optional] 
**budget** | **float** | The budget amount associated with the campaign. | [optional] 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | [optional] 
**start_date** | **string** | The YYYYMMDD start date for the campaign. If this field is not set to a value, the current date is used. | [optional] 
**end_date** | **string** | The YYYYMMDD end date for the campaign. Must be greater than the value for &#x60;startDate&#x60;. If not specified, the campaign has no end date and runs continuously. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\State**](State.md) |  | [optional] 
**serving_status** | **string** | |Value|Description| |-----|-----------| |asinNotBuyable| The associated ASIN cannot be purchased due to eligibility or availability.| |billingError| The billing information associated with the account requires correction.| |ended| The value specified in the &#x60;endDate&#x60; field is in the past.| |landingPageNotAvailable| The specified landing page is not available. This may be caused by an incorrect address or a landing page with less than three ASINs.| |outOfBudget| The campaign has run out of budget.| |paused|The campaign state is set to &#x60;paused&#x60;.| |pendingReview| A newly created campaign that has not passed moderation review. Note that moderation review may take up to 72 hours. |ready| The campaign is scheduled for a future date.| |rejected| The campaign failed moderation review.| |running| The campaign is enabled and serving.| |scheduled| A transitive state between &#x60;ready&#x60; and &#x60;running&#x60;, as child entities associated with the campaign move to a running state.| |terminated| The state of the campaign is set to &#x60;archived&#x60;.| &lt;br/&gt; | [optional] 
**brand_entity_id** | **string** | The brand entity identifier. Note that this field is required for sellers. For more information, see the [Stores reference](https://advertising.amazon.com/API/docs/v2/reference/stores) or [Brands reference](https://advertising.amazon.com/API/docs/v3/reference/SponsoredBrands/Brands). | [optional] 
**portfolio_id** | **int** | The identifier of the portfolio to which the campaign is associated. | [optional] 
**landing_page** | [**OneOfSBListCampaignsResponseCommonLandingPage**](OneOfSBListCampaignsResponseCommonLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

