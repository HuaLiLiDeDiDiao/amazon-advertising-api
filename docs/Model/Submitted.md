# Submitted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **float** | The index of the image task in the array from the request body | [optional] 
**task_id** | **string** | The identifier of image generation task | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

