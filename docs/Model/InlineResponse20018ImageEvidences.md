# InlineResponse20018ImageEvidences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**violating_image_crop** | [**\AmazonAdvertisingApi\Model\InlineResponse20018ViolatingImageCrop**](InlineResponse20018ViolatingImageCrop.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

