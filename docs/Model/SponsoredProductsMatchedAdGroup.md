# SponsoredProductsMatchedAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_name** | **string** | The name of the adGroup. | [optional] 
**campaign_targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCampaignTargetingType**](SponsoredProductsCampaignTargetingType.md) |  | [optional] 
**ad_group_targeting_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsAdGroupTargetingType**](SponsoredProductsAdGroupTargetingType.md) |  | [optional] 
**campaign_id** | **string** | The unique identifier of the of the campaign that the adGroup belongs to. | [optional] 
**state** | **string** | The state of the adGroup. | [optional] 
**campaign_name** | **string** | Name of the campaign that the adGroup belongs to. | [optional] 
**ad_group_id** | **string** | The unique identifier of the adGroup that contains the same ASIN/SKUs as the AT adGroup in the request. | [optional] 
**default_bid** | **double** | The defualt bid of the adGroup. | [optional] 
**budget** | **double** | The budget of the campaign that the adGroup belongs to. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

