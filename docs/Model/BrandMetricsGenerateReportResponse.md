# BrandMetricsGenerateReportResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**report_id** | **string** | The identifier of the report. | 
**format** | **string** | Format of the report | [default to 'JSON']
**status_details** | **string** | A human-readable description of the current status. | 
**location** | **string** | The URI address of the report. | [optional] 
**expiration** | **int** | The expiration time of the URI in the location property in milliseconds. The expiration time is the interval between the time the response was generated and the time the URI expires. | 
**status** | **string** | The build status of the report. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

