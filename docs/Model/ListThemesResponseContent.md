# ListThemesResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**themes** | [**\AmazonAdvertisingApi\Model\Theme[]**](Theme.md) | List of themes | [optional] 
**next_token** | **string** | If nextToken is not null, it means there are more results. | [optional] 
**total_count** | **float** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

