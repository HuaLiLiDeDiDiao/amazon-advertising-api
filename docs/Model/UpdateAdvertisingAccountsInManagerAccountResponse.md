# UpdateAdvertisingAccountsInManagerAccountResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failed_accounts** | [**\AmazonAdvertisingApi\Model\AccountToUpdateFailure[]**](AccountToUpdateFailure.md) | List of Advertising accounts or advertisers failed to Link/Unlink with [Manager Account](https://advertising.amazon.com/help?ref_&#x3D;a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8). | [optional] 
**succeed_accounts** | [**\AmazonAdvertisingApi\Model\AccountToUpdate[]**](AccountToUpdate.md) | List of Advertising accounts or advertisers successfully Link/Unlink with [Manager Account](https://advertising.amazon.com/help?ref_&#x3D;a20m_us_blog_whtsnewfb2020_040120#GU3YDB26FR7XT3C8). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

