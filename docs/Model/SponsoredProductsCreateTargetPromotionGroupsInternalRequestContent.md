# SponsoredProductsCreateTargetPromotionGroupsInternalRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_ids** | **string[]** |  | [optional] 
**existing_campaign_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExistingCampaignDetails**](SponsoredProductsExistingCampaignDetails.md) |  | [optional] 
**api_gateway_context** | [**\AmazonAdvertisingApi\Model\SponsoredProductsApiGatewayContext**](SponsoredProductsApiGatewayContext.md) |  | 
**ad_group_id** | **string** | Entity object identifier | 
**new_campaign_details** | [**\AmazonAdvertisingApi\Model\SponsoredProductsNewCampaignDetails**](SponsoredProductsNewCampaignDetails.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

