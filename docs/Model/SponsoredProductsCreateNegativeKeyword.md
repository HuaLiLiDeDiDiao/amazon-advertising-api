# SponsoredProductsCreateNegativeKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**native_language_keyword** | **string** | The unlocalized keyword text in the preferred locale of the advertiser | [optional] 
**native_language_locale** | **string** | The locale preference of the advertiser. | [optional] 
**campaign_id** | **string** | The identifer of the campaign to which the keyword is associated. | 
**match_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateNegativeMatchType**](SponsoredProductsCreateOrUpdateNegativeMatchType.md) |  | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this keyword is associated. | 
**keyword_text** | **string** | The keyword text. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

