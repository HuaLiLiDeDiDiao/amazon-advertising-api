# SDCategoryRecommendationV33TargetableAsinCountRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**range_lower** | **int** |  | [optional] 
**range_upper** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

