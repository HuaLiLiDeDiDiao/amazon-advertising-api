# BulkGetBillingStatusSuccess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | 
**billing_status** | [**\AmazonAdvertisingApi\Model\BillingStatus**](BillingStatus.md) |  | 
**advertiser_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

