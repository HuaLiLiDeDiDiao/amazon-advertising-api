# OptimizationRulesAPIAmazonAdvertisingApiSingleOptimizationRuleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | An enumerated success or error code for machine use. | [optional] 
**optimization_rule** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule**](OptimizationRulesAPIAmazonAdvertisingApiOptimizationRule.md) |  | [optional] 
**details** | **string** | A human-readable description of the error, if unsuccessful. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

