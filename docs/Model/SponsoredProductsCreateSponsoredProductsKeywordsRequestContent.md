# SponsoredProductsCreateSponsoredProductsKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keywords** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateKeyword[]**](SponsoredProductsCreateKeyword.md) | An array of keywords. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

