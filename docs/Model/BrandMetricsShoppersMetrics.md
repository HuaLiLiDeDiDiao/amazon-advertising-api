# BrandMetricsShoppersMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**viewed_detail_page_only** | **int** | Detail page views only | [optional] 
**branded_searches_only** | **int** | Searches which include the brand name or close variant | [optional] 
**high_value_customers** | **int** | High value purchasers who are subscribe and saved or in the top 10% of sales drivers | [optional] 
**brand_customers** | **int** | 0%-90% of sales contributing shoppers | [optional] 
**branded_searches_and_detail_page_views** | **int** | Users who searched for the brand, then visted at least one detail page in the selected category | [optional] 
**total_brand_purchasers** | **int** | total number of customers that purchased from the brand | [optional] 
**add_to_carts** | **int** | Number of shoppers who have added an ASIN in the selected category to their cart but not purchased | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

