# SponsoredProductsCreateTargetPromotionGroupsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_promotion_group** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetPromotionGroup**](SponsoredProductsTargetPromotionGroup.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

