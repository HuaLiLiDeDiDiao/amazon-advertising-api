# RuleNotificationError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | [**\AmazonAdvertisingApi\Model\RuleCampaignId**](RuleCampaignId.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationRuleError**](CampaignOptimizationRuleError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

