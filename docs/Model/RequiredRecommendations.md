# RequiredRecommendations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_recommendation_groups** | **int** | Maximum number of recommendations groups that API should return for given type. (recommendations are not guaranteed). | [optional] 
**type** | **string** | Type of recommendations. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

