# CreativeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | The HTTP status code of the response. | [optional] 
**description** | **string** | A human-readable description of the response. | [optional] 
**creative_id** | **float** | The identifier of the creative. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

