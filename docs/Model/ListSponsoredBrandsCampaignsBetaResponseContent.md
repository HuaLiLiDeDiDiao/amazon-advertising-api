# ListSponsoredBrandsCampaignsBetaResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaigns** | [**\AmazonAdvertisingApi\Model\Campaign[]**](Campaign.md) |  | [optional] 
**next_token** | **string** | Token value allowing to navigate to the next response page. | [optional] 
**total_count** | **float** | The total number of entities. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

