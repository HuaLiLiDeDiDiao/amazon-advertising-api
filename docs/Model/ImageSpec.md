# ImageSpec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resolution** | **string** | Image resolution, default is 1200x628. New values will be added later. |   Resolution  |   Value       | |---------------|---------------| |   1200x628  |   1200x628  | | [optional] 
**file_format** | **string** | Valid values are PNG and JPEG, default is PNG. New values will be added later. |   File Format  |   Value       | |---------------|---------------| |   PNG          |   PNG         | |   JPEG         |   JPEG        | | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

