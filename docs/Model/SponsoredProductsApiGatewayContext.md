# SponsoredProductsApiGatewayContext

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_id** | **string** |  | [optional] 
**client_id** | **string** |  | [optional] 
**request_id** | **string** |  | [optional] 
**entity_type** | **string** |  | [optional] 
**entity_id** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 
**advertiser_id** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

