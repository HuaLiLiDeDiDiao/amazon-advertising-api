# CampaignRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seven_days_estimated_opportunities** | [**\AmazonAdvertisingApi\Model\SevenDaysEstimatedOpportunities**](SevenDaysEstimatedOpportunities.md) |  | [optional] 
**bidding_strategy_recommendation** | [**\AmazonAdvertisingApi\Model\BiddingStrategyRecommendation**](BiddingStrategyRecommendation.md) |  | [optional] 
**campaign_id** | **string** | The identifier of the campaign. | [optional] 
**keyword_targeting_recommendations** | [**\AmazonAdvertisingApi\Model\KeywordTargetingRecommendation[]**](KeywordTargetingRecommendation.md) |  | [optional] 
**budget_recommendation** | [**\AmazonAdvertisingApi\Model\BudgetRecommendation**](BudgetRecommendation.md) |  | [optional] 
**targeting_group_bid_recommendations** | [**\AmazonAdvertisingApi\Model\TargetingGroupBidRecommendation[]**](TargetingGroupBidRecommendation.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

