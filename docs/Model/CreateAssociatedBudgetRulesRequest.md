# CreateAssociatedBudgetRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**budget_rule_ids** | **string[]** | A list of budget rule identifiers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

