# BrandMetricsBrandsResponseBrands

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Brand Name | [optional] 
**id** | **string** | Brand Id from Brand Aid | [optional] 
**brand_metrics_unavailable_reason** | [**\AmazonAdvertisingApi\Model\BrandMetricsBrandsResponseBrandMetricsUnavailableReason**](BrandMetricsBrandsResponseBrandMetricsUnavailableReason.md) |  | [optional] 
**brand_metrics_available** | **bool** | Signifies whether the data for the brand is available or not | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

