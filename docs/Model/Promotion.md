# Promotion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_consumed_date** | [**\AmazonAdvertisingApi\Model\\DateTime**](\DateTime.md) |  | 
**amount** | [**\AmazonAdvertisingApi\Model\CurrencyAmount**](CurrencyAmount.md) |  | 
**description** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

