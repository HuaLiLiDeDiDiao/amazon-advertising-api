# SDTargetingBidRecommendationsResponseV31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost_type** | [**\AmazonAdvertisingApi\Model\SDCostTypeV31**](SDCostTypeV31.md) |  | 
**bid_recommendations** | [**\AmazonAdvertisingApi\Model\OneOfSDTargetingBidRecommendationsResponseV31BidRecommendationsItems[]**](.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

