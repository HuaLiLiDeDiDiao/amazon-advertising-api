# SBListCampaignItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**landing_page** | [****](.md) |  | [optional] 
**bid_optimization** | **bool** | Set to true to allow Amazon to automatically optimize bids for placements below top of search. | [optional] 
**bid_multiplier** | **double** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBCreative**](SBCreative.md) |  | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

