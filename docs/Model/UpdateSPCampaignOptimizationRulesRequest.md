# UpdateSPCampaignOptimizationRulesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | [**\AmazonAdvertisingApi\Model\RecurrenceType**](RecurrenceType.md) |  | 
**rule_action** | [**\AmazonAdvertisingApi\Model\RuleAction**](RuleAction.md) |  | 
**campaign_optimization_id** | [**\AmazonAdvertisingApi\Model\CampaignOptimizationId**](CampaignOptimizationId.md) |  | 
**rule_condition** | [**\AmazonAdvertisingApi\Model\RuleConditionList**](RuleConditionList.md) |  | 
**rule_type** | [**\AmazonAdvertisingApi\Model\RuleType**](RuleType.md) |  | 
**rule_name** | [**\AmazonAdvertisingApi\Model\RuleName**](RuleName.md) |  | [optional] 
**campaign_ids** | [**\AmazonAdvertisingApi\Model\RuleCampaignId[]**](RuleCampaignId.md) | A list of campaign ids | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

