# AllOfBidErrorBidError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lower_limit** | **string** |  | [optional] 
**upper_limit** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

