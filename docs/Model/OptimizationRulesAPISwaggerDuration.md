# OptimizationRulesAPIAmazonAdvertisingApiDuration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_time** | **string** | Time of optimization rule creation in ISO 8061. | 
**end_time** | **string** | Time of optimization rule creation in ISO 8061. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

