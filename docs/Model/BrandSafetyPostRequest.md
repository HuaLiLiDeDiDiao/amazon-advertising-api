# BrandSafetyPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domains** | [**\AmazonAdvertisingApi\Model\BrandSafetyDenyListDomain[]**](BrandSafetyDenyListDomain.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

