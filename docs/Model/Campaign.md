# Campaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | [**\AmazonAdvertisingApi\Model\CampaignId**](CampaignId.md) |  | [optional] 
**tactic** | [**\AmazonAdvertisingApi\Model\Tactic**](Tactic.md) |  | [optional] 
**delivery_profile** | **string** |  | [optional] 
**rule_based_budget** | [**\AmazonAdvertisingApi\Model\RuleBasedBudget**](RuleBasedBudget.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

