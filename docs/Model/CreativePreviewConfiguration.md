# CreativePreviewConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | [**\AmazonAdvertisingApi\Model\CreativePreviewConfigurationSize**](CreativePreviewConfigurationSize.md) |  | [optional] 
**products** | [**\AmazonAdvertisingApi\Model\CreativePreviewConfigurationProducts[]**](CreativePreviewConfigurationProducts.md) | The products to preview. Currently only the first product is previewable. | [optional] 
**landing_page_url** | [**\AmazonAdvertisingApi\Model\LandingPageURL**](LandingPageURL.md) |  | [optional] 
**landing_page_type** | [**\AmazonAdvertisingApi\Model\LandingPageType**](LandingPageType.md) |  | [optional] 
**ad_name** | [**\AmazonAdvertisingApi\Model\AdName**](AdName.md) |  | [optional] 
**is_mobile** | **bool** | Preview the creative as if it is on a mobile environment. | [optional] 
**is_on_amazon** | **bool** | Preview the creative as if it is on an amazon site or third party site. The main difference is whether the preview will contain an AdChoices icon. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

