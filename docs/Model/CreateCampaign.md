# CreateCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tactic** | [**\AmazonAdvertisingApi\Model\Tactic**](Tactic.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

