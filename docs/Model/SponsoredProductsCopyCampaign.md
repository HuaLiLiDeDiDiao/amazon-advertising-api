# SponsoredProductsCopyCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_campaign_attributes** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetCampaignAttributes**](SponsoredProductsTargetCampaignAttributes.md) |  | 
**source_campaign_id** | **string** | entity object identifier | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

