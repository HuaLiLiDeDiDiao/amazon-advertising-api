# AdvertiserMarketplace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_id** | **string** |  | 
**advertiser_type** | [**\AmazonAdvertisingApi\Model\AdvertiserTypes**](AdvertiserTypes.md) |  | [optional] 
**advertiser_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

