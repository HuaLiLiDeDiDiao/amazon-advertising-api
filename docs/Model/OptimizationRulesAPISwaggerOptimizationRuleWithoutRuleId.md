# OptimizationRulesAPIAmazonAdvertisingApiOptimizationRuleWithoutRuleId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurrence** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrence**](OptimizationRulesAPIAmazonAdvertisingApiRuleRecurrence.md) |  | 
**rule_category** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleCategory**](OptimizationRulesAPIAmazonAdvertisingApiRuleCategory.md) |  | 
**rule_sub_category** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleSubCategory**](OptimizationRulesAPIAmazonAdvertisingApiRuleSubCategory.md) |  | 
**rule_name** | **string** | The rule name. | [optional] 
**action** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleAction**](OptimizationRulesAPIAmazonAdvertisingApiRuleAction.md) |  | 
**conditions** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleCondition[]**](OptimizationRulesAPIAmazonAdvertisingApiRuleCondition.md) |  | [optional] 
**status** | [**\AmazonAdvertisingApi\Model\OptimizationRulesAPIAmazonAdvertisingApiRuleStatus**](OptimizationRulesAPIAmazonAdvertisingApiRuleStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

