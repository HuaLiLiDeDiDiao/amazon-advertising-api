# BulkAdOperationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | [**\AmazonAdvertisingApi\Model\AdSuccessResponseItem[]**](AdSuccessResponseItem.md) |  | [optional] 
**error** | [**\AmazonAdvertisingApi\Model\AdFailureResponseItem[]**](AdFailureResponseItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

