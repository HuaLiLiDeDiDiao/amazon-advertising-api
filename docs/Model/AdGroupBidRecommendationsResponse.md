# AdGroupBidRecommendationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **float** | The identifier of the ad group. | [optional] 
**suggested_bid** | [**\AmazonAdvertisingApi\Model\SuggestedBid**](SuggestedBid.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

