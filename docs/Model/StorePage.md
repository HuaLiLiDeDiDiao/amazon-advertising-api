# StorePage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **string** | Display Name of the store page shown on a store spotlight campaign. | [optional] 
**primary_asin** | **string** | Selected asin from the store page which is displayed on the store spotlight campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

