# SponsoredProductsCopySponsoredProductsCampaignsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**copy_campaign_responses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCopyCampaignResponse[]**](SponsoredProductsCopyCampaignResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

