# AllOfinlineResponse2008TargetsBidsRecommendationErrorResultsItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBTargetingExpressions**](SBTargetingExpressions.md) |  | [optional] 
**targets_index** | [**\AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex**](SBBidRecommendationKeywordIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

