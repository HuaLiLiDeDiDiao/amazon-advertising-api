# AllOfinlineResponse2008KeywordsBidsRecommendationErrorResultsItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword** | [**\AmazonAdvertisingApi\Model\SBKeywordExpression**](SBKeywordExpression.md) |  | [optional] 
**keyword_index** | [**\AmazonAdvertisingApi\Model\SBBidRecommendationKeywordIndex**](SBBidRecommendationKeywordIndex.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

