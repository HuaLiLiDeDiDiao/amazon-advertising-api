# SPListAssociatedBudgetRulesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**associated_rules** | [**\AmazonAdvertisingApi\Model\SPCampaignBudgetRule[]**](SPCampaignBudgetRule.md) | A list of associated budget rules. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

