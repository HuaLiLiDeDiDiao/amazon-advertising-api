# BidValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggested_bid** | **double** | The suggested bid. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

