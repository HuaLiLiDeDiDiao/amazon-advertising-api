# DspAudienceEditRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dsp_audience_edit_request_items** | [**\AmazonAdvertisingApi\Model\DspAudienceEditRequestItem[]**](DspAudienceEditRequestItem.md) | A list of audience edit objects containing fields to be overwritten. For each object, specify fields and their values to be modified. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

