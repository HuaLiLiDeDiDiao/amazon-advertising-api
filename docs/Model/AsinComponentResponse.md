# AsinComponentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pre_moderation_status** | **string** | The pre-moderation status of the component. | [optional] 
**component_type** | **string** | Type of Asin component. | [optional] 
**policy_violations** | [**\AmazonAdvertisingApi\Model\AsinPolicyViolation[]**](AsinPolicyViolation.md) | A list of policy violations for the component that were detected during pre moderation. Note that this field is present in the response only when preModerationStatus is set to REJECTED. | [optional] 
**asin** | **string** | Pre-moderated Asin Id. | [optional] 
**id** | **string** | Id of the component. This is the same id sent as part of the request. This can be used to uniquely identify the component. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

