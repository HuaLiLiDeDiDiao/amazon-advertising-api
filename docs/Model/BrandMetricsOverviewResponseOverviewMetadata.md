# BrandMetricsOverviewResponseOverviewMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_tree_name** | **string** | The node at the top of a browse tree. It is the start node of a tree | [optional] 
**category_path** | **string[]** |  | [optional] 
**category_id** | **string** | Category Node ID represents the catalog node that can be used in product targeting with categories | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

