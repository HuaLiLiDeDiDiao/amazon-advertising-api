# SponsoredProductsTargetingExpressionPredicateMarketValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace** | [**\AmazonAdvertisingApi\Model\SponsoredProductsMarketplace**](SponsoredProductsMarketplace.md) |  | [optional] 
**value** | **string** | The expression value | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

