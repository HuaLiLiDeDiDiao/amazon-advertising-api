# SponsoredProductsDeleteSponsoredProductsDraftKeywordsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id_filter** | [**\AmazonAdvertisingApi\Model\SponsoredProductsObjectIdFilter**](SponsoredProductsObjectIdFilter.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

