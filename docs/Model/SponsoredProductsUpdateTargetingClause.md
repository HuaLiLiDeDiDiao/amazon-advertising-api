# SponsoredProductsUpdateTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetingExpressionPredicateWithoutOther[]**](SponsoredProductsTargetingExpressionPredicateWithoutOther.md) | The targeting expression. | [optional] 
**target_id** | **string** | The target identifier | 
**expression_type** | [**\AmazonAdvertisingApi\Model\SponsoredProductsExpressionTypeWithoutOther**](SponsoredProductsExpressionTypeWithoutOther.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateEntityState**](SponsoredProductsCreateOrUpdateEntityState.md) |  | [optional] 
**bid** | **double** | The bid for ads sourced using the target. Targets that do not have bid values in listTargetingClauses will inherit the defaultBid from the adGroup level. For more information about bid constraints by marketplace, see [bid limits](https://advertising.amazon.com/API/docs/en-us/concepts/limits#bid-constraints-by-marketplace). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

