# KeywordTargetingRecommendation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **string** | The identifier of the keyword targeting. | [optional] 
**suggested_bid** | **double** | The suggested bid value associated with this keyword targeting clause. | [optional] 
**match_type** | **string** | Keyword match type. | Value | Description | | --- | --- | | &#x60;BROAD&#x60; | Use BROAD to broadly match your keyword targeting ads with search queries.| | &#x60;EXACT&#x60; | Use EXACT to exactly match your keyword targeting ads with search queries.| | &#x60;PHRASE&#x60; | Use PHRASE to match your keyword targeting ads with search phrases.| | [optional] 
**action** | **string** | Type of action for the keyword targeting. | [optional] 
**ad_group_id** | **string** | The ad group identifier. | [optional] 
**keyword_text** | **string** | The keyword text. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

