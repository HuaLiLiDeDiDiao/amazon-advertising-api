# AdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | [**\AmazonAdvertisingApi\Model\AdGroupId**](AdGroupId.md) |  | [optional] 
**tactic** | [**\AmazonAdvertisingApi\Model\Tactic**](Tactic.md) |  | [optional] 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeType**](CreativeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

