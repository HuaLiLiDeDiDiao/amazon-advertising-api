# TimeOfDay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_time** | **string** | The start time of intra-day budget rule window in the format &#x27;hh:mm:ss&#x27; | [optional] 
**end_time** | **string** | The end time of intra-day budget rule window in the format &#x27;hh:mm:ss&#x27;. Required to be greater than start-time. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

