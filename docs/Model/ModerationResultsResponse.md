# ModerationResultsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**moderation_results** | [**\AmazonAdvertisingApi\Model\ModerationResult[]**](ModerationResult.md) |  | [optional] 
**next_token** | [**\AmazonAdvertisingApi\Model\NextToken**](NextToken.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

