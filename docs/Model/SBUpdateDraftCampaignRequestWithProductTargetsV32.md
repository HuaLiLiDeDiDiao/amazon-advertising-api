# SBUpdateDraftCampaignRequestWithProductTargetsV32

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | [**\AmazonAdvertisingApi\Model\SBUpdateDraftCampaignRequestWithProductTargetsTargets[]**](SBUpdateDraftCampaignRequestWithProductTargetsTargets.md) |  | [optional] 
**negative_targets** | [**\AmazonAdvertisingApi\Model\SBUpdateDraftCampaignRequestWithProductTargetsNegativeTargets[]**](SBUpdateDraftCampaignRequestWithProductTargetsNegativeTargets.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

