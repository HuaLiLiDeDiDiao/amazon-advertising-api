# MultiAdGroupAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The ad identifier. | 
**campaign_id** | **string** | The campaign identifier. | 
**landing_page** | [**\AmazonAdvertisingApi\Model\LandingPage**](LandingPage.md) |  | [optional] 
**name** | **string** | The name of the ad. | 
**state** | [**\AmazonAdvertisingApi\Model\EntityState**](EntityState.md) |  | 
**ad_group_id** | **string** | The adGroup identifier. | 
**creative** | [**\AmazonAdvertisingApi\Model\Creative**](Creative.md) |  | [optional] 
**extended_data** | [**\AmazonAdvertisingApi\Model\AdExtendedData**](AdExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

