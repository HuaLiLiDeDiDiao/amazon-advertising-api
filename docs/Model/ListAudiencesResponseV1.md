# ListAudiencesResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_token** | **string** |  | [optional] 
**audiences** | [**\AmazonAdvertisingApi\Model\AudienceV1[]**](AudienceV1.md) | Array of segments matching given filters sorted by create time, earliest first. | [optional] 
**match_count** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

