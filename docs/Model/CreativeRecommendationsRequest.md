# CreativeRecommendationsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asins** | **string[]** | An array of ASINs associated with the creative. Note, do not pass an empty array, this results in an error. | 
**ad_format** | **string** | Ad format of the creative. | 
**required_recommendations** | [**\AmazonAdvertisingApi\Model\RequiredRecommendations[]**](RequiredRecommendations.md) | Required recommendations details. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

