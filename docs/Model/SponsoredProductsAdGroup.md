# SponsoredProductsAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of the campaign to which the keyword is associated. | 
**name** | **string** | The name of the ad group. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsEntityState**](SponsoredProductsEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the keyword. | 
**default_bid** | **double** | A bid value for use when no bid is specified for keywords in the ad group. For more information about bid constraints by marketplace, see [bid limits](https://advertising.amazon.com/API/docs/en-us/concepts/limits#bid-constraints-by-marketplace). | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsAdGroupExtendedData**](SponsoredProductsAdGroupExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

