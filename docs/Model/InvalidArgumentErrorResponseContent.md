# InvalidArgumentErrorResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | [**\AmazonAdvertisingApi\Model\InvalidArgumentErrorCode**](InvalidArgumentErrorCode.md) |  | 
**request_id** | **string** |  | 
**message** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

