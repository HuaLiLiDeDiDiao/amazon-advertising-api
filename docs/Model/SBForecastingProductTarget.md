# SBForecastingProductTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expressions** | [**\AmazonAdvertisingApi\Model\SBForecastingProductExpression[]**](SBForecastingProductExpression.md) |  | [optional] 
**bid** | **float** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

