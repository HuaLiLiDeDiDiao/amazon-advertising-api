# SponsoredProductsCreateSponsoredProductsDraftAdGroupsRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftAdGroup[]**](SponsoredProductsCreateDraftAdGroup.md) | An array of draftAdGroups. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

