# SponsoredProductsGlobalProductAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **string** | The product ad identifier. | 
**campaign_id** | **string** | The campaign identifier. | 
**custom_text** | **string** | The custom text that is associated with this ad. Defined for custom text ads only. | [optional] 
**name** | **string** | Name for the product Ad | [optional] 
**asin** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityState**](SponsoredProductsGlobalEntityState.md) |  | 
**sku** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductIdentifiers**](SponsoredProductsGlobalProductIdentifiers.md) |  | [optional] 
**ad_group_id** | **string** | The ad group identifier. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalProductAdExtendedData**](SponsoredProductsGlobalProductAdExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

