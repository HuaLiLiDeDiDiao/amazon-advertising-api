# SBForecastingKeyword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_text** | **string** | The keyword text. Maximum of 10 words. | [optional] 
**match_type** | **string** | The match type. Valid value: EXACT, PHRASE, BROAD. For more information, see [match types](https://advertising.amazon.com/help#GHTRFDZRJPW6764R) in the Amazon Advertising support center. | [optional] 
**bid** | **float** | The associated bid. Note that this value must be less than the budget associated with the Advertiser account. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

