# BrandMetricsGetResponseCustomerConversionRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**this_brand** | **double** | Percentage of shoppers moving from \&quot;considering\&quot; [Brand Name] in the [categoryPath] to \&quot;purchased\&quot; in the [lookBackPeriod] | [optional] 
**top_performers** | **double** | Percentage of shoppers moving from \&quot;considering\&quot; the average of the top 95th-99th percent of peers in the [categoryPath] to \&quot;purchased\&quot; in the l[lookBackPeriod] | [optional] 
**peer_median** | **double** | Percentage of shoppers moving from \&quot;considering\&quot; the peer median in the [categoryPath] to \&quot;purchased\&quot; in the [lookBackPeriod] | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

