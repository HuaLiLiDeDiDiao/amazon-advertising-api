# BrandSafetyDenyListProcessedDomain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_id** | **int** | The identifier of the Brand Safety List domain. | [optional] 
**name** | **string** | The website or app identifier. This can be in the form of full domain (eg. &#x27;example.com&#x27; or &#x27;example.net&#x27;), or mobile app identifier (eg. &#x27;com.example.app&#x27; for Android apps or &#x27;1234567890&#x27; for iOS apps) | [optional] 
**type** | [**\AmazonAdvertisingApi\Model\BrandSafetyDenyListDomainType**](BrandSafetyDenyListDomainType.md) |  | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\BrandSafetyDenyListDomainState**](BrandSafetyDenyListDomainState.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | The date time the domain was created at. Format YYYY-MM-ddT:HH:mm:ssZ | [optional] 
**last_modified** | [**\DateTime**](\DateTime.md) | The date time the domain was last modified. Format YYYY-MM-ddT:HH:mm:ssZ | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

