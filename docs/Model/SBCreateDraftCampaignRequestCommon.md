# SBCreateDraftCampaignRequestCommon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The name of the draft campaign. Maximum 128 characters. Names must be unique to the Amazon Ads account to which they are associated. | 
**budget** | **float** | The budget associated with the draft campaign. | 
**budget_type** | [**\AmazonAdvertisingApi\Model\BudgetType**](BudgetType.md) |  | 
**start_date** | **string** | The YYYYMMDD start date of the campaign. Must be equal to or greater than the current date. If not specified, is set to current date by default. | [optional] 
**end_date** | **string** | The YYYYMMDD end date of the campaign. Must be greater than the value specified in the &#x60;startDate&#x60; field. If not specified, the campaign has no end date and runs continuously. | [optional] 
**brand_entity_id** | **string** | The brand entity identifier to which the draft campaign is associated. Note that this field is required for sellers. Retrieve using the getBrands or getStores operations in the /v2/stores resource. | [optional] 
**bid_optimization** | **bool** | Set to &#x60;true&#x60; to have Amazon automatically optimize bids for placements below top of search. | [optional] [default to true]
**bid_multiplier** | **float** | A bid multiplier. Note that this field can only be set when &#x27;bidOptimization&#x27; is set to false. Value is a percentage to two decimal places. Example: If set to -40.00 for a $5.00 bid, the resulting bid is $3.00. | [optional] 
**bid_adjustments** | [**\AmazonAdvertisingApi\Model\BidAdjustment[]**](BidAdjustment.md) | List of bid adjustment for each placement group. BidMultiplier cannot be specified when bidAdjustments presents. &#x60;Not supported for video campaigns&#x60; | [optional] 
**portfolio_id** | **int** | The identifier of the Portfolio to which the draft campaign is associated. | [optional] 
**creative** | [**\AmazonAdvertisingApi\Model\SBCreative**](SBCreative.md) |  | [optional] 
**landing_page** | [**\AmazonAdvertisingApi\Model\SBLandingPage**](SBLandingPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

