# SbthemeslistThemeTypeFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include** | [**\AmazonAdvertisingApi\Model\ThemeType[]**](ThemeType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

