# AsinsThemeBasedBidRecommendationRequestBidding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adjustments** | [**\AmazonAdvertisingApi\Model\PlacementAdjustment[]**](PlacementAdjustment.md) | Placement adjustment configuration for the campaign. | [optional] 
**strategy** | [**\AmazonAdvertisingApi\Model\BiddingStrategy**](BiddingStrategy.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

