# SBKeywordResponseErrors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_error** | [**\AmazonAdvertisingApi\Model\SBKeywordResponseKeywordError**](SBKeywordResponseKeywordError.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

