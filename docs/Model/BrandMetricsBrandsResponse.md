# BrandMetricsBrandsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brands** | [**\AmazonAdvertisingApi\Model\BrandMetricsBrandsResponseBrands[]**](BrandMetricsBrandsResponseBrands.md) | List of brands IDs owned by the calling advertiser. | 
**next_token** | **string** | Token  to fetch additional results (if any). Subsequent calls must be made with same  parameters as in the previous requests. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

