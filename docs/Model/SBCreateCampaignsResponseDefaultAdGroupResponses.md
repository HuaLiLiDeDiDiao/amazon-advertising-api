# SBCreateCampaignsResponseDefaultAdGroupResponses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_group_id** | **int** | The ad group identifier. | [optional] 
**code** | **string** | An enumerated response code. | [optional] 
**description** | **string** | A human-readable description of the enumerated response code in the &#x60;code&#x60; field. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

