# SBStoreSpotlightCreativeSubpages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_title** | **string** | The title of the subpage. Maximum length is 50 characters. | [optional] 
**asin** | **string** | An ASIN on the Store subpage. The ASIN image will be used to represent the subpage. | [optional] 
**url** | **string** | URL of an existing Store page. Must be a subpage of the Store landing page associated with the campaign. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

