# Expression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\SBAPIProductPredicateType**](SBAPIProductPredicateType.md) |  | [optional] 
**value** | **string** | The text of the targeting expression. The - token defines a range. For example, 2-4 defines a range of 2, 3, and 4. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

