# ModerationResultsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version_id_filter** | [**\AmazonAdvertisingApi\Model\VersionId[]**](VersionId.md) | Filter by specific version id of the ad. The API will return the ad&#x27;s all versions moderation status if this field is empty. | [optional] 
**id_type** | [**\AmazonAdvertisingApi\Model\IdType**](IdType.md) |  | 
**ad_program_type** | [**\AmazonAdvertisingApi\Model\ModerationResultsAdProgramType**](ModerationResultsAdProgramType.md) |  | 
**next_token** | [**\AmazonAdvertisingApi\Model\NextToken**](NextToken.md) |  | [optional] 
**max_results** | **int** | Sets a limit on the number of results returned by an operation. | 
**moderation_status_filter** | [**\AmazonAdvertisingApi\Model\ModerationStatus[]**](ModerationStatus.md) | Filter by specific moderation status. | [optional] 
**id** | [**\AmazonAdvertisingApi\Model\Id**](Id.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

