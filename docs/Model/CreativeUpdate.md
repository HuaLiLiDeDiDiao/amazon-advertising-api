# CreativeUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creative_id** | **float** | Unique identifier of the creative. | 
**creative_type** | [**\AmazonAdvertisingApi\Model\CreativeTypeInCreativeRequest**](CreativeTypeInCreativeRequest.md) |  | [optional] 
**properties** | [**\AmazonAdvertisingApi\Model\CreativeProperties**](CreativeProperties.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

