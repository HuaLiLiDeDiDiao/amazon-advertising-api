# SponsoredProductsCreateSponsoredProductsDraftAdGroupsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkDraftAdGroupOperationResponse**](SponsoredProductsBulkDraftAdGroupOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

