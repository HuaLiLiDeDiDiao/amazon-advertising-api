# GetLandingPageMetadataRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_product** | **string** | An ad product is a top level offering from amazon ads as defined in our marketing, with a given feature set, and business rules and logic applied consistently across the product. Currently the only supported ad product is SPONSORED_BRANDS. | Program Type       | |--------------------| | SPONSORED_BRANDS   | | 
**landing_page_url** | **string** | The URL of the landing page. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

