# SponsoredProductsCreateGlobalAdGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign_id** | **string** | The identifier of the campaign to which the AdGroup is associated. | 
**applicable_marketplaces** | **string[]** |  | [optional] 
**name** | **string** | The name of the ad group. | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateOrUpdateGlobalEntityState**](SponsoredProductsCreateOrUpdateGlobalEntityState.md) |  | 
**default_bid** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalBid**](SponsoredProductsGlobalBid.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

