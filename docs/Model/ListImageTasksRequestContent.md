# ListImageTasksRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_filter** | [**\AmazonAdvertisingApi\Model\StatusFilter**](StatusFilter.md) |  | [optional] 
**max_results** | **float** |  | [optional] 
**next_token** | **string** | Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the &#x60;NextToken&#x60; field is empty, there are no further results. | [optional] 
**task_id_filter** | [**\AmazonAdvertisingApi\Model\TaskIdFilter**](TaskIdFilter.md) |  | [optional] 
**batch_id** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

