# DateComponent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**component_type** | **string** | Type of the date component. | 
**end_date** | **string** | End date of the component in yyyy-MM-dd HH:mm:ss format | [optional] 
**id** | **string** | Id of the component. The same will be returned as part of the response as well. This can be used to uniquely identify the component from the pre moderation response. | 
**start_date** | **string** | Start date of the component in yyyy-MM-dd HH:mm:ss format | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

