# SponsoredProductsDeleteSponsoredProductsGlobalProductAdsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_ads** | [**\AmazonAdvertisingApi\Model\SponsoredProductsBulkGlobalProductAdOperationResponse**](SponsoredProductsBulkGlobalProductAdOperationResponse.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

