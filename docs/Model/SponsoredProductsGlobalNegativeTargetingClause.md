# SponsoredProductsGlobalNegativeTargetingClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The NegativeTargeting expression. | 
**target_id** | **string** | The target identifier | 
**resolved_expression** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalTargetingExpressionPredicate[]**](SponsoredProductsGlobalTargetingExpressionPredicate.md) | The resolved NegativeTargeting expression. | 
**campaign_id** | **string** | The identifier of the campaign to which this target is associated. | 
**name** | **string** | Name for the negative targeting clause | 
**state** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalEntityState**](SponsoredProductsGlobalEntityState.md) |  | 
**ad_group_id** | **string** | The identifier of the ad group to which this target is associated. | 
**extended_data** | [**\AmazonAdvertisingApi\Model\SponsoredProductsGlobalNegativeTargetingClauseExtendedData**](SponsoredProductsGlobalNegativeTargetingClauseExtendedData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

