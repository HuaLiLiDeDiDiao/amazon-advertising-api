# DspAudienceEditRequestItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idempotency_key** | **string** | unique request token for this Audience Edit request. | 
**name** | **string** | The audience name. | [optional] 
**lookback** | **int** | The specified time period (in days) to include those who performed the action in the audience. Lookback Constraints Table: Provides available valid values of lookback allowed for given audienceType | audienceType | lookback range | |------------------------------|-------| | PRODUCT_PURCHASES            | 1-365 | | PRODUCT_VIEWS                |  1-90 | | PRODUCT_SEARCH               |  1-90 | | PRODUCT_SIMS                 |  1-90 | | [optional] 
**description** | **string** | The audience description. | [optional] 
**rules** | [**\AmazonAdvertisingApi\Model\DSPAudienceRule[]**](DSPAudienceRule.md) | Set of rules to define an audience, these rules will be ORed. | [optional] 
**audience_id** | **string** | The audience identifier of the audience to be edited. | 
**audience_type** | [**\AmazonAdvertisingApi\Model\AudienceType**](AudienceType.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

