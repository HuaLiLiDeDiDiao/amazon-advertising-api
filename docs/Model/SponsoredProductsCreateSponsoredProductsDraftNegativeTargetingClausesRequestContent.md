# SponsoredProductsCreateSponsoredProductsDraftNegativeTargetingClausesRequestContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**negative_targeting_clauses** | [**\AmazonAdvertisingApi\Model\SponsoredProductsCreateDraftNegativeTargetingClause[]**](SponsoredProductsCreateDraftNegativeTargetingClause.md) | An array of negativeTargetingClauses. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

