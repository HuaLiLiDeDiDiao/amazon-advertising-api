# SbNegativeKeywordsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyword_id** | **int** | The identifier of the negative keyword. | [optional] 
**ad_group_id** | **int** | The identifier of the ad group to which the negative keyword is associated. | [optional] 
**campaign_id** | **int** | The identifier of the campaign to which the negative keyword is associated. | [optional] 
**state** | [**\AmazonAdvertisingApi\Model\SBNegativeKeywordState**](SBNegativeKeywordState.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

