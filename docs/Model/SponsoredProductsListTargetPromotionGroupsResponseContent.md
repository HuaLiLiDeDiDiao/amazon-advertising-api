# SponsoredProductsListTargetPromotionGroupsResponseContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_results** | **int** | The total number of results available. | [optional] 
**next_token** | **string** | To retrieve the next page of results, call the same operation and specify this token in the     request. If the nextToken field is empty, there are no further results. | [optional] 
**target_promotion_groups** | [**\AmazonAdvertisingApi\Model\SponsoredProductsTargetPromotionGroup[]**](SponsoredProductsTargetPromotionGroup.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

