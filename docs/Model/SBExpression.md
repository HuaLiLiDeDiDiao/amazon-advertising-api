# SBExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AmazonAdvertisingApi\Model\ProductPredicateType**](ProductPredicateType.md) |  | [optional] 
**value** | **string** | The text of the targeting expression. The &#x60;-&#x60; token defines a range. For example, &#x60;2-4&#x60; defines a range of 2, 3, and 4. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

